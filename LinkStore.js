/* LinkStore.js 

   The LinkStore object manages link graphs in the Docuplextron. 

   Each graph is parsed into a dictionary of node objects and a
   collection of triples. The triples are stored as DOM elements so
   that we can query them quickly with CSS selectors.

   So, given the following graph document:

   { "@id" : "http://example.site/graph1",
     "@context" : "http://alph.io/terms",
     "@type" : "AlphLinkDocument",
     "name" : "A Sample Graph Document",
     "@graph" : [
       { "@id" : "http://foo.site/bar#123-456",
         "@type" : "AlphSpan",
	 "origin" : 123,
	 "offset" : 456,
	 "src" : "http://foo.site/bar",
	 // --- Link relations for this node begin here --
	 "http://bar.site/relation" : {
	   "@type" : "EDL",
	   "@list" : [
	     "http://example.site/path/resource#111-222",
	     "http://example.site/path2/resource2#345-678"
	     ]
	   }
	 } 
       ]
     }

   Which is a link between a media fragment, and a two-fragment EDL,
   the LinkStore would replicate this information in this way:

   Let's assume we have a LinkStore named linkStore. An object is
   added to its graph property for this document, keyed with its
   id.

   linkStore.graphs["http://example.site/graph1"] = { 

   // Copy the root object of the graph document into here so that we
   // keep any miscellaneous attributes that the graph's author may
   // have set.

     "@context" : "http://alph.io/terms",
     "name" : "A Sample Graph Document",

   // @type, name, ... whatever else...
   // Then we add these:

     "nodes" : {}, 
     "graph" : document.createElement("Graph")
   }
   
   Two node objects would be created for the A and B nodes; these are
   copies of the nodes in the graph that include all of their
   attribute (@-) properties, but with the remaining properties left
   out (they get converted into triples). They are added to the
   LinkStore's "nodes" property, keyed with their id.
   
   linkStore.nodes["http://foo.site/bar#123-456"] = {
         "@type" : "AlphSpan",
	 "origin" : 123,
	 "offset" : 456,
	 "src" : "http://foo.site/bar"
	 }

   linkStore.nodes["_:XYZ"] = {
         "@type" : "EDL",
	 "@list" : [
	   { "@id" : "http://example.site/path/resource#111-222",
	     "@type" : "AlphSpan",
	     "origin" : 111,
	     "offset" : 222,
	     "src" : "http://example.site/path/resource" },
	   { "@id" : "http://example.site/path2/resource2#345-678",
	     "@type" : "AlphSpan",
	     "origin" : 345,
	     "extent" : 678,
	     "src" : "http://example.site/path2/resource2" }
	   ]
	 }

   Notice that the EDL, which was an anonymous object (a "blank node")
   in the JSON-LD document, has been assigned an id. This id will
   export with the node from the LinkStore. Also, the items in the
   EDLs list have been converted to AlphSpan objects.

   Next, the graph is recreated as a set of <Link> elements inside 
   our LinkStore's 'graph' property, which itself is a <Graph> element.

   var newLink = document.createElement("Link");
   newLink.setAttribute("anode", "http://foo.site/bar#123-456");
   newLink.setAttribute("rel", "http://bar.site/relation");
   newLink.setAttribute("bnode", "http://foo.site/bar#123-456");
   linkStore.graph.appendChild(newLink);

   Which creates an element like this:

   <Link anode="http://foo.site/bar#123-456" 
         rel="http://bar.site/relation"
	 bnode="_:XYZ"/>
   
   And that's it. The benefits of this design are that we can quickly
   query links by relation, A node, or B node with
   linkStore.graph.querySelectorAll(), and we can easily establish
   what nexûs to try and draw on the workspace by zipping through
   linkStore.nodes. Additionally, we retain any attribute data that
   graph authors may have attached to nodes.

   */


function LinkStore(){
    this.graphs = {};
    this.autosaveInterval = 11000; // 11 seconds.

    /* Load-in all of the graphs from localStorage */
    
    if(parent == window){
	var localKeys = Object.keys(localStorage);
	for (var ix in localKeys){
	    var key = localKeys[ix];
	    if(key.startsWith("dptronGraph-")){
		try{
		    var graphDoc = JSON.parse(localStorage.getItem(key));
		} catch(e){
		    console.log("Unable to parse graph ", key);
		    continue;
		};
		/* now convert the serialized graph to our internal 
		   representation*/
		var graphElement = document.createElement("Graph");
		for(var lix in graphDoc.graph){
		    var linkElement = document.createElement("Link");
		    linkElement.setAttribute("anode", graphDoc.graph[lix].anode);
		    linkElement.setAttribute("bnode", graphDoc.graph[lix].bnode);
		    linkElement.setAttribute("rel", graphDoc.graph[lix].rel);
		    graphElement.appendChild(linkElement);
		}
		graphDoc.graph = graphElement;
		this.graphs[graphDoc["@id"]] = graphDoc;
	    }
	}

	/* I think that in the future, we won't just autosave all of these on
	   a timer; rather we'll just make sure to save them whenever a change
	   is made to the graph document. But for now, this. */
	
	this.timer = setInterval(this.autosave,this.autosaveInterval,this);
    }
}

LinkStore.prototype.autosave = function(linkstore){
    var graphKeys = Object.keys(linkstore.graphs);
    
    for (var graphIx in graphKeys){
	
	var graphDoc = linkstore.graphs[graphKeys[graphIx]];
	
	/* Graphs that have http(s): URLs shouldn't be saved to localStorage */

	if(graphDoc["@id"].startsWith("http")){

	    /* Ummm... do NOTHING with them? I think so...? */

	} else {

	    /* In the LinkStore, the graph's "graph" property is a DOM
	       element and it won't stringify, so we need to make a
	       copy of the graph and replace its "graph" with a
	       localStorage-safe representation. */

	    var graphDocCopy = JSON.parse(JSON.stringify(graphDoc));
	    var safeGraph = [];
	    var linkElements = Array.from(graphDoc.graph.children);
	    for(var lix in linkElements){
		safeGraph.push(
		    {
			"anode" : linkElements[lix].getAttribute("anode"),
			"bnode" : linkElements[lix].getAttribute("bnode"),
			"rel" : linkElements[lix].getAttribute("rel")
		    }
		);
	    }

	    graphDocCopy.graph = safeGraph;

	    localStorage.setItem("dptronGraph-" + graphDocCopy["@id"],
				 JSON.stringify(graphDocCopy));
	    
	}
    }
}

LinkStore.prototype.loadLD = function( newGraph ){
    
    /* Takes a parsed JSON-LD link graph and explodifies it
       into our internal link representation. */

    // Does it have a @graph?
    if (!newGraph["@graph"]){
	console.log("JSON document does not have an @graph property.");
	return;
    }

    /* Iterate over the @graph and convert all nodes to a consistent
       object form, then store them in our new nodes object.  */

    newGraph.nodes = {};
    newGraph.graph = document.createElement("Graph");
    
    for(var ix in newGraph["@graph"]){
	
	var nodeA = LinkStore.nodify( newGraph["@graph"][ix] );
	
	if (nodeA["@type"] == "EDL"){

	    /* Descend into the EDL's @list and explodify each entry
	       if necessary. Also, give this EDL node an id if it's
	       currently anonymous. */

	    for (var iz in nodeA["@list"]){
		nodeA["@list"][iz] = LinkStore.nodify( nodeA["@list"][iz] );
	    }

	    if(!nodeA["@id"]) nodeA["@id"] = "_:" + ix;
	    
	} 

	/* Now, go over all of the properties of the A node. Every
	   non-attribute property key is taken to be a link
	   relation. The value of each of these properties will either
	   be a string -- which COULD be a media fragment, or simply a
	   document URL -- or an object of the AlphSpan or EDL types,
	   OR an array of those same kinds of things. */

	var relations = Object.keys(nodeA);
	
	for (var iy in relations){
	    
	    var rel = relations[iy];
	    
	    if (rel.startsWith("@")) continue; // Skip attributes
	    
	    var bNodes;

	    /* Is this an array? If not, make an array of nodes for processing. */

	    if(typeof(nodeA[rel]) == "object" && Object.keys(nodeA[rel])[0] == "0"){
		bNodes = nodeA[rel];
	    } else {
		if (typeof(nodeA[rel]) == "string"){
		    /*  If this is a string, we don't want to process it and add 
			it to the nodes list unless it's an HTTP(S) URL with a fragment
		        selector. */
		    if(nodeA[rel].startsWith("http")){
			if (!nodeA[rel].includes("#")) continue;
		    } else {
			continue;
		    }
		}
		bNodes = [ nodeA[rel] ];
	    }
	    
	    for (var iy in bNodes) {
		var nodeB = LinkStore.nodify(bNodes[iy]);
		if (nodeB["@type"] == "EDL") {
		    
		    /* And finally, if this B-node is an EDL, we want to process
		       all of ITS @list items into objects as well. Now, it's valid in
		       JSON-LD for this to nest infinitely deeps, but this is as deep
		       as we're going to go. Should this recurse? Probably. */

		    for(var iz in nodeB["@list"]){
			nodeB["@list"][iz] = LinkStore.nodify( nodeB["@list"][iz] );
		    }
		    if(!nodeB["@id"]) nodeB["@id"] = "_:" + ix + "-" + iy;
		}
		/* Add this node to our nodes property. */
		newGraph.nodes[nodeB["@id"]] = nodeB;
		
		/* Then put this <Link> into our <Graph>*/
		var newLink = document.createElement("Link");
		newLink.setAttribute("anode", nodeA["@id"]);
		newLink.setAttribute("bnode", nodeB["@id"]);
		newLink.setAttribute("rel", rel);
		newGraph.graph.appendChild(newLink);
	    }
	    /* Now that we're done with all of these relations, we can
	       delete this property from the A-node. */
	    delete nodeA[rel];
	}
	/* Then, store the A-node to our nodes object. */
	newGraph.nodes[nodeA["@id"]] = nodeA;
    }

    /* Now we can do away with the old @graph property. It'll be
       recreated when exported. */

    delete newGraph["@graph"];
    
    /* Add the explodified graph to the LinkStore. */
    
    this.graphs[newGraph["@id"]] = newGraph;
    
    return newGraph;
}

LinkStore.prototype.exportGraph = function(graph, newID){
    /* Convert one of our graphs back into a JSON-LD document and return it.
       'graph' is just the @id of the graph document; if a newID string is passed
       it will be used as the @id of the exported graph. */
    
    /* Make sure the graph exists .*/
    if(!this.graphs[graph]){
	return;
    }

    var graph = this.graphs[graph];
    var graphDOM = graph.graph;
    var graphDoc = JSON.parse(JSON.stringify(graph));
    if (newID) graphDoc["@id"] = newID;
    var nodeIDs = Object.keys(graph.nodes);

    delete graphDoc.graph;
    graphDoc["@graph"] = [];

    delete graphDoc.nodes;

    /* Go through all of the nodes...
       - look for links in which that node is the A node
       - if there are any, create an entry in graphDoc["@graph"] for the node
       - then, for each link, add the relation and B node to the A node object */
    for (var ix in nodeIDs){
	var aNodeID = nodeIDs[ix];
	var aNode = graph.nodes[aNodeID];
	var nodeLinks = Array.from(graphDOM.querySelectorAll("[anode='" + aNodeID + "']"));
	if(nodeLinks.length > 0){
	    var n = JSON.parse(JSON.stringify(aNode));
	    for(var iy in nodeLinks){
		var l = nodeLinks[iy];
		n[l.getAttribute("rel")] = JSON.parse(JSON.stringify(graph.nodes[l.getAttribute("bnode")]));
	    }
	    graphDoc["@graph"].push(n);
	} else if(graphDOM.querySelector("[rel='" + aNodeID + "']")){
	    /* In case we have a node that is actually being used as a
	       relation in the graph, we want to make sure it gets
	       exported, too.  */
	    graphDoc["@graph"].push( JSON.parse(JSON.stringify(aNode)) );
	}
    }

    return JSON.stringify(graphDoc, null, '    ');
}

LinkStore.nodify = function(node) {
    /* Convert string nodes to objects, or untyped object nodes with fragment
       selectors into AlphSpan objects. 

       Turns this: 

         "http://foo.bar/resource"

       into this:

         { "@id" : "http://foo.bar/resource" }

       Or, turns this:

         { "@id" : "http://foo.bar/resource#123-456" }

       into: 
    
         { "@id" : "http://foo.bar/resource#123-456",
           "@type" : "AlphSpan",
           "src" : "http://foo.bar/resource",
           "origin" : "123",
           "extent" : "456" } 
    */
    
    if (typeof(node) == "string") node = { "@id" : node  };

    if ( !node["@type"] && !node["@id"].startsWith("#") && node["@id"].includes("#") ){
	var newSpan = AlphSpan.fromURL(node["@id"]);
	node["@type"] = "AlphSpan";
	node["src"] = newSpan.src;
	node["origin"] = newSpan.origin;
	node["extent"] = newSpan.extent;
    }
    
    return node;
}

LinkStore.prototype.getLinksWithNode = function(node){

    /* Riffle thru' the LinkStore for links with nodes matching the
       passed node. 

       The passed node should be an id string, like: 
         "https://some.site/resource#123-456", or
	 "http://some.site/resource", or
	 "~ABCD1234" ...

       We return an array of match objects that look like this:

       { "graph" : id of the graph in which the match was found,
         "link" : the Link element that contains the passed node }

    */
    var matches = [];

    for (var graphId in this.graphs) {
	
	var graphDoc = this.graphs[graphId];

	/* Do a query for elements with the node as either their A node 
	   or their B node... */
	
	var matchingLinks = Array.from( graphDoc.graph.querySelectorAll(
	    "[anode='" + node + "'], [bnode='" + node + "']"));

	for ( var lix in matchingLinks ){
	    matches.push( {"graph": graphId,
			   "link" : matchingLinks[lix] } );
	}
    }
    
    return matches;
}

LinkStore.Graph = function(id){
    
    /* Return a new, empty graph document with the passed ID. This is
       the internal representation of a graph document. For the
       JSON-LD version, call LinkStore.exportGraph(graph) on this
       object. */
    
    if (!id) id = "~" + Date.now().toString(36).toUpperCase();
    return { "@id" : id,
	     "@context" : "http://alph.io/terms",
	     "@type" : "AlphLinkDocument",
	     "nodes" : {},
	     "graph" : document.createElement("Graph")
	   };
}

LinkStore.prototype.deleteGraph = function(id){

    /* Easy enough to remove from the link store... */
    delete this.graphs[id];

    /* And remove from localStorage as well... */
    localStorage.removeItem("dptronGraph-" + id);
    
}

LinkStore.prototype.addTo = function(link,graphId){
    /* Add a link to a graph document. 

       The 'link' should be an object that looks like this: 

       { "nodeA" : {"@id" : "node_A_URI"} ,
         "rel" : "relation_URI",
	 "nodeB" : { "@id" : "node_B_URI"} 
	}

       'graphId' is the "@id" of the document to be modified.
    */

    console.log("Trying to store ", link, " to ", graphId);
    
    var graphDoc = this.graphs[graphId];
    if(!graphDoc){
	console.log("Graph document ", graph, " is not present in this LinkStore.");
	return false;
    }

    /* See if the link exists already. */
    
    var linkMatches = Array.from( graphDoc.graph.querySelectorAll(
	"[anode='" + link.nodeA["@id"] + "'][bnode='" + link.nodeB["@id"] +
	    "'][rel='" + link.rel + "']"));

    if (linkMatches.length > 0){

	/* Link already exists! Do... nothing? Or should we do a
	   simple node merge just in case this method is being used to
	   update/modify nodes? Like so:
	   
	   graphDoc.nodes[link.nodeA["@id"]] = {...graphDoc.nodes[link.nodeA["@id]], ...link.nodeA};
	   graphDoc.nodes[link.nodeB["@id"]] = {...graphDoc.nodes[link.nodeB["@id]], ...link.nodeB};
	*/

	return false;
	
    } else {

	/* Create the nodes if necessary. */

	if(!graphDoc.nodes[link.nodeA["@id"]]){
	    graphDoc.nodes[link.nodeA["@id"]] = link.nodeA;
	}
	
	if(!graphDoc.nodes[link.nodeB["@id"]]){
	    graphDoc.nodes[link.nodeB["@id"]] = link.nodeB;
	}

	/* Add the <Link> to the graph... */
	var linkElement = document.createElement("Link");
	linkElement.setAttribute("anode", link.nodeA["@id"]);
	linkElement.setAttribute("bnode", link.nodeB["@id"]);
	linkElement.setAttribute("rel", link.rel);
	graphDoc.graph.appendChild(linkElement);

	return true;
    }
}

LinkStore.prototype.removeFrom = function(link,graphId){
    /* Remove a link from a graph document. 

       The 'link' should be an object that looks like this: 

       { "nodeA" : {"@id" : "node_A_URI"} ,
         "rel" : "relation_URI",
	 "nodeB" : { "@id" : "node_B_URI"} 
	}

       'graphId' is the "@id" of the document to be modified.
    */

    var graphDoc = this.graphs[graphId];
    if(!graphDoc){
	console.log("Graph document ", graph, " is not present in this LinkStore.");
	return false;
    }

    /* See if the link exists already. */
    
    var linkMatches = Array.from( graphDoc.graph.querySelectorAll(
	"[anode='" + link.nodeA["@id"] + "'][bnode='" + link.nodeB["@id"] +
	    "'][rel='" + link.rel + "']"));
    
    if (linkMatches.length > 0){
	/* Remove any matches (should only ever be 1, but you never know. ) */
	for (var ix in linkMatches){
	    linkMatches[ix].remove();
	}

	/* Then, if either of the nodes are no longer referenced in the graph, 
	   delete them from the nodes object. */
	if (!graphDoc.graph.querySelector("[anode='" + link.nodeA["@id"] + "'], " +
					  "[bnode='" + link.nodeA["@id"] + "'] ")){
	    delete graphDoc.nodes[link.nodeA["@id"]];
	} 
	
	if (!graphDoc.graph.querySelector("[anode='" + link.nodeB["@id"] + "'], " +
					  "[bnode='" + link.nodeB["@id"] + "'] ")){
	    delete graphDoc.nodes[link.nodeB["@id"]];
	} 
	
	return true;	
    } else {
	return false;
    }
}

LinkStore.prototype.rebaseNodes = function(oldsrc, newsrc, neworigin, graph){

    /* For when a linked resource has moved from one server to another,
       or when a noodle has been committed to the network and we need
       to change all of the link terminals that were pointing at it.

       This looks for all nodes, in a single graph or in all graphs,
       which begin with the string oldsrc, such as

         "http://alph.io"

	or,

	 "~K96LM401"

       and replaces that initial string with newsrc.

       graph should be the id of the graph you want the nodes rebased in
       or "all" if you want it done to all graphs.

       neworigin, if passed, will be added to the origin and extent
       of the existing node. */

    
    /* This really needs to be more sophisticated if it's going to work
       with every type of origin/extent that we're planning on supporting. */
    
    var neworigin = neworigin || 0;

    /* Iterate over all graphs... */
    for (var graphId in this.graphs){

	/* Filter out graphs that we're not looking to change... */
	if ((graph == graphId) || (graph == "all")){
	    
	    var graphDoc = this.graphs[graphId];

	    /* Now iterate over nodes in this graph... */
	    for(var nodeId in graphDoc.nodes){

		var node = graphDoc.nodes[nodeId];
		var newNode = Object.create(node); // Make a copy...

		/* If this is an AlphSpan node... */
		if (node["@type"] && node["@type"] == "AlphSpan"){

		    /* And if its src is the one we're looking for... */
		    if(node["src"] == oldsrc){

			newNode["src"] = newsrc; // Update the src

			/* At some point, this needs to handle multidimensional
			   origin/extent values, and *maybe* handle point() and
			   element() selectors as well... */
			
			newNode["origin"] = parseInt(newNode["origin"]) + neworigin; 
			newNode["extent"] = parseInt(newNode["extent"]) + neworigin;
			
			newNode["@id"] = newsrc + "#" + newNode["origin"] + "-" + newNode["extent"];
			console.log("Rebasing node ", node, " to: ", newNode);
			this.replaceNode( nodeId, graphId, newNode);
		    }
		} else if (node["@type"] && node["@type"] == "EDL"){
		    /* ...ugh... */
		} else {
		    if(node["@id"].startsWith("~") && node["@id"] == oldsrc){

			/* This is a noodle. We should be getting called with a 
			   neworigin because the noodle has been written to a
			   network source, and we need to convert this to an
			   AlphSpan node. */
			
			var noodle = dptron.noodleManager.get(oldsrc);
			
			newNode["@type"] = "AlphSpan";
			newNode["src"] = newsrc;
			newNode["origin"] = neworigin;
			newNode["extent"] = noodle.text.length + neworigin;
			newNode["name"] = noodle.title;
			newNode["@id"] = newsrc + "#" + neworigin + "-" + newNode["extent"];
			
			console.log("Rebasing node ", node, " to: ", newNode);
			this.replaceNode( node["@id"], graphId, newNode );
			
		    } else {

			/* This is just a named node with an @id property
			   and nothin' else. */
			
			if(node["@id"].split("#")[0] == oldsrc){
			    /* It is POSSIBLE that this node's @id has a fragment
			       selector in it. If so, preserve it. */
			    if(node["@id"].contains("#")){
				newNode["@id"] = newsrc + "#" + node["@id"].split("#")[1];
			    } else {
				newNode["@id"] = newsrc;
			    }
			    this.replaceNode( node["@id"], graphId, newNode );
			}
		    }
		}
	    }
	}
    }
}


LinkStore.prototype.replaceNode = function(nodeId, graphId, newNode){

    /* Replaces the referenced node in the referenced graph with the
       passed object. */

    var graphDoc = this.graphs[graphId];
    if(!graphDoc){
	console.log("Graph document ", graph, " is not present in this LinkStore.");
	return false;
    }

    if(newNode["@id"] == nodeId){

	/* If the @id of the passed node object is the same as the passed nodeId, then
	   we simply overwrite it in the nodes object. Easy! */

	graphDoc.nodes[nodeId] = newNode;

    } else {
	
	/* In this case, the @id has changed -- could be a change to
	   the origin/offset of a media fragment, a document
	   substitution, or whatever. So we have to delete the old
	   node, add the new one, and then replace all references to
	   the old node in the graph. */

	delete graphDoc.nodes[nodeId];
	graphDoc.nodes[newNode["@id"]] = newNode;

	var alinks = Array.from(graphDoc.graph.querySelectorAll("[anode='" + nodeId + "']"));
	var blinks = Array.from(graphDoc.graph.querySelectorAll("[bnode='" + nodeId + "']"));
	for (var ix in alinks){
	    alinks[ix].setAttribute("anode",newNode["@id"]);
	}
	for (var ix in blinks){
	    blinks[ix].setAttribute("bnode",newNode["@id"]);
	}
    }
}

LinkStore.prototype.getRels = function(){
    
    /* Return a list of all of the relation types being used in 
       the LinkStore */

    var rels = [];

    for (var graphID in this.graphs){

	 var graphDoc = this.graphs[graphID];

	/* Go over each link in the graph and see if its
	   relation is in the list yet. */

	var links = Array.from(graphDoc.graph.childNodes);
	for (var ix in links){
	    var rel = links[ix].getAttribute("rel");
	    if(!rels.includes(rel)) rels.push(rel);
	}
    }
    
    return rels;
}
