class Ximg extends HTMLImageElement {

    /* This is a custom element that represents a portion of an image.*/
       
    /* It's just a generic inline-block element that uses the
       background-image and background-position CSS property to place
       the image, and the usual width/height CSS properties to crop
       it. */

    /* x-img elements will assume every source image is 96ppi, unless
       something different is set in the 'resolution' attribute. When
       loading images with the Docuplextron, the image's LD data will
       be checked for a different resolution. */

    /* THE BIG QUESTION RIGHT NOW IS: Should this just extend 
       HTMLImageElement, or should it be its own thing? */
    
    static get observedAttributes() {
	return ['origin','extent','resolution'];
    }

    constructor(source, origin, extent, resolution){
	super();

	/* To do this properly, I need to do some checks to make sure
	   that all of these variables are coming in correctly, but I'm
	   just hacking this together right now. */

	if (source){
	    /* ...input filtering here... */
	    this.setAttribute("src", source);
	}

	if (resolution) {
	    this.setAttribute("resolution", parseFloat(resolution) || "96");
	}

	if (origin) {
	    /* This should be the first part of an Alph AREA selector. */
	    this.setAttribute("origin", origin);
	}

	if (extent) {
	    /* This should be the second part (after the hyphen) of an
	     * Alph AREA selector. */
	    this.setAttribute("extent", extent);
	}

	/* Now that the attributes are set, we can call the update()
	   function */
	
	Ximg.update(this);
	
   } 

    connectedCallback() {
	Ximg.update(this);
    }

    disconnectedCallback() {
    }

    adoptedCallback() {
    }

    attributeChangedCallback(name, oldValue, newValue) {
	Ximg.update(this);
    }
    
}

Ximg.update = function(e){
    
    
    /* The Alph AREA selectors for images are pixel-based and follow the
       CSS convention of one pixel being 1/96th of an inch. If the source
       image is a higher print resolution, we can correct for that here. */
    
    var res = e.getAttribute("resolution") || "96";
    var scaling = 96 / parseFloat(res);

    /* This needs to be fixed. We should put something in a try() block
       and see if we can get the image's metrics from the Docuplextron's 
       source info store -- but for now, we're just setting it to auto */

    //e.style.backgroundSize = (scaling * 100).toString() + "%";
    //e.style.backgroundSize = "auto";
    e.style.objectFit = "none";
    
    /* Setting image size and origin is pretty straight-forward. */

    var origin = e.getAttribute("origin") || "0,0";
    var extent = e.getAttribute("extent") || "0,0";
    
    var origins = origin.split(',');
    var extents = extent.split(',');
    
    e.width = parseFloat(extents[0]) - parseFloat(origins[0]);
    e.height = parseFloat(extents[1]) - parseFloat(origins[1]);
    e.style.objectPosition = (0 - origins[0]) + "px " + (0 - origins[1]) + "px";
    
}

customElements.define('x-img', Ximg, {extends: "img"});
