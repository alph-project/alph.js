/*

  ALPH.JS
  The Alph Project <http://alph.io/>
  ©2016-2023 Adam C. Moore (LÆMEUR) <adam@laemeur.com>

  This software is a prototype Nelson document interface for
  xanalogical Web media. It includes support for the Xanadu Project's
  <http://xanadu.com> current xanadoc and xanalink formats.

  This is free software and is released under the terms of the current
  GNU General Public License <https://www.gnu.org/copyleft/gpl.html>

  Source code and revision history are available on GitLab:
  <https://gitlab.com/alph-project/alph.js>

*/

/* What's in this file:
   - Initialization stuff (inlcuding the overlay redraw loop)
   - Input handling
   - Command palettes */

var alph = new Alphjs();
var dptron = new Docuplextron();

/* 





   CONTROLS

 
   First up,
   KEYBOARD HANDLING




*/

function keyup(evt){

    var tgt;

    dptron.keys[evt.which] = false;
    
    if(evt.which == 18) {
	evt.preventDefault();
	dptron.altKey = false;
	dptron.DOMCursor.toggle("off");
    }
    
    if(dptron.activePalette){
	dptron.activePalette.command({"code":evt.which,
				      "alt":evt.altKey,
				      "shift":evt.shiftKey,
				      "ctrl":evt.ctrlKey});
	dptron.DOMCursor.updatePosition();
    }

    /* Is this event coming from inside a shadowDOM? */

    tgt = evt.composedPath()[0];
    
    /* If the target is an editable <x-text>, update its extent. If
       it's a noodle, update the noodleStore. */

    if( tgt.tagName == "X-TEXT" && tgt.contentEditable ){
	if ( tgt.getAttribute("src").startsWith("~") ){
	    var noodleID = tgt.getAttribute("src").substring(1);
	    dptron.noodleManager.set( noodleID, tgt.textContent );
	    
	    /* And then! Update the contents of any other x-texts 
	       sourcing this noodle.*/
	    
	    var otherSpans = Array.from(
		document.querySelectorAll('x-text[src="~' + noodleID + '"]'));

	    /* We also need to find matching spans in shadowDOMs... */

	    var sdoms = Array.from(
		document.querySelectorAll("x-dom"));
	    for(var sdom in sdoms){
		otherSpans = otherSpans.concat(
		    Array.from(sdoms[sdom].shadowRoot.querySelectorAll('x-text[src="~' + noodleID + '"]')));
	    }
	    
	    for(var ix in otherSpans){
		if(otherSpans[ix] === tgt) continue;
		otherSpans[ix].textContent = tgt.textContent;
		otherSpans[ix].setAttribute("origin",0);
		otherSpans[ix].setAttribute("extent",tgt.textContent.length);
	    }
	}
	tgt.setAttribute("extent",tgt.textContent.length);
    }

    dptron.activeElement = dptron.DOMCursor.target || null;

    var selFocus = window.getSelection().focusNode;
    if(selFocus.nodeType == Node.TEXT_NODE &&
       evt.which != 18 &&
       evt.which != 17 &&
       evt.which != 16){
	var cRange = new Range();
	cRange.setStart(selFocus, window.getSelection().focusOffset);
	cRange.collapse(true);
	var cRect = cRange.getBoundingClientRect();

	dptron.cursorOnscreen(cRect.top,
			      cRect.bottom,
			      cRect.left,
			      cRect.right,
			      document.body);
 
    }
}

/***/

function keydn(evt){

    /* Ignore key repeats when modifiers are held 
    if(evt.altKey || evt.ctrlKey || evt.metaKey){
	if(dptron.keys[evt.which] == true) return;
    }*/

    /* Record the key's down state */
    dptron.keys[evt.which] = true;
    
    /* If a command palette is open, ignore keydowns. */
    if(dptron.activePalette) {
	evt.preventDefault();
	if(dptron.activePalette.palName.startsWith("Color") && !evt.repeat){
	    if(dptron.activePalette.palName == "Color1"){
		dptron.makePalette("Color2");
	    } else {
		dptron.makePalette("Color1");
	    }
	} 
	return;
    }

    /* If the event target is inside of a floater, make that floater
       our activeItem. */
    
    var parentItem = Docuplextron.getParentItem(evt.target);  
    if(parentItem){
	//dptron.activate(parentItem);
    }

    /* Okay, here's the big keymap. What a mess. */
    
    switch (evt.which){
    case 8: // [Bksp],[Mac Delete]
	
	/* Same as case 46! Remember to update both */
	
	if(evt.altKey){
	    evt.preventDefault();
	    for(var ix in dptron.activeItems){
		dptron.actionTarget = dptron.activeItems[ix];
		if(dptron.actionTarget.classList.contains("noodleBox")){
		    dptron.command("close and delete");
		} else {
		    dptron.command("close");
		}
	    }
	}
	break;
    case 9: // [Tab]
	/* If we're inside of a noodleBox, let [Tab] actually insert a tab instead
	   of warping to the next input. */

	if(evt.target.tagName == "X-TEXT" &&
	   evt.target.contentEditable == "true" &&
	   evt.target.getAttribute("src").startsWith("~")){
	    evt.preventDefault();
	    var s = window.getSelection();
	    var r = s.getRangeAt(0);
	    
	    /* The fun thing here is that sometimes there will be multiple
	       Text nodes inside of this X-TEXT. We want to insert the tab
	       without losing track of our current position within the X-TEXT, 
	       but we can't simply get the offset of the Range's anchor because it
	       might only be the offset into the second or twelfth Text node. */
	    
	    r.deleteContents();
	    r.insertNode(document.createTextNode("\t"));
	    r.setStart(evt.target.firstChild,0);
	    var newOffset = r.toString().length;
	    evt.target.textContent = evt.target.textContent; // Concatenates Text nodes
	    r.setStart(evt.target.firstChild,newOffset);
	}
	break;
    case 13: // [Enter]
	if(evt.ctrlKey){
	    if(evt.target.id == "dptronUserCSS"){
		
		/* Set user stylesheet from the textarea on the settings pane.
		   This should live somewhere else eventually. */
		
		var uSheet = document.getElementById("dptronUserStyle") ||
		    document.getElementById(document.body.appendChild(document.createElement("style")).id = "dptronUserStyle");
		uSheet.textContent = evt.target.value;
		evt.preventDefault();
	    } else {
		
		evt.preventDefault();
		if (evt.shiftKey) {
		    dptron.commitSpan(true);
		} else {
		    dptron.commitSpan();
		}

	    }
	} else if(evt.altKey){

	//} else if(evt.shiftKey){

	} else {
	    Alphjs.insertNewline(evt);
	}
	break;
    case 18: // [Alt]
	dptron.altKey = true;
	dptron.DOMCursor.toggle("on");
	break;
    case 27: // [Esc]
	if(dptron.linker){
	    dptron.linker.hide();
	}
	break;
    case 32: // [Space]
	break;
    case 38: // [Up]
	if(evt.altKey){
	    evt.preventDefault();
	    if(evt.shiftKey){ 
		dptron.DOMCursor.move("shiftUp");
	    } else { 
		dptron.DOMCursor.move("up");
	    }
	    if(dptron.DOMCursor.target){
		var trect = dptron.DOMCursor.getTargetRect();
		dptron.cursorOnscreen(trect.top, trect.bottom, trect.left, trect.right, document.body);
	    }
	    dptron.postEDL(Docuplextron.getParentItem(evt.target));	    
	} else if(evt.shiftKey){
	    alph.selectionChange();
	}
	break;
    case 40: // [Down]
	if(evt.altKey){
	    evt.preventDefault();
	    if(evt.shiftKey){
		dptron.DOMCursor.move("shiftDown");
	    } else {
		dptron.DOMCursor.move("down");
	    }
	    if(dptron.DOMCursor.target){
		var trect = dptron.DOMCursor.getTargetRect();
		dptron.cursorOnscreen(trect.top, trect.bottom, trect.left, trect.right, document.body);
	    }
	    dptron.postEDL(Docuplextron.getParentItem(evt.target));
	} else if(evt.shiftKey){
	    alph.selectionChange();
	}
	break;
    case 39: // [Right]
	if(evt.altKey){
	    evt.preventDefault();
	    if(evt.shiftKey){
		dptron.DOMCursor.move("shiftRight");
	    } else {
		dptron.DOMCursor.move("right");
	    }
	    if(dptron.DOMCursor.target){
		var trect = dptron.DOMCursor.getTargetRect();
		dptron.cursorOnscreen(trect.top, trect.bottom, trect.left, trect.right, document.body);
	    }
	} else if(evt.shiftKey){
	    alph.selectionChange();
	}
	break;
    case 37: // [Left]
	if(evt.altKey){
	    evt.preventDefault();
	    if(evt.shiftKey){
		dptron.DOMCursor.move("shiftLeft");
	    } else {
		dptron.DOMCursor.move("left");
	    }
	    if(dptron.DOMCursor.target){
		var trect = dptron.DOMCursor.getTargetRect();
		dptron.cursorOnscreen(trect.top, trect.bottom, trect.left, trect.right, document.body);
	    }
	    
	} else if(evt.shiftKey){
	    alph.selectionChange();
	}
	break;
    case 46: // [Del]
	/* Same as case 8! Remember to update both */
	
	if(evt.altKey){
	    evt.preventDefault();
	    for(var ix in dptron.activeItems){
		dptron.actionTarget = dptron.activeItems[ix];
		if(dptron.actionTarget.classList.contains("noodleBox")){
		    dptron.command("close and delete");
		} else {
		    dptron.command("close");
		}
	    }
	}
	break;
    case 48: // 0 (zero)
	if(dptron.floaterManager.inhand){
	    evt.preventDefault();
	    evt.stopPropagation();
	    dptron.floaterManager.inhand.resetZoom();
	}
	break;
	
    case 49: // 1 - DOMpath to console.
	//console.log(getDomPath(alph.activeElement));
	break;
	
    case 173: // [-] Font size decrease
	if(evt.ctrlKey || evt.altKey){
	    for(var ix in dptron.activeItems){
		var activeItem = dptron.activeItems[ix];
		if(evt.shiftKey){
		    var oldScale = parseFloat(activeItem.getAttribute("data-scale"));
		    var newScale = oldScale * 0.952;
		    activeItem.setAttribute("data-scale",newScale);
		    activeItem.style.transform = "scale(" + newScale + ")";
		} else {
		    var oldSize = window.getComputedStyle(activeItem).fontSize;
		    activeItem.style.fontSize = (parseInt(oldSize) - 1) + "px";
		}
	    }
	    // Note that this SHOULD block the default enlarge/reduce operations
	    // of the browser ...but it doesn't?
	    evt.preventDefault();
	}
	break;

    case 61: // [+] Font size increase
	if(evt.ctrlKey || evt.altKey){
	    // There needs to be a message send here, to increse font size in
	    // subordinate frames as well.
	    for(var ix in dptron.activeItems){
		var activeItem = dptron.activeItems[ix];
		if(evt.shiftKey){
		    var oldScale = parseFloat(activeItem.getAttribute("data-scale"));
		    var newScale = oldScale * 1.05;
		    activeItem.setAttribute("data-scale",newScale);
		    activeItem.style.transform = "scale(" + newScale + ")"; 
		} else {
		    var oldSize = window.getComputedStyle(activeItem).fontSize;
		    activeItem.style.fontSize = (parseInt(oldSize) + 1) + "px";
		}
	    }
	    evt.preventDefault();
	}
	break;
    case 222: // ['] Copy current selection url to system clipboard
	
	break;
    case 65: // [A]lph palette
	if(evt.altKey){
	    evt.preventDefault();
	    dptron.makePalette("A");
	}
	break;
    case 66: // Alph [B]ar
	if(evt.altKey){
	    evt.preventDefault();
	    if(dptron.bar.style.top == "0px"){
		dptron.bar.style.top = "-50px";
		dptron.bar.querySelector("#dptronSideFlap").style.top = "0px";
		dptron.floaterManager.topBoundary = 0;
	    } else {
		dptron.bar.style.top = "0px";
		dptron.bar.querySelector("#dptronSideFlap").style.top = window.getComputedStyle(dptron.bar).height;
		dptron.floaterManager.topBoundary = parseInt(window.getComputedStyle(dptron.bar).height);
	    }    
	}	
	break;
    case 67: // [C]opy xanalogically
	if(evt.altKey){
	    evt.preventDefault();
	    
	    if(window.getSelection().isCollapsed){
		
		/* Nothing highlighted -- do we have anything selected
		   with the DOMCursor? */
		
		if(dptron.DOMCursor.target){
		    var copySource = document.createDocumentFragment();
		    copySource.appendChild(dptron.DOMCursor.target.cloneNode(true));
		    console.log("copySource from DOMCursor:",copySource);
		} else {
		    // Nothing to copy!
		    console.log("Nothing selected!")
		    return;
		}
		
	    } else {
		var copySource = alph.selection.toFragment();
		console.log("copySource from AlphSelection:",copySource);
	    }

	    /* Push the document fragment onto the kill-ring (the
	       kill-ring is just an array of document
	       fragments. )*/

	    dptron.killring.push(copySource);

	}
	break;	
    case 68: // [D]elete, [Shift]+[D]e-parent
	if(evt.altKey){
	    if(evt.shiftKey){
		/* "De-parent" the target of the DOMCursor. */
		if(dptron.DOMCursor.target){
		    dptron.DOMCursor.select(Docuplextron.deParent(dptron.DOMCursor.target));
		}
	    } else {
		evt.preventDefault();
		dptron.dupe();
		// DUPLICATE noodle (put this in a function, Adam)
		/*
		for(var ix in dptron.activeItems){
		    var floater = dptron.activeItems[ix];
		    if(floater.classList.contains("noodleBox")){
			// Only for noodleBoxen at the moment.
			var sourceNoodle = dptron.noodleManager.get(
			    floater.querySelector("x-text").getAttribute("src"));
			if (!sourceNoodle) return;
			var newdle = dptron.newdleBox(null,sourceNoodle.text);
			newdle.style.cssText = floater.style.cssText;
			newdle.style.left = floater.getBoundingClientRect().right + "px";
			var newLimbic = dptron.noodleManager.get(newdle.querySelector("x-text").getAttribute("src"));
			newdle.tab.text.textContent = newLimbic.title = sourceNoodle.title + "(copy)";
			newdle.toTop();
		    }
		    } */
		
		/* What was going on here?
		  dptron.DOMCursor.select(Alphjs.cut()[0]);
		  dptron.postEDL();
		*/
	    }
	}
	break;
    case 69: // [E]dit attributes
	if(evt.altKey && dptron.DOMCursor.enabled && dptron.DOMCursor.target){
	    evt.preventDefault();
	    Docuplextron.attributeMunglor(dptron.DOMCursor.target);
	}
	break;
    case 70: // [F]use / [F]etch-and-fill
	if(evt.altKey){
	    evt.preventDefault();
	    if(evt.shiftKey){
		alph.fetchAndFill();
	    } else {
		dptron.merge();
	    }
	}
	break;
    case 72: // [H]TML surround
	if(evt.altKey){
	    evt.preventDefault();
	    if(evt.ctrlKey){
		/* 1) Not xanalogical
		   2) document.execCommand() is obsolete and needs to be replaced.
		   This needs to first determine whether the user is manipulating 
		   orphan text or transcluded text, then act accordingly. Perhaps
		   this should all be handled in Alphhs.surround() ?
		*/
		var HTML = window.getSelection().toString();
		document.execCommand("insertHTML", false, HTML);
	    } else {
		// HTML element palette. This is ...not great. Rewrite it.
		var munglor = dptron.floaterManager.create(null,null,dptron.lastX,dptron.lastY);
		munglor.stickyToggle();
		munglor.classList.add("styleMunglor");
		munglor.style.width = "180px";
		var elements = ["b","i","u","s","em","dfn","sup","sub","ol","ul","li","blockquote","p","nav","section","article","header","footer","aside","h1","h2","h3","h4","h5","h6"];
		for(var ix in elements){
		    var button = document.createElement("input");
		    button.type = "button";
		    button.value = elements[ix];
		    button.onclick = function(m){
			var newElement = Alphjs.surround(this.value);
			dptron.postEDL();
			dptron.findSomeMatchingContent(Docuplextron.getParentItem(newElement));
			m.remove();
		    }.bind(button,munglor);
		    munglor.appendChild(button);
		}
	    }
	}
	break;
    case 73: // [I]nsert Editable span
	if(evt.altKey){
	    evt.preventDefault();
	    if(evt.shiftKey){
		Alphjs.split(true);
		dptron.insertEditable();
		//dptron.insertEditable("sibling");
	    } else {
		dptron.insertEditable();
	    }
	}
	break;
    case 74: // [J]oin
	if(evt.altKey){
	    evt.preventDefault();
	    Alphjs.joinContiguousSpans();
	    dptron.postEDL();
	}
	break;
    case 75: // [K]ill
	if(evt.altKey){
	    evt.preventDefault();
	    dptron.killring.push(dptron.DOMCursor.target);
	    var newTarget = dptron.DOMCursor.kill();
	    var newTargetContainer = Docuplextron.getParentItem(newTarget);
	    Alphjs.joinContiguousSpans(newTarget.parentElement);
	    dptron.postEDL(parentItem);
	    dptron.findSomeMatchingContent(newTargetContainer.id);
	}
	break;
    case 76: // [L]ink
	if(evt.altKey){
	    if(evt.shiftKey){
		/* Open a link editor. If the Alph selection is not empty, 
		   pass the selection as either an AlphSpan or EDL node
		   to the editor. */
		if(alph.selection.spans.length == 1){
		    // Single span
		    dptron.linkMunglor(alph.selection.spans[0].toLinkStoreEDLItem());
		} else if (alph.selection.spans.length > 1){
		    var n = {"@id" : "_:EDL-" + Date.now().toString(36).toUpperCase(),
			     "@type" : "EDL",
			     "@list" : [ ] };
		    for (var ix in alph.selection.spans){
			n["@list"].push(alph.selection.spans[ix].toLinkStoreEDLItem());
		    }
		    dptron.linkMunglor(n);
		} else {
		    dptron.linkMunglor();
		}
	    } else {
		if(dptron.linker){
		    if(dptron.linker.active){
			dptron.linker.hide();
		    } else {
			dptron.linker.show();
		    }
		}
	    }
	}
	break;
    case 77: // [M]ungle style
	if(evt.altKey){
	    if(dptron.DOMCursor.target){
		return newStyleMunglor(dptron.DOMCursor.target);
	    }
	};
	break;
    case 78: // [N]ew Composition
	if(evt.altKey){
	    evt.preventDefault();
	    evt.stopPropagation();
	    if(evt.shiftKey){
		dptron.newdleBox(null,null,true).toTop();
	    } else {
		dptron.newComposition().toTop();
	    }
	}
	break;
    case 79: // [O]pen for editing
	if(evt.altKey){
	    if(evt.target.tagName == "X-TEXT" && evt.target.getAttribute("src").startsWith("~")){
		if (evt.target.contentEditable){
		    evt.target.contentEditable = false;
		}else{
		    evt.target.contentEditable =  true;
		    evt.target.focus();
		}
	    } else {
		for(var ix in dptron.activeItems){
		    if(dptron.activeItems[ix].classList.contains("noodleBox")){
			var xt = dptron.activeItems[ix].querySelector("x-text");
			xt.contentEditable = true;
			xt.focus();
		    }
		}
	    }
	}
	break;
    case 80: // [P]op-out
	if(evt.altKey){
	    evt.preventDefault();
	    if(evt.shiftKey){
		dptron.domPopout();
	    } else {
		// Alph pop-out. Creates a new floater containing the current Alph selection.
		dptron.popout();
	    }
	}
	break;
    case 81: // [Q] rebuild transpointers
	if(evt.altKey){
	    evt.preventDefault();	    
	    dptron.rebuildTranspointers();
	}
	break;
    case 82: // [R]efresh
	if(evt.altKey){
	    evt.preventDefault();
	    overlayLoop();
	    dptron.updateAllRects();
	}
	break;
    case 83 : // [S]plit
	if(evt.altKey){
	    evt.preventDefault();
	    //Alphjs.split();
	    dptron.DOMCursor.select(dptron.split(evt.shiftKey)[0]);
	    /* DOMCursor.prototype.select() puts the system caret at
	       the beginning of the selection, but in this
	       application, we want it at the end. I should probably
	       modify DOMCursor.prototype.select() to take an argument
	       that tells which end to put the caret at. */
	    if(window.getSelection().focusNode.nodeType == Node.ELEMENT_NODE &&
	       window.getSelection().focusNode.tagName == "X-TEXT"){
		window.getSelection().collapse(
		    window.getSelection().focusNode.firstChild,
		    window.getSelection().focusNode.firstChild.length );
	    }
	    dptron.postEDL();
	} else if(evt.ctrlKey){
	    // Reserved for SIZE op w/ wheel
	    evt.preventDefault();
	    evt.stopPropagation();
	}
	break;
    case 84:  // [T]ranspointer view mode
	if(evt.altKey){
	    evt.preventDefault();
	    if(evt.shiftKey){
		dptron.changeTpointerMode("up");
	    } else {
		dptron.changeTpointerMode();
	    }
	}
	break;	
    case 85:  // [U]ndo

	break;
    case 86: // [V] paste xanalogically
	if(evt.altKey){
	    evt.preventDefault();
	    if(dptron.killring.length > 0){
		console.log("Calling Alphjs.paste() at: ", window.getSelection().getRangeAt(0));
		
		if(evt.shiftKey){
		    /* Shift+Alt+V is insert-after-DOMCursor-target */
		    var frag = dptron.DOMCursor.insertAfter(dptron.killring[dptron.killring.length -1]);
		    alph.fetchAndFill(frag.parentElement);
		    
		} else {
		    /* Alt+V is insert-at-point, which will split the element that the
		       caret is currently placed inside. */
		    console.log("Bar");
		    var frag = Alphjs.paste(dptron.killring[dptron.killring.length -1]);
		    alph.fetchAndFill(frag);
		}
		
		Docuplextron.taintId(
		    Docuplextron.getParentItem(window.getSelection().anchorNode));
		Alphjs.cullEmptyNodes();
		dptron.postEDL();
		dptron.findSomeMatchingContent(Docuplextron.getParentItem(frag));
	    }
	}
	break;
    case 87: // [W]ebuild EDLs
	// dptron.rebuildEdlStore();
	break;
    case 88: // e[X]cise
	if(evt.altKey){
	    evt.preventDefault();
	    if(evt.shiftKey){	    
		dptron.domPopout(true);
	    } else {
		dptron.popout(true);
	    }	    
	}
	break;
    case 90: // [Z] Delete empty nodes
	if(evt.altKey){
	    evt.preventDefault();
	    Alphjs.cullEmptyNodes();
	}
	break;
    case 219: // [[] Grow/shrink span origin
	if(evt.altKey){
	    evt.preventDefault();
	    if(evt.shiftKey){
		alph.resizeSpan("origin",-1);
	    } else {
		alph.resizeSpan("origin",1);
	    }
	    dptron.postEDL(parentItem);
	    dptron.findSomeMatchingContent(window.getSelection().anchorNode.parentElement);
	}
	break;
    case 221: // []] Grow/shrink span extent
	if(evt.altKey){
	    evt.preventDefault();
	    if(evt.shiftKey){
		alph.resizeSpan("extent",-1);
	    } else {
		alph.resizeSpan("extent",1);
	    }
	    dptron.postEDL(parentItem);
	    dptron.findSomeMatchingContent(window.getSelection().anchorNode.parentElement);	    
	}
	break;
    }
	
}

/*





   And now...
   MOUSE HANDLING




*/

function mousemv(evt){

    /* Lots of stuff happens when the mouse moves! */

    /* First up, if the user has a floater (or multiple floaters)
       "in hand", we need to move/zoom them accordingly... */

    if(dptron.floaterManager.inhand != null){
	
	evt.preventDefault();
	evt.stopPropagation();
	
	if(dptron.floaterManager.inhand == "all"){

	    /* If the floaterManager's 'inhand' property is set to
	       "all", that means we're panning (or zooming, if CTRL
	       is being held) the workspace. */

	    if(evt.ctrlKey){
		dptron.floaterManager.zoomWorkspace(evt.movementX, evt.movementY, dptron.lastX, dptron.lastY);	
	    } else {
		dptron.floaterManager.panWorkspace(evt.movementX,evt.movementY);
	    }
	    
	} else if(evt.ctrlKey){
	    
	    /* If the CTRL key is being pressed while a single floater
	       or group of selected floaters (the activeItems list) are
	       in-hand, then we're going to zoom them according to
	       vertical mouse movement, and move them according to
	       horizontal mouse movement. */

	    if(dptron.activeItems.includes(dptron.floaterManager.inhand)){

		for(var ix in dptron.activeItems){
		    dptron.activeItems[ix].zMove(
			evt.movementX,evt.movementY,dptron.lastX,dptron.lastY,true );
		    dptron.activeItems[ix].move(evt.movementX,evt.movementY);
		}
		
	    } else {
		
		dptron.floaterManager.inhand.zMove(
		    evt.movementX, evt.movementY, dptron.lastX,dptron.lastY,true );
		dptron.floaterManager.inhand.move(evt.movementX, evt.movementY);
	    } 
	    
	} else if(evt.shiftKey && !evt.altKey){

	    /* SHIFT + horizontal drag changes the width of the 
	       held floater. */
	    
	    dptron.floaterManager.inhand.changeWidth(evt.movementX);
	    dptron.updateOverlays = true;
	    
	} else {

	    /* No modifier keys, so we just slide the floaters around. */
	    
	    if(!dptron.floaterManager.inhand.classList.contains("alphActiveItem")){
		dptron.floaterManager.inhand.move(evt.movementX,evt.movementY);
	    } else {
		for(var ix in dptron.activeItems){
		    dptron.activeItems[ix].move(evt.movementX,evt.movementY);
		}
	    }
	}
    }

    /* CTRL+SHIFT while moving the mouse pans the workspace. */
    
    if(evt.ctrlKey && evt.shiftKey){
	dptron.floaterManager.panWorkspace(evt.movementX,evt.movementY);
    }

    /* Update the currentContext property. If the user is in the 
       context-oriented transpointer view mode (mode 1), then 
       transpointers will be redrawn as the pointer moves over 
       different floaters. */
    
    if(dptron.dptronMode){
	var contextFloater = Docuplextron.getParentItem(evt.target);
	if(contextFloater){
	    if(contextFloater.id != dptron.currentContext){
		dptron.currentContext = contextFloater.id;
		if(dptron.transpointerMode == 1){
		    dptron.rebuildTranspointers();
		}
	    }
	} else if(evt.target == dptron.mainDiv){
	    dptron.currentContext = null;
	} else {
	    
	    /* We land here if/when the mouse is over some UI elements,
	       like the topbar or the flap or the Alphbox. Do we want
	       to do anything here? */
	    
	}
    }
    
    /* If there is currently a SELECTION and we are dragging the mouse,
       update the Alph selection and keep the Alphbox visible. */
    
    if (!window.getSelection().isCollapsed){
	if (evt.buttons > 0){
	    /* User is making a text selection ... */
   	    alph.selectionChange();
	} else {
	    /* A selection is active, but the user is not changing it... */
	    alph.box.show();
	}
    } else if (dptron.selectionRect && (evt.buttons > 0)) {
	
	/* This means that the user is currently dragging a selection
	   rectangle over an image. */
	
	var tgt = evt.composedPath()[0];

	if(tgt == dptron.selectionRect.target){
	    var newWidth = evt.offsetX - dptron.selectionRect.anchorX;
	    var newHeight = evt.offsetY - dptron.selectionRect.anchorY;
	    dptron.selectionRect.setAttribute("width", newWidth);
	    dptron.selectionRect.setAttribute("height", newHeight);

	    alph.selectionChange(Docuplextron.selectionRectToAlphSpan(dptron.selectionRect));
	}
    } else {
	
	/* There is no selection. So, only pop-up the Alphbox when we
	   mouse over transclusible media. */

	/* Set 'tgt' to point at the target element; if the mouse is
	   moving over a shadow DOM, we'll figure out what it's pointing
	   at inside of the shadow DOM here. We'll need references to 
	   evt.target and tgt at various points. */
	
	var tgt = (evt.target.shadowRoot) ? 
	    evt.target.shadowRoot.elementFromPoint(evt.clientX, evt.clientY) :
	    evt.target;

	if (!tgt) {
	    
	    /* I'm noticing in the console that sometimes tgt is
	       coming back null. ...I don't know why, but we'll just
	       safely do nothing here if that happens. */

	} else if (!tgt.namespaceURI){
	    
	    /* Hrm. What was this in here for? SVG?
	       We might be able to use this to get the linker tool to latch
	       onto SimpleNexus... */
	    
	} else if (tgt.hasAttribute("src")){

	    /* We have moused over an element with an 'src' attribute; could
	       be an x-text, could be an img, could be a video or audio player...

	       Create an AlphSpan object for the media fragment... */
	    
	    var xsrc = tgt.getAttribute("src");

	    if(xsrc.includes(Alphjs.TRANSCLUDE_QUERY)){
		var alphSpan = AlphSpan.fromURL(xsrc);
	    } else if(tgt.tagName == "X-TEXT") {
		var alphSpan = AlphSpan.fromXtext(tgt);
	    } else if(tgt.hasAttribute("src") &&
		      tgt.hasAttribute("origin") &&
		      tgt.hasAttribute("extent")){
		// Actually, fromXtext() works for x-img as well...
		var alphSpan = AlphSpan.fromXtext(tgt);
	    } else if(tgt.tagName == "IMG"){
		var alphSpan = new AlphSpan(xsrc,
					    "0,0",
					    tgt.naturalWidth + "," + tgt.naturalHeight);
	    } else {
		var alphSpan = new AlphSpan(xsrc,null,null);
	    }
	    
	    if(!dptron.currentSpan ||
	       alphSpan.toURL() != dptron.currentSpan.toURL()){
		/* If we don't have a currentSpan or if alphSpan is different
		   from currentSpan...*/
		dptron.currentSpan = alphSpan;
		alph.popFooter(dptron.currentSpan);
		if(dptron.transpointerMode == 2) dptron.rebuildTranspointers();
		if(dptron.linker) dptron.linker.spanChange(alphSpan);
	    } else {
		alph.box.show();
	    }
	} else if(Docuplextron.getParentItem(evt.target) ||
		  evt.target.classList.contains("titleTab") ||
		  !dptron.dptronMode){

	    /* We only want this stuff to fire-off if the pointer is inside of
	       a floater; we don't need the AlphBox populating with element()
	       pointers to the UI. */

	    var alphSpan = new AlphSpan();
	    
	    if(evt.target.classList.contains("titleTab")){

		/* If we're over a title tab, then we'll just treat that like
		   we're over the floater and use the floater's id. */
		
		//alphSpan.src = evt.target.floater.id;
		alphSpan = AlphSpan.fromURL(evt.target.floater.id);

	    } else if(dptron.dptronMode) {

		/* If we're in Nelson document mode, let's get a handle on 
		   the event target's floater */
		
		var floater = Docuplextron.getParentItem(evt.target);
		
		/* If the floater has a "content-source", then it contains an HTML
		   document (or fragment) loaded from the network. */
		
		if(floater.hasAttribute("content-source") &&
		  floater != evt.target ){

		    /* If we're mousing over HTML elements inside of the floater
		       (but not on the x-floater element itself) get an 
		       element pointer. I think we're making sure that any
		       fragment selector is stripped from the content-source,
		       because Alphjs.getElementPointer() looks at the 'resource'
		       attribute of the x-dom element (if there is one) to fully
		       reconstruct the element pointer. */
		    
		    alphSpan.src = floater.getAttribute("content-source").split("#")[0];
		    alphSpan.origin = Alphjs.getElementPointer(tgt,floater);
		    
		} else {
		    
		    /* NoodleBox... */
		    
		    if(floater.classList.contains("noodleBox")){
			
			alphSpan.src = floater.querySelector("x-text").getAttribute("src");
			
		    } else {
			
			alphSpan = AlphSpan.fromURL(floater.id);
			//alphSpan.src = floater.id;
		    }
		}
		
	    } else {
		/* Not in the Nelson document view */
		alphSpan.src = window.location.toString().split("#")[0] + "#" +
		    Alphjs.getElementPointer(evt.target);
	    }	    


	    if(!dptron.currentSpan ||
	       alphSpan.toURL() != dptron.currentSpan.toURL()){
		dptron.currentSpan = alphSpan;
		alph.popFooter(dptron.currentSpan);
		if(dptron.transpointerMode == 2) dptron.rebuildTranspointers();
		if(dptron.linker) dptron.linker.spanChange(alphSpan);
	    } else {
		alph.box.show();
	    }
	} else {
	    if(dptron.currentSpan){
		dptron.currentSpan = null;
		if(dptron.transpointerMode == 2) dptron.rebuildTranspointers();
		if(dptron.linker) dptron.linker.spanChange();
	    }
	}
	
    }

    dptron.lastX = evt.clientX;
    dptron.lastY = evt.clientY;
}

/***/

function mouseup(e){
    //dptron.movement = 0;

    // Drop any held floaters.
    if(dptron.floaterManager.inhand){
	e.preventDefault();
	e.stopPropagation();
    }
    dptron.floaterManager.drop();

    // Put away any command palettes.
    dptron.killPalette();

    if(dptron.activeMenu){
	if(dptron.activeMenu.grace){
	    dptron.activeMenu.grace = false;
	} else {
	    dptron.activeMenu.remove();
	    dptron.activeMenu = null;
	}
    }
    
    dptron.activeElement = dptron.DOMCursor.target || null;

    if(dptron.selectionRect){
	if(dptron.selectionRect.active){
	    if((dptron.selectionRect.getAttribute("width") <= 1) &&
	       (dptron.selectionRect.getAttribute("height") <= 1)){
		// Just kill any selection rectangles that are 1x1 or smaller
		dptron.selectionRect.remove();
		dptron.selectionRect = null;
	    } else {
		dptron.selectionRect.active = false;
		if(dptron.linker.active){
		    if(dptron.linker.nexusA.held){
			dptron.linker.spanChange(alph.selection.toLinkMunglorNode(),
						 dptron.linker.nexusA);
			dptron.selectionRect.remove();
			dptron.selectionRect = null;
		    } else if(dptron.linker.nexusB.held){
			dptron.linker.spanChange(alph.selection.toLinkMunglorNode(),
						 dptron.linker.nexusB);
			dptron.selectionRect.remove();
			dptron.selectionRect = null;
		    } else {
			dptron.activeMenu = dptron.contextMenuOn(dptron.selectionRect,
							     ["make 🅐 node",
							      "make 🅑 node"]);
		    }
		} else {
		    dptron.activeMenu = dptron.contextMenuOn(dptron.selectionRect,
							     ["crop to selection",
							      "pop out",
							      "link to area",
							      "link from area",
							      "copy span to clipboard"]);
		}
	    }
	} else {
	    dptron.selectionRect.remove();
	    dptron.selectionRect = null;
	}
    }

    /* Drop a linker nexus if we have one held. */
    if(dptron.linker){
	dptron.linker.globalMouseup();
    }
    
    if(dptron.updateOverlays){
	dptron.rebuildTranspointers();
	dptron.paintLinkTerminals();
	dptron.updateOverlays = false;
    }
}

/***/

function contextmenu(e){
    var tgt = (e.target.shadowRoot) ? 
	e.target.shadowRoot.elementFromPoint(e.clientX, e.clientY) :
	e.target;

    if(e.ctrlKey){
	if (Docuplextron.getParentItem(e.target)){
	    dptron.activeMenu = dptron.contextMenuOn(
		Docuplextron.getParentItem(e.target));
	}
	e.preventDefault();
	e.stopPropagation();
    } else if(tgt.hasAttribute("src")){
	e.preventDefault();
	e.stopPropagation();
	if(tgt.getAttribute("src").startsWith("~")){
	    // Noodle ...
	    var floater = Docuplextron.getParentItem(e.target);
	    if(floater.classList.contains("noodleBox")){
		// Noodle in a noodleBox...
		dptron.activeMenu = dptron.contextMenuOn(tgt,["source info",
							      "link to this noodle",
							      "link from this noodle"]);
	    } else {
		// Noodle in a composition...
		dptron.activeMenu = dptron.contextMenuOn(tgt,["source info",
							      "link to this noodle",
							      "link from this noodle"]);
	    }
	} else {
	    dptron.activeMenu = dptron.contextMenuOn(tgt);
	}
    } else if(e.target.tagName == "X-FLOATER"){
	e.preventDefault();
	e.stopPropagation();
	dptron.activeMenu = dptron.contextMenuOn(e.target);
    }
}

function titleTabContextMenu(floater,evt){
    if(floater.classList.contains("noodleBox")){
	dptron.activeMenu = dptron.contextMenuOn(floater,
						 [ "title",
						   "set colours",
						   "clear colours",
						   "select colour group",
						   "link to this resource",
						   "link from this resource",
						   "export as plain-text",
						   //"progenitors",
						   "close and delete",
						   "close"]);				
    }else{
	dptron.activeMenu = dptron.contextMenuOn(floater);
    }
}

function titleTabMouseDown(floater,evt){
    dptron.movement = 0;
    dptron.floaterManager.pickUp(floater);
}

function titleTabMouseUp(floater,evt){
    if(dptron.movement < 15){
	dptron.activate(floater,evt.shiftKey);
    } 
}
/***/

function mousedn(e){
    dptron.movement = 0;

    var parentFloater = Docuplextron.getParentItem(e.target);
    var tgt = e.composedPath()[0]; // event target inside shadowDOM
    
    if(e.button == 0){
	if(e.target == dptron.mainDiv){
	    e.preventDefault();
	    dptron.floaterManager.pickUp("all");
	} else if(parentFloater){
	    if(e.shiftKey){
		e.preventDefault();
		e.stopPropagation();
	    }
	    if(e.target.tagName == "X-FLOATER" ||
	       e.ctrlKey){
		dptron.floaterManager.pickUp(e.target);
	    } else if (tgt.tagName == "IMG"){
		
		/* Start a selection rectangle on an image */
		
		e.preventDefault(); // Suppress drag-and-drop
		
		dptron.selectionRectOn(tgt, e.offsetX, e.offsetY);
	    }
	}
    } else if(e.button == 1){ // Middle button
	if(parentFloater){
	    e.preventDefault();
	    e.stopPropagation();
	    dptron.floaterManager.pickUp(parentFloater);
	} 
    } else if(e.button == 2){
	// Nothing here. See contextmenu()
    }
}

/***/

function click(e){
    
    var target = (e.target.shadowRoot) ?
	e.target.shadowRoot.elementFromPoint(e.clientX, e.clientY) :
	e.target;
    var topLevel = (parent == window);
    var parentItem = Docuplextron.getParentItem(e.target);

    if(e.button == 0){
	if(target == dptron.mainDiv){
	    dptron.activate(target);
	} else if(dptron.movement < 15){
	    if(target.classList.contains("titleTab")){
		//dptron.activate(e.target.floater,e.shiftKey);
	    } else if(parentItem){
		dptron.activate(parentItem,e.shiftKey);
	    }
	}
	dptron.DOMCursor.select(target);
    } else if(e.button == 1){
	e.preventDefault();
	e.stopPropagation();
    }
    
    //dptron.postEDL(); // This should be removed?

    /* If a link is clicked while we're in multi-document mode or 
       when we're a subordinate frame in multi-document mode, 
       open it in a floater. Ctrl+click, however, should work
       as normal.*/

    var anchor = Docuplextron.getParentItem(target,"A");
    
    if(anchor && ( e.button == 0 ) && !anchor.download){
	if(topLevel){
	    if(dptron.dptronMode && !e.ctrlKey &&
	       dptron.floaterManager.layer.contains(e.target)){
		e.preventDefault();
		e.stopPropagation();
		dptron.loadExternal(anchor.href);
	    }
	} else if(!e.ctrlKey){
	    e.preventDefault();
	    e.stopPropagation();
	    dptron.loadExternal(anchor.href);
	}
    }
}

/***/

function wheel(e){
    if(e.target == dptron.mainDiv ||
       dptron.floaterDiv.contains(e.target)){
	e.preventDefault();
	/* Wheel events seem to have changed in Firefox 89? Scrolling way too fast. 
	   This is okay, because now we can get scrolling behaviour more consistent 
	   between FF and Chromium. */
	var deltaX = e.deltaX;
	var deltaY = e.deltaY;
	if(e.deltaMode == WheelEvent.DOM_DELTA_PIXEL){
	    /* We'll just divide by 16, because for our purposes it really
	       doesn't need to be perfect, it just needs to be a smaller value */
	    deltaX = deltaX / 16;
	    deltaY = deltaY / 16;
	} else if(e.deltaMode == WheelEvent.DOM_DELTA_LINE){
	    // This is what FF does. Don't need to change anything.
	} else {
	    // A DOM_DELTA_PAGE deltaMode? I don't think these appear in the wild ever.
	    deltaX = e.deltaX; // ???
	    deltaY = e.deltaY / window.innerHeight;
	}
	   
	dptron.wheelMessage(e.target,{"deltaX":deltaX,
				      "deltaY":deltaY,
				      "altKey":e.altKey,
				      "shiftKey":e.shiftKey,
				      "crtlKey":e.ctrlKey,
				      "clientX":e.clientX,
				      "clientY":e.clientY});
    }
}

/***/

Docuplextron.prototype.wheelMessage = function(target,e){

    /* Handler for wheel events coming in through postMessage() */

    /* Leaving this as-is for now instead of moving it into wheel(), 
       because as much as I don't want to, I may have to reimplement some
       of the old postMessage() system, as wheel events vanish into IFRAMEs
       and will stop a floater from scrolling. */
    
    var p = Docuplextron.getParentItem(target);    
    var clientX = e.clientX;
    var clientY = e.clientY;
    
    if(target.tagName == "IFRAME"){
	var clientRect = target.getBoundingClientRect();
	var scale = parseFloat(p.style.transform.match(/scale\((.+)\)/)[1]);
	clientX = (e.clientX * scale) + clientRect.left;
	clientY = (e.clientY * scale) + clientRect.top;
    }
    
    if(dptron.keys[17]){ // Ctrl
	if(dptron.keys[16]){ // Shift
	    // Ctrl+Shift+wheel anywhere 
	    dptron.floaterManager.zoomWorkspace(e.deltaX,e.deltaY,clientX, clientY);
	} else if(p){
	    if(p.classList.contains("alphActiveItem")){
		// Ctrl+wheel on activated floater
		for(var ix in dptron.activeItems){
		    p = dptron.activeItems[ix];
		    if(p) p.zMove(e.deltaX,e.deltaY,clientX,clientY,true);
		}
	    } else {
		// Ctrl+wheel over a floater
		p.zMove(e.deltaX,e.deltaY,clientX,clientY,true);
	    }
	}
    } else {
	p = Docuplextron.getParentItem(target);
	if(p){
	    if(p.classList.contains("alphActiveItem")){
		// Wheel over an activated floater
		for(var ix in dptron.activeItems){
		    p = dptron.activeItems[ix];
		    if(p) p.move(0,e.deltaY);
		}
	    } else {
		// Wheel over any floater
		p.move(0,e.deltaY);
	    }
	} else {
	    // Wheel anywhere else
	    dptron.floaterManager.panWorkspace(e.deltaX,e.deltaY);
	}
    }

}



/*




 
   COMMAND PALETTES
   This all needs to be reworked, modularized, etc.





*/

dptron.makePalette = function(x){
    
    /* Okay, so a better way to do something like this is to create
       a div with a bunch of spans in it, where each span has an event
       listener for keydowns and keyups, listening for specific keys,
       so that they can give some visual feedback of what's been
       pressed, yadda, yadda -- but this is a quick and dirty command
       palette mechanism that works. */

    // Nuke any existing palette if there is one
    
    if(dptron.activePalette){
	dptron.activePalette.remove();
    }

    // Make the <div>, place it, put it in the DOM
    
    dptron.activePalette = document.createElement("div");
    dptron.activePalette.id = "dptron-palette";
    dptron.activePalette.pending = 1;
    dptron.activePalette.style.cssText = "font-weight: normal; padding: 0.2em;";
    dptron.activePalette.style.top = (parseInt(window.getComputedStyle(dptron.bar).top) < 0) ?
	"0px" :
	(parseInt(window.getComputedStyle(dptron.bar).top) +
	 parseInt(window.getComputedStyle(dptron.bar).height)) + "px";
    document.body.appendChild(dptron.activePalette);
    dptron.activePalette.isPending = function(){
	if(dptron.activePalette.pending > 0){
	    dptron.activePalette.pending --;
	    return true;
	} else {
	    return false;
	}
    }
    
    // Which palette?
    switch(x){
    case "Color1":
	dptron.activePalette.palName = "Color1";
	if(dptron.mainDiv.classList.contains("alphActiveItem")){
	    Docuplextron.makePaletteItems(["Use wheel to select workspace primary background color; press |C| again to select secondary background color."],dptron.activePalette);
	} else if(document.activeElement.tagName == "X-TEXT"){
	    Docuplextron.makePaletteItems(["Use wheel to select noodle background color; press |C| again to select text color."],dptron.activePalette);
	} else if(dptron.activeItems.length == 1){
	    Docuplextron.makePaletteItems(["Use wheel to select noodle border/tab color"],dptron.activePalette);
	} else if(dptron.activeItems.length > 1){
	    Docuplextron.makePaletteItems(["Use wheel to select border/tab color of selected noodles"],dptron.activePalette);
	}
	dptron.activePalette.command = function(keypress){
	    if(dptron.activePalette.isPending()){return};
	    // Do the command...
	    switch (String.fromCharCode(keypress.code)){
	    case "C": // [C]leanup simplePointers
		dptron.makePalette("Color2");
		return; // Return before destroying palette! 
		break;
	    }
	    dptron.killPalette();
	}	
	break;
    case "Color2":
	dptron.activePalette.palName = "Color2";
	if(dptron.mainDiv.classList.contains("alphActiveItem")){
	    Docuplextron.makePaletteItems(["Use wheel to select workspace secondary background color; press |C| again to select primary background color."],dptron.activePalette);
	} else if(document.activeElement.tagName == "X-TEXT"){
	    Docuplextron.makePaletteItems(["Use wheel to select noodle text color; press |C| again to select background color."],dptron.activePalette);
	} else if(dptron.activeItems.length == 1){
	    Docuplextron.makePaletteItems(["Use wheel to select noodle border/tab color"],dptron.activePalette);
	} else if(dptron.activeItems.length > 1){
	    Docuplextron.makePaletteItems(["Use wheel to select border/tab color of selected noodles"],dptron.activePalette);
	}
	dptron.activePalette.command = function(keypress){
	    if(dptron.activePalette.isPending()){return};
	    // Do the command...
	    switch (String.fromCharCode(keypress.code)){
	    case "C": // [C]leanup simplePointers
		dptron.makePalette("Color1");
		return; // Return before destroying palette! 
		break;
	    }
	    dptron.killPalette();
	}	
	break;
    case "A":
	dptron.activePalette.palName = "Alph";	
	Docuplextron.makePaletteItems(["|A|lph-mode",
				       "DOM|C|ursor",
				       "|D|ebug…",
				       "|N|ew-Item",
				       "New nood|L|e",
				       "|H|TML…",
				       "S|P|lit Paragraphs",
				       "|R|earrange…",
				      "|S|election"],dptron.activePalette);
	dptron.activePalette.command = function(keypress){
	    if(dptron.activePalette.isPending()){return};
	    // Do the command...
	    switch (String.fromCharCode(keypress.code)){
	    case "A": // Alph mode (takeover)
		dptron.takeover();
		break;
	    case "B":
		dl = document.createElement("a");
		dl.href = "data:text/plain;charset=utf-8," + encodeURI(JSON.stringify(localStorage));
		dl.download = "limbic.txt";
		dl.target = "new";
		dl.textContent = "";
		dl.onclick = function(){ this.remove(); }.bind(dl);
		dptron.bar.appendChild(dl);
		dl.click();
		break;
	    case "C": // DOMcursor
		// *** This needs to be fixed.
		var t = document.getElementById("alph-editoverlaytoggle");
		t.checked = (t.checked) ? false : true;
		dptron.DOMCursor.toggle();
		break;
	    case "D": // [D]EBUG
		dptron.subPalette("DEBUG")
		return; // Return before destroying palette! 
		break;
	    case "H": // [H]TML surround
		dptron.subPalette("HTML")
		return; // Return before destroying palette! 
		break;
	    case "N": // [N]ew composition
		dptron.newComposition();
		break;
	    case "L": // New ~noodle
		dptron.newdleBox(null,null,true).toTop();
		break;
	    case "P": // TESTING...
		Alphjs.splitToParagraphs(dptron.DOMCursor.target);
		break;
	    case "R": // arRange...
		dptron.subPalette("ARRANGE")
		return;  // Return before destroying palette! 
		break;
	    case "S":
		dptron.subPalette("SELECTION")
		return;  // Return before destroying palette! 
		break;
	    }
	    dptron.killPalette();
	}
	break;
    case "DEBUG":
	dptron.activePalette.palName = "DEBUG";
	Docuplextron.makePaletteItems(["s|A|ve Session Now",
				       "|D|ump Tables",
				       "|L|ink Terminals",
				       "|N|oodle Dump",
				       "|O|verlayLoop",
				       "|S|ession Export"],dptron.activePalette);	
	dptron.activePalette.command = function(keypress){
	    if(dptron.activePalette.isPending()){return};
	    // Do the command...
	    switch (String.fromCharCode(keypress.code)){
	    case "A": // s[A]ve session NOW; was: b[A]rf?
		dptron.sessionSave();
		//dptron.command("barf");
		break;
	    case "B": // [B]uild link bridges
		buildLinkBridges();
		break;
	    case "C": // [C]leanup simplePointers
		//cleanupSimplePointers();
		dptron.connectLinkTerminals();
		break;
	    case "D": // [D]ump tables to console (debuggin')
		console.log("simplePointers: ", dptron.simplePointers, "simpleNexi:", dptron.simpleNexi, "DOMCursor:",dptron.DOMCursor, "contexts:",dptron.contexts,"edlStore:",dptron.edlStore,"transpointerSpans:",dptron.transpointerSpans,dptron.linkStore);
		break;
	    case "L": // Light-up [L]ink terminals
		dptron.paintLinkTerminals();
		break;
	    case "N":
		var f = dptron.floaterManager.create(null,null, dptron.lastX, dptron.lastY);
		f.setAttribute("title","Alph Text Export");
		var p = document.createElement("pre");
		p.appendChild(
		    document.createTextNode(dptron.noodleManager.textDump())
		);
		p.style.whiteSpace = "pre-wrap";
		f.insertBefore(p, f.querySelector("svg"));
		break;
	    case "O":
		dptron.overlayLoop();
		break;
	    case "S":
		dptron.exportSession();
		break;
	    case "Z":
		dptron.floaterManager.compressZSpace();
		break;
	    }
	    dptron.killPalette();
	}
	break;
    case "ARRANGE":
	dptron.activePalette.palName = "ARRANGE";
	dptron.activePalette.textContent = '[L]ineUp';
	dptron.activePalette.command = function(keypress){
	    if(dptron.activePalette.isPending()){return};
	    // Do the command...
	    switch (String.fromCharCode(keypress.code)){
	    case "L": // Line-up
		dptron.floaterManager.lineUp();
		break;
	    }
	    dptron.killPalette();
	}
	break;
    case "SELECTION":
	dptron.activePalette.palName = "SELECTION";
	Docuplextron.makePaletteItems(["|C|ontentEditable",
				       "|E|val",
				       "|R|e-Tag"], dptron.activePalette);
	dptron.activePalette.command = function(keypress){
	    if(dptron.activePalette.isPending()){return};
	    // ...
	    switch (String.fromCharCode(keypress.code)){
	    case "C": // ContentEditable
		dptron.DOMCursor.target.contentEditable = (dptron.DOMCursor.target.isContentEditable) ?
		    false : true;
		break;
	    case "E": // Evaluate selection as Javascript;
		var sel = window.getSelection();
		var result = "";
		try {
		    result = eval(sel.toString());
		} catch(e) {
		    result = e;
		}
		sel.collapseToEnd();
		var fnode = sel.focusNode;
		var foff = sel.focusOffset;
		var newText = fnode.textContent.substr(0,foff) +  result + fnode.textContent.substr(foff);
		var newTextNode = document.createTextNode(newText);
		fnode.parentNode.replaceChild(newTextNode, fnode);
		sel.setBaseAndExtent(newTextNode, foff, newTextNode, foff + result.toString().length);
		break;
	    case "R": // Re-tag
		dptron.DOMCursor.reTag(window.prompt("New HTML tag:"));
		break;
	    }
	    dptron.killPalette();
	}
	break;
    case "HTML":
	dptron.activePalette.palName = "HTML";
	Docuplextron.makePaletteItems(["|H|yperlink",
				       "|B|old",
				       "|C|ode",
				       "|I|talic",
				       "|K|bd",
				       "|U|nderline",
				       "|E|m",
				       "|D|fn",
				       "bi|G|",
				       "s|M|all",
				       "s|T|rong",
				       "H|1-6|",
				       "|P|",
				       "|S|ection",
				       "|A|rticle",
				       "block|Q|uote",
				       "| |pre",
				       "|O|l",
				       "|N|(ul)",
				       "|L|i"], dptron.activePalette);
	dptron.activePalette.keysToElements = {
	    "1":"h1",
	    "2":"h2",
	    "3":"h3",
	    "4":"h4",
	    "5":"h5",
	    "6":"h6",
	    " ":"pre",
	    "A":"article",
	    "B":"b",
	    "C":"code",
	    "D":"dfn",
	    "E":"em",
	    "G":"big",
	    "H":"hyperlink",
	    "I":"i",
	    "K":"kbd",
	    "L":"li",
	    "M":"small",
	    "N":"ul",
	    "O":"ol",
	    "P":"p",
	    "Q":"blockquote",
	    "S":"section",
	    "T":"strong",
	    "U":"u"
	    }
	dptron.activePalette.command = function(keypress){
	    var key = String.fromCharCode(keypress.code);
	    if(dptron.activePalette.isPending()){return};
	    var tag = dptron.activePalette.keysToElements[key]; 
	    if(tag){
		if(tag == "hyperlink"){
		    oldLinkEdit();
		} else {
		    dptron.DOMCursor.select(
			Alphjs.surround(
			    tag,dptron.DOMCursor.target)
		    );
		}
	    }
	    dptron.killPalette();
	}
	break;

    }
    return dptron.activePalette;
}

/***/

dptron.subPalette = function(palette){
    var p = dptron.makePalette(palette);
    p.pending--;
}

/***/

dptron.killPalette = function(){
    if(dptron.activePalette){
	dptron.activePalette.remove();
	dptron.activePalette = null;
    }
}

/*





  INIT





 */

function commonInit(){

    /* Init routine common to top-level contexts and iframes */

    window.addEventListener('mousemove',mousemv);
    window.addEventListener('keydown',keydn);
    window.addEventListener('keyup',keyup);
    window.addEventListener('mousedown',mousedn);
    window.addEventListener('mouseup',mouseup);
    window.addEventListener('wheel',wheel, {"passive":false});
    window.addEventListener('click',click);
    window.addEventListener('contextmenu',contextmenu);
    
    alph.getXSources();
    dptron.postEDL();
    //dptron.pushAllLinks();

    /* I  GUESS this is where we should restore the pin list? */
    dptron.restorePins();

    overlayLoop();
}

/***/

function iframeInit(){

    /* Init stuff for IFRAME contexts */

    dptron.initMain();
    dptron.initBar("iframe");

    dptron.noodleManager = new Lamian();
    alph.lamian = dptron.noodleManager;
    dptron.DOMCursor.toggle();
    
    commonInit();
}

/***/

function init(){
    var metaTags = Array.from(document.getElementsByTagName("meta"));
    var autoplex = false;
    var expiry;
    for(var ix in metaTags){
	if(metaTags[ix].hasAttribute("docuplextron")){
	    autoplex = metaTags[ix].getAttribute("docuplextron");
	} else if(metaTags[ix].hasAttribute("dptronexpiry")){
	    expiry = parseInt(metaTags[ix].getAttribute("dptronexpiry"));
	}
    }

    if(parent == window &&
       typeof(chrome) != "undefined" &&
       typeof(chrome.extension) != "undefined" &&
       document.contentType == "text/html" &&
       Array.from(document.getElementsByTagName("X-TEXT")).length == 0){

	/* If we are running as a WebExtension, if this is the parent
	   context (not an IFRAME), and the document is an HTML
	   document, and it has no <x-text> elements, don't do
	   anything. */

	return;
	
    } else if(document.contentType == "text/plain"){

	/* If we're loading a plain text document, wrap the text in an <x-text>, 
	   and let the Alph tools load -- even if it's not an Alphic source, 
	   we can treat it as a transclusible source. */

	var plaintext = document.body.querySelector("pre").childNodes[0];

	var xtext = document.createElement("x-text");

	/* This *could* be a transclusion that we're looking at, so
	   dereference the URL into an AlphSpan if we need to. */
	
	var srcUrl = window.location.toString().split("?")[0].split("#")[0];

	if(window.location.toString().includes("fragment=")){
	    var aSpan = AlphSpan.fromURL(window.location.toString());
	} else {
	    alph.getDescription(srcUrl,true);
	    var aSpan = new AlphSpan(srcUrl,0,alph.sources[srcUrl]["alph:textLength"]);
	}
	
	xtext.setAttribute("src", aSpan.src);
	xtext.setAttribute("origin",aSpan.origin);
	xtext.setAttribute("extent",aSpan.extent);
	xtext.appendChild(plaintext);
	document.body.querySelector("pre").appendChild(xtext);
    }

    if(parent != window){

	if(typeof(chrome) == "undefined" ||
	   typeof(chrome.extension) == "undefined"){

	    /* If we are running as a page script in an iframe/embed, do nothing. */
	    
	    return;
	} else {
	    /* We are in an <embed> or <iframe>. Ask our parent to trigger an init. */

	    /* We used to do this in the IFRAME days: 

	       dptron.reportSizeToParent();
	       parent.postMessage({"op":"isAlphMyParent"},"*");

	       But now... should we even do anything? Probably not. return! 
	    */
	    return;
	}
    } else {
	
	dptron.initMain(autoplex);
	dptron.initFloaterDiv();
	dptron.floaterManager = new FloaterManager(dptron.floaterDiv);

	dptron.noodleManager = new Lamian();
	alph.lamian = dptron.noodleManager;
	alph.docuplextron = dptron;
	
	dptron.initOverlay();
	dptron.initBar(autoplex);

	dptron.initButtons();
	dptron.initScratchpad();

	// TESTING AREA ====

	// =================
	
	commonInit();

	if (autoplex != false){
	    dptron.takeover(autoplex,expiry);
	}
    }
}

function overlayLoop(){
    
    // Don't do any overlay stuff if we're a subordinate window. 
    if(parent != window) return;

    dptron.movement++;
    
    requestAnimationFrame(overlayLoop);

    dptron.loopCounter++;
    if(dptron.loopCounter % 600 == 500){
	var asToggle = document.getElementById("dptronSessionAutosaveToggle");
	if(dptron.dptronMode != false &&
	   dptron.autoSaveSafe &&
	   asToggle &&
	   asToggle.checked){
	    dptron.sessionSave();
	}
	
    }

    // We don't need 60 FPS.
    dptron.frameskip++;
    dptron.frameskip = dptron.frameskip % 2;
    if(dptron.frameskip != 1){
	return;
    }    
    
    dptron.redraw();
    if(dptron.currentContext){
	if(dptron.currentContext.tagName == "IFRAME"){
	    dptron.floaterManager.updateTabs([Docuplextron.getParentItem(dptron.currentContext)]);
	}else{
	    dptron.floaterManager.updateTabs([dptron.currentContext]);
	}
    } else {
	dptron.floaterManager.updateTabs([dptron.currentContext]);
    }
}

init();
