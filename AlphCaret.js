function AlphCaret(svgLayer){

    /* Please pass me an SVG element to draw myself into. */
    
    this.svgLayer = svgLayer || document.createElementNS("http://www.w3.org/2000/svg","svg");

    this.anchorBug = document.createElementNS("http://www.w3.org/2000/svg","polyline");
    this.focusBug = document.createElementNS("http://www.w3.org/2000/svg","polyline");
    this.anchorGroup = document.createElementNS("http://www.w3.org/2000/svg","g");
    this.focusGroup = document.createElementNS("http://www.w3.org/2000/svg","g");

    this.anchorGroup.appendChild(this.anchorBug);
    this.anchorGroup.setAttribute("stroke","red");
    this.anchorGroup.setAttribute("stroke-width","1.5");
    
    this.focusGroup.appendChild(this.focusBug);
    this.focusGroup.setAttribute("stroke","red");
    this.focusGroup.setAttribute("stroke-width","1.5");    
    
    this.anchorBug.setAttribute("points","0,0 -5,-5 5,-5 0,0");
    this.focusBug.setAttribute("points","0,0 5,5 -5,5 0,0");
    this.anchorBug.id = "alphAnchorBug";
    this.focusBug.id = "alphFocusBug";

    this.changeLayer();    
}

AlphCaret.prototype.changeLayer = function(svg){

    if (svg) this.svgLayer = svg;

    this.anchorGroup.remove();
    this.focusGroup.remove();

    this.svgLayer.appendChild(this.anchorGroup);
    this.svgLayer.appendChild(this.focusGroup);
}

AlphCaret.prototype.update = function(){

    var anchorCoords = "0,0";
    var focusCoords = "0,0";

    var s = window.getSelection();

    try {

	/* Sometimes getSelection() gives us an unusable object. So,
	   if either of these throw an error, just move the caret to
	   0,0 */
	
	var anchorRange = s.getRangeAt(0);
	var focusRange = s.getRangeAt(s.rangeCount-1);
	
    } catch(e){
	
	this.anchorGroup.setAttribute("transform",
				      "translate(" + anchorCoords + ")");
	this.focusGroup.setAttribute("transform",
				     "translate(" + focusCoords + ")");
	return;
    }

    if(s.isCollapsed){
	
	/* If the selection is collapsed, and the character to the left of
	   the caret is a newline, the system caret appears in the correct
	   position, but the boundingClientRect of the selection range
	   is still showing where the character *before* the newline is.

	   So, create a Range that selects the character to the right
	   of the caret, and use the left edge of its bounding box. This
	   seems to do the trick. */

	var r2 = document.createRange();
	r2.setStart(anchorRange.startContainer, anchorRange.startOffset);
	r2.setEnd(anchorRange.startContainer,
		  Math.min(anchorRange.startOffset + 1, anchorRange.startContainer.length));
	var r2Rect = r2.getBoundingClientRect();
	if(r2Rect){
	    anchorCoords = r2Rect.left + "," + r2Rect.top;
	    focusCoords = r2Rect.left + "," + r2Rect.bottom;
	}
    } else {
	var anchorRect = anchorRange.getClientRects()[0];
	if(anchorRect){
	    anchorCoords = anchorRect.x + "," + anchorRect.y;
	}
	
	var focusRect = focusRange.getClientRects()[
	    focusRange.getClientRects().length - 1];
	if(focusRect){
	    focusCoords = focusRect.right + "," + focusRect.bottom;
	}
    }

    
    this.anchorGroup.setAttribute("transform",
				 "translate(" + anchorCoords + ")");
    this.focusGroup.setAttribute("transform",
				 "translate(" + focusCoords + ")");
}
