/* Floaters
   A 2.5D Windowing framework for Alph.js and whatever else. 
*/

function FloaterManager(layer){
    this.layer = layer || document.body;
    this.inhand = null;
    this.topBoundary = 0;
    this.bottomBoundary = window.innerHeight;
    this.leftBoundary = 0;
    this.rightBoundary = window.innerWidth;
    this.lastOpenedAt = {
	"x": 100,
	"y": 100
    };
    
    this.getMembers = function(){
	return Array.from(this.layer.querySelectorAll("x-floater"));
    }
    this.getMouseThieves = function(){
	return Array.from(this.layer.querySelectorAll("iframe"));
    }
}

FloaterManager.prototype.autoPlace = function(floater){
    this.lastOpenedAt.x = (this.lastOpenedAt.x > (window.innerWidth * 0.66) ) ?
	parseInt(window.innerWidth * 0.25) : this.lastOpenedAt.x + 20;
    this.lastOpenedAt.y = (this.lastOpenedAt.y > (window.innerHeight * 0.5) ) ?
   	parseInt(window.innerHeight * 0.1) : this.lastOpenedAt.y + 20;
    floater.style.top = this.lastOpenedAt.y + "px";
    floater.style.left = this.lastOpenedAt.x + "px";
}

FloaterManager.prototype.destroy = function(floater){
    floater.tab.remove();
    floater.remove(); // Remove from DOM.
    floater = null; // Hopefully let it get garbage collected?
}

FloaterManager.prototype.destroyAll = function(){
    var floaters = this.getMembers();
    for(var ix in floaters){
	floaters[ix].destroy();
    }
}

FloaterManager.prototype.splode = function(floater){
    var r = floater.getBoundingClientRect();

    floater.tab.style.display = "none";
    floater.style.transition = "all 0.5s";
    floater.style.opacity = "0";
    floater.style.left = (r.left - ((r.width / 2) * 1.5)) + "px";
    floater.style.top = (r.top - ((r.height / 2 ) * 1.5)) + "px";
    floater.style.transform = "scale(1.5)";
    setTimeout(function(){
	this.destroy();
    }.bind(floater),500);
}

FloaterManager.prototype.scurry = function(floater){
    floater.tab.style.display = "none";
    floater.style.transition = "all 0.5s";
    floater.style.left = (window.innerWidth * 1.5) + "px";
    return setTimeout(function(){
	this.destroy();
    }.bind(floater),500);
}

FloaterManager.prototype.animatedMove = function(floater,toX,toY){
    floater.style.transition = "all 0.5s";
    floater.style.left = toX;
    floater.style.top = toY;
    setTimeout(function(){
	this.style.transition = "";
    }.bind(floater),500);
}

FloaterManager.prototype.zoomTo = function(floater,atX,atY){
    /* Scale the workspace relative to the natural size of the passed floater */
    var oldScale = parseFloat(floater.getAttribute("data-scale"));
    var fRect = floater.getBoundingClientRect();
    var atX = atX || ( fRect.left + (fRect.width / 2) );
    var atY = atY || ( fRect.top + (fRect.height / 2) );

    /* The scale() function responds to mouse input and takes delta values, 
       not a ratio to 1, so we need to convert our scale amount to deltas.  */
    
    var relScale = ((1 / oldScale) - 1) * 100;

    /* Let's animate the zoom. Turn on transitions temporarily for all floaters. */

    var members = this.getMembers();
    for (var ix in members){
	var floater = members[ix];
	floater.style.transition = "all 0.5s";
	setTimeout(function(){
	    this.style.transition = "";
	}.bind(floater),500);
    }
    
    this.zoomWorkspace(relScale,relScale,atX,atY);
}

FloaterManager.prototype.zTop = function(){
    
    /* Return the z-index of the topmost floater */
    
    var members = this.getMembers();
    var zTop = 0;
    for(var ix in members){
	var myZ = parseInt(members[ix].getAttribute("data-z")) ||
	    parseInt(members[ix].style.zIndex);
	/* Maintenance, here. This shouldn't be happening, so something up
	   the line is broken. */
	if(!members[ix].hasAttribute("data-z") ||
	   !parseInt(members[ix].getAttribute("data-z"))){
	    members[ix].setAttribute("data-z",myZ);
	}
	zTop = Math.max(myZ, zTop);
    }
    return zTop;
}

FloaterManager.prototype.isTop = function(floater){
    
    /* Return whether or not this floater is topmost. We have to check not only z-index,
       but also document order. */
    
    var zTop = this.zTop();
    var floaterZ = parseFloat(floater.getAttribute("data-z"));
    if( floaterZ < zTop ){
	return false;
    } else {
	var members = this.getMembers();
	for(var ix = members.indexOf(floater) + 1; ix < members.length; ix++){
	    var memberZ = parseFloat(members[ix].getAttribute("data-z"));
	    if(memberZ = floaterZ) return false;
	}
	return true;
    }
}

FloaterManager.prototype.toTop = function(floater){

    var floaterZ = floater.getAttribute("data-z");
    var topZ = this.zTop();
    if(floaterZ < topZ){
	floater.setAttribute("data-z",topZ + 1);
    } else if(!this.isTop(floater)){
	floater.setAttribute("data-z",topZ + 1);
    } else {
	// Is already top!
    }
    floater.style.zIndex = floater.getAttribute("data-z");
}

FloaterManager.prototype.pickUp = function(floater){
    if(floater == "all"){
	this.inhand = "all";
    } else if(floater.tagName &&
       floater.tagName == "X-FLOATER"){
	this.inhand = floater;
	var thieves = this.getMouseThieves();
	for(var ix in thieves){
	    thieves[ix].style.pointerEvents = "none";
	}
	//console.log("Picked-up ",this.inhand);
	floater.focus();
    } else {
	this.inhand = null;
    }
}

FloaterManager.prototype.drop = function(){
    this.inhand = null;
    var thieves = this.getMouseThieves();
    for(var ix in thieves){
	thieves[ix].style.pointerEvents = "all";
    }
    this.compressZSpace();
}

FloaterManager.prototype.panWorkspace = function(movementX,movementY){
    var floaters = this.getMembers();
    for (var fx = 0; fx < floaters.length; fx++){
	var floater = floaters[fx];
	
	if(floater.sticky) continue;

	/*
	var parallax = (floater.style.zIndex) ?
	    parseFloat(floater.style.zIndex) / 100 : 1;
	*/

	var parallax = 1;
	
	floater.style.left = parseFloat(floater.style.left) + (parallax * movementX) + "px";
	floater.style.top = parseFloat(floater.style.top) + (parallax * movementY) + "px";
	// Wrap-around. Could be a problem when dealing with very
	// large (or very zoomed-in) items.
	//floater.style.left = (( (parseFloat(floater.style.left) + window.innerWidth) % (window.innerWidth * 3))) - window.innerWidth + "px";
	//floater.style.top = (( (parseFloat(floater.style.top) + window.innerHeight) % (window.innerHeight * 3))) - window.innerHeight + "px";
    }
}

FloaterManager.prototype.smoothPanWorkspace = function(movementX,movementY){
    var floaters = this.getMembers();
    for (var fx = 0; fx < floaters.length; fx++){
	var floater = floaters[fx];
	
	if(floater.sticky) continue;

	var parallax = 1;

	this.animatedMove(floater,
			  parseFloat(floater.style.left) + (parallax * movementX) + "px",
			  parseFloat(floater.style.top) + (parallax * movementY) + "px");

    }
}

FloaterManager.prototype.zoomWorkspace = function(deltaX,deltaY,clientX,clientY){
    var floaters = this.getMembers();
    for (var ix in floaters){
	if(!floaters[ix].sticky){
	    floaters[ix].scale(deltaX,deltaY,clientX,clientY);
	}
    }
}

FloaterManager.prototype.compressZSpace = function(){

    /* This needs to be rewritten. */
    
    var floaters = this.getMembers();

    /* I SHOULD sort them first, and make sure their order is maintained. */
    
    var by = 100 / this.zTop();
    
    for (var ix in floaters){
	var oldZ = parseFloat(floaters[ix].style.zIndex);
	floaters[ix].style.zIndex = parseInt( oldZ * by );
	floaters[ix].setAttribute("data-z", floaters[ix].style.zIndex);
    }
}

FloaterManager.prototype.lineUp = function(atX,atY){
    
    /* Line-up all floaters in a row at 50% zoom */
    
    var floaters = this.getMembers();
    for(var ix in floaters){
	var f = floaters[ix];
	f.setAttribute("data-z",50);
	f.setAttribute("data-scale",0.5);
	f.style.transform = "scale(0.5)";
	f.style.zIndex = "50";
	f.style.top = (atY) ? atY + "px" : "0px";
	if(ix > 0){
	    f.style.left = (parseInt(floaters[ix-1].style.left) + floaters[ix-1].getBoundingClientRect().width) + "px";
	} else {
	    f.style.left = (atX) ? atX + "px" : "0px";
	}
    }
}

/***/

FloaterManager.prototype.createTab = function(floater){
    var tab = document.createElement("div");
    var bar = document.createElement("div");
    tab.className = "titleTab";
    tab.floater = floater;

    tab.bar = bar;
    tab.appendChild(tab.bar);
    bar.classList.add("tabBar");
    
    tab.button = document.createElement("a");
    tab.button.textContent = " ";
    tab.button.className = "titleTabBurger";
    tab.button.onmousedown = function(evt){
	// Somewhere, define a function that will do this...
	titleTabContextMenu(this,evt);
	evt.preventDefault();
	evt.stopPropagation();
    }.bind(floater);
    
    tab.text = document.createElement("span");
    tab.text.textContent = "[tab]";

    tab.appendChild(tab.button);
    tab.appendChild(tab.text);

    floater.parentElement.appendChild(tab);

    /* These are updated dynamically with calls to updateTabs() */
    tab.style.top = tab.style.left = "0px";
    tab.style.zIndex = "100";
    
    tab.onmousedown = function(evt){
	evt.preventDefault();
	evt.stopPropagation();
	titleTabMouseDown(this,evt);
    }.bind(floater);
    tab.onclick = function(evt){
	evt.preventDefault();
	evt.stopPropagation();
	titleTabMouseUp(this,evt);
    }.bind(floater);
    tab.ondblclick = function(){
	this.bringIntoView();
    }.bind(floater);
    return tab;
}

/***/

FloaterManager.prototype.updateTabs = function(showfor){
    var showfor = showfor || [];
    var floaters = this.getMembers();
    for(var ix in floaters){
	var floater = floaters[ix];
	var tab = floater.tab;
	var floaterLeft = parseFloat(floater.style.left) || 0;
	var floaterTop = parseFloat(floater.style.top) || 0;
	if(showfor.includes(floater) ||
	   showfor.includes(floater.id) ||
	   floater == this.inhand ||
	   floater.classList.contains("alphActiveItem") ||
	   floater.isOffScreen()){
	    tab.style.backgroundColor = floater.style.borderColor || window.getComputedStyle(floater).borderColor;
	    if(tab.style.backgroundColor.startsWith("var(--dptron-BG")){
		tab.style.color = tab.style.backgroundColor.replace("BG","FG");
	    }
	    tab.style.display = "block";
	} else {
	    tab.style.display = "none";
	}

	/* Move tabs to their floaters... */
	
	var tabRect = tab.getBoundingClientRect();

	tab.style.left = Math.min(
	    Math.max( floaterLeft, this.leftBoundary),
	    this.rightBoundary - tabRect.width
	) + "px";

	tab.style.top = Math.min(
	    Math.max( floaterTop - tabRect.height, this.topBoundary ),
	    this.bottomBoundary - tabRect.height
	) + "px";


	/* Here's the little progress-bar indicator that shows how much
	   of the floater is currently visible in the workspace */
	
	var floaterRect = floater.getBoundingClientRect();
	var wid = Math.min(
	    1,
	    (this.bottomBoundary - floaterRect.top) / floaterRect.height
	) * 100 ;
	if(parseFloat(wid) != wid){
	    console.log("Invalid width in updateTabs()",wid);
	} else {
	    tab.bar.style.width = (wid || 100) + "%";
	}
	

	/* Rounded corner styling... ugh. */

	/*
	tabRect = tab.getBoundingClientRect();
	if(tabRect.left == this.leftBoundary){
	    if(tabRect.top == this.topBoundary){
		// upper-left corner
		tab.style.borderRadius = "0px 0px 1em 0px";
	    } else if(tabRect.bottom == this.bottomBoundary){
		// lower-left corner
		tab.style.borderRadius = "0px 1em 0px 0px";		
	    } else {
		// left edge
		tab.style.borderRadius = "0px 1em 1em 0px";
	    }
	} else if(tabRect.right == this.rightBoundary ||
		  window.getComputedStyle(tab).right == "0px"){
	    if(tabRect.top == this.topBoundary){
		// upper-right corner
		tab.style.borderRadius = "0px 0px 0px 1em";
	    } else if (tabRect.bottom == this.bottomBoundary ||
		       window.getComputedStyle(tab).bottom == "0px"){
		// lower-right corner
		tab.style.borderRadius = "1em 0px 0px 0px";
	    } else {
		// right edge
		tab.style.borderRadius = "1em 0px 0px 1em";
	    }
	} else if(tabRect.top == this.topBoundary){
	    if( floater.isOffScreen() ){
		// top edge, floater is completely off-screen
		tab.style.borderRadius = "0px 0px 1em 1em";
	    } else {
		// top edge, floater is still in view
		tab.style.borderRadius = "1em";
	    }
	} else {
	    // bottom edge or somewhere in the middle
	    tab.style.borderRadius = "1em 1em 0px 0px";
	}
	*/  

	tab.style.zIndex = floater.style.zIndex;
    }
}

/***/
FloaterManager.prototype.create = function(className,id,atX,atY){
    
    /* Create a new Floater, add it to the FloaterMgr's DOM layer with
       its left,top coordinates set to atX, atY, and return a
       reference to the new Floater. */

    var svgns = "http://www.w3.org/2000/svg";
    
    var floater = document.createElement("x-floater");
    floater.manager = this;
    floater.setAttribute("data-sticky",false);
    floater.sticky = false;
    floater.id = id || ("context_" + Date.now());
    floater.setAttribute("class","alph-floater");
    if(className) floater.classList.add(className);
    
    floater.setAttribute("data-z",100);
    floater.style.zIndex = 100;

    floater.setAttribute("data-scale",1);
    floater.style.transform = "scale(1)";
    floater.style.transformOrigin = "left top 0px";

    floater.style.whiteSpace = "normal";

    if(atX && atY){
	floater.style.left = atX + "px";
	floater.style.top = atY + "px";
    } else {
	this.autoPlace(floater);
    }

    floater.overlay = document.createElementNS(svgns,"svg");
    floater.overlay.setAttribute("class","alph-floater-overlay");
    
    floater.appendChild(floater.overlay);
    this.layer.appendChild(floater);

    floater.tab = this.createTab(floater);
    floater.tab.text.textContent = floater.id.substr(0,100);

    floater.destroy = function(){
	this.manager.destroy(this);
    }.bind(floater);

    floater.splode = function(){
	this.manager.splode(this);
    }.bind(floater);

    floater.scurry = function(){
	this.manager.scurry(this);
    }.bind(floater);

    floater.isOffScreen = function(){
	var rect = this.getBoundingClientRect();
	if(rect.bottom < 0 ||
	   rect.top > window.innerHeight ||
	   rect.right < 0 ||
	   rect.left > window.innerWidth){
	    return true;
	} else {
	    return false;
	}
    }.bind(floater);
    
    floater.stickyToggle = function(){
	this.sticky = (this.sticky) ? false : true;
	this.setAttribute("data-sticky",this.sticky);
    }.bind(floater);
    
    floater.resetZoom = function(){
	this.style.zIndex = "100";
	this.setAttribute("data-z",100);
	this.style.transform = "scale(1)";
	this.setAttribute("data-scale",1);
    }.bind(floater);
    
    floater.move = function(x,y){
	if(!this.style.left) this.style.left = "0px";
	if(!this.style.top) this.style.top = "0px";
	this.style.left = (parseFloat(this.style.left) + x) + "px";
	this.style.top = (parseFloat(this.style.top) + y) + "px";
    }.bind(floater);

    
    floater.zMove = function(deltaX,deltaY,clientX,clientY,withScale){
	var oldZ = parseFloat(this.getAttribute("data-z")) || 100;
	var newZ = oldZ + deltaY;
	if((newZ < oldZ && newZ < 1) ||
	   (newZ > oldZ && this.manager.isTop(this))){
	    newZ = oldZ;
	}
	
	this.setAttribute("data-z", newZ);
	this.style.zIndex = Math.max(0,parseInt(this.getAttribute("data-z")));
	
	if(withScale){
	    this.scale(deltaX,deltaY,clientX,clientY);
	}	
    }.bind(floater);

    floater.scale = function(deltaX,deltaY,clientX,clientY){
	var oldScale = parseFloat(this.getAttribute("data-scale")) || 1;
	var scaleFactor = 1 + (deltaY / 100); //oldScale / (oldZ / 100);
	var newScale = oldScale * scaleFactor;
	//if(newScale < 0.05 || newScale > 3) newScale = oldScale;
	this.setAttribute("data-scale", newScale);
	
	var relScale = oldScale - newScale;
	var rect = this.getBoundingClientRect();

	var oldRelTop = parseFloat(this.style.top) - clientY;
	var oldRelLeft = parseFloat(this.style.left) - clientX;
	
	this.style.top = ((oldRelTop * scaleFactor) + clientY) + "px";
	this.style.left = ((oldRelLeft * scaleFactor) + clientX) + "px";

	this.style.transform = "scale(" + newScale + ")";
	this.style.transformOrigin = "top left";

    }
    
    floater.changeWidth = function(amount){
	// Sets a fixed-width if the floater is currently on "auto"
	var oldWidth = parseInt(
	    this.style.width || window.getComputedStyle(this).width
	);
	this.style.width = (oldWidth + amount) + "px";
	this.style.maxWidth = "none";
    }.bind(floater);

    floater.toTop = function(){
	this.manager.toTop(this);
    }.bind(floater);
    
    floater.bringIntoView = function(atX,atY){

	/* Restore a floater to its natural size and bring it into
	   the middle of the workspace view; remember its position
	   before being restored so that we can return it back to
	   it's prior position if called again. */

	// Nice, smooth, css animation for the zooming...
	this.style.transition = "all 0.5s";
	setTimeout(function(){
	    this.style.transition = "";
	}.bind(this),1000);

	
	if(this.hasAttribute("data-restore")){
	    
	    /* Ignore stored settings if the floater is currently 
	       off-screen. In such a case, the user probably DOES
	       want the floater brought into view. */

	    var rect = this.getBoundingClientRect();
	    if(rect.right < 0 ||
	       rect.left > window.innerWidth ||
	       rect.bottom < 0 ||
	       rect.top > window.innerHeight){

		// Fall-through to the end of the function
		
	    } else {
		var restore = JSON.parse(this.getAttribute("data-restore"));
		this.setAttribute("data-z",restore.z);
		this.style.zIndex = restore.z;
		this.setAttribute("data-scale",restore.scale);
		this.style.transform = "scale(" + restore.scale + ")";
		this.style.left = restore.x;
		this.style.top = restore.y;
		this.removeAttribute("data-restore");
		return;
	    }
	} 
	var restore = {};
	restore.z = this.getAttribute("data-z");
	restore.scale = this.getAttribute("data-scale");
	restore.x = this.style.left;
	restore.y = this.style.top;
	this.setAttribute("data-restore",JSON.stringify(restore));
	/*
	if(!atX){
	    var atX = ((window.innerWidth / 2) -
		       (parseInt(window.getComputedStyle(this).width) / 2 )) + "px";
	    var atY = (window.innerHeight / 6) + "px";
	}
	
	if(atY){ this.style.top = atY; }
	if(atX){ this.style.left = atX; }
	*/
	
	this.resetZoom();	
	this.toTop();

	if(atX && atY){
	    this.style.top = atY;
	    this.style.left = atX;
	} else {
	    this.manager.autoPlace(this);
	}
	
    }.bind(floater);

    floater.toTop();
    return floater;
}
