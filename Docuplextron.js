/*

  ALPH.JS  -  DOCUPLEXTRON
  The Alph Project <http://alph.io/>
  ©2016-2023 Adam C. Moore (LÆMEUR) <adam@laemeur.com>

  This WebExtension is a prototype multi-document interface for
  xanalogical Web media as implemented by the Alph specifications. It
  includes support for the Xanadu Project's <http://xanadu.com>
  current xanadoc and xanalink formats.

  This is free software and is released under the terms of the current
  GNU General Public License <https://www.gnu.org/copyleft/gpl.html>

  Source code and revision history are available on GitLab:
  <https://gitlab.com/alph-project/alph.js>

*/

function Docuplextron(){
    this.lang = "en";
    this.lastX = 0;
    this.lastY = 0;
    this.movement = 0;
    this.keys = [];
    this.altKey = false;
    this.actionTarget = null;  // Commands
    this.activePalette = null; // Command palette
    this.activeItems = [];     // Floaters
    this.activeElement = null; // DOM elements
    this.activeGraphs = []; // Graph IDs (Strings)
    this.pins = []; // Urls and graph IDs (Strings)
    this.mouseBypass = false;
    this.TRANSPOINTER_MODES = 3;
    this.transpointerMode = 0;
    this.contexts = [ window ]; // Not using this now, but I need to be.
    this.edlStore = [];
    this.transpointerSpans = {};
    this.linkStore = new LinkStore();
    this.linkpointerSpans = [];
    this.currentSpan = null;
    this.currentContext = window;
    this.dptronMode = false;
    this.commitTarget = "";
    this.graphTarget = "";
    this.hoveredLink = null; // HACK
    this.frameskip = 0;
    this.killring = [];
    this.loopCounter = 0;
    this.simplePointers = [];
    this.simpleNexi = {};
    this.DOMCursor = new DOMCursor();
    this.autoSaveSafe = false;
    this.credStore = {};
    this.updateOverlays = false;
    this.selectionRectangle = false;
    this.alphCaret = new AlphCaret();
    this.linkerTarget = null;
}

Docuplextron.urls = {
    "topbar" : 'templates/topbar',
    "halp" : 'templates/HALP',
    "loadFromClipboard" : 'icons/svg/alph-icon-load_from_clipboard.svg',
    ">>" : 'icons/svg/fast-forward-double-right-arrows.svg',
    "interrobang" : 'icons/svg/Interrobang.svg',
    "cog" : 'icons/svg/cog.svg',
    "sources" : 'icons/svg/xsources.svg',
    "slideout" : 'icons/svg/slideout.svg',
    "scratchpad" : 'icons/svg/edit-document.svg',
    'alph' : 'icons/svg/alph.svg',
    'linkmodeAll' : 'icons/svg/linkmode-all.svg',
    'linkmodeContext' : 'icons/svg/linkmode-context.svg',
    'linkmodeSpan' : 'icons/svg/linkmode-span.svg',
    'linkmodeNone' : 'icons/svg/linkmode-none.svg'
}

/* These are a few IANA-registered link relations. */

Docuplextron.relations = [
    "tag",
    "about",
    "alternate",
    "answered-by",
    "author",
    "current",
    "commentary",
    "describes",
    "describedBy",
    "note",
    "predecessor-version",
    "related",
    "successor-version",
    "via"
];

/*
if(typeof(chrome) != "undefined" &&
   typeof(chrome.extension) != "undefined"){
    var urlKeys = Object.keys(Docuplextron.urls);
    for(var ix in urlKeys){
	Docuplextron.urls[urlKeys[ix]] = chrome.extension.getURL(Docuplextron.urls[urlKeys[ix]]);
    }
}
*/
if(typeof(browser) != "undefined" &&
   typeof(browser.runtime) != "undefined"){
    var urlKeys = Object.keys(Docuplextron.urls);
    for(var ix in urlKeys){
	Docuplextron.urls[urlKeys[ix]] = browser.runtime.getURL(Docuplextron.urls[urlKeys[ix]]);
    }
}



/* 





   INITIALIZATION METHODS





*/


Docuplextron.prototype.initMain = function(){

    /* Create the container DIV for the whole magilla. */
    
    this.mainDiv = document.createElement("div");
    this.mainDiv.id = "alph-main";
    document.body.appendChild(this.mainDiv);

    this.pukeBox = document.createElement("div");
    this.pukeBox.id = "pukeBox";
    this.mainDiv.appendChild(this.pukeBox);
}

/***/

Docuplextron.prototype.initFloaterDiv = function(){

    /* Create the DIV in which all floaters (windows) shall live. */
    
    this.floaterDiv = document.createElement("div");
    this.floaterDiv.id = "alph-floaters";
    this.mainDiv.appendChild(this.floaterDiv);
}

/***/

Docuplextron.prototype.initBar = function(type){

    /* Create and initialize  the top bar and the side flaps.. */

    this.bar = document.createElement("div");
    this.bar.id = "dptron-flap"; // <-- confusing name. 
    document.body.appendChild(this.bar);
    
    /* The bar is built by copying over all of the elements from the
       'topbar' HTML template. */
    
    var assetLoader = new XMLHttpRequest();
    assetLoader.open('GET',Docuplextron.urls['topbar'],false);
    assetLoader.send();

    var parser = new DOMParser();
    var tempBar = parser.parseFromString(assetLoader.responseText,"text/html");
    tempBar.querySelector("#alph-posttarget-graphic").src = Docuplextron.urls[">>"];
    tempBar.querySelector("#sideFlapTabToggle").src = Docuplextron.urls["slideout"];
 
    for (var ix = 0; ix < tempBar.body.children.length; ix++){
	this.bar.appendChild(tempBar.body.children.item(ix).cloneNode(true));
    }

    // Define functions for some buttons, forms, etc.

    var loadForm = this.bar.querySelector("#dptron-goBarDiv");
    loadForm.onsubmit = function(e){
	var loadbox = this.bar.querySelector("#alph-loadbox");
	this.loadExternalFromBar(loadbox.value,"alph-floater");
	return false;
    }.bind(this);
    
    var loadButton = this.bar.querySelector("#alph-loadFromClipboard");
    loadButton.onclick = function(e){
	var box = this.bar.querySelector("#alph-loadbox");
	// The "Paste" execCommand only works on a <textarea> in FF. So,
	// create a temporary textarea and paste into it.
	var vbox = document.createElement("textarea");
	vbox.contentEditable = true;
	box.parentElement.appendChild(vbox);
	vbox.focus();
	document.execCommand("Paste");
	var loadAs = "";
	if(e.ctrlKey) loadAs = "ld";
	this.loadExternal(vbox.value, loadAs);
	box.value = vbox.value;
	vbox.remove();
    }.bind(this)

    var domCursorToggle = this.bar.querySelector("#alph-editoverlaytoggle");
    domCursorToggle.onchange = function(){
	this.DOMCursor.toggle();
    }.bind(this);
    
    var sideFlapTabToggle = this.bar.querySelector("#sideFlapTabToggle");
    sideFlapTabToggle.onclick = function(){
	this.toggleFlap();
    }.bind(this);
    
    var helpPaneToggle = this.bar.querySelector("#helpPaneToggle");
    helpPaneToggle.onclick = function(){
	this.showFlapTab("help");
    }.bind(this);
    
    var linksPaneToggle = this.bar.querySelector("#linksPaneToggle");
    linksPaneToggle.onclick = function(){
	this.showFlapTab("links");
    }.bind(this);
    
    var settingsPaneToggle = this.bar.querySelector("#settingsPaneToggle");
    settingsPaneToggle.onclick = function(){
	this.showFlapTab("settings");
    }.bind(this);
    
    var sourcesPaneToggle = this.bar.querySelector("#sourcesPaneToggle");
    sourcesPaneToggle.onclick = function(){
	this.showFlapTab("sources");
    }.bind(this);
    
    var sessionPaneToggle = this.bar.querySelector("#sessionPaneToggle");
    sessionPaneToggle.onclick = function(){
	this.showFlapTab("session");
    }.bind(this);

    /* And some functions for the SESSION tab button/links... */
    var sessionSaveBtn = this.bar.querySelector("#sessionExport");
    sessionSaveBtn.onclick = function(){
	    
    }.bind(this);
	
    var sessionImportBtn = this.bar.querySelector("#sessionImport");
    sessionImportBtn.onclick = function(evt){
	evt.stopPropagation();
	evt.preventDefault();
	var i = Docuplextron.newDialog("input","Load session from file.");
	i.floater.bringIntoView();
	i.input.type = "file";
	i.okbutton.onclick = function(dialog){
	    alert(dialog.input.value);
	}.bind(this,i);
	i.input.click();
    }.bind(this);
    
    var fragImportBtn = this.bar.querySelector("#textFragImport");
    fragImportBtn.onclick = function(evt){
	evt.stopPropagation();
	evt.preventDefault();
	var i = Docuplextron.newDialog("input","Import text from file.");
	i.floater.bringIntoView();
	i.input.type = "file";
	i.okbutton.onclick = function(dialog){
	    alert(dialog.input.value);
	}.bind(this,i);
	i.input.click();	
    }.bind(this);
	
    var fragExportTextBtn = this.bar.querySelector("#saveFragsAsText");
    fragExportTextBtn.onclick = function(){
	    
    }.bind(this);
    var fragExportJSONBtn = this.bar.querySelector("#saveFragsAsJSON");
    fragExportJSONBtn.onclick = function(){
	    
    }.bind(this);

    /* Fill sessions list... */
    this.updateSessionList();    
    
    var sessionNameBtn = this.bar.querySelector("#dptronSessionNameBtn");
    var sessionCreateBtn = this.bar.querySelector("#dptronSessionCreateBtn");
    var sessionNameInput = this.bar.querySelector("#dptronSessionNameBox");
    sessionNameBtn.onclick = function(input,evt){
	evt.preventDefault();
	evt.stopPropagation();
	var cleanInput = input.value;
	if(!cleanInput) return;
	/* There is a host of things I should be doing
	   here... there must be a library or a badass regexp for
	   this. */
	cleanInput = cleanInput.replace(" ","_");
	window.location = window.location.toString().split("#")[0] +
	    "#" + cleanInput;
	this.sessionSave();
	this.updateSessionList();
    }.bind(this,sessionNameInput);
    
    sessionCreateBtn.onclick = function(input,evt){
	evt.preventDefault();
	evt.stopPropagation();
	var cleanInput = input.value;
	if(!cleanInput) return;
	/* There is a host of things I should be doing
	   here... there must be a library or a badass regexp for
	   this. */
	cleanInput = cleanInput.replace(" ","_");
	window.location = window.location.toString().split("#")[0] +
	    "#" + cleanInput;
	this.sessionRestore();
	this.sessionSave();
	this.updateSessionList();
    }.bind(this,sessionNameInput);

    
    assetLoader.open('GET',Docuplextron.urls["halp"],false);
    assetLoader.send();
    var helpDoc = parser.parseFromString(assetLoader.responseText,"text/html");
    var flap = this.bar.querySelector("#helpPane");
    flap.style.overflow = "auto";
    flap.style.width = "100%";
    flap.style.height = "100%";
    for (var ix = 0; ix < helpDoc.body.children.length; ix++){
	flap.appendChild(helpDoc.body.children.item(ix).cloneNode(true));
    }

    /* Aaaand.... create the linker tool here?! */
    this.linker = new LinkerTool(this);
    
    return this.bar;
}

/***/

Docuplextron.prototype.initBarGDS = function(graph){

    /* Update/set the active graph selector in the top-bar. */

    if (graph) this.graphTarget = graph;
    
    var newgds = this.graphDocSelector(graph);
    document.getElementById("alph-graphtarget").replaceWith(newgds);
    newgds.id = "alph-graphtarget";

}

/***/

Docuplextron.prototype.initOverlay = function(){
    this.overlay = document.createElementNS(svgns,"svg");
    this.overlay.id = "alphOverlay";
    this.overlay.setAttribute("width", window.innerWidth);
    this.overlay.setAttribute("height", window.innerHeight);
    // ↑ This is updated at regular intervals by redraw();
    document.body.appendChild(this.overlay);

    /* Arrowhead pointer used for link pointers. */
    var pointerMarker = document.createElementNS(svgns, "marker");
    pointerMarker.setAttribute("id","dptron-linkPointer");
    pointerMarker.setAttribute("markerWidth", 6);
    pointerMarker.setAttribute("markerHeight", 4);
    pointerMarker.setAttribute("refX", 6);
    pointerMarker.setAttribute("refY", 2);
    pointerMarker.setAttribute("orient", "auto");
    pointerMarker.innerHTML = "<polygon points='0 0, 6 2, 0 4'/>";
    this.overlay.appendChild(pointerMarker);

    /* Arrowhead pointer for the linker tool. */
    var linkerMarker = pointerMarker.cloneNode(true);
    linkerMarker.setAttribute("id","dptron-linkerPointer");
    this.overlay.appendChild(linkerMarker);

    /* A node nexus for linker tool */
    var linkerANode = document.createElementNS(svgns, "symbol");
    linkerANode.setAttribute("id","linkerANode");
    linkerANode.innerHTML = "<circle r='6' cx='6' cy='18'></circle>" +
	"<text x='3' y='9' class='linkerNodeText'>A</text>";
    this.overlay.appendChild(linkerANode);

    /* The other node nexus for linker tool */
    var linkerBNode = document.createElementNS(svgns, "symbol");
    linkerBNode.setAttribute("id","linkerBNode");
    linkerBNode.innerHTML = "<circle r='6' cx='6' cy='18'></circle>" +
	"<text x='3' y='9' class='linkerNodeText'>B</text>";
    this.overlay.appendChild(linkerBNode);
    
    this.alphCaret.changeLayer(this.overlay);
}


/***/

Docuplextron.prototype.initScratchpad = function(){
    this.scratchpad = document.createElement("textarea");
    this.scratchpad.id = "alph-bufferinput";
    this.scratchpad.setAttribute("cols",40);
    this.scratchpad.setAttribute("rows",12);
    this.scratchpad.show = function(){
	var floater;
	if(Docuplextron.getParentItem(this.scratchpad)){
	    floater = Docuplextron.getParentItem(this.scratchpad);
	    this.floaterManager.pickUp(floater);
	} else {
	    floater = this.floaterManager.create(null,"Scratchpad",this.lastX,this.lastY);
	    floater.appendChild(this.scratchpad);
	    floater.style.width = "auto";
	    // Make scratchpad "sticky" by default.
	    floater.stickyToggle();
	}
	floater.style.top = this.bar.getBoundingClientRect().bottom + "px";
	floater.style.left = "0px";
	floater.style.display = "block";

    }.bind(this);
}

/***/

Docuplextron.prototype.initButtons = function(){
    var scratchpadBtn = document.createElement("img");
    scratchpadBtn.id = "dptron-scratchpad-button";
    scratchpadBtn.src = Docuplextron.urls["scratchpad"];
    scratchpadBtn.setAttribute("class","dptron-flap-icon");
    scratchpadBtn.onclick = function(){
	this.scratchpad.show();
    }.bind(this);
    
    var alphBtn = document.createElement("img");
    alphBtn.id = "dptron-alph-button";
    alphBtn.src = Docuplextron.urls["alph"];
    alphBtn.setAttribute("class","dptron-flap-icon");
    alphBtn.onclick = function(){
	this.takeover();
    }.bind(this);

    var linkViewBtn = document.createElement("img");
    linkViewBtn.id = "dptron-linkview-button";
    linkViewBtn.src = Docuplextron.urls["linkmodeAll"];
    linkViewBtn.setAttribute("class","dptron-flap-icon");
    linkViewBtn.onclick = function(){
	this.changeTpointerMode();
    }.bind(this);

    document.getElementById("alph-loadFromClipboard").src = Docuplextron.urls["loadFromClipboard"];

    this.bar.insertBefore(linkViewBtn,this.bar.firstElementChild);
    this.bar.insertBefore(scratchpadBtn,this.bar.firstElementChild);
    this.bar.insertBefore(alphBtn,this.bar.firstElementChild);
}

/* PINS */

Docuplextron.prototype.restorePins = function(){
    
    /* Grab the pin list from localStorage... */

    var savedPins = localStorage.getItem("dptronPins");

    if(savedPins){
	savedPins = JSON.parse(savedPins);
	this.pins = savedPins["pins"];
    } else {
	this.pins = [];
    }

    /* Load all pins as background resources... */

    for (var pinix in this.pins){
	this.loadExternal(this.pins[pinix], "pin");
    }
    
}

Docuplextron.prototype.setPin = function(pinId){
    /* Make sure we're not duplicating an entry... */

    var pinIx = this.pins.indexOf(pinId);
    if(pinIx < 0){
	/* I can't think of any cases where we'd want to be pinning
	   something that was not an HTTP(S) resource or a graph from
	   the LinkStore ...? */
	if(pinId.startsWith("http:") || pinId.startsWith("https:")){
	    this.pins.push(pinId);
	} else {
	    this.pukeText("NOT pinning resource: " + pinId);
	}
    }
    
    /* Save changes */
    localStorage.setItem("dptronPins",
			 JSON.stringify({"pins": this.pins}));
}

Docuplextron.prototype.removePin = function(pinId){
    if (!pinId) return;

    if(this.pins.indexOf(pinId) >= 0){
	this.pins.splice(this.pins.indexOf(pinId),1);
    }
    
    /* Save changes */
    localStorage.setItem("dptronPins",
			 JSON.stringify({"pins" : this.pins}));
}

/* * */

Docuplextron.prototype.changeTpointerMode = function(dir){
    if(dir == "up"){
	this.transpointerMode = (this.transpointerMode < this.TRANSPOINTER_MODES) ?
	    this.transpointerMode + 1 : 0;
    } else {
	this.transpointerMode = (this.transpointerMode > 0 ) ?
	    this.transpointerMode - 1 :
	    this.TRANSPOINTER_MODES;
    }

    if(document.getElementById("dptron-linkview-button")){
	var viewModeBtn = document.getElementById("dptron-linkview-button");
	viewModeBtn.src = [Docuplextron.urls["linkmodeAll"],
			   Docuplextron.urls["linkmodeContext"],
			   Docuplextron.urls["linkmodeSpan"],
			   Docuplextron.urls["linkmodeNone"] ][this.transpointerMode];
    }

    this.postEDL(); 
    this.rebuildTranspointers();

}

Docuplextron.prototype.updateSessionList = function(){
    var sessionList = document.createElement("ul");
    sessionList.id = "dptronSessionList";

    this.bar.querySelector("#dptronSessionNameBox").value = window.location.hash.substr(1);

    // Get the localStorage keys for session saves, then sort 'em
    var lsKeys = [];
    var unsortedKeys = Object.keys(localStorage);
    for(var ix in unsortedKeys){
	if(unsortedKeys[ix].startsWith("dptronSession")){
	    lsKeys.push(unsortedKeys[ix]);
	}
    }
    lsKeys.sort();
    
    for(var ix in lsKeys){
	var key = lsKeys[ix];
	var li = document.createElement("li");
	li.setAttribute("data-sessionKey",key);
	var a = document.createElement("a");
	a.href = key.substr("dptronSession-".length);
	a.textContent = a.href.substr(window.location.origin.length);
	a.onclick = function(key,evt){
	    evt.preventDefault();
	    evt.stopPropagation();
		
	    var url = key.substr("dptronSession-".length);
	    var path = url.split("#")[0];
		
	    if(url == window.location.toString()){
		
	    } else if(path == window.location.toString().split("#")[0]){
		/* Hash change; changing window.location won't actually reload
		   the document, so do this... */
		this.sessionSave();
		
		window.location = url;		    
		this.sessionRestore(key);

		this.bar.querySelector("#dptronSessionNameBox").value = window.location.hash.substr(1);
		this.updateSessionList();
	    } else {
		this.sessionSave();
		window.location = url;
	    }
	}.bind(this,key);
	a.oncontextmenu = function(li,evt){
	    /* ... */
	    evt.preventDefault();
	    evt.stopPropagation();
	    this.activeMenu = this.contextMenuOn(li,["erase session"]);	    
	}.bind(this,li);
	li.appendChild(a);
	    
	if(key == "dptronSession-" + window.location.toString()){
	    a.classList.add("currentSession");
	    li.appendChild(document.createTextNode("(current) "));
	}

	var b = document.createElement("a");
	b.textContent = " X";
	b.style.fontSize = "smaller";
	b.style.color = "red";
	b.style.cursor = "pointer";
	b.onclick = function(li,evt){
	    evt.preventDefault();
	    evt.stopPropagation();
	    this.command("erase session",li);	
	}.bind(this,li)
	li.appendChild(b);
	    
	sessionList.appendChild(li);
	/* Make links, yadda yadda... */
    }
    this.bar.querySelector("#dptronSessionList").parentElement.replaceChild(sessionList,this.bar.querySelector("#dptronSessionList"));
}

Docuplextron.prototype.updateLinksFlap = function(){
    
    /* Refresh the contents of the links flap, applying filters. 

       Pretty ugly at the moment. A nicer way to do this would be to
       have each graph document foldable, where what we first see is a
       list of just the graph ids, with their name property if they
       have one, then we can drill-down to see the expanded view of
       the graph document's attributes, and then drill-down further to
       look at its nodes and its graph. */
    
    var flapDiv = document.getElementById('linksPane');
    
    var linksFilter = document.getElementById('linksFilter').value.split(":");
    // This should be in some kind of  init section!
    document.getElementById('linksFilter').oninput = function(){
	this.updateLinksFlap();
    }.bind(this)

    var linkListDiv = document.createElement("div");
    linkListDiv.id = "dptronLinkListDiv";

    var linkKeys = Object.keys(this.linkStore.graphs);
    for( var ix in linkKeys){
	var graphID = linkKeys[ix];
	var doMe = false;

	/* --- FILTER GOES HERE --- */

	/* Like the filter in the sources pane, I think what we'll do here
	   is have three fields that do simple matching of A node, B node,
	   and relation fields.
	
	   The filter is moot right now. Just let everything through
	   until we design how we're actually going to do this. */

	doMe = true;

	
	if(!doMe){
	    continue;
	}
	
	var listItem = document.createElement("div"); // The container for the graph listing

	var listItemHead = document.createElement("div");
	listItemHead.classList.add("dptronGraphListHead");
	
	var detailsToggle = document.createElement("span");
	var activeToggle = document.createElement("input");
	var itemName = document.createElement("span");
	var editBtn = document.createElement("button");
	var deleteBtn = document.createElement("button");
	var exportBtn = document.createElement("button");
	listItemHead.append( detailsToggle, activeToggle, itemName, editBtn, deleteBtn, exportBtn);
	
	var detailsBox = document.createElement("div"); // The area containing details about the graph
	detailsBox.style.display = "none";
	
	detailsToggle.textContent = "▶";
	detailsToggle.classList.add("toggler");
	detailsToggle.onclick = function(b){
	    b.style.display = (b.style.display == "none") ? "block" : "none";
	    this.textContent = (this.textContent == "▶") ? "▼" : "▶";
	}.bind(detailsToggle,detailsBox);
	
	activeToggle.setAttribute("type","checkbox");
	activeToggle.title = "Show this graph in the workspace";
	activeToggle.checked = (this.activeGraphs.indexOf(graphID) < 0 ) ? false : true ;
	activeToggle.onchange = function(e, graphId){
	    if(e.checked){
		/* Add to active graphs list -- only if it's not there already. */
		if(this.activeGraphs.indexOf(graphId) < 0){
		    this.activeGraphs.push(graphId);
		    this.paintLinkTerminals();
		}
	    } else {
		/* Remove from active graphs list */
		if(this.activeGraphs.indexOf(graphId) >= 0){
		    this.activeGraphs.splice(this.activeGraphs.indexOf(graphId),1);
		    this.paintLinkTerminals();		    
		}
	    }
	}.bind(this,activeToggle,graphID);
	    
	editBtn.textContent = "edit";
	editBtn.onclick = function(link){
	    // linkEdit(link);
	}.bind(this,this.linkStore.graphs[graphID]);
	
	deleteBtn.textContent = "delete";
	deleteBtn.onclick = function(id){
	    // delete this.linkStore.graphs[id];
	    this.linkStore.deleteGraph(id);
	    this.updateLinksFlap();
	}.bind(this, graphID);
	
	exportBtn.textContent = "export";
	exportBtn.onclick = function(graph){
	    /* ... */
	    var urlGuess = graph["@id"];
	    if (urlGuess.startsWith("~")) urlGuess = urlGuess.substr(1);
	    
	    var exportUrl = window.prompt("URL to export graph to:", urlGuess);
	    if(exportUrl){
	    
		this.postMedia("PUT",this.linkStore.exportGraph(graph["@id"], exportUrl), true, exportUrl);
		console.log("Graph export: ", this.linkStore.exportGraph(graph["@id"]));
		//this.linkPostModal(link);
		this.updateLinksFlap();
	    }
	}.bind(this,this.linkStore.graphs[graphID]);
	
	listItem.classList.add("linksFlapItem");
	listItem.appendChild(listItemHead);
	listItem.appendChild(detailsBox);

	/* Make a pared-down version of the graph document that we
	   can run through Alphjs.objectToHTML() */
	
	var paredGraphDoc = JSON.parse(JSON.stringify(this.linkStore.graphs[graphID]));
	delete paredGraphDoc.graph;
	delete paredGraphDoc.nodes;
	var graphDocList = Alphjs.objectToHTML(paredGraphDoc);
	detailsBox.appendChild(graphDocList);

	itemName.classList.add("graphName");
	itemName.textContent = (paredGraphDoc["name"]) ?
	    paredGraphDoc["name"] : 
	    "[" + paredGraphDoc["@id"] + "]";
	
	/* I want the node list to be foldable, so let's make a little container...*/

	var nodeFolder = document.createElement("li");
	var nodeToggler = document.createElement("div");
	nodeToggler.textContent = "▶ nodes";  // Redo this style stuff with classes, Adam
	nodeToggler.style.cursor = "pointer";
	nodeToggler.onclick = function(){
	    if(this.nextSibling.style.display == "block"){
		this.nextSibling.style.display = "none";
		this.textContent = "▶ nodes";
	    } else {
		this.nextSibling.style.display = "block";
		this.textContent = "▼ nodes";
	    }
	};
	var nodeFolderHolder = document.createElement("div");
	nodeFolderHolder.style.display = "none";
	
	nodeFolder.appendChild(nodeToggler);
	nodeFolder.appendChild(nodeFolderHolder);
	nodeFolderHolder.appendChild( Alphjs.objectToHTML(this.linkStore.graphs[graphID].nodes) );

	graphDocList.appendChild(nodeFolder);
	
	/* And... I should probably do a folder for these, too ... */
	
	var graphFolder = document.createElement("li");
	var graphToggler = document.createElement("div");
	graphToggler.textContent = "▶ graph:";  // Redo this style stuff with classes, Adam
	graphToggler.style.cursor = "pointer";
	graphToggler.onclick = function(){
	    if(this.nextSibling.style.display == "block"){
		this.nextSibling.style.display = "none";
		this.textContent = "▶ graph:";
	    } else {
		this.nextSibling.style.display = "block";
		this.textContent = "▼ graph:";
	    }
	};
	var graphFolderHolder = document.createElement("div");
	graphFolderHolder.style.display = "none";

	graphFolder.appendChild(graphToggler);
	graphFolder.appendChild(graphFolderHolder);
	
	/* Then make a simple list (table?) of this document's graph. */
	
	var linkTable = document.createElement("table");
	linkTable.classList.add("graphTable");
	
	var graphLinks = Array.from(this.linkStore.graphs[graphID].graph.children);
	for(var linkIx in graphLinks){
	    var l = graphLinks[linkIx];
	    var newRow = document.createElement("tr");

	    /* Maybe an onclick() here for the table row, to open-up a link
	       editor? ...*/

	    var linkTd = document.createElement("td");
	    
	    var aNodeTd = document.createElement("div");
	    aNodeTd.classList.add("graphTable-anode");
	    aNodeTd.title = "Edit this node";
	    
	    var bNodeTd = document.createElement("div");
	    bNodeTd.classList.add("graphTable-bnode");
	    bNodeTd.title = "Edit this node";
	    
	    var relTd = document.createElement("div");
	    relTd.classList.add("graphTable-rel");

	    linkTd.appendChild(aNodeTd);
	    linkTd.appendChild(relTd);
	    linkTd.appendChild(bNodeTd);

	    var xTd = document.createElement("td");
	    var xBtn = document.createElement("button");
	    xBtn.textContent = "X";
	    xBtn.onclick = function(link, graphID, row){
		/* We could just call remove() on the link, but using 
		   LinkStore.prototype.removeFrom() also cleans-up any
		   nodes that are left unlinked in the graph. */
		
		if ( this.linkStore.removeFrom(
		    {
			"nodeA" : {"@id" : link.getAttribute("anode") },
			"nodeB" : {"@id" : link.getAttribute("bnode") },
			"rel" : link.getAttribute("rel")
		    }, graphID)) {
		    row.remove();
		    this.paintLinkTerminals();
		}
	    }.bind(this, graphLinks[linkIx], graphID, newRow);
	    xTd.appendChild(xBtn);
	    
	    /* And probably launch a node editor when these are clicked. */
	    aNodeTd.textContent = "🅐 " + l.getAttribute("anode");
	    aNodeTd.onclick = function(nid, gid){
		var f = this.floaterManager.create("nonContextual");
		f.classList.add("linkEditor");
		f.classList.add("dptronUI");		
		this.nodeMunglor(nid, gid, f);
	    }.bind(this, l.getAttribute("anode"), graphID);
	    
	    bNodeTd.textContent = "🅑" + l.getAttribute("bnode");
	    bNodeTd.onclick = function(nid , gid){
		var f = this.floaterManager.create("nonContextual");
		f.classList.add("linkEditor");
		f.classList.add("dptronUI");				
		this.nodeMunglor(nid, gid, f);
	    }.bind(this, l.getAttribute("bnode"), graphID);

	    relTd.textContent = "┋ " + l.getAttribute("rel");
	    relTd.onclick = function(graphID, anodeID, bnodeID, rel ){
		this.linkMunglor(this.linkStore.graphs[graphID].nodes[anodeID],
				 this.linkStore.graphs[graphID].nodes[bnodeID],
				 rel, graphID);
	    }.bind(this, graphID, l.getAttribute("anode"), l.getAttribute("bnode"), l.getAttribute("rel") );
		   
	    //newRow.appendChild(aNodeTd);
	    //newRow.appendChild(relTd);
	    //newRow.appendChild(bNodeTd);
	    newRow.appendChild(linkTd);
	    newRow.appendChild(xTd);
	    
	    linkTable.appendChild(newRow);
	}
	graphFolderHolder.appendChild(linkTable);
	graphDocList.appendChild(graphFolder);
	
	/* 
	   Commenting this out for the moment, but it'll be reimplemented.

	   It should work like this: when a node is moused-over, flashRects()
	   is called and a SimplePointer is drawn if the content is visible
	   in the workspace. 
	
	listItem.onmouseover = function(listItem,link,evt){
	    // BODY
	    for(var ix in link.body.items){
		var node = link.body.items[ix];
		var rects = this.getNodeRects(node);
		for(var iy in rects){
		    flashRects(this.overlay,rects[iy].rects);
		}
	    }
	    // TARGET
	    for (var ix in link.target.items){
		var node = link.target.items[ix];
		var rects = this.getNodeRects(node);
		for(var iy in rects){
		    flashRects(this.overlay,rects[iy].rects);
		}
	    }
	}.bind(this,listItem,this.linkStore[linkKeys[ix]]); //!577
	
	listItem.onmouseout = function(listItem){
	    removeSimplePointersTo(this.simplePointers,listItem);
	}.bind(this,listItem);

	*/

	linkListDiv.appendChild(listItem);
    }

    /* Stick a pad in here at the bottom;*/
    var pad = document.createElement("div");
    pad.style.height = "50px";
    linkListDiv.appendChild(pad);

    
    /* Edge color palette...*/

    var edgePalette = document.createElement("div");
    edgePalette.id = "dptronLinkInfoDiv";

    var edgePaletteHeader = document.createElement("div");
    edgePaletteHeader.classList.add("heading");
    edgePaletteHeader.textContent = "Edge Colours";
    edgePalette.appendChild(edgePaletteHeader);

    var edgeIntensity = document.createElement("input");
    edgeIntensity.setAttribute("id","dptron-edgeIntensity");
    edgeIntensity.setAttribute("type","range");
    edgeIntensity.setAttribute("min", "0");
    edgeIntensity.setAttribute("max", "1");
    edgeIntensity.setAttribute("step", "0.005");
    edgeIntensity.value = "0.7";
    edgeIntensity.style.float = "right";
    edgeIntensity.oninput = this.setEdgeColours;
    edgePaletteHeader.appendChild(edgeIntensity);
    
    var edgePaletteList = document.createElement("div");
    edgePaletteList.id = "edgePaletteList";

    /* We'll need the user stylesheet in a moment, here... */
    var sheet = document.getElementById("dptronUserStyle").textContent;
    
    var relList = this.linkStore.getRels();
    for (var ix in relList){
	var relItem = document.createElement("div");
	relItem.classList.add("edgeColourListItem");

	var relItemToggle = document.createElement("input");
	relItemToggle.setAttribute("type","checkbox");
	relItemToggle.setAttribute("data-rel",relList[ix]);
	relItemToggle.checked = true;
	relItemToggle.onchange = this.setEdgeColours;

	var relItemName = document.createElement("span");
	relItemName.classList.add("edgeName");
	relItemName.textContent = relList[ix];

	var relItemColourPicker = document.createElement("input");
	relItemColourPicker.setAttribute("type","color");
	relItemColourPicker.onchange = this.setEdgeColours;

	/* Is there a rule for this style in the user stylesheet? */
	if(sheet.includes("#alphOverlay *[rel='" + relList[ix] + "']")){
	    /* Get the stroke colour... */
	    var re = new RegExp('\\#alphOverlay \\*\\[rel=\\\'' + relList[ix] + '\\\'\\].*stroke:(.*)\\}');
	    var stroke = sheet.match(re);
	    if(stroke){
		stroke = stroke[1].trim();
		relItemColourPicker.setAttribute("value", (stroke.startsWith("rgb")) ?
						 rgbToHex(stroke) : stroke.split(";")[0] );
	    } else {
		relItemColourPicker.setAttribute("value", "");
	    }
	}
	relItem.append(relItemToggle, relItemColourPicker, relItemName);
	edgePaletteList.appendChild(relItem);
    }

    edgePalette.appendChild(edgePaletteList);
    
    /* Stick a pad in here at the bottom;*/
    var pad2 = document.createElement("div");
    pad2.style.height = "50px";
    edgePalette.appendChild(pad);

    document.getElementById("dptronLinkListDiv").parentElement.replaceChild(linkListDiv,document.getElementById("dptronLinkListDiv"));
    document.getElementById("dptronLinkInfoDiv").parentElement.replaceChild(edgePalette,document.getElementById("dptronLinkInfoDiv"));

}

Docuplextron.prototype.setEdgeColours = function(){
    var relList = Array.from(
	document.getElementById("edgePaletteList").querySelectorAll(".edgeColourListItem")
    );
    var separator = "/* --- EDGE COLOURS --- */\n\n";
    var cssText = "" + separator;
    
    for(var ix in relList){
	var rel = relList[ix].querySelector(".edgeName").textContent;
	var color = relList[ix].querySelector("[type='color']").value;
	var toggle = relList[ix].querySelector("[type='checkbox']").checked;
	toggle = (toggle) ? "inline" : "none" ;
	cssText += "#alphOverlay *[rel='" + rel + "'] { ";
	if (color != "#000000") cssText += "stroke: " + color + "; ";
	cssText += "display: " + toggle + "; }\n";
    }

    cssText += "#alphOverlay { --dptron-edgeIntensity: " + document.getElementById("dptron-edgeIntensity").value + "; }\n";
    
    var userCSS = document.getElementById("dptronUserStyle").textContent.split(separator)[0];
    document.getElementById("dptronUserStyle").textContent = userCSS + cssText;
    document.getElementById("dptronUserCSS").value = userCSS + cssText;
}

Docuplextron.prototype.updateSourcesFlap = function(){
    var flapDiv = document.getElementById('sourcesPane');

    /* I'd like this filter to work better. I'd like the text color to
       change to red when the filter text has been modified (oninput),
       and when it's been applied (onchange) I'd like the text to go
       back to white and for a line underneath it to appear saying
       "sources matching '(filter text)':" ... or something along
       those lines. */
    
    var filterBox = flapDiv.querySelector('#sourcesFilter');
    filterBox.onchange = function(){
	this.updateSourcesFlap();
    }.bind(this);
    
    var sourceListDiv = document.createElement("div");
    sourceListDiv.id = "dptronSourceListDiv";

    var sourceInfoDiv = document.createElement("div");
    sourceInfoDiv.id = "dptronSourceInfoDiv";
    sourceInfoDiv.appendChild(document.createElement("span")).textContent = "No source selected.";
    
    var sourceList = document.createElement("ul");
    sourceListDiv.appendChild(sourceList);

    document.getElementById("dptronSourceInfoDiv").parentElement.replaceChild(sourceInfoDiv,document.getElementById("dptronSourceInfoDiv"));
    document.getElementById("dptronSourceListDiv").parentElement.replaceChild(sourceListDiv,document.getElementById("dptronSourceListDiv"));
    
    /* Show network resources first */

    var listItem = document.createElement("li");
    listItem.textContent = "Network Sources";
    listItem.classList.add("sourcesHeader");
    sourceList.appendChild(listItem);
    
    var sourceKeys = Object.keys(alph.sources);
    for(var ix in sourceKeys){
	var sourceKey = sourceKeys[ix];
	listItem = document.createElement("li");
	listItem.setAttribute("data-sourceId",sourceKey);
	listItem.textContent = sourceKey;
	listItem.onclick = function(alph,key,div){
	    div.removeChild(div.firstElementChild);
	    alph.makeSourceInfo(key,div);
	}.bind(this,alph,sourceKey,sourceInfoDiv);
	listItem.ondblclick = function(li){
	    this.loadExternal(li.getAttribute("data-sourceid"));
	}.bind(this, listItem);
	listItem.oncontextmenu = function(li,evt){
	    evt.preventDefault();
	    evt.stopPropagation();
	    this.activeMenu = this.contextMenuOn(li);
	}.bind(this,listItem);
	sourceList.appendChild(listItem);

	/* is this a pinned resource? */

	if(this.pins.indexOf(sourceKey) >= 0){
	    listItem.classList.add("pinned");
	}

	/* If there's something in the filter box, we want to look at
	   the cached content of each source and provide matches; */
		
	if(filterBox.value){

	    if(Object.keys(alph.sources[sourceKey]).includes("mediaCache")){

		/* Issue: we're only caching text from Alph.py servers
		   when a request has been filled for the entire
		   document.  We should probably just send out a
		   request for the full document when we fire
		   getDescription(). */

		/* Let's do a list item for each match, then have that
		   list item open-up a transclusion fragment in a
		   floater when selected? */

		var mc = alph.sources[sourceKey].mediaCache;
		var filterRE = new RegExp(filterBox.value,'gi'); // global, ignore case
		let match;
		while((match = filterRE.exec(mc)) != null) {
		    listItem = document.createElement("li");
		    listItem.classList.add("sourcesMatch");
		    
		    /* RegExp.exec() gives us the origin/extent of the matching text -- handy! */
		    
		    var matchOrigin = match.index;
		    var matchExtent = filterRE.lastIndex;

		    /* We also want to get origin/extent of the PARAGRAPH containing the match... */

	            var fragOrigin = matchOrigin;
		    var fragExtent = matchExtent;
		    while(fragOrigin > 0){
			if (mc.charAt(fragOrigin) != "\n"){
			    fragOrigin--;
			} else {
			    if ((mc.charAt(fragOrigin - 1) == "\n") ||
				(mc.charAt(fragOrigin - 1) == "\r" && mc.charAt(fragOrigin - 2) == "\n")){
				break;
			    } else {
				fragOrigin--;
			    }
			}
		    }
		    while(fragExtent < mc.length){
			if (mc.charAt(fragExtent) != "\n"){
			    fragExtent++;
			} else {
			    if((mc.charAt(fragExtent + 1) == "\n") || (mc.charAt(fragExtent + 1) == "\r")){
				break;
			    } else {
				fragExtent++;
			    }
			}
		    }

		    Docuplextron.highlightMatching(mc.substr(Math.max(matchOrigin - 40, 0), filterBox.value.length + 100),
						   filterBox.value,
						   listItem);
		    listItem.onclick = function(source,origin,extent,match,container){
			container.textContent = "";
			var fragID = source.id + "#" + origin + "-" + extent;
			var fragIDLine = document.createElement("p");
			fragIDLine.textContent = fragID;
			container.appendChild(fragIDLine);
			var frag = document.createElement("div");
			Docuplextron.highlightMatching(source.mediaCache.substring(origin,extent),match,frag);
			container.appendChild(frag);
		    }.bind(this,alph.sources[sourceKey],fragOrigin,fragExtent,filterBox.value,sourceInfoDiv);
		    listItem.ondblclick = function(url){
			this.loadExternal(url);
		    }.bind(this, alph.sources[sourceKey].id + "#" + fragOrigin + "-" + fragExtent);
		    sourceList.appendChild(listItem);
		}
	    }
	}

    }

    /* Now show noodles */

    listItem = document.createElement("li");
    listItem.textContent = "Local Sources (noodles)";
    listItem.classList.add("sourcesHeader");
    sourceList.appendChild(listItem);

    var noodles = this.noodleManager.noodles;
    var noodleKeys = Array.from(Object.keys(noodles));
    noodleKeys.sort(function(a,b) { 
	if (parseInt(a,36) < parseInt(b,36))
	    return -1;
	else if (parseInt(a,36) > parseInt(b,36))
	    return 1;
	else 
	    return 0;
    });
    for(var ix in noodleKeys){
	var noodle = noodles[noodleKeys[ix]];
	// Filter!
	if(!noodle.text.toLowerCase().includes(filterBox.value.toLowerCase())){
	    continue;
	}
	var listItem = document.createElement("li");
	listItem.setAttribute("data-noodleid",noodle.key);
	// !!! Make this a function, highlightMatching() or something.
	var matchIx = noodle.text.toLowerCase().indexOf(filterBox.value.toLowerCase());
	var matchKey = document.createElement("small");
	//matchKey.textContent = noodle.key + ":";
	
	matchKey.textContent = (noodle.title) ? noodle.title : "[" + noodle.key + "]";
	matchKey.textContent += ": ";
	var matchHead = noodle.text.substring(Math.max(matchIx - 40, 0),matchIx);
	var matchBody = document.createElement("mark");
	matchBody.textContent = noodle.text.substr(matchIx,filterBox.value.length);
	var matchTail = noodle.text.substr(matchIx + filterBox.value.length, 100);
	listItem.appendChild(matchKey);
	listItem.appendChild(document.createTextNode(matchHead));
	listItem.appendChild(matchBody);
	listItem.appendChild(document.createTextNode(matchTail));
	
	//listItem.textContent = noodle.key + ":" + noodle.title + ":" + noodle.text.substr(matchIx,160);
	listItem.onclick = function(div,noodle,filter,evt){
	    if(evt.button == 0){
		div.textContent = "";
		var noodlePretty = document.createElement("div");
		var noodleKey = document.createElement("div");
		var noodleTitle = document.createElement("div");
		var noodleText = document.createElement("pre");

		var dato = new Date();
		dato.setTime(parseInt(noodle.key,36));
		noodleKey.textContent = noodle.key + " – " + dato.toLocaleString();
		noodleTitle.textContent = noodle.title || "[no title]";

		Docuplextron.highlightMatching(noodle.text,filter,noodleText);

		noodlePretty.appendChild(noodleKey);
		noodlePretty.appendChild(noodleTitle);
		noodlePretty.appendChild(noodleText);
		
		div.appendChild(noodlePretty);
		//div.appendChild(Alphjs.objectToHTML(noodle));
	    }
	}.bind(this,sourceInfoDiv,noodle,filterBox.value);

	listItem.onmouseover = function(listItem,noodle,evt){
	    var spans = Array.from(
		document.querySelectorAll('x-text[src="~' + noodle.key + '"]')
	    );
	    for(var ix in spans){
		var target = spans[ix];
		var targetFloater = Docuplextron.getParentItem(target);
		if(targetFloater){
		    if(targetFloater.isOffScreen()) target = targetFloater.tab;
		}
		this.simplePointers.push(new SimplePointer(listItem,target,this.overlay));
	    }
	}.bind(this,listItem,noodle);

	listItem.onmouseout = function(listItem){
	    removeSimplePointersTo(this.simplePointers,listItem);
	}.bind(this,listItem)
	
	listItem.oncontextmenu = function(li,noodle,evt){
	    evt.preventDefault();
	    evt.stopPropagation();
	    this.activeMenu = this.contextMenuOn(li);	    
	}.bind(this,listItem,noodle)

	listItem.ondblclick = function(li,evt){
	    evt.preventDefault();
	    evt.stopPropagation();
	    this.actionTarget = li;
	    this.command("open/show noodle");
	}.bind(this,listItem);
	
	sourceList.appendChild(listItem);
    }
}

/***/

Docuplextron.prototype.takeover = function(autoplex,expiry){
    
    /* This "takes-over" the browser from a single-document view and loads the
       docuplextron UI/space around that document. */

    if(this.dptronMode != false) return;
    
    this.dptronMode = autoplex || true;

    // Remember the document body's width
    var oldWidth = window.getComputedStyle(document.body).width;
    var oldMinWidth = parseInt(window.getComputedStyle(document.body).minWidth);

    // Create a temporary container and move the document body into it
    var tdiv = document.createElement("div");
    while(document.body.children.length > 0){
	tdiv.appendChild(document.body.children.item(0));
    }

    // Move the Docuplextron stuff back out
    document.body.appendChild(this.mainDiv);
    document.body.appendChild(this.overlay);
    document.body.appendChild(this.bar);
    document.body.appendChild(this.DOMCursor.div);
    document.body.appendChild(this.DOMCursor.label);

    // Style all the UI stuff so that it's visible and positioned
    // appropriately
    this.bar.style.top = "0px";
    this.bar.querySelector("#dptronSideFlap").style.top = this.bar.getBoundingClientRect().height + "px";
    this.floaterManager.topBoundary = this.bar.getBoundingClientRect().height;
    this.mainDiv.style.height = "100vh";
    this.mainDiv.style.backgroundColor = "transparent";
    document.body.style.margin = "auto";
    document.body.style.overflow = "hidden";
    document.body.style.maxWidth = "10000px";

    // Remove any stylesheets in the <head>?
    var styles = Array.from(document.head.querySelectorAll("style"));
    for (var ix in styles){
	styles[ix].remove();
    }

    styles = Array.from(
	document.head.querySelectorAll("link[rel='stylesheet']"));
    for (var ix in styles){
	styles[ix].remove();
    }
    
    /* Now, set the new boundsCheck() function on our DOMCursor */
    this.DOMCursor.boundsCheck = function(el){
	var parentItem = Docuplextron.getParentItem(el);
	if(!parentItem) return false;
	if(parentItem.classList.contains("nonContextual")) return false;
	if(el.tagName){
	    if(el.tagName == "X-FLOATER") return false;
	}
	return true;
    }

    this.loadExternal(window.location.href);

    this.rebuildEdlStore();
    
    //this.linkStore = new LinkStore();
    
    this.autoRestore();

}


/*




  SIMPLEPOINTER




*/

function OldSimplePointer(anchorA,anchorB,layer,classname){
    // SVG pointer between two elements.
    this.anchorA = anchorA;
    this.anchorB = anchorB;
    this.layer = layer || document.body;
    this.dead = false;
    this.line = document.createElementNS(svgns,"line");
    this.line.setAttribute("class",classname || "dptron-simplepointer");
    this.layer.appendChild(this.line);
    this.update = function(){
	var aRect;
	var bRect;
	try {
	    // Not a valid element?
	    aRect = anchorA.getClientRects()[0];
	    bRect = anchorB.getClientRects()[0];
	} catch(e){
	    //console.log(e);
	    this.dead = true;
	    this.line.remove();
	    return;
	}
	// Removed from the DOM?
	if(!document.body.contains(this.anchorA) ||
	   !document.body.contains(this.anchorB)){
	    this.dead = true;
	    this.line.remove();
	    return;
	}
	// Visible?
	if(window.getComputedStyle(this.anchorA).display != "none" &&
	   window.getComputedStyle(this.anchorB).display != "none"){
	    this.line.setAttribute("x1",aRect.left);
	    this.line.setAttribute("y1",aRect.top);
	    this.line.setAttribute("x2",bRect.left);
	    this.line.setAttribute("y2",bRect.top);
	}
    };
}

function SimplePointer(anchorA,anchorB,layer,classname, attrs){
    // SVG pointer between two elements.
    this.anchorA = anchorA;
    this.anchorB = anchorB;
    this.layer = layer || document.body;
    this.dead = false;
    //this.line = document.createElementNS(svgns,"polygon");
    this.line = document.createElementNS(svgns,"line");
    this.line.setAttribute("class",classname || "dptron-pointerBridge");
    if (attrs){
	for(var ix in attrs){
	    this.line.setAttribute(ix, attrs[ix]);
	}
    }
    this.layer.appendChild(this.line);
}

SimplePointer.prototype.update = function(){
    var aRect;
    var bRect;
    var ssA = 0;
    var ssB = 0;
    try {
	// Not a valid element?
	aRect = this.anchorA.getClientRects()[0];
	bRect = this.anchorB.getClientRects()[0];
    } catch(e){
	console.log("SimplePointer dies, because: ",e);
	this.dead = true;
	this.line.remove();
	return;
    }
    // Removed from the DOM?
    if(!this.anchorA.isConnected || !this.anchorB.isConnected){
	// console.log("SimplePointer dies, because: document body does not contain", this.anchorA, " or ", this.anchorB);
	this.dead = true;
	this.line.remove();
	return;
    }
    // Visible?
    if(window.getComputedStyle(this.anchorA).display != "none" &&
       window.getComputedStyle(this.anchorB).display != "none"){

	/* If the anchor element is a circle, we want a collapsed
	   bounding box so that our lines will point at its center. */
	
	if(this.anchorA.tagName.toLowerCase() == "circle"){
	    ssA = parseFloat(this.anchorA.getAttribute("r"));
	    aRect = {};
	    aRect.left = aRect.right = parseFloat(this.anchorA.getAttribute("cx"));
	    aRect.top = aRect.bottom = parseFloat(this.anchorA.getAttribute("cy"));
	}
	if(this.anchorB.tagName.toLowerCase() == "circle"){
	    ssB = parseFloat(this.anchorB.getAttribute("r"));	    
	    bRect = {};
	    bRect.left = bRect.right = parseFloat(this.anchorB.getAttribute("cx"));
	    bRect.top = bRect.bottom = parseFloat(this.anchorB.getAttribute("cy"));
	}

	/*
	  We want the pointer to go between boxes, not overlap them, so figure
	  out which link terminal is on the right and which is on the left.
	  Then, because we always want the line's x1 and y1 attributes pointing
	  at the A node, and x2, y2 pointing at the B node (this makes arrows
	  work), we have different orders of setting the attributes.
	*/
	
	if(aRect.left >= bRect.left){
	    // A node is on the right
	    try {
		this.line.setAttribute("x1", aRect.left - ssA);
		this.line.setAttribute("y1", (aRect.top + aRect.bottom) / 2);
		this.line.setAttribute("x2", bRect.right + ssB);
		this.line.setAttribute("y2", (bRect.top + bRect.bottom) / 2);
	    } catch (e){
		console.log("Couldn't set simplepointer coordinates: ", e);
	    }
	} else {
	    // B node is on the right
	    try {
		this.line.setAttribute("x1", aRect.right + ssA);
		this.line.setAttribute("y1", (aRect.top + aRect.bottom) / 2);
		this.line.setAttribute("x2", bRect.left - ssB);
		this.line.setAttribute("y2", (bRect.top + bRect.bottom) / 2);
	    } catch (e){
		console.log("Couldn't set simplepointer coordinates: ", e);
	    }
	}

	/*
	  This could/should be much more sophisticated. For example, make the 
	  line point at the containing floater's tab if the floater is 
	  off-screen, or point at the tops/bottoms terminals if their spatial
	  relationship is more vertical than horizontal, etc...
	*/
    }
}


function SimpleNexus(nodes,layer,docuplextron,link,isBody, type){
    
    /* I am a little bug that floats near link nodes to provide a visual
       indicator of which nodes are members of a body and which are members of a 
       target, and to provide bridge points between body and target.

       I'll probably do more later.

       My "nodes" are DOM elements. 
       My "layer" is an <SVG> element that I am drawn onto. */

    this.nodes = nodes || [];
    this.layer = layer || document.body;
    this.docuplextron = docuplextron; // or... what? A new one?
    this.link = link || {};
    this.type = type || "normal";
    if(isBody == null || isBody == undefined) {
	this.isBody = true;
    } else {
	this.isBody = isBody;
    }
    this.simplePointers = this.docuplextron.simplePointers;
    this.dot = document.createElementNS(svgns,"circle");
    this.dot.setAttribute("class","dptron-simpleNexus");
    this.dot.setAttribute("r",5);
    this.dot.nexus = this;
    layer.appendChild(this.dot);
    this.dead = false;
    this.xBias = 0;
    this.yBias = 0;
    this.mean = {"top":0, "bottom":0, "left":0, "right":0 };
    
    this.dot.onmouseover = function(){
	this.flashNodes();
	//this.coNexus.flashNodes();
    }.bind(this);
    this.dot.onmouseout = function(){
	this.deFlashNodes();
	//this.coNexus.deFlashNodes();
    }.bind(this);    
    this.dot.onclick = function(){
	this.docuplextron.activeMenu = this.docuplextron.linkContextMenu(this.link);
	//linkEdit(this.link);
    }.bind(this);
}

SimpleNexus.prototype.remove = function(){
    this.dot.remove();
    this.dead = true;
}

SimpleNexus.prototype.flashNodes = function(){
    var col = (this.isBody) ? "hot" : "cold" ;
    for(var ix in this.nodes){
	this.nodes[ix].classList.add(col);
    }
}

SimpleNexus.prototype.deFlashNodes = function(){
    var col = (this.isBody) ? "hot" : "cold" ;
    for(var ix in this.nodes){
	this.nodes[ix].classList.remove(col);
    }
}

SimpleNexus.prototype.adjustBias = function(x,y){
    this.xBias += (x || 0);
    this.yBias += (y || 0); 
    
}

SimpleNexus.prototype.update = function(){

    /* Update the nexus' position in the workspace, and make sure
       that its nodes are still active. If they're not, make the
       nexus self-destruct. */
    
    this.mean.left = 0;
    this.mean.right = 0;
    this.mean.top = 0;
    this.mean.bottom = 0;
    var zTop = 0;
    
    let multicontext = false;
    let lastnodefloater; // This will be a reference to a floater in a moment;
    
    if(this.nodes.length == 0){
	
	/* If I have no nodes, I have no reason for being. Goodbye! */
	
	return this.remove();
    }
    
    for(var ix in this.nodes){

	if(!this.nodes[ix].isConnected){

	    /* If the node is not present in the document body... ? */
	    
	    return this.remove();
	}
	try{
	    
	    var nodeRect = this.nodes[ix].getBoundingClientRect();

	} catch(e) {
	    
	    /* If I can't get a bounding client rect, something's wrong. Goodbye! */
	    
	    return this.remove();
	}

	/* We want to find out whether this nexus is going to be
	   pointing at link terminals in multiple different floaters.
	   (It affects how we'll position the nexus in the workspace.)
	   Record which floater this one's pointing at, and (if this
	   isn't the first time through the loop) compare it to the
	   last floater. If they don't match, set the multicontext
	   variable to true. */
	
	let nodefloater = Docuplextron.getParentItem(this.nodes[ix]);
	if(ix > 0){
	    if(lastnodefloater != nodefloater) multicontext = true;
	}
	lastnodefloater = nodefloater;
	
	/* Not sure I even need this since SVG doesn't care about z-index... */
	//zTop = Math.max(zTop,
	//		parseInt(Docuplextron.getParentItem(this.nodes[ix]).style.zIndex));

	this.mean.left += nodeRect.left;
	this.mean.right += nodeRect.right;
	this.mean.top += nodeRect.top;
	this.mean.bottom += nodeRect.bottom;
    }

    // We bias these values slightly for aesthetics and clarity.
    var bias = 5 * this.nodes.length;
    this.mean.left = (this.mean.left / this.nodes.length) - bias;
    this.mean.right = (this.mean.right / this.nodes.length) + bias;

    //if(this.nodes.length > 1){
    if(multicontext){	
	this.dot.setAttribute("cx",
			      (this.mean.left + this.mean.right) / 2 );
    } else {
	this.dot.setAttribute("cx",
			  (this.xBias > 0) ? this.mean.right : this.mean.left );
    }
    
    this.dot.setAttribute("cy",
			  ((this.mean.top + this.mean.bottom)/2) / this.nodes.length );
    
    //this.dot.style.zIndex = zTop + 1;
}


function removeSimplePointersTo(array,element){
    for(var ix in array){
	if(array[ix].anchorA == element || array[ix].anchorB == element){
	    array[ix].line.remove();
	    /* This is stupid. Clean-up the array, Adam. */
	    array[ix].dead = true;
	}
    }
}

function cleanUpSimplePointers(array){
    var newlen = 0;
    for(var ix in array){
	if(!array[ix].dead){
	    array[newlen] = array[ix];
	    newlen++;
	}
    }
    return array.slice(0,newlen);
}

function LinkerTool(dptron){

    /* Graphical linker tool.

       This operates with a two-pushpins-and-a-rubber-band interface.

       The "pins" -- the A node and B node of a link -- can be picked-up and moved 
       around the workspace. As they are placed on linkable fragments/resources,
       those link anchors are outlined. 

       There's a relation pull-down too, which appears just below the Active Graph
       selector on the TopBar.

       Once the user placed the anchors where they want them, they hit [Enter] to have
       the link stored to their chosen graph with the selected relation, and the
       linker is then hidden. If the user presses [Esc], the linker is hidden, and 
       no link is stored. */
    
    this.dptron = dptron;
    this.active = false;
    this.elementLinkingEnabled = false;
    
    /*
      These are the node terminals that the user can pick-up and move around:
    */
    
    this.nexusA = svgLinkerNexus("a");
    this.nexusA.which = "A";
    
    this.nexusB = svgLinkerNexus("b");
    this.nexusB.which = "B";
    
    /* The link relation selector box... */
    this.relSelectorBox = document.createElement("div");
    this.relSelectorBox.id = "dptronRelSelectorBox";
    this.relSelectorBox.style.left = document.getElementById("alph-graphtarget").getBoundingClientRect().left + "px";

    /* The label for the select element */
    var l = document.createElement("label");
    l.setAttribute("for", "dptronRelSelector");
    l.textContent = "Relation: ";
    l.title = "Select the relation between nodes.\n\n" +
	"If the relation is a verb, the link should read as:\n" +
	"A describes: B\n\n" +
	"If the relation is a noun, imagine the word 'has' before it.\n" +
	"Ex.: A [has] author: B.\n\n"+ 
	"If the relation is an adjective, ...ugh. They're confusing.\n" +
	"Think of the adjective as describing B in relation to A. For example, A current: B should be read as 'B is the current version of A'; or, A related: B is to be read as 'B is related to A'."; 

    /* The select element: */
    this.relSelector = document.createElement("select");
    this.relSelector.id = "dptronRelSelector";

    /* The node-swap button */
    this.swapNodesBtn = document.createElement("button");
    this.swapNodesBtn.textContent = "🅐⇄🅑";
    this.swapNodesBtn.title = "Swap nodes A and B";
    this.swapNodesBtn.onclick = function(){
	this.swapNodes();
    }.bind(this);
    
    /* The okay button */
    this.okay = document.createElement("button");
    this.okay.textContent = "🔗";
    this.okay.onclick = function(e){
	/* If the user has selected [prompt] in the relation
	   selector, prompt them! */
	var rel = this.linker.relSelector.value;
	if(rel == "[prompt]"){
	    rel = "";
	    while(rel == ""){ // Be an asshole and keep prompting them...
		rel = window.prompt("Name the link relation:");
	    }
	}
	this.linkMunglor((this.linker.nexusA.span.toLinkMunglorNode) ?
			 this.linker.nexusA.span.toLinkMunglorNode() : this.linker.nexusA.span,
			 (this.linker.nexusB.span.toLinkMunglorNode) ?
			 this.linker.nexusB.span.toLinkMunglorNode() : this.linker.nexusB.span,
			 rel,
			 document.getElementById("alph-graphtarget"),
			 (!e.shiftKey) ? true : false);
	this.linker.hide();
    }.bind(this.dptron);
    
    /* Append everything... */
    this.relSelectorBox.appendChild(l);
    this.relSelectorBox.appendChild(this.relSelector);
    this.relSelectorBox.appendChild(this.swapNodesBtn);
    this.relSelectorBox.appendChild(this.okay);
    
    //this.show();    
    
}

LinkerTool.prototype.show = function(spanA, spanB){
    this.active = true;
    
    /* reattach our parts to the DOM */
    this.dptron.overlay.appendChild(this.nexusA);
    this.dptron.overlay.appendChild(this.nexusB);
    this.dptron.bar.appendChild(this.relSelectorBox);
    this.refillSelectors();
    /* recreate the simplePointer */
    this.dptron.simplePointers.push(new SimplePointer(
	this.nexusA,
	this.nexusB,
	this.dptron.overlay,
	"dptron-linkerBridge",
	{"marker-end":"url('#dptron-linkerPointer')"}));

    /* If we've been passed some spans to link... */

    if(spanA && spanB){
	this.spanChange(spanA, this.nexusA);
	this.spanChange(spanB, this.nexusB);
	this.nexusA.held = this.nexusB.held = false;
	this.nexusA.style.pointerEvents = this.nexusB.style.pointerEvents = "all";
	/* update() will snap these to their spans... */
    } else if(spanA){
	this.spanChange(spanA, this.nexusA);
	this.nexusA.held = false;
	this.nexusA.style.pointerEvents = "all";
	
	this.nexusB.setAttribute("x", this.dptron.lastX - 6);
	this.nexusB.setAttribute("y", this.dptron.lastY - 6);
	this.nexusB.held = true;
	this.nexusB.style.pointerEvents = "none";
    } else if(spanB){
	this.spanChange(spanB, this.nexusB);
	this.nexusB.held = false;
	this.nexusB.style.pointerEvents = "all";
	
	this.nexusA.setAttribute("x", this.dptron.lastX - 6);
	this.nexusA.setAttribute("y", this.dptron.lastY - 6);
	this.nexusA.held = true;
	this.nexusA.style.pointerEvents = "none";
    } else {
	/* Otherwise, when we show the linker let's assume the user
	   wants to "drop anchor" where the pointer is, so we'll latch
	   nexus A to point at the currentSpan, and pick-up the B node. */

	this.nexusA.setAttribute("x", this.dptron.lastX - 6);
	this.nexusA.setAttribute("y", this.dptron.lastY - 15);
	this.nexusB.setAttribute("x", this.dptron.lastX - 6);
	this.nexusB.setAttribute("Y", this.dptron.lastY - 15);

	this.spanChange(null, this.nexusA);
	if (!window.getSelection().isCollapsed){
	    window.getSelection().collapse(
		window.getSelection().anchorNode, window.getSelection().anchorOffset );
	}
	this.nexusA.held = false;
	this.nexusA.style.pointerEvents = "all";
    
	this.nexusB.held = true;
	this.nexusB.style.pointerEvents = "none";
    }
}

LinkerTool.prototype.hide = function(){

    this.active = false;
    this.nexusA.held = false;
    this.nexusB.held = false;

    this.nexusA.span = new AlphSpan();
    this.nexusB.span = new AlphSpan();
    
    /* Remove our parts from the DOM */
    this.nexusA.remove();
    this.nexusB.remove();
    this.relSelectorBox.remove();
    /* The SimplePointer between the nexus will be cleaned-up by its update() function. */

    var oldTerms = Array.from(document.querySelectorAll(".dptron-linkerTerminalA, .dptron-linkerTerminalB"));
    for (var ix in oldTerms){
	oldTerms[ix].remove();
    }
}

LinkerTool.prototype.globalMouseup = function(){

    if(this.nexusA.held || this.nexusB.held){
	if (!window.getSelection().isCollapsed){
	    this.spanChange();
	    window.getSelection().collapse(
		window.getSelection().anchorNode, window.getSelection().anchorOffset );
	};
    }

    if(this.nexusA.held){
	this.nexusA.held = false;
	this.nexusA.style.pointerEvents = "all";
    }
    if(this.nexusB.held){
	this.nexusB.held = false;
	this.nexusB.style.pointerEvents = "all";
    }    
}

LinkerTool.prototype.spanChange = function(hotSpan,n){
    
    var hotNexus;

    if (n == this.nexusA || n == this.nexusB) {
	hotNexus = n;
    }
    else if (this.nexusA.held) {
	hotNexus = this.nexusA;
    } else if (this.nexusB.held) {
	hotNexus = this.nexusB;
    } else {
	return;
    }
    
    if (hotSpan){
	if (!this.elementLinkingEnabled &&
	    hotSpan.origin &&
	    hotSpan.origin.toString().startsWith("element(")){
	    var span = new AlphSpan(hotSpan.src);
	} else {
	    var span = hotSpan;
	}
    } else {
	if (!window.getSelection().isCollapsed){
	    /* There's an active selection. Use that. */
	    var span = alph.selection.toLinkMunglorNode();
	} else if (this.dptron.currentSpan) {
	    /* There's an active span. Use that. */
	    if (!this.elementLinkingEnabled &&
		this.dptron.currentSpan.origin &&
		this.dptron.currentSpan.origin.toString().startsWith("element(")){
		var span = new AlphSpan(this.dptron.currentSpan.src);
	    } else {
		var span = this.dptron.currentSpan;
	    }
	} else {
	    /* No selection, no active span -- we're on the 
	       background or some UI element. */
	    var span = null;
	}
    }

    /* Remove any existing terminals from the DOM, then clear the
       terminals array. */
    for (var ix in hotNexus.terminals){
	hotNexus.terminals[ix].remove();
    }
    hotNexus.terminals = [];

    
    if(!span){
	/* If span is null, well, set the nexus' span to null. */
	
	console.log("Clearing nexus " + hotNexus.which + "'s span and terminals.");
	hotNexus.span = span;
	hotNexus.context = null;
	
    } else {
	
	console.log("Setting nexus " + hotNexus.which + " to:", span, " and pinning to context ", this.dptron.currentContext);
	hotNexus.span = span;
	hotNexus.context = this.dptron.currentContext;
    
	/* We're going to be duplicating some code from 
	   paintLinkTerminals() here, I reckon... */
    
	
	/* Get DOMRects for matching content in the workspace.
	   span *might* be an EDL, so... */
	if(span["@type"]){
	    if(span["@type"] == "EDL"){
		var rects = [];
		for(var ir in span["@list"]){
		    rects = rects.concat(
			this.dptron.getNodeRects( new AlphSpan( span["@list"][ir]["src"],
								span["@list"][ir]["origin"],
								span["@list"][ir]["extent"])));
		}
	    } else {
		var rects = this.dptron.getNodeRects( new AlphSpan ( span["src"],
								     span["origin"],
								     span["extent"]) );
	    }
	} else {
	    var rects = this.dptron.getNodeRects(span);
	}

	if(!rects){
	    hotNexus.span = null;
	    hotNexus.terminals = [];
	    console.log("Couldn't get rects from ", span);
	    return;
	}
	var wholeMatch = false;
	for (var iy in rects){
	    if(rects[iy].clip == "none") wholeMatch = true;
	    var term = linkTerminal(
		rects[iy].rects,
		rects[iy].floater,
		rects[iy].clip );
	    term.classList.add( (hotNexus.which == "A") ? "dptron-linkerTerminalA" : "dptron-linkerTerminalB");
	    hotNexus.terminals.push(term);
	    rects[iy].floater.querySelector("svg").appendChild(term);
	}
    }
}

LinkerTool.prototype.swapNodes = function(){
    var tempNexus = {};

    /* Put everything down */
    this.nexusA.held = this.nexusB.held = false;

    tempNexus.x = this.nexusA.getAttribute("x");
    tempNexus.y = this.nexusA.getAttribute("y");
    tempNexus.span = this.nexusA.span;

    this.spanChange(this.nexusB.span, this.nexusA);
    this.nexusA.setAttribute("x", this.nexusB.getAttribute("x"));
    this.nexusA.setAttribute("y", this.nexusB.getAttribute("y"));

    this.spanChange(tempNexus.span, this.nexusB);
    this.nexusB.setAttribute("x", tempNexus.x);
    this.nexusB.setAttribute("y", tempNexus.y);
}

LinkerTool.prototype.refillSelectors = function(){
    
    /* Populate the relation selector box with relations
       from the linkstore... */

    var rels = ["[prompt]", ...Docuplextron.relations];
    var moreRels = this.dptron.linkStore.getRels();
    for (var ix in moreRels){
	if(!rels.includes(moreRels[ix])) rels.push(moreRels[ix]);
    }

    this.relSelector.textContent = ""; // Empty it out
    for(var ix in rels){
	this.relSelector.appendChild(selectOption(rels[ix],rels[ix]));
    }
    /* ... */
}

LinkerTool.prototype.update = function(){

    if(!this.active) return;
    
    /* Update positions of nexus
       Update span highlights
         reattach SimplePointers
	 ...
    */
    if(this.nexusA.held){
	this.nexusA.setAttribute("x",this.dptron.lastX - 6);
	this.nexusA.setAttribute("y",this.dptron.lastY - 15);
    } else if(this.nexusA.terminals[0]){
	this.snapToTerm(this.nexusA);
    }
    
    if(this.nexusB.held){
	this.nexusB.setAttribute("x",this.dptron.lastX - 6);
	this.nexusB.setAttribute("y",this.dptron.lastY - 15);
    } else if(this.nexusB.terminals[0]){
	this.snapToTerm(this.nexusB);
    }

}

LinkerTool.prototype.snapToTerm = function(nexus){

    /* We want the nexus to stay "pinned" to at least one
       of its link terminals. Go through the terminals, 
       see which one's closest to the nexus' current
       position, then get its top-left corner and snap
       the nexus to it. */
    var nearest = nexus.terminals[0];
    var nrect = nearest.getBoundingClientRect();
    var nexusLoc = {"x": parseFloat(nexus.getAttribute("x")) ,
		    "y": parseFloat(nexus.getAttribute("y")) };
    
    for(var ix in nexus.terminals){
	var term = nexus.terminals[ix];
	var trect = term.getBoundingClientRect();
	
	if (Math.abs(nexusLoc.x - trect.left) < Math.abs(nexusLoc.x - nrect.left) &&
	    Math.abs(nexusLoc.y - trect.top) < Math.abs(nexusLoc.y - nrect.top)){
	    nearest = term;
	    nrect = nearest.getBoundingClientRect();
	}
    }

    /* This isn't perfect, but it'll do for the moment.
       Ideal solution: put the nexus at the point on the 
       nearest terminal polygon that is closest to the
       other nexus. Code that someday, Adam. */
    
    nexus.setAttribute("x", nrect.left + 5);
    nexus.setAttribute("y", nrect.top + 5);

}

/*





  MODALS / FORMS





*/

Docuplextron.newDialog = function(type,infotext){
    
    /* Simple dialog with a textarea and two buttons. By default, OK button does
       nothing, and Cancel button removes the dialog from the DOM. */
    
    var dialog = {};
    dialog.floater = dptron.floaterManager.create(null,null,dptron.lastX,dptron.lastY);
    dialog.floater.style.zIndex = "500";
    dialog.floater.classList.add("nonContextual");
    dialog.floater.classList.add("dptronUI");
    dialog.floater.tab.text.textContent = " ";
    
    dialog.infotext = document.createElement("div");
    dialog.input = document.createElement(type || "div");
    dialog.okbutton = document.createElement("input");
    dialog.cancelbutton = document.createElement("input");
    
    dialog.okbutton.type = "button";
    dialog.okbutton.value = "OK";
    dialog.infotext.textContent = infotext || "";
    
    dialog.cancelbutton.type = "button";
    dialog.cancelbutton.value = "cancel";
    dialog.cancelbutton.onclick = function(){
	this.floater.destroy();
    }.bind(dialog);

    var buttonDiv = document.createElement("div");
    
    dialog.floater.appendChild(dialog.infotext);
    dialog.floater.appendChild(dialog.input);
    dialog.floater.appendChild(buttonDiv);
    buttonDiv.appendChild(dialog.okbutton);
    buttonDiv.appendChild(dialog.cancelbutton);

    dialog.input.focus();
    return dialog;
}

/***/

Docuplextron.credModal = function(infotext){
    var dialog = Docuplextron.newDialog("input",infotext);
    dialog.pwField = document.createElement("input");
    dialog.pwField.type = "password";
    
    dialog.pwField.placeholder = "password";
    dialog.input.placeholder = "username";

    dialog.pwField.style.display = "block";
    dialog.input.style.display = "block";

    dialog.input.insertAdjacentElement('afterend',dialog.pwField);
    
    return dialog;
}

/***/

Docuplextron.headMunglor = function(sdom){
    /* Inelegant (but effective) way to edit the <head> of a shoadow DOM. Just pass
       me the shadowRoot. */
    var munglor = Docuplextron.newDialog("textarea","Edit the HTML of the document's <head> element.");
    munglor.floater.classList.add("headMunglor");
    munglor.floater.tab.text.textContent = "Editing <HEAD> element";
/*    munglor.input.style.width = "100%";
    munglor.input.style.minHeight = "15vh";
    munglor.input.style.fontFamily = "monospace";
    */
    munglor.headElement = sdom.querySelector("head");
    if (!munglor.headElement){
	munglor.headElement = document.createElement("head");
	sdom.querySelector("body").insertAdjacentElement("beforebegin", munglor.headElement);
	console.log("How about now?", munglor.headElement);
    }
    munglor.input.value = munglor.headElement.innerHTML;
    munglor.okbutton.value = "APPLY";
    munglor.okbutton.onclick = function(){
	this.headElement.innerHTML = this.input.value;
    }.bind(munglor);
    munglor.cancelbutton.value = "DISMISS";
    return munglor;
}
/***/

Docuplextron.prototype.newGraphDialog = function(callback){

    /* Creates a simple dialog for creating a new graph document in
       the LinkStore. If a callback function is passed, then the @id
       of the new graph is passed to it when the OK button is
       clicked. */
    
    var dialog = Docuplextron.newDialog("input","Create a new graph document in the LinkStore.");

    /* */
    dialog.input.placeholder = "@id";
    dialog.input.value = "~" + Date.now().toString(36).toUpperCase();
    dialog.input.disabled = true;
    
    dialog.nameInput = document.createElement("input");
    dialog.nameInput.placeholder = "A Nice Name Goes Here";

    dialog.input.insertAdjacentElement("afterend", dialog.nameInput);
    
    dialog.okbutton.onclick = function(dialog, callback){
	var newGraph = LinkStore.Graph(dialog.input.value);
	newGraph["name"] = dialog.nameInput.value;
	this.linkStore.graphs[newGraph["@id"]] = newGraph;
	this.updateLinksFlap();
	dialog.floater.destroy();
	if (callback) callback(newGraph["@id"]);
    }.bind(this, dialog, callback)

    return dialog;
}

/***/

Docuplextron.prototype.linkPostModal = function(l){
    l = Alinks.asJsonLD(l);
    var dialog = Docuplextron.newDialog("input","Review/modify the link:");
    dialog.floater.tab.text.textContent = "Link Export";
    dialog.floater.style.width = "640px";
        
    if (l.id.startsWith("~")) {
	/* If this is an ephemeral link, take the tilde off of its id 
	   before posting. */
	l.id = "#" + l.id.substring(1);
    } else if (l.id.startsWith("http") && l.id.includes("#")){
	/* If the link's id is a hash-bearing URL, change the id
	   to the hash alone, and auto-fill the export URL field. */
	var s = l.id.split("#");
	dialog.input.value = s[0];
	l.id = "#" + s[1];
    }

    dialog.codeReview = document.createElement("textarea");
    dialog.codeReview.style.cssText = "font-family: monospace; height: 25vh; width: auto; ";
    dialog.codeReview.value = JSON.stringify(l, null, '   ');


    
    dialog.putbutton = document.createElement("input");
    dialog.putbutton.type = "button";
    dialog.putbutton.value = "PUT";

    dialog.okbutton.value = "POST";
    dialog.okbutton.insertAdjacentElement('beforebegin',dialog.putbutton);
    
    dialog.input.placeholder = "URL to post to";
    dialog.input.style.width = "auto";

    dialog.okbutton.onclick = function(dialog){
	this.postMedia("POST",
		       dialog.codeReview.value,
		       null,
		       dialog.input.value);
	dialog.floater.destroy();
    }.bind(this, dialog);

    dialog.putbutton.onclick = function(dialog){
	this.postMedia("PUT",
		       dialog.codeReview.value,
		       null,
		       dialog.input.value);
	dialog.floater.destroy();
    }.bind(this, dialog);

    dialog.input.insertAdjacentElement('beforebegin',dialog.codeReview);

    dialog.floater.bringIntoView();
    return dialog;
}

/***/

function newStyleMunglor(target){
    
    /* Editor dialog for element styles.*/
    
    var munglor = Docuplextron.newDialog("textarea","");
    dptron.simplePointers.push(new SimplePointer(munglor.floater,
						 target,
						 dptron.overlay) );
    munglor.target = target;
    munglor.floater.classList.add("styleMunglor");
    munglor.floater.classList.add("nonContextual");
    munglor.floater.stickyToggle();
    munglor.floater.tab.text.textContent = "CSS Munglor for " + target.tagName + " element";

    munglor.cancelbutton.value = "dismiss";

    if (munglor.target.style.cssText) {
	munglor.input.value = munglor.target.style.cssText;
    }
    munglor.input.setAttribute("cols",40);
    
    munglor.okbutton.value = "apply";
    munglor.okbutton.onclick = function(){
	munglor.target.style.cssText = munglor.input.value;
    };
    return munglor;
}

/***/

Docuplextron.prototype.nodeMunglor = function(node, graphDocID, floater, opts){

    /* Creates a form for editing the attributes of a LinkStore
       node. Optionally takes a reference to a floater, so this can be
       embedded in other forms or free-floating.

       A 'node' may either be a node object or a string referencing a 
       node in the specified graph document. If'node' is a string but 
       no valid graph document is specified, we just create
       an empty node.  

       JESUS CHRIST. This thing turned into a novel. Break it up,
       Adam! Lordy.

       Also, this was a horrible way to code this. Next time, make a
       proper NodeMunglor prototype and write setters and 
       getters that update the form. This thing is ...a real mess.*/

    if (!opts) opts = [];
    
    var graph = this.linkStore.graphs[graphDocID];
    
    if(typeof(node) == "string"){
	if(!graph){
	    node = { "@id" : node };
	} else if (graph.nodes[node]){
	    node = graph.nodes[node];
	} else {
	    node = { "@id" : node }

	}
    }
	    
    var initialID = node["@id"];
    
    var container = document.createElement("div");
    container.classList.add("padded");
    container.classList.add("nodeMunglor");

    var nodeHeader = document.createElement("header");
    if(opts.includes("anode")){
	nodeHeader.innerHTML = "<big><b>A</b></big> <small>NODE:</small> <span class='nodeMunglor-oldNode'>" + node["@id"] + "</span>";
    } else if (opts.includes("bnode")){
	nodeHeader.innerHTML = "<big><b>B</b></big> <small>NODE:</small> <span class='nodeMunglor-oldNode'>" + node["@id"] + "</span>";
    } else {
	nodeHeader.innerHTML = "<small>EDITING NODE:</small> <span class='nodeMunglor-oldNode'>" + node["@id"] + "</span><br>" +
	    "in <i>" + graphDocID + "</i></header>", "text/html";
    }
    
    container.appendChild(nodeHeader);

    /* Give it a @type attribute if it doesn't have one. Empty types 
       will be discarded when the node is saved, but for the purposes
       of this form, we want one. */
    
    if(!node["@type"]) node["@type"] = "";
    
    /* The attributes... */
    if(node["@type"] == "EDL"){
	/* The EDL needs to be expanded into a sub-list with span editors */

	/* First, put the attributes at the top of the editor as per usual. */
	var nodeCopy = JSON.parse(JSON.stringify(node));
	delete nodeCopy["@list"];
	container.appendChild( Docuplextron.attributeForm(nodeCopy) );

	/* Then make the span list. */
	var spanListContainer = document.createElement("div");
	spanListContainer.classList.add("shaded");
	spanListContainer.classList.add("padded");
	spanListContainer.style.backgroundColor = "#282828";
	spanListContainer.style.padding = "0.5em";
	
	var spanList = document.createElement("div");
	spanList.classList.add("spanList");

	var listHead = document.createElement("span");
	listHead.textContent = "@list:";

	for(var item in node["@list"]){
	    spanList.appendChild(Docuplextron.SpanForm(node["@list"][item]));
	}
	var addBtn = document.createElement("button");
	//addBtn.textContent = "ADD EMPTY";
	addBtn.innerHTML = "<span style='transform: rotate(90deg); display: inline-block;'>↵</span>ADD EMPTY";
	addBtn.onclick = function(){
	    this.appendChild(Docuplextron.SpanForm());
	}.bind(spanList);
	
	var addFromSel = document.createElement("button");
	//addFromSel.textContent = "ADD FROM SELECTION";
	addFromSel.innerHTML = "<span style='transform: rotate(90deg); display: inline-block;'>↵</span>ADD FROM SELECTION";
	addFromSel.onclick = function(){
	    for(var ss in alph.selection.spans){
		this.appendChild(Docuplextron.SpanForm(
		    alph.selection.spans[ss].toLinkStoreEDLItem() ) );
	    }
	}.bind(spanList);

	var replaceWithSel = document.createElement("button");
	//replaceWithSel.textContent = "REPLACE WITH SELECTION";
	replaceWithSel.innerHTML = "<span style='transform: rotate(90deg); display: inline-block;'>↵</span>REPLACE WITH SELECTION";
	replaceWithSel.onclick = function(){
	    var oldSpans = Array.from(this.children);
	    for (var c in oldSpans){
		oldSpans[c].remove();
	    }
	    for(var ss in alph.selection.spans){
		this.appendChild(Docuplextron.SpanForm(
		    alph.selection.spans[ss].toLinkStoreEDLItem() ) );
	    }
	}.bind(spanList);

	spanListContainer.appendChild(listHead);
	spanListContainer.appendChild(spanList);
	spanListContainer.appendChild(addBtn);
	spanListContainer.appendChild(addFromSel);
	spanListContainer.appendChild(replaceWithSel);
	container.appendChild(spanListContainer);
	//container.appendChild(document.createElement("br"));
	
    } else if(node["@type"] == "AlphSpan") {
	container.appendChild( Docuplextron.attributeForm(node) );
	/* Add a button to get the span from the current selection. */
	var fromSel = document.createElement("button");
	fromSel.textContent = "GET FROM SELECTION";
	fromSel.onclick = function(container){
	    if(alph.selection){
		var keys = Array.from(container.querySelectorAll(".attributeKey"));
		var idInput;
		var srcInput;
		var originInput;
		var extentInput;
		for (var k in keys){
		    if(keys[k].value == "@id"){
			idInput = keys[k].nextSibling;
		    } else if (keys[k].value == "origin"){
			originInput = keys[k].nextSibling;
		    } else if (keys[k].value == "extent"){
			extentInput = keys[k].nextSibling;
		    } else if (keys[k].value == "src"){
			srcInput = keys[k].nextSibling;
		    }
		}
		var s = alph.selection.spans[0];
		srcInput.value = s.src;
		originInput.value = s.origin;
		extentInput.value = s.extent;
		idInput.value = s.src + "#" + s.origin + "-" + s.extent;
		idInput.oninput();
	    }
	}.bind(this, container);
	container.appendChild(fromSel);
    } else {
	container.appendChild( Docuplextron.attributeForm(node) );
    }

    /* And some buttons: 

       STORE -- saves the node directly to the graph's node list; If
       the @id has been changed, then links to/from this node will not
       be effected.

       REPLACE -- replaces the node in the graph's node list, and
       replaces all references to it

       REVERT -- Resets the form with the node as it is currently
       stored in the graph's node list
    */
    
    var storeBtn = document.createElement("button");
    storeBtn.textContent = "UPDATE";
    storeBtn.title = "Save this node to the graph document.";
    storeBtn.onclick = function(form, gd, oid, opts){

	if(!gd) {
	    window.alert("No graph document specified.");
	    return;
	}

	/* Turn the form into a node object. */
	var n = Docuplextron.attributeFormToObject(form);
	if(n["@type"] == "EDL"){
	    n["@list"] = Docuplextron.grokEDLForm(form);
	} else if (n["@type"] == "") { // Remove empty type attributes
	    delete n["@type"];
	}

	/* Store it to the graph document. */
	gd.nodes[n["@id"]] = n;

	/* If this is a new node (different from the @id we first created
	   the form for) then reload the form */
	if(oid != n["@id"]){
	    form.replaceWith(this.nodeMunglor(n["@id"], gd["@id"], null, opts));
	}

	/* Update the links flap! */
	this.updateLinksFlap();
    }.bind(this, container, graph, initialID, opts);
    
    var replaceBtn = document.createElement("button");
    replaceBtn.textContent = "REPLACE";
    replaceBtn.title = "Save this node to the graph document, and replace all references to the node in the graph.";
    replaceBtn.style.display = "none"; // Hidden until the user changes the node's @id
    replaceBtn.onclick = function(form, gid, oid, opts){
	if(!gid){
	    window.alert("No graph document specified.");
	} else {

	    /* Objectify the form ... */
	    var n = Docuplextron.attributeFormToObject(form);
	    if(n["@type"] == "EDL"){
		n["@list"] = Docuplextron.grokEDLForm(form);
	    } else if (n["@type"] == "") {
		delete n["@type"];
	    }
	    
	    /* Replace the node... */
	    this.linkStore.replaceNode(oid, gid, n);
	    
	    /* Reload the form */
	    form.replaceWith(this.nodeMunglor(n["@id"], gid, null, opts));
	    
	    /* Update the flap! */
	    this.updateLinksFlap();
	}
    }.bind(this, container, graphDocID, initialID, opts);
    
    var revertBtn = document.createElement("button");
    revertBtn.textContent = "REVERT";
    revertBtn.title = "Reload this form with the node as it is stored in the graph document.";
    revertBtn.onclick = function(gd, oid, form, opts){
	if(!gd){
	    window.alert("Cannot restore an unsaved node!");
	} else {
	    form.replaceWith(this.nodeMunglor(oid, gd, null, opts));
	}
    }.bind(this, graphDocID, initialID, container, opts);


    /* Now add some handlers for attributes of particular types. */

    
    var attrInputs = Array.from(container.querySelectorAll("input.attributeKey"));
    for (var k in attrInputs){

	var idKey = attrInputs[k];
	var idVal = idKey.nextSibling;

	if (attrInputs[k].value == "@id"){
	    /* The @id attribute is special. First off, we don't want
	       it to be deletable, so we remove its "X" button. Next,
	       if the user modifies the @id, then when they click
	       "STORE", it will just put a new node in the node list
	       and the graph wont change at all.  What they MIGHT want
	       to do, though, is replace all links to/from the node
	       they opened the editor on with this new one, so we show
	       the "REPLACE" button.  */

	    idVal.classList.add("idInput");
	    idVal.nextSibling.remove(); // Remove the X button.

	    idVal.oninput = function(oid, btn, sBtn, container){
		
		if (this.value == initialID){
		    replaceBtn.style.display = "none";
		    sBtn.textContent = "UPDATE";
		    sBtn.title = "Update the information for [" + initialID + "] in the graph document.";
		    this.style.color = "";
		} else {
		    replaceBtn.style.display = "inline";
		    replaceBtn.title = "Save this as a new node in the graph document, and replace all references to [" + oid + "] with references to [" + this.value + "] in the graph.";
		    sBtn.textContent = "CREATE";
		    sBtn.title = "Save this node as [" + this.value + "] in the graph document, leaving [" + oid +"] unchanged. If [" + this.value + "] already exists in the graph document, it will be overwritten.";		    
		    this.style.color = "pink";		    
		}

		/* Validate @id input depending on rel type...? */
		
		var relInput = floater.querySelector("#relInput");
		if(relInput && (relInput.value == "tag")){
		    if(!this.value.startsWith("tag:")){
			this.value = "tag:" + this.value;
			/* This is a kludgy way to do it. Might be better to do 
			   this validation after the field has been filled and
			   submitted? */
		    }
		    this.value = this.value.replaceAll(" ","_");
		}
		
	    }.bind(idVal, initialID, replaceBtn, storeBtn, floater)
	} else if (attrInputs[k].value == "@type"){
	    /* For @type attributes, we want to add a selector after
	       the value field which will modify the form
	       appropriately for whichever type is chosen. */

	    var typeSelector = document.createElement("select");
	    typeSelector.innerHTML = "<option value=''>none/other</option>" +
		"<option value='AlphSpan'>AlphSpan</option>" +
		"<option value='EDL'>EDL</option>";

	    if(node["@type"] == ""){
		typeSelector.children[0].setAttribute("selected", true);
	    } else if(node["@type"] == "AlphSpan"){
		typeSelector.children[1].setAttribute("selected", true);
	    } else if(node["@type"] == "EDL"){
		typeSelector.children[2].setAttribute("selected", true);
	    }
	    
	    typeSelector.onchange = function(typeSelector, idVal, form, graphDocID, opts){

		/* If the user changes the @type, then we will want to
		   make sure that the node conforms to the structure
		   of that type, and reload the form so that the
		   necessary tooling is loaded. */
		
		idVal.value = typeSelector.value;
		
		if (typeSelector.value == "EDL"){

		    /* Objectify the node from the form. */

		    var n = Docuplextron.attributeFormToObject(form);

		    /* EDL nodes should always have "blank node" IDs. */
		    if ( !n["@id"].startsWith("_:") ){
			n["@id"] = "_:EDL-" + Date.now().toString(36).toUpperCase();
		    }

		    if (n["src"] && n["origin"] && n["extent"]){

			/* If this was an AlphSpan before the change,
			   it'll have src, origin, extent
			   attributes. Use those as the first item in
			   the EDL. */

			n["@list"] = [ {"@type" : "AlphSpan",
					"src" : n["src"],
					"origin" : n["origin"],
					"extent" : n["extent"],
					"@id" : n["src"] + "#" + n["origin"] + "-" + n["extent"] }
				     ];

			/* And then remove them. */
			
			delete n["src"];
			delete n["origin"];
			delete n["extent"];
			
		    } else {

			/* Otherwise, use grokEDLForm(), which might
			   just return an empty list. */
			
			n["@list"] = Docuplextron.grokEDLForm(form);
		    }

		    /* Then reload the form */
		    form.replaceWith(this.nodeMunglor(n ,graphDocID, null, opts));
		    
		} else if (typeSelector.value == "AlphSpan") {
		    var n = Docuplextron.attributeFormToObject(form);
		    if(!n["src"]) n["src"] = "";
		    if(!n["origin"]) n["origin"] = "";
		    if(!n["extent"]) n["extent"] = "";
		    // Destructive. 
		    form.replaceWith(this.nodeMunglor(n ,graphDocID, null, opts));
		} else {
		    /* Unspecified or unknown types don't need to
		       be modified, but the form should be reloaded
		       all the same. */
		    var n = Docuplextron.attributeFormToObject(form);
		    
		    // Destructive
		    form.replaceWith(this.nodeMunglor(n ,graphDocID, null, opts));
		}
	    }.bind(this, typeSelector, idVal, container, graphDocID, opts);
	    
	    idVal.insertAdjacentElement('afterend', typeSelector);
	} else if (node["@type"] == "AlphSpan" && (  attrInputs[k].value == "src" ||
						     attrInputs[k].value == "origin" ||
						     attrInputs[k].value == "extent" ) ) {
	    /* The src, origin, and extent fields of an AlphSpan
	       need to cause the @id to be regenerated. So... ugh. Get
	       a handle on all of the relevant inputs first, so we can
	       bind 'em to a function that we'll attach to all
	       three. */

	    var srcVal;
	    var originVal;
	    var extentVal;
	    var idVal;
	    for(var kk in attrInputs){
		if(attrInputs[kk].value == "@id"){
		    idVal = attrInputs[kk].nextSibling;
		} else if(attrInputs[kk].value == "src"){
		    srcVal = attrInputs[kk].nextSibling;
		} else if(attrInputs[kk].value == "origin"){
		    originVal = attrInputs[kk].nextSibling;
		} else if(attrInputs[kk].value == "extent"){
		    extentVal = attrInputs[kk].nextSibling;
		}
	    }

	    attrInputs[k].nextSibling.oninput = function(i, s, o, e){
		idVal.value = srcVal.value + "#" + originVal.value + "-" + extentVal.value;
		idVal.oninput();
	    }.bind(this, idVal, srcVal, originVal, extentVal);
	    
	}
    }
    
    container.appendChild(storeBtn);
    container.appendChild(replaceBtn);
    container.appendChild(revertBtn);

    if(floater){
	/* Attach the form to the floater */
	//floater.tab.text.textContent = "NODE MUNGLOR";
	floater.appendChild(container);

	/* Attach a simplePointer ...? */
	// ...

	
	/* And a button to close the floater */
	/* ...or not? This screws with the layout of the link editor. 
	var dismissBtn = document.createElement("button");
	dismissBtn.textContent = "DISMISS";
	dismissBtn.title = "Close this form. Unsaved changes will be discarded.";
	dismissBtn.onclick = function(){
	    this.destroy();
	}.bind(floater);

	floater.appendChild(dismissBtn);
	*/
    }

    return container;
    
    /* And this concludes our presentation of One Ugly, Huge Method, a
       production of LÆMEURCOM, in partnership with Phillips Brewing &
       Malting Co. */
}

/***/

Docuplextron.prototype.graphDocSelector = function(initialSelection){
    
    /* Returns a <select> element populated with the names/ids of the graphs in the
       docuplextron's linkStore. */
    
    var gds = document.createElement("select");

    gds.update = function(gds, selection){
	
	/* We define a function to update the selector because we'll need to
	   use it as a callback if/when we spawn a graph creation dialog. 

	   Creates a bunch of <option> elements for each graph in the 
	   LinkStore. */
	
	gds.innerHTML = "";
	
	for(var k in this.linkStore.graphs){
	    var opt = document.createElement("option");
	    if(this.linkStore.graphs[k]["name"]){
		opt.textContent = this.linkStore.graphs[k]["name"] + "(" + k + ")";
	    } else {
		opt.textContent = "[" + k + "]";
	    }
	    opt.value = k;
	    if(k == selection) opt.selected = true;
	    gds.appendChild(opt);
	}
	if(Object.keys(this.linkStore.graphs).length == 0){
	    /* If the linkStore is empty, we need a filler option for the selector */
	    var nullOpt = document.createElement("option");
	    nullOpt.textContent = "-----";
	    nullOpt.value = "-----";
	    gds.appendChild(nullOpt);
	}
	var newOpt = document.createElement("option");
	newOpt.textContent = "new";
	newOpt.value = "new";
	gds.appendChild(newOpt);
    }.bind(this, gds);

    gds.onchange = function(gds){
	/* The default behaviour for this is simply to update the graphTarget and/or
	   create a new graph. 

	   The question is: should this always try to update() all of the graphDocSelectors
	   in the workspace to keep theym all in sync? Or is it okay to have multiple link 
	   editors/annotations open with different active graph targets? */

	if(gds.value == "-----"){
	    // Do nothin'
	} else if(gds.value == "new"){
	    this.newGraphDialog(gds.update);
	} else {
	    this.graphTarget = gds.value;
	}
	
	
    }.bind(this, gds);

    // Load the initial set of graph names/ids.
    gds.update(initialSelection);

    return gds;
}

/***/

Docuplextron.prototype.linkMunglor = function(anode, bnode, rel, graphDocID, justDoIt){

    /* Provide a form with two embedded nodeMunglors for creating/modifying links */

    var anode = anode || {"@id" : "~unspecified"};
    var bnode = bnode || {"@id" : "~unspecified"};
    var rel = rel || "";
    var graphDocID = graphDocID;
    if(!graphDocID){
	if (Object.keys(this.linkStore.graphs).length > 0){
	    var graphDocID = this.linkStore.graphs[Object.keys(this.linkStore.graphs)[0]]["@id"];
	} else {
	    var graphDocID = "" ;// What SHOULD this be?
	}
    }
    
    var floater = this.floaterManager.create("nonContextual");
    floater.classList.add("linkEditor");
    floater.classList.add("dptronUI");
    floater.tab.text.textContent = "LINK MUNGLOR";
    floater.stickyToggle();
    
    var container = document.createElement("div");
    var anodeContainer = document.createElement("div");
    var bnodeContainer = document.createElement("div");
    var anodeForm = this.nodeMunglor(anode, graphDocID, floater, ["anode"]);
    var bnodeForm = this.nodeMunglor(bnode, graphDocID, floater, ["bnode"]);

    anodeContainer.appendChild(anodeForm);
    bnodeContainer.appendChild(bnodeForm);

    /* Set the node containers to inline-block, so they'll site side-by-side */
    anodeContainer.style.display = bnodeContainer.style.display = "inline-block";
    anodeContainer.style.verticalAlign = bnodeContainer.style.verticalAlign = "top";

    container.innerHTML = "<h3 style='margin: 0px;'>LINK EDITOR</h3>";
    
    var header = document.createElement("header");
    header.style.backgroundColor = "#222"; // Don't do these explicitly, Adam. Use the stylesheet.
    header.style.padding = "0.5em";
    header.innerHTML = "GRAPH:";

    // Create a graph selector...
    
    var graphDocSelector = this.graphDocSelector(this.graphTarget);    
    
    graphDocSelector.onchange = function(anodeCont, bnodeCont, gdSelector, floater){

	/* Reload the nodeMunglors. This is because of the way that I
	   wrote the nodeMunglors, binding the graph ID to the
	   functions of the buttons... ugh. Poor design. */

	if(gdSelector.value == "-----"){
	    // Do nothin'
	} else if(gdSelector.value == "new"){
	    this.newGraphDialog(gdSelector.update);
	} else {
	    var anodeForm = anodeCont.firstElementChild;
	    var bnodeForm = bnodeCont.firstElementChild;
	
	    aNodeFromForm = Docuplextron.attributeFormToObject(anodeForm);
	    if(aNodeFromForm["@type"] == "EDL") aNodeFromForm["@list"] = Docuplextron.grokEDLForm(anodeForm);
	
	    bNodeFromForm = Docuplextron.attributeFormToObject(bnodeForm);
	    if(bNodeFromForm["@type"] == "EDL") bNodeFromForm["@list"] = Docuplextron.grokEDLForm(bnodeForm);
	
	    anodeForm.replaceWith(this.nodeMunglor(aNodeFromForm, gdSelector.value, floater, ["anode"]));
	    bnodeForm.replaceWith(this.nodeMunglor(bNodeFromForm, gdSelector.value, floater, ["bnode"]));

	    this.initBarGDS(gdSelector.value);
	    
	}
    }.bind(this, anodeContainer, bnodeContainer, graphDocSelector, floater);

    header.appendChild(graphDocSelector);

    var relArea = document.createElement("div");
    relArea.innerHTML = "A→B RELATION:";
    relArea.style.padding = "0.5em";
    relArea.style.backgroundColor = "black";
    
    var relSelector = document.createElement("select");
    relSelector.innerHTML = "<option value='" + rel + "'>" + rel + "</option>" +
	"<option value='tag'>tag</option>" +
	"<option value='about'>about</option>" +
	"<option value='alternate'>alternate</option>" +
	"<option value='answered-by'>answered-by</option>" +
	"<option value='author'>author</option>" +
	"<option value='current'>current</option>" +
	"<option value='commentary'>commentary</option>" +
	"<option value='describes'>describes</option>" +
	"<option value='describedBy'>describedBy</option>" +
	"<option value='note'>note</option>" +
	"<option value='predecessor-version'>predecessor-version</option>" +
	"<option value='related'>related</option>" +
	"<option value='successor-version'>successor-version</option>" +
	"<option value='via'>via</option>";
    
    var relInput = document.createElement("input");
    relInput.id = "relInput";
    relInput.placeholder = "link relation";
    relInput.value = rel;

    relArea.appendChild(relSelector);
    relArea.appendChild(relInput);

    relSelector.addEventListener("change", function(relInput) {
	relInput.value = this.value;
    }.bind(relSelector, relInput) );
        
    container.appendChild(header);
    container.appendChild(relArea);    
    container.appendChild(anodeContainer);
    container.appendChild(bnodeContainer);
    floater.appendChild(container);

    /* Now, some buttons... */
    var buttonRow = document.createElement("div");
    buttonRow.classList.add("shaded");

    var storeBtn = document.createElement("button");
    storeBtn.textContent = "STORE";
    storeBtn.onclick = function(anodeCont, bnodeCont, rel, graphID, floater){
	var anode = Docuplextron.attributeFormToObject(anodeCont);
	if(anode["@type"] == "EDL"){
	    anode["@list"] = Docuplextron.grokEDLForm(anodeCont);
	} else if (anode["@type"] == "") { // Remove empty type attributes
	    delete anode["@type"];
	}

	var bnode = Docuplextron.attributeFormToObject(bnodeCont);
	if(bnode["@type"] == "EDL"){
	    bnode["@list"] = Docuplextron.grokEDLForm(bnodeCont);
	} else if (bnode["@type"] == "") { // Remove empty type attributes
	    delete bnode["@type"];
	}
	
	if (!rel.value) {
	    rel.select();
	    this.pukeText("No relation! Cannot store link.");
	    return console.log("No relation; cannot store link.");
	}
	

	this.linkStore.addTo({ "nodeA" : anode,
			       "nodeB" : bnode,
			       "rel" : rel.value },
			     graphID.value);
	
	this.updateLinksFlap();

	anodeCont.firstElementChild.replaceWith(
	    this.nodeMunglor(anode, graphID.value, floater, ["anode"]));	
	bnodeCont.firstElementChild.replaceWith(
	    this.nodeMunglor(bnode, graphID.value, floater, ["bnode"]));

	this.paintLinkTerminals();
	
    }.bind(this, anodeContainer, bnodeContainer, relInput, graphDocSelector, floater);

    var closeBtn = document.createElement("button");
    closeBtn.textContent = "DISMISS";
    closeBtn.onclick = function(){
	this.destroy();
    }.bind(floater);

    buttonRow.appendChild(storeBtn);
    buttonRow.appendChild(closeBtn);    
    container.appendChild(buttonRow);

    if(justDoIt){
	storeBtn.onclick();
	closeBtn.onclick();
    }
}

/***/

Docuplextron.prototype.graphMunglor = function(){
    
}

/***/

Docuplextron.grokEDLForm = function(el) {
    /* Pass me an element full of span forms, and I'll convert the mess
       into a nice "EDL"-type object. */
    var forms = Array.from(el.querySelectorAll("form"));
    var edl = [];
    for(var ix in forms){
	edl.push(
	    {
		"@type" : "AlphSpan",
		"@id" : forms[ix].children[0].value + "#" +
		    forms[ix].children[1].value + "-" +
		    forms[ix].children[2].value,
		"src" : forms[ix].children[0].value,
		"origin" : forms[ix].children[1].value,
		"extent" : forms[ix].children[2].value
	    }
	);
    }
    return edl;
}

/***/
Docuplextron.attributeMunglor = function(target){
    
    /* Creates a floater with an attribute editing form for the passed
       DOM element, and creates a SimplePointer between the editor
       form and the element. */
    
    var munglor = Docuplextron.newDialog(null,"");
    dptron.simplePointers.push(new SimplePointer(munglor.floater,
						 target,
						 dptron.overlay) );
    munglor.target = target;
    munglor.jsonAttrs = {};
    munglor.floater.classList.add("styleMunglor");
    munglor.floater.classList.add("nonContextual");
    munglor.floater.stickyToggle();
    munglor.floater.tab.text.textContent = "Attribute Munglor for " + target.tagName + " element";

    munglor.cancelbutton.value = "dismiss";

    munglor.makeAttributeList = function(){
	
	/* Fill jsonAttrs with a dictionary of all the element's attributes 
	   that HAVE readable values. */

	var attrs = this.target.attributes;
	for(var ix in attrs){
	    var attrValue = this.target.getAttribute(attrs[ix].name);
	    if(attrValue){
		this.jsonAttrs[attrs[ix].name] = attrValue;
	    }
	}
    }.bind(munglor)

    munglor.makeAttributeList();
    
    /* Stringify jsonAttrs and send it to Docuplextron.attributeForm() - this 
       is what actually generates the name/value fields in the dialog. */

    var attrString = JSON.stringify(munglor.jsonAttrs);
    munglor.attrForm = Docuplextron.attributeForm( JSON.parse(attrString) );
    munglor.input.appendChild( munglor.attrForm	);

    munglor.fixInputs = function(){
	/* Okay, so the idea here is to modify all of the input fields so that
	   when ESC is pressed, they return to their default values, and when
	   ENTER is pressed, the attribute being edited is applied and the form
	   refreshes. This way we can edit attributes one-by-one, rather than
	   having to apply them all at once. PROBLEM: Deletions will still
	   require the user to click the Apply button. :( */
    
	var valueInputs =
	    Array.from(this.floater.querySelectorAll(".attributeValue"));
	for (var ix in valueInputs){
	    valueInputs[ix].onkeydown = function(munglor, event){
		if(event.which == 13){ // Enter
		    munglor.apply(this.previousSibling.value, this.value);
		} else if(event.which == 27){ //Esc
		    this.value = this.getAttribute("oldValue");
		    this.classList.remove('inputChanged');
		}
	    }.bind(valueInputs[ix], this);
	}
    }.bind(munglor);
    
    munglor.fixInputs();
    
    munglor.applybutton = munglor.okbutton.cloneNode(true);
    munglor.okbutton.insertAdjacentElement('afterend',munglor.applybutton);

    munglor.okbutton.onclick = function(){
	this.apply();
	this.floater.destroy();
    }.bind(munglor);
    
    munglor.applybutton.value = "apply";
    munglor.applybutton.onclick = function(){
	this.apply();
    }.bind(munglor);
    
    munglor.apply = function(key,value){
	if(key && value){

	    /* If we've been passed a key and value, just update that 
	       specific attribute in the element, and refresh the form. */

	    this.target.setAttribute(key, value);
	    this.makeAttributeList();
	    var attrString = JSON.stringify(this.jsonAttrs);
	    this.attrForm.remove();
	    this.attrForm = Docuplextron.attributeForm(JSON.parse(attrString));
	    this.input.appendChild(this.attrForm);
	    this.fixInputs();
	    
	} else {
	    
	    /* If no key/value pair were passed, then update all of the
	       element's attributes, and delete those that have been 
	       removed from the form. */
	    
	    var formKeys = Array.from(this.floater.querySelectorAll(".attributeKey"));
	    console.log("form keys:",formKeys);
	    for(var ix in formKeys){
		var keyElement = formKeys[ix];
		var valElement = keyElement.nextSibling;
		this.target.setAttribute(keyElement.value, valElement.value);
		delete this.jsonAttrs[keyElement.value];
	    }
	
	    attrKeys = Object.keys(this.jsonAttrs);
	    for(var ix in attrKeys){
		this.target.removeAttribute(attrKeys[ix]);
	    }
	}
    }.bind(munglor);
    return munglor;
}

/***/

Docuplextron.attributeFormToObject = function(container){
    var returnObject = {};
    var formKeys = Array.from(container.querySelectorAll(".attributeKey"));
    for(var ix in formKeys){
	var keyElement = formKeys[ix];
	var valElement = keyElement.nextSibling;
	returnObject[keyElement.value] = valElement.value;
    }
    return returnObject;
}

/***/

Docuplextron.attributeForm = function(aObject){
    
    /* Returns an editable <list> of key/value pairs ( generated by
       Docuplextron.attributePair() ) for the passed object, plus a
       "+" button at the end of the list which will add a new, blank
       key/value pair to the list. */

    var keys = Object.keys(aObject);
    var container = document.createElement("ul");
    container.className = "attributeForm";
    for (var ix in keys){
	container.appendChild(Docuplextron.attributePair(
	    keys[ix],
	    aObject[keys[ix]],
	    true
	));
    }
    var attrCreate = document.createElement("input");
    attrCreate.type = "button";
    attrCreate.value = "ADD ATTRIBUTE";
    attrCreate.onclick = function(container){
	var attrPair = Docuplextron.attributePair("name","value");
	container.insertBefore(attrPair,this);
	attrPair.querySelector('input.attributeKey').select();
    }.bind(attrCreate,container);
    container.appendChild(attrCreate);
    
    return container;
}

Docuplextron.attributePair = function(key,value,keylock){

    /* Returns a list-item (<li>) element, with two <input> fields for
       the passed 'key' and 'value'. If 'keylock' is passed as true,
       the key input is disabled, while the value input remains
       editable. A button is placed at the end which will remove the
       <li> from the DOM if clicked. */
    
    var listItem = document.createElement("li");
	
    var keyInput = document.createElement("input");
    keyInput.type = "text";
    keyInput.className = "attributeKey";
    keyInput.value = key;
    if (keylock) keyInput.disabled = true;

    var valueInput = document.createElement("input");
    valueInput.type = "text";
    valueInput.className = "attributeValue";
    valueInput.value = (Array.isArray(value)) ? value.join(" ") : value;
    valueInput.setAttribute("oldValue",valueInput.value);
    valueInput.oninput = function(){
	if(this.value == this.getAttribute("oldValue")){
	    this.classList.remove('inputChanged');
	} else {
	    this.classList.add('inputChanged');
	}
    }

    var attrDelete = document.createElement("input");
    attrDelete.type = "button";
    attrDelete.value = "X";
    attrDelete.onclick = function(){
	this.remove();
    }.bind(listItem);
    
    listItem.appendChild(keyInput);
    listItem.appendChild(valueInput);
    listItem.appendChild(attrDelete);
    return listItem;
}

function parseAttributeForm(form){
    
}


/*






  CONTEXT MENU COMMANDS






*/

Docuplextron.prototype.command = function(c,actionTarget){
    if(actionTarget){ this.actionTarget = actionTarget; }
    switch(c){
    case "copy span to clipboard":
	/* Select the text contents of the infotext amd copy it to the clipboard */
	alph.box.content.textContent = alph.selection.asString();
	alph.box.show();
	window.getSelection().selectAllChildren(alph.box.content);
	document.execCommand("copy");
	this.pukeText("Copied '" + alph.selection.asString() + "' to clipboard.");
	break;
    case "crop to selection":
	/* When we have an image selection, replace the current image with an 
	   x-img that only contans the contents of the selection rectangle. */

	// Grab the AlphSpan that describes the image fragment
	var s = alph.selection.spans[0];

	// Create the new x-img
	var newImg = document.createElement("img",{"is":"x-img"});
	newImg.setAttribute("src",s.src);
	newImg.setAttribute("origin",s.origin);
	newImg.setAttribute("extent",s.extent);

	/* If the image was alone in a floater and the floater's id was set 
	   to the URL of the image, change the id of that floater to match 
	   the new image fragment -- otherwise, when the workspace is 
	   reloaded, the fragment will be replaced with the full image 
	   (or previous fragment) */
	var i = Docuplextron.getParentItem(this.actionTarget.target);
	if(i.id.startsWith(s.src)){
	    var urlString = alph.selection.asString();
	    i.id = urlString;
	    i.tab.text.textContent = urlString.substr(urlString.lastIndexOf("/"));
	}
	
	// Finally, replace the image
	this.actionTarget.target.replaceWith(newImg);	
	break;
    case "export as plain-text":
	//var barfie = window.open("about:blank");
	var outText = "";
	var targets = (this.actionTarget != this.activeItems) ?
	    this.activeItems : [ this.actionTarget ];

	for(var ix in targets){
	    var target = targets[ix];
	    if(target.classList.contains("noodleBox")){
		var noodle = this.noodleManager.get(target.querySelector("x-text").getAttribute("src"));
		outText += Noodle.toText(noodle);
	    } else {
		var firstLine = (target.hasAttribute("title")) ?
		    target.getAttribute("title") : target.id;
		outText += firstLine + "\n" + target.textContent + "\n\n\n\n";
	    }
	}

	var dia = Docuplextron.newDialog("a","Download noodle as plain text:");
	dia.okbutton.remove();
	dia.cancelbutton.value = "close";
	dia.input.href="data:text/plain;encoding=utf-8,"+encodeURIComponent(outText);
	dia.input.download = "noodleExport.txt";
	dia.input.textContent = "Download";

	break;
    case "foomenu...":
	this.activeMenu = this.contextMenuOn(this.actionTarget,["foo","bar","bas"]);
	break;
    case "fetch and fill":
	var sdom = this.actionTarget.querySelector("x-dom");
	if(sdom){
	    alph.fetchAndFill(sdom.shadowRoot);
	} else {
	    alph.fetchAndFill(this.actionTarget);
	}
	break;
    case "generate EDL":
	var f = this.floaterManager.create(null,null,this.lastX,this.lastY);
	f.style.fontFamily = "monospace";
	f.style.whiteSpace = "pre-wrap";
	f.textContent = Alphjs.domToEdl(this.actionTarget);
	break;
    case "erase session":
	/* Comes from a list item in #dptronSessionList */
	localStorage.removeItem(this.actionTarget.getAttribute("data-sessionKey"));
	this.updateSessionList();
	break;
    case "edit head":
	var sdom = this.actionTarget.querySelector("x-dom");
	this.simplePointers.push(new SimplePointer(this.actionTarget,
						   Docuplextron.headMunglor(sdom.shadowRoot).floater,
						   this.overlay));
	break;
    case "export to view":
	var sdom = this.actionTarget.querySelector("x-dom");

	var f = this.floaterManager.create();
	f.style.width = "720px";
	var pre = document.createElement("pre");
	pre.style.whiteSpace = "pre-wrap";
	if (sdom){
	    pre.textContent = sdom.shadowRoot.innerHTML;
	} else {
	    pre.textContent = this.exportContextHTML(this.actionTarget);
	}
	f.appendChild(pre);
	break;
    case "export to URL" :
	this.exportContextToURL(this.actionTarget);
	break;
    case "pop out" :
	var f = this.floaterManager.create();
	var i = document.createElement("img",{"is":"x-img"});
	i.setAttribute("src", alph.selection.spans[0].src);
	i.setAttribute("origin",alph.selection.spans[0].origin);
	i.setAttribute("extent",alph.selection.spans[0].extent);
	f.appendChild(i);
	this.postEDL(f);
	//this.findSomeMatchingContent(f);
	this.paintLinkTerminals();
	break;
    case "put back to server" :

	this.exportContextToURL(this.actionTarget,this.actionTarget.id);

	break;
    case "reload":
	this.loadExternal(this.actionTarget.id);
	break;
    case "link to this noodle":
	break;
    case "link from this noodle":
	break;
    case "link to this resource":
	var n;
	if(this.actionTarget.tagName == "X-FLOATER"){
	    if(this.actionTarget.classList.contains("noodleBox")){
		n = {"@id" : this.actionTarget.querySelector("x-text").getAttribute("src") };
	    } else {
		n = { "@id" : this.actionTarget.id };
	    }
	} else if(this.actionTarget.hasAttribute("src")){
	    n =	{ "@id" : this.actionTarget.getAttribute("src") };
	}
	this.linkMunglor( alph.selection.toLinkMunglorNode(), n );
	break;
    case "link from this resource":
	var n;
	if(this.actionTarget.tagName == "X-FLOATER"){
	    if(this.actionTarget.classList.contains("noodleBox")){
		n = {"@id" : this.actionTarget.querySelector("x-text").getAttribute("src") };
	    } else {
		n = { "@id" : this.actionTarget.id };
	    }
	} else if(this.actionTarget.hasAttribute("src")){
	    n =	{ "@id" : this.actionTarget.getAttribute("src") };
	}
	this.linkMunglor(n, alph.selection.toLinkMunglorNode() );	    
	break;
    case "link to this fragment":
	/* This should only be coming from elements with a "src" attribute.
	   They may or may not have "origin" and "extent" attributes. */
	if(this.actionTarget.hasAttribute("origin") &&
	   this.actionTarget.hasAttribute("extent")){
	    this.linkMunglor(
		alph.selection.toLinkMunglorNode() ,
		{"@type":"AlphSpan",
		 "@id" : this.actionTarget.getAttribute("src") + "#" +
		 this.actionTarget.getAttribute("origin") + "-" +
		 this.actionTarget.getAttribute("extent") ,
		 "src" : this.actionTarget.getAttribute("src"),
		 "origin" : this.actionTarget.getAttribute("origin"),
		 "extent" : this.actionTarget.getAttribute("extent") }
	    );
	} else {
	    this.linkMunglor(
		alph.selection.toLinkMunglorNode() ,
		{"@id" : this.actionTarget.getAttribute("src")});
	}
	break;
    case "link from this fragment":
	if(this.actionTarget.hasAttribute("origin") &&
	   this.actionTarget.hasAttribute("extent")){
	    this.linkMunglor({"@type":"AlphSpan",
			      "@id" : this.actionTarget.getAttribute("src") + "#" +
			      this.actionTarget.getAttribute("origin") + "-" +
			      this.actionTarget.getAttribute("extent") ,
			      "src" : this.actionTarget.getAttribute("src"),
			      "origin" : this.actionTarget.getAttribute("origin"),
			      "extent" : this.actionTarget.getAttribute("extent") },
			     alph.selection.toLinkMunglorNode()
			    );
	} else {
	    this.linkMunglor({"@id" : this.actionTarget.getAttribute("src")},
			     alph.selection.toLinkMunglorNode() );
	}

	break;
    case "link to area": // Comes from x-img selection menu
	this.linker.show(null, alph.selection.toLinkMunglorNode());
	/* Boy, this is a corny workaround! */
	setTimeout(function(n){ n.pickUp(); }.bind(this, this.linker.nexusA), 100);
	break;
    case "link from area": // Comes from x-img selection menu
	this.linker.show(alph.selection.toLinkMunglorNode(),null);
	/* Boy, this is a corny workaround! */
	setTimeout(function(n){ n.pickUp(); }.bind(this, this.linker.nexusB), 100);
	break;
    case "make 🅐 node":
	this.linker.spanChange(alph.selection.toLinkMunglorNode(), this.linker.nexusA);
	break;
    case "make 🅑 node":
	this.linker.spanChange(alph.selection.toLinkMunglorNode(), this.linker.nexusB);
	break;
    case "headlinks" :
	/* Look for <link> elements in the shadow DOM's <head> (if there is one)
	   and fire-off a context menu with them in it, */
	var sdom = this.actionTarget.querySelector("x-dom").shadowRoot;
	this.activeMenu = this.headLinkMenu(sdom);
	break;
    case "respect whitespace" : 
	this.actionTarget.style.whiteSpace = "pre-wrap";
	break;
    case "set colours" :

	/* Create color-picker submenu */
	
	var menu = this.contextMenuOn(this.actionTarget,[]);
	menu.grace = true; // Submenus must be given grace, so that they're
	// not removed on mouseup
	
	var borderPicker = Docuplextron.colorSwatches(null,null,"BORDER/GROUP COLOR");
	borderPicker.action = function(c1,c2){
	    this.style.borderColor = c1;
	}.bind(this.actionTarget);
	menu.appendChild(borderPicker);
    
	var colorPicker = Docuplextron.colorSwatches(null,null,"BODY COLOR");
	colorPicker.action = function(c1,c2){
	    this.style.backgroundColor = c1;
	    this.style.color = c2;
	}.bind(this.actionTarget);
	menu.appendChild(colorPicker);

	this.activeMenu = menu;
	break;
    case "select colour group" :
	var floaters = this.floaterManager.getMembers();
	var col = this.actionTarget.style.borderColor;
	this.activate(this.actionTarget);
	for(var ix in floaters){
	    if(floaters[ix].style.borderColor == col &&
	       floaters[ix] != this.actionTarget){
		this.activate(floaters[ix],true);
	    }
	}
	break;
    case "ignore whitespace" : 
	this.actionTarget.style.whiteSpace = "normal";
	break;
    case "source info" :
	if(this.actionTarget.returnAddress){
	    var baseSrc = this.actionTarget.src.split("?")[0];
	} else {
	    var baseSrc = this.actionTarget.getAttribute("src").split("?")[0];
	}
	// Fire getDescription() synchronously just in case the metadata is old.
	alph.getDescription(baseSrc,true);
	var infoFloater = this.floaterManager.create("dptronUI", null,
						     this.lastX,
						     this.lastY
						    );
	infoFloater.style.width = "500px";
	infoFloater.tab.text.textContent = "Metadata for " + baseSrc;
	alph.makeSourceInfo(baseSrc,infoFloater );
	break;
    case ">>target" : 
	this.commitTarget = this.actionTarget.getAttribute("src");
	document.getElementById("alph-posttarget").value = this.commitTarget;	
	break;
    case "edit metadata" : 
	var url = this.actionTarget.getAttribute("src").split("?")[0];
	this.loadExternal(url + "?editmeta");
	break;
    case "links to this fragment" :
	if(this.actionTarget.src){
	    var span = new AlphSpan(this.actionTarget.src,
				    this.actionTarget.origin,
				    this.actionTarget.extent);
	} else {
	    var span = new AlphSpan(this.actionTarget.getAttribute("src"),
				    this.actionTarget.getAttribute("origin"),
				    this.actionTarget.getAttribute("extent"));
	}
	alph.queryLinks(span,
		       	this.floaterManager.create(null, null,
						     this.lastX,
						     this.lastY ) );
	break;
    case "links to this resource" :
	if(this.actionTarget.src){
	    var baseSrc = this.actionTarget.src.split("?")[0];
	} else {
	    var baseSrc = this.actionTarget.getAttribute("src").split("?")[0];
	}
	var span = new AlphSpan(baseSrc);
	alph.queryLinks(span,
		   this.floaterManager.create(null, null,
						this.lastX,
						this.lastY ) );
	break;
    case "clear colours":
	this.actionTarget.style.borderColor = "";
	this.actionTarget.style.color = "";
	this.actionTarget.style.backgroundColor = "";
	break;
    case "close" : 
	// Don't actually remove the scratchpad from the DOM.
	if(this.actionTarget.contains(this.scratchpad)){
	    this.actionTarget.style.display = "none";
	} else {
	    // If we're removing a floater with an iframe, be sure to remove the
	    // EDL of its document.
	    if(this.actionTarget.querySelector("iframe")){
		this.removeEDL(this.actionTarget.querySelector("iframe"));
	    }
	    this.removeEDL(this.actionTarget.id);
	    this.actionTarget.scurry();
	    setTimeout(function(){
		this.paintLinkTerminals();
	    }.bind(this), 1000);
	}
	break;
    case "close and delete" :
	// Close a noodleBox and delete the noodle it contains from localStorage
	var noodleID = this.actionTarget.querySelector("x-text").getAttribute("src").substr(1);
	this.actionTarget.destroy();
	if (confirm("Are you SURE you want to delete noodle ~" + noodleID + "?")){
	    this.noodleManager.remove(noodleID);
	}
	this.updateSourcesFlap();
	break;
    case "title" :
	var noodleID = (this.actionTarget.querySelector("x-text")) ?
	    this.actionTarget.querySelector("x-text").getAttribute("src") :
	    false;
	var dialog = Docuplextron.newDialog("input","Title for this noodle:");
	if(noodleID && noodleID.startsWith("~")){
	    var noodle = this.noodleManager.get(noodleID);
	    if(noodle.title) dialog.input.value = noodle.title;
	    dialog.okbutton.onclick = function(noodle, dialog, noodleBox){
		noodle.title = dialog.input.value;
		noodleBox.tab.text.textContent = noodle.title;
		dialog.floater.splode();
	    }.bind(this, noodle, dialog ,this.actionTarget);
	} else {
	    // Just change the ID of the floater
	    dialog.input.value = this.actionTarget.id;
	    dialog.okbutton.onclick = function(dialog,floater){
		floater.id = dialog.input.value;
		floater.tab.text.textContent = dialog.input.value;
		dialog.floater.splode();
	    }.bind(this,dialog,this.actionTarget);
	}
	break;
    case "progenitors" :
	break;
    case "set as wallpaper":
	this.mainDiv.style.background = "url('" + this.actionTarget.src + "') 0px 0px/100vw 100vh";
	break;
    case "flip horizontal":
	if (this.actionTarget.style.transform &&
	    this.actionTarget.style.transform.includes("rotate")){
	    this.actionTarget.style.transform = "";
	} else {
	    this.actionTarget.style.transform = "rotateY(180deg)";
	}
	break;
    case "get context document":
	if(this.actionTarget.src){
	    var xsrc = this.actionTarget.src.split("?")[0];
	} else {
	    var xsrc = this.actionTarget.getAttribute("src").split("?")[0];
	}
	/* If we have metadata for this resource, and it has a "viewingContext" 
	   property, bring-up another menu with all of the linked contexts.*/
	if(alph.sources[xsrc]["viewingContext"]){
	    var vCon = alph.sources[xsrc]["viewingContext"];
	    var vContexts = [];
	    console.log("vCon: ", vCon); 
	    if(typeof(vCon) == "string"){
		/* A single viewing context, as an IRI */
		vContexts.push( {"@value": vCon,
				 "name" : "" });		
	    } else if(vCon instanceof Array){
		/* An array... */
		for(var ix in vCon){
		    /* Convert any/all string types to objects*/
		    if(typeof(vCon[ix]) == "string"){
			vCon[ix] = { "@value" : vCon[ix], "name" : "" }; 
		    }
		}
		vContexts = vCon;
	    } else {
		/* A single object. */
		vContexts.push(vCon);
	    }
	    
	    console.log("vContexts", vContexts);

	    if(vContexts.length > 0){
		var menu = this.contextMenu();
		for (var ix in vContexts){
		    var menuItem = Docuplextron.menuItem(
			(vContexts[ix]["name"] || "") + ": " +
			    (vContexts[ix]["@value"] || "!!bad link!!"));
		    
		    menuItem.onmouseup = function(url){
			this.loadExternal(url);
		    }.bind(this, vContexts[ix]["@value"]);

		    menu.appendChild(menuItem);
		}
		menu.getOnScreen();
		this.activeMenu = menu;
	    }
	}
	break;
    case "get source media" :
	if(this.actionTarget.src){
	    this.loadExternal(this.actionTarget.src.split("?")[0]);
	} else {
	    this.loadExternal(this.actionTarget.getAttribute("src").split("?")[0]);
	}
	break;
    case "load all sources" :
	var sources = Object.keys(alph.sources);
	for(var ix = 0; ix < sources.length; ix++){
	    if(sources[ix].startsWith("http")){
		this.loadExternal(sources[ix]);
	    }
	}
	break;
    case "widen context(raw)" :
	if(this.actionTarget.src){
	    var xsrc = this.actionTarget.src;
	    var origin = parseInt(this.actionTarget.origin);
	    var extent = parseInt(this.actionTarget.extent);
	} else {
	    var xsrc = this.actionTarget.getAttribute("src");
	    var origin = parseInt(this.actionTarget.getAttribute("origin"));
	    var extent = parseInt(this.actionTarget.getAttribute("extent"));
	}
	origin = Math.max(origin - 5000, 0);
	extent = Math.min(extent + 5000, alph.sources[xsrc]["alph:textLength"]);
	this.loadExternal(xsrc + Alphjs.TRANSCLUDE_QUERY + origin + "-" + extent);
	break;
    case "unstick" :
	this.actionTarget.stickyToggle();
	break;
    case "stick" :
	this.actionTarget.stickyToggle();
	break;
    case "(un)lock width" : 
	if(!this.actionTarget.style.width ||
	   this.actionTarget.style.width == "auto"){
	    var scale = (this.actionTarget.style.transform) ?
		parseFloat(this.actionTarget.style.transform.split("scale(")[1]) :
		1;
	    this.actionTarget.style.width = (this.actionTarget.getBoundingClientRect().width / scale) + "px";
	} else {
	    this.actionTarget.style.width = null;
	}
	break;
    case "zoom to" :
	this.floaterManager.zoomTo(this.actionTarget);
	break;
    case "open/show noodle":
	// Put some tests in here, Adam.
	var noodleID = this.actionTarget.getAttribute("data-noodleid");
	this.openNoodle(noodleID);
	break;
    case "delete noodle":
	this.noodleManager.remove(this.actionTarget.getAttribute("data-noodleid"));
	this.actionTarget.remove();
	break;
    case "pin":
	this.setPin(this.actionTarget.getAttribute("data-sourceId"));
	this.updateSourcesFlap();
	break;
    case "unpin":
	this.removePin(this.actionTarget.getAttribute("data-sourceId"));
	this.updateSourcesFlap();	
	break;
    }
}



/*






  CONTEXT MENUS





*/

Docuplextron.prototype.headLinkMenu = function(source){

    /* 'source' should be a document, Node, DOM fragment, etc.  */

    var menu = this.contextMenu(true);
    
    var links = Array.from(source.querySelectorAll("link"));
    for(l in links){
	var link = links[l];
	var menuItem = Docuplextron.menuItem(
	    link.getAttribute("rel") + ": " + link.getAttribute("href"));
	
	menuItem.onmouseup = function(url, menuItem){
	    /* Should do some simplepointer voodoo in here ...*/
	    this.loadExternal(url);
	}.bind(this, link.getAttribute("href"), menuItem);
	
	menu.appendChild(menuItem);
    }
    console.log("Trying to make headlinks menu for", source, "Made menu:", menu);
    menu.getOnScreen();
    
    return menu;

}

Docuplextron.prototype.linkContextMenu = function(target){

    /* This gets called when a user clicks on a SimpleNexus. */
    
    var menu = this.contextMenu();
    
    var linkMenuHead = Docuplextron.menuItem( "NODE: " + target["@id"] );
    linkMenuHead.classList.add("alph-contextmenutitle");
    linkMenuHead.onmouseup = function(node){
	// ...? nodeMunglor()? But which graph document?
    }.bind(this,target);
    menu.appendChild(linkMenuHead);

    for(var graphID in this.linkStore.graphs){
	var graph = this.linkStore.graphs[graphID];
	var alinks = Array.from(graph.graph.querySelectorAll("[anode='" + target["@id"] + "']"));
	var blinks = Array.from(graph.graph.querySelectorAll("[bnode='" + target["@id"] + "']"));

	if(alinks.length == 0 && blinks.length == 0){
	    continue;
	} else {
	    var graphName = (graph["name"]) ? graph["name"] : "";
	    var graphHead = Docuplextron.menuItem( "  IN GRAPH: " + graphName + " [" + graphID + "]");
	    graphHead.classList.add("alph-contextmenudivider");
	    graphHead.title = "Click to modify this node in graph " + graphID;
	    // ...What action? How do we make it look different?
	    graphHead.onmouseup = function(nid, gid){
		var f = this.floaterManager.create("nonContextual");
		f.classList.add("linkEditor");
		f.classList.add("dptronUI");
		this.nodeMunglor(nid, gid, f);
	    }.bind(this, target["@id"], graphID);
	    menu.appendChild(graphHead);
	}
	
	/* Should we sort alinks by relation here? Probably. */
	
	for(var ix in alinks){
	    var rel = alinks[ix].getAttribute("rel");
	    var bnode = alinks[ix].getAttribute("bnode");
	    var bNodeName = "";
	    /* Try to get a name or title... */
	    if(bnode.startsWith("~")){
		bNodeName = this.noodleManager.get(bnode.split("#")[0]).title;
	    } else if(graph.nodes[bnode]["name"]) {
		bNodeName = graph.nodes[bnode]["name"];
	    } else if(alph.sources[bnode.split("#")[0]] &&
		      alph.sources[bnode.split("#")[0]]["name"]){
		bNodeName = alph.sources[bnode.split("#")[0]]["name"];
	    }
	    if(bNodeName != ""){
		var item = Docuplextron.menuItem("    → " + rel + ": " + bNodeName +
						 ((bnode.split("#")[1]) ? " [" + bnode.split("#")[1] + "]" : ""));
	    } else {
		var item = Docuplextron.menuItem("    → " + rel + ": " + bnode );
	    }
	    item.title = bnode;
	    item.onmouseover = function(bnode){
		if(this.simpleNexi[bnode]){
		    this.simpleNexi[bnode].flashNodes();
		}
	    }.bind(this,bnode)
	    item.onmouseout = function(bnode){
		if(this.simpleNexi[bnode]){
		    this.simpleNexi[bnode].deFlashNodes();
		}
	    }.bind(this,bnode)
	    item.onmouseup = function(bnode){

		/* This should check to see if the media is in the
		   workspace and, if so, zoom to it.  Otherwise, try
		   loadExternal(); */
		
		this.loadExternal(bnode);
	    }.bind(this,bnode);
	    menu.appendChild(item);
	}

	for(var ix in blinks){
	    var rel = blinks[ix].getAttribute("rel");
	    var anode = blinks[ix].getAttribute("anode");
	    var aNodeName = "";
	    /* Try to get a name or title... */
	    if(anode.startsWith("~")){
		aNodeName = this.noodleManager.get(anode.split("#")[0]).title;
	    } else if(graph.nodes[anode]["name"]) {
		aNodeName = graph.nodes[anode]["name"];
	    } else if(alph.sources[anode.split("#")[0]] &&
		      alph.sources[anode.split("#")[0]]["name"]){
		aNodeName = alph.sources[anode.split("#")[0]]["name"];
	    }
	    if(aNodeName != ""){
		var item = Docuplextron.menuItem("    → " + rel + ": " + aNodeName +
						 ((anode.split("#")[1]) ? " [" + anode.split("#")[1] + "]" : ""));
	    } else {
		var item = Docuplextron.menuItem("    → " + rel + ": " + anode );
	    }
	    item.title = anode;
	    item.onmouseover = function(anode){
		if(this.simpleNexi[anode]){
		    this.simpleNexi[anode].flashNodes();
		}
	    }.bind(this,anode)
	    item.onmouseout = function(anode){
		if(this.simpleNexi[anode]){
		    this.simpleNexi[anode].deFlashNodes();
		}
	    }.bind(this,anode)
	    item.onmouseup = function(anode){
		this.loadExternal(anode);
	    }.bind(this,anode);
	    menu.appendChild(item);
	}
    }
    
    menu.getOnScreen();
    
    return menu;
    
}

Docuplextron.prototype.linkLineContextMenu = function(line){

    var anode = LinkStore.nodify(line.getAttribute("anode"));
    var bnode = LinkStore.nodify(line.getAttribute("bnode"));
    var link = {"nodeA" : anode,
		"nodeB" : bnode,
		"rel" : line.getAttribute("rel") };
    
    var menu = this.contextMenu();
    var title = Docuplextron.menuItem(line.getAttribute("rel"));
    title.classList.add("alph-contextmenutitle");
    menu.appendChild(title);

    var d = Docuplextron.menuItem("delete link");
    d.onmouseup = function(link, graphId){
	this.linkStore.removeFrom(link, graphId);
	this.paintLinkTerminals();
    }.bind(this, link, line.getAttribute("graph"));
    menu.appendChild(d);

    
    menu.getOnScreen();
    
    return menu;
}


Docuplextron.prototype.contextMenuOn = function(target,items){

    //if(!target.returnAddress){
	this.actionTarget = target;
    //}
    
    var menu = this.contextMenu();
    
    /* Put a title or heading on the menu */
    var title =  document.createElement("div");
    title.classList.add("alph-contextmenuitem");
    title.classList.add("alph-contextmenutitle");
    if(target.tagName == "X-FLOATER"){
	if(target.classList.contains("noodleBox")){
	    var noodleId = target.querySelector("x-text").getAttribute("src");
	    title.textContent = "Noodle " + noodleId;
	} else if(target.id){
	    title.textContent = Docuplextron.titleTrim(target.id);
	}
	menu.appendChild(title);

    } else {
	if(target.id){
	    title.textContent = target.id;
	    menu.appendChild(title);
	}
    }    
    
    
    /* Create the menu with the passed items if any were passed */

    if(items){
	for(var ix in items){
	    menu.appendChild(Docuplextron.menuItem(items[ix]));
	}
    } else {
	
	// Some defaults for various menu targets...
	if(target.tagName == "X-FLOATER"){
	    menu.appendChild( (target.getAttribute("data-sticky") == "true") ?
			      Docuplextron.menuItem("unstick") : Docuplextron.menuItem("stick") );
	    menu.appendChild(Docuplextron.menuItem("title"));
	    if(target.querySelector("x-dom")){
		menu.appendChild(Docuplextron.menuItem("headlinks"));
		menu.appendChild(Docuplextron.menuItem("edit head"));
	    }
	    menu.appendChild(Docuplextron.menuItem("(un)lock width"));	    
	    menu.appendChild(Docuplextron.menuItem("zoom to"));
	    menu.appendChild(Docuplextron.menuItem("set colours"));
	    menu.appendChild(Docuplextron.menuItem("clear colours"));
	    menu.appendChild(Docuplextron.menuItem("select colour group"));
	    menu.appendChild(Docuplextron.menuItem("export as plain-text"));
	    if(target.id.startsWith("http:") ||
	       target.id.startsWith("https:")){
		menu.appendChild(Docuplextron.menuItem("link to this resource"));
		menu.appendChild(Docuplextron.menuItem("link from this resource"));
	    }
	    if(!target.classList.contains("noodleBox")){
		menu.appendChild(Docuplextron.menuItem("fetch and fill"));
		if(target.style.whiteSpace == "normal"){
		    menu.appendChild(Docuplextron.menuItem("respect whitespace"));
		} else {
		    menu.appendChild(Docuplextron.menuItem("ignore whitespace"));
		}
		menu.appendChild(Docuplextron.menuItem("export to view"));
		menu.appendChild(Docuplextron.menuItem("export to URL"));
	    }
	    //menu.appendChild(Docuplextron.menuItem("generate EDL"));
	    //menu.appendChild(Docuplextron.menuItem("load all sources"));
	    if( (target.id.startsWith("http://") ||
		 target.id.startsWith("https://")) &&
		!target.id.includes("#") ){
		menu.appendChild(Docuplextron.menuItem("put back to server"));
		menu.appendChild(Docuplextron.menuItem("reload"));
	    }
	    menu.appendChild(Docuplextron.menuItem("close"));
	       
	} else if(target.returnAddress || target.hasAttribute("src")){
	    menu.appendChild(Docuplextron.menuItem("source info"));
	    menu.appendChild(Docuplextron.menuItem("widen context(raw)"));
	    if(Object.keys(alph.sources).includes(target.getAttribute("src"))){
		if(alph.sources[target.getAttribute("src")]["viewingContext"]){
		    menu.appendChild(Docuplextron.menuItem("get context document"));
		}
	    }
	    menu.appendChild(Docuplextron.menuItem("get source media"));
	    //menu.appendChild(Docuplextron.menuItem("links to this fragment"));
	    //menu.appendChild(Docuplextron.menuItem("links to this resource"));
	    menu.appendChild(Docuplextron.menuItem("link to this fragment"));
	    menu.appendChild(Docuplextron.menuItem("link from this fragment"));
	    menu.appendChild(Docuplextron.menuItem("link to this resource"));
	    menu.appendChild(Docuplextron.menuItem("link from this resource"));
	    if(target.tagName == "X-TEXT"){
		menu.appendChild(Docuplextron.menuItem(">>target"));
	    }
	    menu.appendChild(Docuplextron.menuItem("edit source metadata"));
	    if(target.tagName == "IMG"){
		menu.appendChild(Docuplextron.menuItem("set as wallpaper"));
		menu.appendChild(Docuplextron.menuItem("flip horizontal"));
	    }
	} else if(target.tagName == "LI" &&
		  target.hasAttribute("data-noodleid")){

	    /* This is a noodle in the SOURCES flap */

	    menu.appendChild(Docuplextron.menuItem("open/show noodle"));
	    menu.appendChild(Docuplextron.menuItem("delete noodle"));

	} else if(target.tagName == "LI" &&
		  target.hasAttribute("data-sourceId")){

	    /* This is a network resource in the SOURCES flap */

	    if (this.pins.indexOf(target.getAttribute("data-sourceId")) >= 0){
		menu.appendChild(Docuplextron.menuItem("unpin"));
	    } else {
		menu.appendChild(Docuplextron.menuItem("pin"));
	    }
	}
	
    }

    menu.getOnScreen();
    
    return menu;
}

Docuplextron.prototype.contextMenu = function(sub){
    
    /* Make the shell of a context menu and return it.*/
    
    var menu = document.createElement("div");
    menu.setAttribute("class","alph-contextmenu");
    menu.style.top = (this.lastY - 5) + "px";
    menu.style.left = (this.lastX - 5) + "px";
    menu.grace = sub || false;
    
    this.floaterDiv.appendChild(menu);

    menu.getOnScreen = function(){
	
	/* Call me after menu is populated with items. I'll get the 
	   menu fully on-screen if it ain't. */
	
	var menuRect = this.getBoundingClientRect();
	if (menuRect.left < 0) this.style.left = "0px";
	if (menuRect.top < 0) this.style.top = "0px";
	if (menuRect.right > window.innerWidth) {
	    this.style.left = (window.innerWidth - menuRect.width) + "px";
	}
	if (menuRect.bottom > window.innerHeight){
	    this.style.top = (window.innerHeight - menuRect.height) + "px";
	}
    }
    
    return menu;    
}

Docuplextron.menuItem = function(text){
    var item = document.createElement("div");
    item.setAttribute("class","alph-contextmenuitem");
    item.textContent = text;
    item.onmouseup = function(cmd){
	dptron.activeMenu.remove();
	dptron.activeMenu = null;
	dptron.command(cmd);
    }.bind(this, text);
    return item;
}

/*






  LINK EDITOR






*/
function NodeForm(alphspan){
    this.span = alphspan || new AlphSpan();
    this.form = document.createElement("form");
    this.src = document.createElement("input");
    this.origin = document.createElement("input");
    this.extent = document.createElement("input");
    this.name = document.createElement("input");
    
    this.form.nodeForm = this;
    
    this.form.appendChild(this.src);
    this.form.appendChild(this.origin);
    this.form.appendChild(this.extent);
    this.form.appendChild(this.name);
    
    this.src.setAttribute("placeholder","Media URI");
    this.src.value = this.span.src;
    this.origin.placeholder = "Origin";
    this.origin.value = this.span.origin || "";
    this.extent.placeholder = "Extent";
    this.extent.value = this.span.extent || "";
    this.name.placeholder = "Name";
    this.name.value = this.span.name || "";
    
    this.src.onchange = function(){
	this.span.src = this.src.value;
    }.bind(this);

    this.origin.onchange = function(){
	this.span.origin = this.origin.value;
    }.bind(this);
    
    this.extent.onchange = function(){
	this.span.extent = this.extent.value;
    }.bind(this);

}

Docuplextron.SpanForm = function(span){
    if(!span){
	span = { "origin" : null,
		      "extent" : null,
		      "src" : null,
		      "type" : "AlphSpan" };
    }
    
    this.form = document.createElement("form");
    this.src = document.createElement("input");
    this.origin = document.createElement("input");
    this.extent = document.createElement("input");
    this.remover = document.createElement("button");
    
    this.form.nodeForm = this;
    
    this.form.appendChild(this.src);
    this.form.appendChild(this.origin);
    this.form.appendChild(this.extent);
    this.form.appendChild(this.remover);
    
    this.src.setAttribute("placeholder","Media URI");
    this.src.value = span["src"] || "";
    this.origin.placeholder = "Origin";
    this.origin.value = span["origin"] || "";
    this.extent.placeholder = "Extent";
    this.extent.value = span["extent"] || "";
    this.remover.textContent = "X";

    this.remover.onclick = function(){
	this.remove();
    }.bind(this.form);

    return this.form;
}

function linkEdit(link){
    /* 
       A link editor!
       
    */
    
    var floater = dptron.floaterManager.create(
	"linkEditor","Link Editor");
    floater.classList.add("nonContextual");
    floater.classList.add("dptronUI");

    var formContainer = document.createElement("div");
    formContainer.style.padding = "0px";
    floater.appendChild(formContainer);

    var bodyNodes = document.createElement("div");
    var targetNodes = document.createElement("div");
    var props = document.createElement("div");
    
    var btnAddBodyNode = document.createElement("button");
    var btnAddTargetNode = document.createElement("button");
    var btnTargetFromSelection = document.createElement("button");
    var btnCreate = document.createElement("button");

    
    var attrsHeader = document.createElement('h3');
    attrsHeader.textContent = "ATTRIBUTES";
    var bodyHeader = document.createElement('h3');
    bodyHeader.textContent = "BODY nodes";
    var targetHeader = document.createElement('h3');
    targetHeader.textContent = "TARGET nodes";

    formContainer.appendChild(attrsHeader);
    formContainer.appendChild(props);
    formContainer.appendChild(bodyHeader);
    formContainer.appendChild(bodyNodes);
    formContainer.appendChild(targetHeader);
    formContainer.appendChild(targetNodes);
    formContainer.appendChild(btnCreate);

    if(!link){

	/* If no link has been passed, we'll just create a blank one.
	   If the link editor was summoned while there was an active selection,
	   we'll make that selection the body. */
	
	var link = { "id" : "~" + Date.now().toString(36).toUpperCase() ,
		     "type" : ["AlphLink"],
		     "rel" : "" }	
	
	if(alph.selection){
	    for(var ix in alph.selection.spans){
		bodyNodes.appendChild(new NodeForm(alph.selection.spans[ix]).form);
	    }
	} else {
	    // If there's no selection, just a completely blank body.
	    bodyNodes.appendChild(new NodeForm().form);
	}
	
	targetNodes.appendChild(new NodeForm().form);
    } else {
	
	/* Make the body/target editors from the passed link. */
	for (var ix in link.body.items){
	    bodyNodes.appendChild(
		new NodeForm(link.body.items[ix]).form
	    );
	}

	for (var ix in link.target.items){
	    targetNodes.appendChild(
		new NodeForm(link.target.items[ix]).form
	    );
	}
    }

    /* Fill-out the properties for the form. */
    var dismemberedLink = {};
    Object.assign(dismemberedLink,link);
    delete dismemberedLink.body;
    delete dismemberedLink.target;
    props.appendChild(Docuplextron.attributeForm(dismemberedLink));

    // Put the buttons on
    bodyNodes.appendChild(btnAddBodyNode);
    targetNodes.appendChild(btnAddTargetNode);
    targetNodes.appendChild(btnTargetFromSelection);

    // And define some button behavior...
    btnCreate.textContent = "OK";
    btnCreate.onclick = function(props,bodyNodes,targetNodes,floater){
	var newLink = {};
	var newBody = {"type":"List","items":[]};
	var newTarget = {"type":"List","items":[]};
	newLink.body = newBody;
	newLink.target = newTarget;
	
	// Create link object properties
	var propKeys = Array.from(props.querySelectorAll(".attributeKey"));
	for(var ix in propKeys){
	    var key = propKeys[ix].value;
	    var val = propKeys[ix].nextSibling.value;
	    if(val.toLowerCase() == "null" || val == "NaN" || val == ""){
		val = null;
	    } else if (key == "type" || key == "@type"){
		val = val.split(" ");
	    }
	    newLink[key] = val;
	}
	
	// Create link body
	var bodyNodeForms = Array.from(bodyNodes.querySelectorAll("form"));
	for(var ix in bodyNodeForms){
	    var newSpan = new AlphSpan(
		bodyNodeForms[ix].children[0].value,
		bodyNodeForms[ix].children[1].value,
		bodyNodeForms[ix].children[2].value,
		bodyNodeForms[ix].children[3].value,
	    );
	    
	    newBody.items.push(newSpan);
	}
	// Create link target
	var targetNodeForms = Array.from(targetNodes.querySelectorAll("form"));
	for(var ix in targetNodeForms){
	    newTarget.items.push(
		new AlphSpan(
		    targetNodeForms[ix].children[0].value,
		    targetNodeForms[ix].children[1].value,
		    targetNodeForms[ix].children[2].value,
		    targetNodeForms[ix].children[3].value
		)
	    );
	}
	
	// Add to the linkStore //!2112
	this.linkStore[newLink.id] = newLink; //!2113
	this.updateLinksFlap();
	this.paintLinkTerminals();
	floater.destroy();
    }.bind(dptron,props,bodyNodes,targetNodes,floater);
    
    btnTargetFromSelection.textContent = "From Selection";
    btnTargetFromSelection.onclick = function(){
	// Remove all old NodeForms
	var oldForms = Array.from(this.querySelectorAll("form"));
	for(var ix in oldForms){
	    oldForms[ix].remove();
	}
	var btns = this.firstElementChild;
	for(var ix in alph.selection.spans){
	    this.insertBefore(new NodeForm(alph.selection.spans[ix]).form,btns);
	}
    }.bind(targetNodes);
  
    btnAddBodyNode.textContent = "+";
    btnAddBodyNode.onclick = function(){
	this.parentElement.insertBefore(new NodeForm().form,this);
    }.bind(btnAddBodyNode);
    
    btnAddTargetNode.textContent = "+";
    btnAddTargetNode.onclick = function(){
	this.parentElement.insertBefore(new NodeForm().form,this);
    }.bind(btnAddTargetNode);

}

function oldLinkEdit(){
    var anchor = Alphjs.surround("a");
    anchor.setAttribute("href","alphLinkEditor");
    var floater = dptron.floaterManager.create(null,"alphLinkEditor",dptron.lastX,dptron.lastY);
    floater.style.width = "auto";

    var foo = SimplePointer(anchor, floater);
    
    var relations = document.createElement("select");
    relations.style.display = "block";
    floater.appendChild(relations);

    var relation = document.createElement("input");
    floater.appendChild(relation);
    relation.style.display = "block";
    relation.setAttribute("disabled",true);

    // https://www.iana.org/assignments/link-relations/link-relations.xml
    var rels = ["","[custom]","about", "alternate", "appendix", "archives", "author",
		"blocked-by", "bookmark", "canonical", "chapter", "collection",
		"contents", "copyright",
    "create-form", "current", "derivedfrom", "describedby", "describes", "disclosure",
    "dns-prefetch", "duplicate", "edit", "edit-form", "edit-media", "enclosure",
    "first", "glossary", "help", "hosts", "hub", "icon", "index", "item", "last",
    "latest-version", "license", "lrdd", "memento", "monitor", "monitor-group",
    "next", "next-archive", "nofollow", "noreferrer", "original", "payment",
    "pingback", "preconnect", "predecessor-version", "prefetch", "preload", "prerender",
    "prev", "preview", "previous", "prev-archive", "privacy-policy", "profile",
    "related", "replies", "search", "section", "self", "service", "start",
    "stylesheet", "subsection", "successor-version", "tag", "terms-of-service",
    "timegate", "timemap", "type", "up", "version-history", "via", "webmention",
    "working-copy", "working-copy-of"];
    for (var ix in rels){
	var opt = document.createElement("option");
	opt.value = rels[ix];
	opt.text = rels[ix];
	relations.add(opt);
    }
    relations.onchange = function(){
	relation.value = this.value;
	if(this.value == "[custom]"){
	    relation.removeAttribute("disabled");
	} else {
	    relation.setAttribute("disabled",true);
	}
    }.bind(relations,relation);    
    
    var title = document.createElement("input");
    title.setAttribute("placeholder","Title");
    title.style.display = "block";
    floater.appendChild(title);

    var target = document.createElement("input");
    target.id = "linkEditorTarget";
    target.setAttribute("placeholder","URL");
    target.style.display = "block";
    floater.appendChild(target);

    var btn = document.createElement("input");
    btn.setAttribute("type","button");
    btn.value = "link";
    btn.onclick = function(relations,title,object,subject,floater){
	if(title.value != ""){
	    object.setAttribute("title",title.value);
	}
	object.setAttribute("rel",relations.value);
	object.setAttribute("href",subject.value);
	//object.setAttribute("href",alph.selection.asString());
	floater.destroy();
	//dptron.pushAllLinks();
    }.bind(btn,relations,title,anchor,target,floater);
    floater.appendChild(btn);

    var cancel = document.createElement("input");
    cancel.setAttribute("type","button");
    cancel.value = "cancel";
    cancel.onclick = function(relations,title,object,subject,floater){
	Docuplextron.deParent(object);
	Alphjs.joinContiguousSpans();
	floater.remove();
	floater = null;
    }.bind(cancel,relations,title,anchor,target,floater);
    floater.appendChild(cancel);
    /*
    var help = document.createElement("p");
    help.innerHTML = '<small>Select a <a href="https://www.iana.org/assignments/link-relations/link-relations.xml" target="new">link relation</a> from the drop-down, and give the link a title if it needs one; then, you can either type (or paste) a URL into the target field, or make a selection of the target media you wish to link to and the target field will be filled automatically.</small>';
    floater.appendChild(help);
    */
}

/*





  IMPORT / EXPORT





*/

Docuplextron.prototype.loadExternalFromBar = function(url,as){
    /* Validate input from the loadbar and then fire-off loadExternal() */

    var prefix = "";
    
    if(url == "") return;

    /* We have a couple of special prefixes to handle.*/
    if(url.startsWith("ld+") || url.startsWith("as+")){
	prefix = url.substr(0,3);
	url = url.substr(3);
    }

    /* If a protocol was specified, make sure it's http(s)://  */
    if(url.startsWith("tag:")){
	console.log("loading a tag: url, and....?");
	/* This one's okay, actually. */
    } else if(url.startsWith("~")){
	/* That's a noodle, I reckon. Not really a 'load external'
	   situation at all, but we'll let it through. */
    } else if(url.startsWith("_:")){
	/* This is a blank node. We'll let it through and look
	   for this node in the LinkStore. */
    } else if(url.startsWith("mailto:")){
	this.pukeText("We can't load email addresses!");
	return;
    } else if(/^[a-zA-Z]+:\/\//.test(url)){
	/* This could be http(s)://, file://, gopher://, ... */
	if(! /^https?:\/\//.test(url)){
	    /* We don't want file://, gopher://, etc. 
	       Now... is this a SMART THING TO DO? Probably not.
	       But we'll see how it goes. */
	    url = url.replace(/^[a-zA-Z]+:\/\//, "http://");
	}
    } else {
	// No protocol given. Prepend http://
	url = "http://" + url;
    }
    
    /* Update the text in the loadbar */
    document.getElementById("alph-loadbox").value = url;
    this.loadExternal(prefix+url, as);
}

Docuplextron.prototype.loadExternal = function(url,as){

    if(url == ""){
	/* Don't try to load an empty URL. Really, there should be some 
	   validation going on here. */
	return;
    } else if(url.startsWith("ld+")){
	/* These are an ugly way to change how we request a document. If the
	   Load: bar is given a URL that starts with "ld+", then we will send
	   an "Accept: application/ld+json" header in the request... */
	url = url.substr(3);
	as = "ld";
    }else if(url.startsWith("as+")){
	/* ...and if we prefix the URL with "as+", we send 
	   "Accept: application/activity+json". */
	url = url.substr(3);
	as = "as";
    } else if(url.startsWith("~")){
	/* We'll open a noodle in a noodleBox, but we don't ever open
	   fragments of noodles, so we'll be sure to chop any fragment
	   selector off, if there is one. Also, openNoodle() wants just
	   the alphanumeric ID, not the tilde, so we chop that, too. */
	if(url.includes("#")){
	    this.openNoodle(url.substring(1,url.lastIndexOf("#")));
	} else {
	    this.openNoodle(url.substr(1));
	}
	return;
    }


    /* See if we've got this resource open in a floater already. If
       so, re-load the resource into its existing floater; if not,
       create a new floater. */
    
    var floater = document.getElementById(url);

    if(floater){
	
	/* If the resource being loaded is the parent context of the
	   workspace... I don't know. Do nothing? */
	
	if (url == window.location.toString()){
	    
	    return floater;

	}

	/* If we're loading a pinned resource, we normally destroy the
	   floater after it loads. However, if the resource is already
	   open in a floater we don't want to destroy it after it loads,
	   so reset the "as" flag. */
	
	if (as == "pin") as = "";
	
    } else {
	
	floater = this.floaterManager.create(null,url,this.lastX,this.lastY);
    }

    /* The remainder of this monstrous function is for http:// or 
       https:// URLs. Let's handle some other protocols here and
       bail out of the function ... */

    if(url.startsWith("tag:")){
	console.log("Handling tag: URL...?");
	
	return this.makeTagFloater(floater,url);
	
    } else if(url.startsWith("_:")){

	return this.loadBlankNode(floater, url);
    }

    

    /* Set the floater's tab text, and fill it with a simple
       "loading..." message. Call bringIntoView() on it, just in case
       it's off in the wilds somewhere. */
    
    floater.tab.text.textContent = url.substr(url.lastIndexOf("/"));
    
    floater.appendChild(document.createElement("div")).textContent = "Loading " + url + " ...\n";
    floater.querySelector("div").style.whiteSpace = "pre-wrap";
    floater.querySelector("div").style.fontFamily = "var(--dptronui-font)";

    /* I'm commenting this out because the behviour is actually very
       irritating, but I'm leaving this note so that I'll come up with
       a better alternative later. The reason I was calling
       bringIntoView() was to let the user know that floater contents
       had been changed or refreshed for floaters that were already in
       the workspace. Having the floater zoomed-in and repositioned in
       the workspace is actually very disruptive,
       though. So... instead, we should have a
       floater.prototype.flashTab() method or something... */
    
    //floater.bringIntoView();


    // Try to get metadata (synchronously) before loading the whole document
    
    var sourceName = url.split("?")[0].split("#")[0];
    if(!alph.sources[sourceName]){
	floater.querySelector("div").textContent += "Requesting metadata...\n";
	alph.getDescription(sourceName,true);
    }

    // Now that we have metadata, does this source support the
    // ?fragment interface? For the moment, we're only interested in
    // plain-text sources that support it.
    var sourceMeta = alph.sources[sourceName];
    if (sourceMeta["alph:interface"] &&
	sourceMeta["alph:interface"].includes("http://alph.io/interfaces/fragment") &&
	sourceMeta.mediaType.startsWith("text/plain")){
	console.log("fragment interface available on plain-text source.");
	// Send a ?fragment query instead of requesting the whole document?
	if(!url.includes("?")){
	    if(url.includes("#")){
		url = url.split("#");
		url = url[0] + Alphjs.TRANSCLUDE_QUERY + url[1];
	    }
	}
    }
    
    
    // Now make the request for the document
    
    var xhr = new XMLHttpRequest();
    xhr.onload = function(xhr,f,url, as){
	this.handleHTTPResponse(xhr, f, url, as);
    }.bind(this, xhr, floater, url, as); 
    xhr.open('GET',url);
    
    if(as){
	if(as == "ld"){
	    xhr.setRequestHeader("Accept","application/ld+json");
	    xhr.responseType = "json";
	} else if(as == "as"){
	    xhr.setRequestHeader("Accept","application/activity+json");
	    xhr.responseType = "json";
	}
    }

    floater.appendChild(new Text("Requesting media...\n"));

    xhr.send();
        
    return floater;
}

Docuplextron.prototype.handleHTTPResponse = function(xhr,f,url,as){
    if(xhr.status == 200){
	var baseUrl = url.split("?")[0].split("#")[0];
	    
	// Keep a reference to our SVG layer
	var foverlay = f.overlay;
	    
	if(f.querySelector("iframe")){
	    // We'll be replacing the iframe in the floater, so remove that
	    // iframe's EDL.
	    this.removeEDL(f.querySelector("iframe"));
	}
	
	// Empty the floater.
	var deadkids = Array.from(f.childNodes);
	for(var ix in deadkids){
	    deadkids[ix].remove();
	}
	
	
	/* Here's a fun thing: some sites (like textfiles.com)
	   don't send a Content-Type header at all for some files!
	   Ugh. So, set the mediatype to text/plain by default,
	   and then check to see if the server sent one. */
	
	var mediatype = "text/plain";
	
	if(xhr.getResponseHeader("Content-Type")){
	    mediatype = xhr.getResponseHeader("Content-Type");
	}
	
	// Different actions depending on rec'd media type...
	
	if(mediatype.startsWith("text/html")){
	    /* Is there a fragment identifier? If so, grab the fragment out of
	       the response document and load it into a floater. 
	       
	       *** Should I be doing this with a Shadow DOM as well, now?
	       J66.1288
	    */
	    
	    var parser = new DOMParser();
	    var responseDoc = parser.parseFromString(xhr.responseText,"text/html");
	    
	    /* Try to resolve any document fragments ... */
	    
	    
	    if(url.includes("#")){
		
		var fragID = url.split("#")[1];
		
		if(fragID.startsWith("xpath:")){
		    
		    fragID = fragID.substr(6);
		    var i = responseDoc.evaluate(
			fragID,
			responseDoc.documentElement,
			null,
			XPathResult.ANY_TYPE,
			null);
		    
		} else if(fragID.startsWith("element(")){
		    
		    var i = Alphjs.resolvePoint(
			fragID.slice(8,-1),
			responseDoc.documentElement)[0];
		    
		} else if(fragID.startsWith("range(")){
		    
		    /* Why isn't this here? The parts are all there in
		       Alph.js, aren't they? */
		    
		} else {
		    
		    var i = responseDoc.getElementById(fragID);
		    if(!i){
			i = document.createTextNode("Could not locate fragment '" + fragID + "'.");
		    }
		    
		}
		
		if(!i){
		    console.log("!",i);
		    f.appendChild(document.createTextNode("Well, that didn't work."));
		    return;
		}
		
  		i.setAttribute("resource",url);
		f.id = "[" + url + "]";
		f.setAttribute("content-source",url.split("#")[0]);
		
		/* Relative URLs in any imported fragment are
		   going to be fucked, so resolve them all
		   explicitly. */
		
		Docuplextron.fixRelativeURLs(i, url);
		
	    } else {
		
		f.setAttribute("content-source",url.split("#")[0]);
		
		var i = document.createElement("x-dom");
		i.setAttribute("resource",url);
		var sdom = i.attachShadow({"mode":"open"});
		
		/* This is just like importing an HTML fragment
		   into the main DOM: relative URLs don't resolve
		   to the correct base URL. So, ... */
		
		Docuplextron.fixRelativeURLs(responseDoc.documentElement, url);
		
		sdom.appendChild(responseDoc.documentElement);

		
	    }
	    
	    f.appendChild(i);
	    
	    /* Next, try to put the document's NAME in its floater tab. */
	    
	    var doctitle = sdom.querySelector("title");
	    if(doctitle){
		if(fragID) {
		    f.tab.text.textContent = "Fragment from: " + doctitle.textContent;
		} else {
		    f.tab.text.textContent = doctitle.textContent;
		}
	    }
	    
	    /* Finally, fetch-and-fill the document -- we do this because we can't really
	       trust the whitespace inside of X-TEXT elements in the HTML file, which will
	       throw off origin/extent indices and cause all sorts of frustrating issues. 
	       If we just load the text resources in and repopulate them from their
	       sources, everything works much better. */
	    
	    if(sdom) {
		
		alph.fetchAndFill(sdom);
	    } else {
		alph.fetchAndFill(f);
	    }
	    
	} else if (mediatype.startsWith("text/") && (
	    xhr.responseType == "text" || xhr.responseType == "")){
	    
	    /* Get a handle on the actual text content. We may have cached it
	       already, and the server may just have sent us a 'content not changed'
	       response with no text. */
	    
	    var media = xhr.responseText;
	    
	    if(alph.sources[baseUrl].mediaCache){
		if (alph.sources[baseUrl].mediaCache.length > media.length){
		    /* If the recieved media is smaller than our cached copy, use our cached copy. */
		    media = alph.sources[baseUrl].mediaCache;
		} else if (!url.includes("?")){
		    /* If the rec'd media is LARGER than our cache, cache the new stuff as long as the
		       URL was not a query/fragment request (that is, if we can expect that the rec'd
		       media is the current entire resource). */
		    alph.sources[baseUrl].mediaCache = media;
		}
	    } else {
		
		/* Is this a full text document, not a fragment?
		   And we do not have a cached copy yet? Cache it!
		   Even if it's coming from an Alph
		   server. Fragmentary retrieval is great and
		   xanalogical and all that jive, but it's just
		   fast and handy to have it all cached if possible. */
		
		if (!url.includes("?")){
		    alph.sources[baseUrl].mediaCache = media;
		}
	    }
	    
	    if(media.match(/,start=/)){
		// Is this a xanadoc EDL?
		f.appendChild(Docuplextron.edlToAlpHTML(media));
		alph.getXSources();
	    } else {
		// Otherwise, it's just plain text...
		var docDiv = document.createElement("article");
		docDiv.setAttribute("class","plaintext");
		f.appendChild(docDiv);
		
		if(url.includes(Alphjs.TRANSCLUDE_QUERY)){
		    // This splits a concatenated fragment string into
		    // a series of X-TEXT elements and inserts them into
		    // docDiv. 
		    Alphjs.splitHref(url,docDiv);
		    alph.fetchAndFill(docDiv);
		} else {
		    var span = AlphSpan.fromURL(url);
		    var doc = span.toDOMFragment();
		    //var doc = document.createElement("x-text");
		    docDiv.appendChild(doc);
		    //doc.textContent = xhr.responseText;
		    doc.textContent = media;
		    //doc.setAttribute("src", url);
		    //doc.setAttribute("origin", 0);
		    //doc.setAttribute("extent", media.length);
		    if(!span.origin){
			doc.setAttribute("origin", 0);
			doc.setAttribute("extent", media.length);
		    } else if(!span.extent){
			doc.setAttribute("extent", media.length);
		    }
		    alph.fetchAndFill(docDiv);
		}
	    }		
	} else if (mediatype.startsWith("image/")){
	    /* Does the URL have an AREA simple selector? */
	    var re = /\d+(.\d+)?,\d+(.\d+)?-\d+(.\d+)?,\d+(.\d+)?/gm;
	    var frag = url.split("#")[1];
	    if(frag && re.test(frag)){
		var i = document.createElement("img",{"is":"x-img"});
		i.setAttribute("src",url.split("#")[0]);
		i.setAttribute("origin",frag.split("-")[0]);
		i.setAttribute("extent",frag.split("-")[1]);
		// Look in the metadata for resolution, too!
	    } else {
		var i = document.createElement("img");
		i.src = url;
	    }
	    f.appendChild(i);
	    f.style.width = "auto";
	    f.style.backgroundColor = "transparent";
	} else if (mediatype.startsWith("audio/")){
	    var i = document.createElement("audio");
	    i.setAttribute("controls",true);
	    i.setAttribute("preload","metadata");
	    i.src = url;
	    f.style.width = "auto";
	    f.appendChild(i);
	} else if (mediatype.startsWith("video/")){
	    var i = document.createElement("video");
	    i.setAttribute("controls",true);
	    i.setAttribute("preload","metadata");
	    i.src = url;
	    f.style.width = "auto";
	    f.appendChild(i);
	} else if ((mediatype.startsWith("application/") && mediatype.includes("json")) ||
		   xhr.responseType == "json"){
	    
	    /* I'm a bit perplexed, here. The MDN docs say that if the responseType is "json", 
	       then the response should be the parsed JSON object. But... that's not how it
	       seems to be working? I'm getting an "object" according to typeof(), but it's
	       just a string or character arroy? */
	    
	    if(typeof(xhr.response == "object")){
		
		var obj = xhr.response;
		
		if(Object.keys(obj)[0] == "0"){ // Array
		    obj  = JSON.parse(obj);
		}
		
		if (!obj["@id"]){
		    
		    /* If the document doesn't set its own @id, do it here. */
		    
		    obj["@id"] = url;
		    
		} else if (obj["@id"] != url){
		    
		    /* If the document provides its own id, but it doesn't match the
		       URL we grabbed it from ... hrm. Do we change the id in the
		       document before loading, or let it clobber other resources
		       that might be open in the workspace? */
		    
		}
		
		/* Is this one of our graph documents? */
		
		if (obj["@type"] && obj["@type"] == "AlphLinkDocument") {
		    
		    /* If loadExternal() was called with an
		       "AlphLink" 'as' argument, then just quietly
		       load the link into the LinkStore. Otherwise, 
		       pretty-print the JSON and give the user a 
		       button to import it into the LinkStore. */
		    
		    if(as == "AlphLink"){
			this.linkStore.loadLD(obj);
			this.pukeText(obj["@id"] + " loaded into LinkStore.");
			f.destroy();
			return;
		    } else {
			f.appendChild(Alphjs.objectToHTML(obj));
			f.id = "ld+" + url;
			var lb = document.createElement("button");
			lb.textContent = "Import to LinkStore";
			lb.onclick = function(gd){
			    this.linkStore.loadLD(gd);
			}.bind(this, obj);
			f.appendChild(lb);
		    }
		    
		} else {
		    
		    /* If not, just give is a pretty-print of the JSON. */
		    
		    f.appendChild(Alphjs.objectToHTML(obj));
		    f.id = "ld+" + url;
		}
	    } else {
		
		/* ...pretty-much nothing should be arriving here. If the mediatype
		   has "application" AND "json" in it, and it parses, then one of the
		   preceding cases should cover it. Just in case, though... */
		
		try {
		    
		    f.appendChild(Alphjs.objectToHTML(JSON.parse(xhr.response)));
		    f.id = "ld+" + url;
		    
		} catch (e) {
		    f.textContent = "I thought this was gonna be JSON, but... I guess not?.\n" +
			"I got Content-Type: " + xhr.getResponseHeader("Content-Type") + "\n" +
			"and xhr.responseType: " + xhr.responseType + "\n" +
			"And then this mess: \n\n" + xhr.response;
		}
	    }
	    f.style.width = "512px";
	} else {
	    f.textContent = "I don't know what to do with this media.\n" +
		"The HTTP headers say Content-Type: " + xhr.getResponseHeader("Content-Type") + "\n" +
		"And the XMLHttpRequest came back with this responseType: " + xhr.responseType + "\n";
	}
	
	/* If this is a background resource, we don't actually want
	   to see the document open in the workspace, so we can
	   nuke the floater and call it done */
	
	if(as == "pin"){
	    f.destroy();
	    return;
	}
	
	// Put the SVG overlay back onto the floater.
	f.appendChild(foverlay);
	
	// Make the parent document aware of the new content.
	this.updateContexts();
	this.postEDL(f);
	//this.pushAllLinks();

    } else {
	//console.log("Bad news?");
	f.textContent = "HTTP " + xhr.status + "; media unavailable.";
    }
    // Last thing -- update dptron.transpointerSpans
    //this.findSomeMatchingContent(f.querySelector("iframe") || window);
    this.findSomeMatchingContent(f);
    this.paintLinkTerminals();
    //this.pushAllLinks();
}


/* * */
Docuplextron.prototype.makeTagFloater = function(floater,url){
    
    /* Nothing too fancy, here. Just put the name of the tag in large 
       text. This floater, because its 'id' will be set to the URI
       of the tag, will give link pointers something to anchor to. */

    floater.tab.text.textContent = url;

    var barename = url.substr(url.lastIndexOf(":"));
    var contentDiv = document.createElement("div");
    
    contentDiv.appendChild(document.createElement("small")).textContent = "tag:";
    contentDiv.appendChild(document.createElement("big")).textContent = barename;
    floater.appendChild(contentDiv);

    this.paintLinkTerminals();
    
    return floater;
}

/* * */

Docuplextron.prototype.loadBlankNode = function(floater, url){

    /* Look for the node in the LinkStore. We give anonymous nodes
       URIs of the form "_:EDL-123456" when creating EDL nodes
       in graph document, but that particular naming isn't mandated
       anywhere, so an EDL might have an id like "_:1.2" or 
       something.

       At the moment, we only support text EDLs (though audio 
       and video EDLs are a long-term goal), so if we find the
       node in the link store, we fill the floater with <x-text>
       elements containing the AlphSpans in the EDL's @list. */

    floater.style.width = "512px";
    
    var links = this.linkStore.getLinksWithNode(url);
    
    if(links.length == 0) {
	floater.appendChild(new Text("Anonymous node " + url + " is not in the LinkStore."));
	return floater;
    }
    
    var graphID = links[0]["graph"];
    var graph = this.linkStore.graphs[graphID];
    var node = graph.nodes[url];
    
    if(!node){
	floater.appendChild(new Text("Anonymous node " + url + " is not in the LinkStore."));
	return floater;
    } else {
	if (node["@type"] && (node["@type"] == "EDL")){
	    for(var ix in node["@list"]){
		var span = node["@list"][ix];
		var x = document.createElement("x-text");
		x.setAttribute("src", span["src"]);
		x.setAttribute("origin", span["origin"]);
		x.setAttribute("extent", span["extent"]);
		floater.appendChild(x);
	    }
	    
	    if(node["viewingContext"]){
		/* Load the EDLs viewing context, too. Or maybe prompt 
		   the user and see if they want to view it. */
	    }
	    alph.fetchAndFill(floater);
	}
    }
    return floater;
}

/* * */

Docuplextron.fixRelativeURLs = function(el, url){
    /* Pass me an element (el) and the document URL from which it was
       sourced (url) and I will try to resolve all of the relative
       URLs in its children */

    /* First, get an array of all of the elements that have an 'href'
       or 'src' attribute -- that should be all <link>, <a>, <img>,
       <script>, <embed>, <audio>, etc... Anything that MIGHT have a
       relative URL in it. */
    
    var srcs = Array.from(el.querySelectorAll("[href],[src]"));

    /* Now we need to isolate some parts of the document's URL. First,
       the URL with no fragments or queries. */
    
    var baseUrl = url.split("?")[0].split("#")[0];

    /* Now we want the root path.
       Match *://*... up to (and including) the first "/".  If no
       match is found, we can assume that the URL is of the form
       "[prot]://some.site", with no path, so we can just use baseUrl
       and append a slash. */
		    
    var urlDomain = baseUrl.match(/^.+:\/\/.+?\//);
    urlDomain = (urlDomain) ? urlDomain[0] : baseUrl + "/";
    
    /* That seems pretty legit. The next bit does a bit of guesswork,
       though.*/
		    
    if(baseUrl.endsWith("/")){
	/*  The simple case:  URL is something like 
	    "http://foo.bar/bas/" */
	var urlDir = baseUrl; 
    } else {
	/* Otherwise, it could be a directory name with no trailing
	   slash (so there's an implicit "/index.html" at the end) OR
	   it could actually be a file.
	   
	   See if it has an extension... */
	if (baseUrl.toLowerCase().endsWith(".html") ||
	    baseUrl.toLowerCase().endsWith(".htm")){
	    var urlDir = baseUrl.substr(0,
					baseUrl.lastIndexOf("/") + 1);
	} else {
	    var urlDir = baseUrl + "/";
	}
    }

    /* Now actually fix the URLs in the fragment. */
		    
    for (var ix in srcs){
	var attr = (srcs[ix].hasAttribute("src")) ? "src" : "href";
	var attrVal = srcs[ix].getAttribute(attr);
	
	if(attrVal.startsWith("#")){
	    // Simply replace the fragment of the passed URL
	    srcs[ix].setAttribute(attr, url.split("#")[0] + attrVal);
	} else if(attrVal.startsWith("/")){
	    // Make relative to root
	    srcs[ix].setAttribute(attr, urlDomain + attrVal.substr(1)); 
	} else if(!attrVal.includes("://")){
	    // If the href/src does not contain a protocol specifier,
	    // we assume it's relative to the current path

	    srcs[ix].setAttribute(attr, urlDir + attrVal);

	    // If it DOES contain a protocol specifier, we leave it alone.
	} 
    }
    return el;
}

/* * */

Docuplextron.prototype.commitSpan = function(forcePut){
    
    /* 
       *** This whole thing needs to be rewritten. *** 

       The user has pressed Ctrl+Enter. That means they want to store
       the orphan text they are currently editing to a network resource.

       If the user is editing in the scratchpad, simply present the
       commit dialog and let it do its thing.

       If they are editing a noodle, present the commit dialog, and 
       after it successfully does its thing, lock the noodle, close 
       it, and replace its floater with an HTML fragment floater that 
       contains the transcluded network resource.

       If they are editing an HTML composition, we need to determine
       first how much orphan text they want to commit. Just the current
       Text node? All orphan text in the paragraph? All orphan text in
       the document? Maybe we should use the DOMCursor target to select
       this?

    */

    /* By default, we'll try to POST the text, but using PUT can be forced. */
    
    forcePut = forcePut || false;
    
    /* Where is the selection right now? If it's inside of an X-TEXT element
       OR inside of the Scratchpad, then we'll proceed. Otherwise, return.
    */
    
    var anchorNode = window.getSelection().anchorNode;
    console.log("commit anchor node: ", anchorNode);
    
    if(anchorNode.nodeType == 3) { // A text node
	var span = anchorNode.parentElement;
	if(!span || !span.tagName || span.tagName != "X-TEXT"){
	    console.log(span, " is not valid commit source.");
	    return;
	}
    } else if (anchorNode.tagName && anchorNode.tagName == "TEXTAREA"){
	var span = anchorNode;
	if(span.id != "alph-bufferinput"){
	    console.log("commit source is a TEXTAREA, but not the scratchpad: ",span);
	    return;
	}
    } else {
	console.log("Not a valid commit source?");
	return;
    }

    /* Set the URL we're going to try to POST to from the contents 
       of the ">>" permascroll input in the topbar. */
    
    this.commitTarget = document.getElementById("alph-posttarget").value;

    var commitTarget = this.commitTarget;

    if(!commitTarget.startsWith("http:") &&
       !commitTarget.startsWith("https:") ){

	/* Something's wrong with the commit target. This should
	   be an HTTP(S):// URL. */

	this.pukeText("COMMIT: FAIL. I can't post text to the address: " +
		      commitTarget);
	return;
    }
    
    /* Get some server info. Force async meta fetch... */
    
    this.pukeText("COMMIT: Fetching target metadata...");     
    var m = alph.getDescription(commitTarget,true,true);

    if(!m["alphic"]){
	this.pukeText("COMMIT: FAIL. Commit target is not an Alph server.");
	//console.log(m);
	return;
    }
    
    /* If we get a 404 on the resource, then it doesn't exist, right?
       Right. So... we assume the user is trying to *create* a new resource,
       and we do a PUT in that case.  If it DOES exist, we do a
       POST. */
    
    if(m["httpError"] &&
       m["httpError"] == 404){
	forcePut = true;
    }

    /* We have to keep track of EXACTLY how many codepoints we're
       sending to the server, and EXACTLY how many codepoints are
       already in the resource. 
       
       "bufend" will store the length/terminal-index of the resource
       we're committing to AFTER the text has been appended/created. */
    
    var bufend = 0;
    
    if(m["alph:textLength"]){
	bufend = m["alph:textLength"];
    }
    
    /* We're going to prepend the text that we post with two newlines
       and a timestamp.

       ↓This should be user-configurable! */
    
    var timestamp = "\n\n* @" + (new Date).toISOString() + "\n";
    
    console.log("trying to commit ", span, this.commitTarget);
    
    if (span.id == "alph-bufferinput"){
	
	/* Committing an edit from the SCRATCHPAD... easy! */

	this.postMedia(
	    (forcePut) ? "PUT" : "POST",
	    span.value,
	    false,
	    commitTarget);
	
    } else if(span.getAttribute("src").startsWith("~")){
	
	/* This is an <x-text> being edited inside of a composition
	   floater. The idea here is that we want to retain the HTML
	   structure of the edited span, while wrapping all Text nodes
	   inside <x-text> tags so they become Xanalogical.

	   Tricky.

	   -----
	   This ~works~ for noodles, but it doesn't work well. Noodle
	   titles are discarded, the resulting <x-text> is bare,
	   instead of wrapped in an <article> or something. */

	var noodle = this.noodleManager.get(span.getAttribute("src"));

	/* In addition to the timestamp from above, we put some extra
	   information at the head of a noodle: 
	   - The noodle's key/id
	   - The base-36 timestamp of when it was last modified
	   - The key/id of any PROGENITORS... 
	*/
	
	timestamp += "<<" + noodle.key + ">> " + noodle.title;
	
	timestamp += (noodle.mod) ?
	    " (" + noodle.mod + ")\n" : "\n";

	if(noodle.progenitors.length > 0){
	    for(var pp in noodle.progenitors){
		timestamp += "[[" + noodle.progenitors[pp] + "]] ";
	    }
	    timestamp += "\n";
	}
	timestamp += "\n";


	/* Extend bufend to include the length of our timestamp/header.
	
	   *** NOTE: We have to be careful about newlines, here. 
	   Firefox/Chrome both use single-byte newlines (LF) in
	   String objects (thus, edited text in contentEditable
	   elements), but they convert these to CR,LF when POSTing to
	   a Web server. So for our origins/offsets we have to
	   increment each index for every newline in a string. */
	
	bufend += timestamp.length + (timestamp.split("\n").length - 1) ;

	/* Okay, let's keep a pointer at the beginning of the text
	   content for rebasing... */
	
	var neworigin = parseInt(bufend);
	
	this.pukeText("COMMIT: appending text to " + bufend + " codepoint permascroll.");
	
	/* This NodeIterator will give us all of the Text nodes in the
	   span in document order. We want to:
	   1) cat all of these Text nodes into a string, and
	   2) wrap all of the text nodes in <x-text> elements.*/
	
	var iterator = document.createNodeIterator(span,4);
	var catspan = "";

	/* This is wonky:
	
	   I'm plugging these text nodes into an Array because the NodeIterator
	   was just ... I don't know what the hell was going on when I tried to
	   run this loop by iterating with the NodeIterator directly. This works,
	   but I'd prefer if I knew what I was doing wrong using the NodeIterator.*/
	
	var tnodes = [];
	var tnode = iterator.nextNode();
	while(tnode){
	    tnodes.push(tnode);
	    tnode = iterator.nextNode();
	}
	
	while(tnode = tnodes.shift()){
	    catspan += tnode.textContent;

	    var newBufend = bufend + tnode.textContent.length + tnode.textContent.split("\n").length -1 ;
	    
	    var subspan = Alphjs.newSpan(commitTarget, bufend, newBufend);
	    tnode.parentElement.insertBefore(subspan,tnode);
	    subspan.appendChild(tnode);
	    bufend = newBufend;
	};
	
	/* Now, we've created all of these new spans inside of a contentEditable
	   span, but we want to break them out of there because we never want to
	   nest transclusion spans. However, we want to keep a reference to the
	   span's parent so that we can refresh the spans from source later, so...*/
	
	var spanParent = span.parentElement;
	Docuplextron.deParent(span);

	/* If the text we're posting was in a noodleBox, remove that class
	   from its floater, otherwise the floater will fail to load on get an error on
	   session restore. */
	
	var floater = Docuplextron.getParentItem(spanParent);
	if (floater && floater.classList.contains("noodleBox")){
	    floater.classList.remove("noodleBox");

	    /* We also want to pad this floater with an <article> element now... */
	    
	    let a = document.createElement("article");
	    floater.appendChild(a);
	    a.appendChild(floater.querySelector("x-text"));
	}
	/* And finally, post the text to the server... */
	
	var form = new FormData();
	form.append("media",timestamp + catspan);

	this.postText(commitTarget,form,spanParent,null, (noodle) ? noodle : null, neworigin);
    }
}

/* * */
Docuplextron.prototype.postText = function(target,form,spanParent,creds,noodle,neworigin){
    var xhr = new XMLHttpRequest();
    xhr.onload = function(xhr,spanParent,target,form){
	var targetBase = target.split("/")[2];
	if(xhr.status == 200){
	    this.loadExternal(target);
	    // Update the media length from the server
	    alph.getDescription(target,false,true);

	    if(noodle){
		this.pukeText("Noodle ", noodle.key, " successfully POSTed to server.");
		/* The noodle is now a network resource.  ask the user
		   if they want to replace the noodle with the network
		   resource in the LinkStore ...*/
		if(confirm("Do you want to change all links that pointed at noodle " + noodle.key +
			   "to now point at " + target + "?")){
		    this.linkStore.rebaseNodes("~" + noodle.key, target, neworigin, "all");
		    /* And then ask if they want to remove the noodle
		       from the noodleManager...*/
		    if(confirm("Would you also like to remove the noodle from the noodleStore?")){
			this.noodleManager.remove(noodle.key);
		    }
		}
	    }
	    
	    alph.fetchAndFill(spanParent);
	    this.postEDL();
	    this.setPin(target); // Pin the resource so that it's searchable in the Sources flap
	} else if(xhr.status == 401){
	    // We need to authenticate this request;
	    if(this.credStore[targetBase]){
		var creds = this.credStore[targetBase];
	    } else {
		// Prompt for credentials
		var creds = prompt("u:p").split(":");
		this.credStore[targetBase] = creds;
	    }
	    this.postText(target,form,spanParent,creds);
	} else {
	    console.log("Error with this: ",
			xhr.status, xhr.response, xhr.getAllResponseHeaders());
	}
    }.bind(this, xhr, spanParent, target, form, noodle, neworigin);
    if(creds){
	xhr.open("POST",target,true,creds[0],creds[1]);
    } else {
	xhr.open("POST",target);
    }
    xhr.send(form);
}
/* * */

Docuplextron.edlToAlpHTML = function(edl){

    /* Read a Xanadu EDL document <http://xanadu.com/xuEDL.html>, and
       convert it to an Alph-flavored HTML document.*/
    
    var lines = edl.split("\n");
    var spans = [];
    for (var ix in lines){
	if(lines[ix].startsWith("span:")){
	    var splitline = lines[ix].replace("span:","");
	    splitline = splitline.trim();
	    splitline = splitline.split(",start=");
	    splitline = [splitline[0]].concat(splitline[1].split(",length="));
	    spans.push(new AlphSpan(splitline[0],
				    parseInt(splitline[1]),
				    parseInt(splitline[1]) + parseInt(splitline[2])
				   )
		      );
	} else if (lines[ix].startsWith("xanalink:")){
	    // Need to get Alph links working again first...
	} else {
	    /* Do nothing? */
	}
    }
    var article = document.createElement("article");
    article.classList.add("xanadoc");
    for(var ix in spans){
	article.appendChild(spans[ix].toDOMFragment());
    }
    return article;
}

/* * */

Docuplextron.prototype.exportContextHTML = function(context){
    /* In brief: make a copy of the DOM, rip all of the Docuplextron
       stuff out of it, then return the HTML of the clean DOM. */


    // Circumstances are a bit different if this is a multi-document view, or a
    // single document (or subordinate iframe) view.

    if(context == document.body){
	// Single-document mode -- maybe we're an iframe

	var ex = document.createDocumentFragment();
	var body = document.body.cloneNode(true);
	var head = document.head.cloneNode(true);
	ex.appendChild(head);
	ex.appendChild(body);

	// Now remove all of the Alph crap.
	try{ex.getElementById("alph-main").remove();} catch(e){console.log(e)};
	try{ex.getElementById("alph-footer").remove();} catch(e){console.log(e)};	
	try{ex.getElementById("alphOverlay").remove();} catch(e){console.log(e)};
	try{ex.getElementById("dptron-flap").remove();} catch(e){console.log(e)};
	try{ex.getElementById("alphDOMCursor").remove();} catch(e){console.log(e)};
	try{ex.getElementById("alphDOMCursorInfotext").remove();} catch(e){console.log(e)};
	
    } else if(context.tagName == "X-FLOATER"){
	/* Multi-document view -- we're exporting the contents of a
	   floater div, OR, we are are exporting the contents of a
	   subordinate IFRAME.

	   This has become very easy with shadow DOMs. We don't have
	   to clone the context's children or anything, we just grab
	   the innerHTML of the shadowRoot. Easy peasy!
	*/

	var sdom = context.querySelector("x-dom");
	if(sdom){
	    return sdom.shadowRoot.innerHTML;
	}

	/* And here's how we handle freshly-made contexts (composition
	 windows that haven't been exported to the network or anything
	 yet). Clone the contents of the floater into a document
	 fragment, serialise it, stick it in-between some <html> tags,
	 slap a <!DOCTYPE html> on top, and foom!  Kinda crappy?
	 Should we just go straight to shadowDOMs in composition
	 floaters? ....perhaps. —L. 20201202*/

	var ex = document.createDocumentFragment();
	var head = document.createElement("head");
	var body = document.createElement("body");
	ex.appendChild(head);
	ex.appendChild(body);

	// Clone the contents of the context node into our export document body

	var kids = Array.from(context.childNodes);
	for(var ix in kids){
	    body.appendChild(kids[ix].cloneNode(true));
	}
	
	/* ***
	   If we had some <body> style or other attributes , they've been lost, here.
	   Come up with a way to retain those.
	   ***

	   Next, figure out what to do for a <head>. If we're exporting the host document,
	   just copy the <head> of the window. If it's some kind of ephemeral view
	   (imported plain-text, new compositions, etc.), then use an empty <head>. */
	
	if(context.id == window.location.toString() ||
	   (context.id == (window.location.toString() + "(modified)")) ){
	    head = document.head.cloneNode(true);
	} else {
	    /* ...? */
	}
    }
    
    /* Next, look for items classed as "do-not-export", and remove them.
       This will include the Alph <script> and stylesheet <link> in the <head>.
       Also, if this was a slider/floater, get rid of the SVG overlay. */
    
    var killables = Array.from(ex.querySelectorAll(".do-not-export, .alph-floater-overlay"));
    while(killables.length > 0){
	var killme = killables.pop();
	killme.remove();
    }
    
    return "<!DOCTYPE html>\n<html>" + head.outerHTML + body.outerHTML + "</html>";

}

/* * */
Docuplextron.prototype.postMedia = function(method,data,bin,url,creds,action){
    
    /* A fairly generic routine for POSTing or PUTting media to an HTTP 
       server, though this is coded specifically for publishing to alph.py 

       method -> String: "PUT" or "POST"
       data -> ...: the data to be uploaded
       bin -> Boolean: include the "binary" parameter in the HTTP request
       url -> String: the URL to send the request to
       creds -> Array(2): username, password
       action -> Function: a callback function that runs if/when the upload completes successfully 

    */
    
    method = method || "PUT";
    bin = bin || true;    
    if(!data)return;
    console.log("Trying to ", method," this: ", data);
    if (!url){
	url = prompt("URL to publish to:");
	if(!url) return;
    } else {

	if( url.endsWith("(modified)")) {
	    url = url.split("(modified)")[0];
	}

	var verify = false;
	if(parent != window) {
	    verify = true;
	} else {
	    if (method == "PUT"){
		var verify = confirm("PUT to " + url + "?\n(This may overwrite/destroy data!)");
	    } else {
		var verify = confirm(method + " to " + url + "?");
	    }
	}
	if(!verify) return;
    }
	
    var xhr = new XMLHttpRequest();
    var form = new FormData();
    form.append("media",data);
    if (bin) form.append("binary","t"); // Alph.py needs this to know how to treat the data
    
    xhr.onload = function(xhr,url,method,data,bin,creds,action){
	var targetBase = url.split("/")[2];
	if (xhr.status == 200){
	    this.pukeText(method + " to " + url + " successful.");
	    if(typeof action == "function"){
		action(xhr,url,method,data,bin);
	    } else if(window.location != url){
		this.loadExternal(url);		
	    } else {
		//window.open(url);
	    }
	} else if(xhr.status == 401){
	    console.log("401 came back. Trying again w/ creds...");
	    // We need to authenticate this request; Make a dialog!

	    var d = Docuplextron.credModal("Credentials required:");
	    d.okbutton.value = method;
	    
	    if(this.credStore[targetBase]){
		var creds = this.credStore[targetBase];
		d.input.value = creds[0];
		d.pwField.value = creds[1];
	    }

	    d.okbutton.onclick = function(dialog,method,data,bin,url,targetBase,action){
		creds = [ dialog.input.value, dialog.pwField.value ];
		this.credStore[targetBase] = creds;
		this.postMedia(method,data,bin,url,creds,action);
		dialog.floater.destroy();
	    }.bind(this,d,method,data,bin,url,targetBase,action);

	} else {
	    console.log("Post failed? Came back with this: ", xhr);
	}
    }.bind(this,xhr,url,method,data,bin,creds,action);

    if(creds){
	xhr.open(method,url,true,creds[0],creds[1]);
    } else {
	xhr.open(method,url);
    }
    xhr.send(form);    
}

/* * */

Docuplextron.prototype.exportContextToURL = function(context,url){
    if (!url){
	url = prompt("URL to publish to:");
	if(!url) return;
    } else {
	if( url.endsWith("(modified)")) {
	    url = url.split("(modified)")[0];
	}
	var verify = false;
	if(parent != window) {
	    verify = true;
	} else {
	    var verify = confirm("PUT to " + url + "?\n(This may overwrite/destroy data!)");
	}
	if(!verify) return;
    }
	
    var xhr = new XMLHttpRequest();
    var form = new FormData();
    form.append("media",this.exportContextHTML(context));
    form.append("binary","t");
    xhr.onload = function(xhr,url,context){
	if (xhr.status == 200){
	    
	    context.id = url;
	    if(window.location != url){
		this.loadExternal(url);		
	    } else {
		//window.open(url);
	    }
	}
    }.bind(this,xhr,url,context);
    xhr.open("PUT",url);
    xhr.send(form);
}

/* 




 EDITING




*/

Docuplextron.prototype.newComposition = function(){

    /* */
    
    var f = this.floaterManager.create(null,
				       "[" + Date.now().toString(36).toUpperCase() + "]"
				       ,this.lastX,this.lastY);

    var a = document.createElement("article");
    a.setAttribute("contenteditable", true);

    //var frag = Alphjs.newSpan("~"+this.noodleManager.newdle().key,"0","9",true);
    var frag = document.createElement("p");
    frag.textContent = "-edit me-";

    a.appendChild(frag);
    f.appendChild(a);
    window.getSelection().removeAllRanges();
    window.getSelection().selectAllChildren(frag);
    frag.focus();
    return f;
}

/* * */
Docuplextron.prototype.openNoodle = function(noodleID){
    
    /* If a noodle is already open in the workspace, I'll call
       bringIntoView() on its floater; if the noodle is not open,
       I'll call newdleBox() and open it.

       I am expecting a noodle ID without the tilde */

    var noodleSpan = document.getElementById("~"+noodleID);
    if(!noodleSpan){
	var noodleBox = this.newdleBox(noodleID);
	noodleBox.style.width = "500px";
	noodleBox.bringIntoView();
    } else {
	var noodleBox = Docuplextron.getParentItem(noodleSpan);
	if(noodleBox){
	    noodleBox.bringIntoView(this.lastX,this.lastY);	
	}
    }
    this.paintLinkTerminals();
}

/* * */

Docuplextron.prototype.newdleBox = function(noodleID,text,editable,atX,atY){
    var atX = atX || this.lastX;
    var atY = atY || this.lastY;
    var editable = editable || false;
    if(noodleID){
	var noodle = this.noodleManager.get(noodleID);
	if(noodle){
	    var text = noodle.text;
	} else {
	    var floater = this.floaterManager.create(null,null,atX,atY);
	    floater.appendChild(document.createTextNode("Fragment " + noodleID +
							" no longer exists."));
	    return floater;
	}
    } else {
	var noodle = this.noodleManager.newdle();
	var text = text || "\n";
	noodle.text = text;
    }
    var floater = this.floaterManager.create("noodleBox",
					 noodle.title || null,
					 atX,atY);
    var span = Alphjs.newSpan("~"+noodle.key,"0",text.length,editable);
    if(!noodle.title) floater.tab.text.textContent = " ";
    span.textContent = text;
    floater.style.width = "500px";
    floater.appendChild(span);
    window.getSelection().removeAllRanges();

    var r = new Range();
    r.selectNodeContents(span);
    r.collapse(true);
    window.getSelection().addRange(r);
    span.focus();
    floater.toTop();
    return floater;
}

/***/

Docuplextron.prototype.insertEditable = function(p){
    
    /* This thing's a mess. Needs to be designed properly. */
    
    var frag = Alphjs.newSpan("~" + this.noodleManager.newdle().key,"0","10",true);
    frag.textContent = "[editable]";
    if(!p){
	Alphjs.paste(frag);
    } else if (p == "sibling"){
	var ae = this.activeElement;
	if(ae.tagName.toLowerCase() != "x-text"){
	    var newSibling = document.createElement(ae.tagName.toLowerCase());
	    newSibling.appendChild(frag);
	} else {
	    var newSibling = frag;
	}
	ae.parentNode.insertBefore(newSibling,ae);
	ae.parentNode.insertBefore(ae,newSibling);
	this.DOMCursor.select(newSibling);
    } else if (p == "child"){
	this.activeElement.appendChild(frag);
	this.DOMCursor.select(frag);
    }
    window.getSelection().removeAllRanges();
    window.getSelection().selectAllChildren(frag);
    frag.focus();
}

/* * */

Docuplextron.prototype.domPopout = function(excise){

    /* DOM pop-out. Copies the currently selected DOM node into a new floater. */
    
    if(this.DOMCursor.target instanceof HTMLElement){
	var f = this.floaterManager.create(null,null,this.lastX,this.lastY);
	if(excise){
	    f.appendChild(this.DOMCursor.target);
	} else {
	    f.appendChild(this.DOMCursor.target.cloneNode(true));
	}
    } 
    this.postEDL();
    this.findSomeMatchingContent(f);
}

/* * */

Docuplextron.prototype.popout = function(excise){

    var anchorItem = Docuplextron.getParentItem(window.getSelection().anchorNode.parentElement);
    if(anchorItem.classList.contains("noodleBox")){
	/* Create a new noodle with a new limbic span. */

	/* Selection.toString() would theoretically be easier, HOWEVER,
	   it destroys whitespace. So, this instead: */
	var newdleText = window.getSelection().getRangeAt(0).cloneContents().textContent;
	
	if(excise){
	    window.getSelection().deleteFromDocument();
	}
	
	var newdleBox = this.newdleBox(null,newdleText,true);
	newdleBox.toTop();
	
    } else {
	/* Create a new composition pane with whatever's in alph.selection. */
    	
	var art = document.createElement("article");
	art.appendChild(alph.selection.toFragment());
	alph.fetchAndFill(art);
	
	if(excise){
	    alph.cut();
	}
	
	var floater = this.floaterManager.create(null,null,this.lastX,this.lastY);
	floater.appendChild(art);
	floater.style.width = "640px";
	this.postEDL();
	this.findSomeMatchingContent(floater);
    }
}

/***/

Docuplextron.prototype.merge = function(){

    /* Combine the currently active noodles into a new noodle. */

    var noodleBoxen = [];
    var noodleIDs = [];
    var catText = "";
    
    /* First, go through the active items and collect all of
       the noodles into noodleBoxen. */
    
    for (var ix in dptron.activeItems){
	if(dptron.activeItems[ix].classList.contains("noodleBox")){
	    noodleBoxen.push(dptron.activeItems[ix]);
	    noodleIDs.push(
		dptron.activeItems[ix].querySelector("x-text").getAttribute("src").substr(1)
	    );
	}
    }

    /* Next, sort by vertical position in the workspace. */
    
    noodleBoxen.sort(function(a,b){
	if ( parseFloat(a.style.top) < parseFloat(b.style.top) ){
	    return -1;
	} else {
	    return 1;
	}
    });

    /* The resulting noodle of this fusion is going to be
       styled like the top-most noodle of the selection, and it's 
       going to be placed where that noodle is. */
    
    var topStyle = noodleBoxen[0].style.cssText;
    var initialNoodle = this.noodleManager.get(noodleBoxen[0].querySelector("x-text").getAttribute("src"));
    var initialX = parseFloat(noodleBoxen[0].style.left);
    var initialY = parseFloat(noodleBoxen[0].style.top);
    var initialWidth = noodleBoxen[0].style.width;

    /* Concatenate the text from all of the noodles into catText. 
       
       The "correct" way to do this is debatable. We're going to 
       try to make sure that there is a double-newline between 
       each fragment, although I can certainly see how this might
       not be what the author wants. 

    */
    
    for (var ix in noodleBoxen){
	var nid = noodleBoxen[ix].querySelector("x-text").getAttribute("src");
	catText += noodleBoxen[ix].querySelector("x-text").textContent;
	if(catText.endsWith("\n\n")){
	    // Ends with a double newline. This is fine.
	} else if (catText.endsWith("\n")){
	    // Ends with only a single newline. Add one.
	    catText += "\n";
	} else {
	    catText += "\n\n";
	}
	noodleBoxen[ix].splode();
    }

    /* Create the new noodle and open it in the workspace. */
    
    var noodleOne = this.noodleManager.newdle();
    noodleOne.progenitors = noodleIDs;
    noodleOne.text = catText;
    noodleOne.title = initialNoodle.title;
    var xtextOne = new Alphjs.newSpan("~" + noodleOne.key,
				      0, noodleOne.text.length, true);
    xtextOne.textContent = noodleOne.text;
    var floaterOne = this.floaterManager.create("noodleBox",
						null,
						initialX,initialY);
    floaterOne.appendChild(xtextOne);
    floaterOne.tab.text.textContent = noodleOne.title || " ";
    floaterOne.style.cssText = topStyle;

    /* As the user if they want to delete the old noodles
       from the NoodleStore. */
    
//    if (window.confirm("Clean-up noodles?")){
	for(var nix in noodleIDs){
	    this.noodleManager.remove(noodleIDs[nix]);	   
	}
//  }
	
    return floaterOne;

}

/***/

Docuplextron.prototype.dupe = function(){

    /* If we're working on a noodle, duplicate the noodle.
       If we're working on a document, duplicate the DOMCursor target. */

    if (!this.activeItems){

	/* No active items! Goodbye! */
	
	return;
	
    } else if (this.activeItems[0].classList.contains("noodleBox")) {

	/* We're only going to deal with the first item in the
	   activeItems list, rather than going through all of
	   them like we used to do when this was in the 'D' keyhandler. */
	
	var sourceBox = this.activeItems[0];

	var sourceNoodle = this.noodleManager.get(
	    sourceBox.querySelector("x-text").getAttribute("src"));

	if (!sourceNoodle) return;

	/* Create a new noodleBox (and noodle), copy the style
	   of the original, and place it to its right. */
	
	var dupeBox = dptron.newdleBox(null,sourceNoodle.text);
	dupeBox.style.cssText = sourceBox.style.cssText;
	dupeBox.style.left = sourceBox.getBoundingClientRect().right + "px";

	/* Get a reference to the new noodle in the NoodleStore so that
	   we can copy over its title, then set the titleTab content. */
	
	var dupeNoodle = dptron.noodleManager.get(
	    dupeBox.querySelector("x-text").getAttribute("src"));
	dupeBox.tab.text.textContent = dupeNoodle.title = sourceNoodle.title + "(copy)";
	dupeBox.toTop();
	
    } else if (this.DOMCursor.target){
	
	/* Very simply, clone the DOMCursor's target node, and
	   insert the copy after the original. */
	
	var copy = this.DOMCursor.target.cloneNode(true);
	this.DOMCursor.target.insertAdjacentElement('afterend',copy);
	
    }
	
}

/***/

Docuplextron.prototype.split = function(shifted){
    
    /* Do a noodle split or Alph split?  */

    var returnSpans = [];

    var sel = window.getSelection();
    var range = sel.getRangeAt(0);
    var anchor = sel.anchorNode;

    /* We want to be inside of a text node, which should be inside of an
       X-TEXT, which should have a limbic source, and should be inside of
       a noodleBox. */
    
    if(anchor.nodeType == 3 &&
       anchor.parentElement.tagName == "X-TEXT" &&
       anchor.parentElement.getAttribute("src").startsWith("~") &&
       Docuplextron.getParentItem(anchor.parentElement).classList.contains("noodleBox")){

	var sourceXtext = anchor.parentElement;
	var sourceText = sourceXtext.textContent;
	var sourceNoodle = this.noodleManager.get(sourceXtext.getAttribute("src").substr(1));
	var sourceFloater = Docuplextron.getParentItem(sourceXtext);

	/* Make absolutely sure that Lamian has the current version of
	   the noodle before we destroy its container later. */

	sourceNoodle.text = sourceText;

	/* Now, make the new noodles, spans, and floaters, and replace the 
	   previous one. */

	var r1 = sourceFloater.getBoundingClientRect();
	
	var noodleOne = this.noodleManager.newdle();
	noodleOne.progenitors.push(sourceNoodle.key);
	noodleOne.text = sourceText.substr(0,range.startOffset);
	noodleOne.title = sourceNoodle.title + "/1";
	var floaterOne = this.newdleBox(noodleOne.key,null,true,r1.left,r1.top);
	floaterOne.style.cssText = sourceFloater.style.cssText;

	var r2 = floaterOne.getBoundingClientRect();
	

	var noodleTwo = this.noodleManager.newdle();
	noodleTwo.progenitors.push(sourceNoodle.key);
	noodleTwo.text = sourceText.substr(range.startOffset);
	noodleTwo.title = sourceNoodle.title + "/2";
	var floaterTwo = this.newdleBox(noodleTwo.key,null,true,r2.left,r2.bottom);
	floaterTwo.style.cssText = sourceFloater.style.cssText;
	floaterTwo.style.top = r2.bottom + "px";
	
	sourceFloater.splode();
	
	this.noodleManager.remove(sourceNoodle.key);

	return [floaterOne.querySelector("x-text"), floaterTwo.querySelector("x-text")];
	
    } else {
	return Alphjs.split(shifted);
    }
}


/* 






 MISCELLANY






*/

Docuplextron.prototype.pukeText = function(text,delay){

    /* Barf some text right onto the background. Text removes itself
       after a couple of seconds. */
    
    var p = document.createElement("p");
    p.classList.add("pukeText");
    p.textContent = text || "FOO BAR.";
    this.pukeBox.appendChild(p);
    setTimeout(function(){
	this.remove();
    }.bind(p), delay || 2000);
}

/***/

Docuplextron.titleTrim = function(txt,len){
    
    /* This is really stupid right now, but it's a placeholder. */

    if(!txt) return;

    var len = len || 80;

    if(txt.length > 80){
	return txt.substr(0,77) + "...";
    } else {
	return txt;
    }

}

Docuplextron.getParentById = function(el,id){

    /* Hunt back up the DOM tree for a parent element with the passed id. */
    
    if (!id) return false;

    try{
	if(!el.nodeType){
	    return false;
	} else if(el.nodeType == Node.TEXT_NODE){
	    var p = el.parentElement;
	} else if(el.nodeType == Node.ELEMENT_NODE){
	    var p = el;
	} else {
	    return false;
	}
    } catch(e){
	debugger;
    }

    while(p && p.id != id){
	p = p.parentElement;
    }
    
    if(!p){
	return false
    } else {
	return p;
    }
    
}


Docuplextron.getParentItem = function(el,tag){

    /* Hunt back up the DOM tree for a parent element of the passed type. */
    
    var tag = tag || "X-FLOATER";

    try{
    if(!el.nodeType){
	return false;
    } else if(el.nodeType == Node.TEXT_NODE){
	var p = el.parentElement;
    } else if(el.nodeType == Node.ELEMENT_NODE){
	var p = el;
    } else {
	return false;
    }
    } catch(e){ debugger; }

    while(p && p.tagName != tag){
	if (!p.parentElement && (p.parentNode instanceof ShadowRoot)){
	    p = p.parentNode.host;
	} else {
	    p = p.parentElement;
	}
    }
    
    if(!p){
	return false
    } else {
	return p;
    }
}

Docuplextron.getTotalScale = function(el){
    
    /* Go back up through the passed element's ancestors
       and calculate all of the CSS scale transformations
       that are being done on it. */

    var p = el;
    var scale = 1;

    while(p && p.tagName != "X-FLOATER"){

	var cssScale = getComputedStyle(p).scale;
	if(cssScale != "none"){
	    scale *= parseFloat(cssScale);
	}
	
	if (!p.parentElement && (p.parentNode instanceof ShadowRoot)){
	    p = p.parentNode.host;
	} else {
	    p = p.parentElement;
	}
    }

    return scale;
}

/***/

Docuplextron.frameOf = function(win){
    // Because .frameElement of cross-domain iframes is restricted, we
    // sometimes have to iterate over the <iframe>s in the document to
    // find the container of an incoming message.
    if(win == window){ return window };
    try{
	var f = win.frameElement;
    } catch(e) {
	var iframes = Array.from(document.getElementsByTagName("IFRAME"));
	for(var ix in iframes){
	    if (iframes[ix].contentWindow == win){
		return iframes[ix];
	    }
	}
	//console.log("Couldn't find iframe.");
	return false;
    }
    return f;
}

/***/

Docuplextron.prototype.saveSelectionRanges = function(){

    /* When we manipulate DOM elements that contain the current selection
       (or caret), this and restoreSelectionRanges() are used to restore the selection.*/
    
    var sel = window.getSelection();
    var ranges = [];

    for (var ix = 0; ix < sel.rangeCount; ix++){
	ranges.push(sel.getRangeAt(ix));
    }
    this.savedRanges = ranges;
}

/***/

Docuplextron.prototype.restoreSelectionRanges = function(){

        /* When we manipulate DOM elements that contain the current selection
       (or caret), this and saveSelectionRanges() are used to restore the selection.*/

    if (!this.savedRanges) return;
    var sel = window.getSelection();
    sel.removeAllRanges();
    while(this.savedRanges.length > 0){
	sel.addRange(this.savedRanges.shift());
    }
}

/***/

Docuplextron.prototype.gotoHelp = function(){
    var flap = this.bar.querySelector("#dptronSideFlap");
    if(flap.style.right != "0px") this.toggleFlap();
    this.showFlapTab("help");
}

/***/

Docuplextron.prototype.toggleFlap = function(){

    var flap = this.bar.querySelector("#dptronSideFlap");
    var toggle = flap.querySelector("#sideFlapTabToggle");
    if (flap.style.right == "0px"){
	toggle.style.transform = "rotate(0deg)";
	toggle.style.left = "-32px";
	flap.style.right = "-500px";
	flap.style.overflow = "visible";
    } else {
	this.updateSourcesFlap();
	this.updateLinksFlap();
	toggle.style.left = "0px";
	toggle.style.transform = "rotate(180deg)";
	flap.style.right = "0px";
	flap.style.overflowX = "hidden";
	flap.style.overflowY = "hidden";
    }
}

/***/

Docuplextron.prototype.showFlapTab = function(tab){
    var flap = this.bar.querySelector("#dptronSideFlap");
    var tabs = {"help":{"toggle":"#helpPaneToggle",
			"pane":"#helpPane"},
		"sources":{"toggle":"#sourcesPaneToggle",
			   "pane":"#sourcesPane"},
		"settings":{"toggle":"#settingsPaneToggle",
			    "pane":"#settingsPane"},
		"session":{"toggle":"#sessionPaneToggle",
			   "pane":"#sessionPane"},
		"links":{"toggle":"#linksPaneToggle",
			 "pane":"#linksPane"}
	       };
    
    var tabKeys = Object.keys(tabs);
    for(var ix in tabKeys){
	var tabKey = tabKeys[ix];
	var thisTab = tabs[tabKey];
	var thisToggle = this.bar.querySelector(thisTab.toggle);
	var thisPane = this.bar.querySelector(thisTab.pane);
	if(tabKey == tab){   
	    thisToggle.classList.add("dptronActiveTab");
	    thisPane.style.display = (tab == "help") ? "block" : "flex";
	} else {
	    thisToggle.classList.remove("dptronActiveTab");
	    thisPane.style.display = "none";
	}
    }

    if(tab == "sources"){
	this.updateSourcesFlap();
    } else if(tab == "session"){
	this.updateSessionList();
    } else if(tab == "links"){
	this.updateLinksFlap();
    }
}

/***/

Docuplextron.taintId = function(el){

    /* Simply append "(modified)" to the id of a context view that has
       been altered. */
    
   if(Docuplextron.getParentItem(el).id){
	if(!Docuplextron.getParentItem(el).id.endsWith("(modified)")){
	    Docuplextron.getParentItem(el).id = Docuplextron.getParentItem(el).id + "(modified)";
	}
    }
}

/***/

Docuplextron.deParent = function(node){
    
    /* Pass me an element, and I will replace that element with its children.
       I return a reference to the passed node's first child if it had any, 
       or its previous sibling, or its parent. Whatever we can get. */
    
    var frag = document.createDocumentFragment();
    var p = (node.firstChild || node.previousSibling || node.parentElement);
    while(node.firstChild){
	frag.appendChild(node.firstChild);
    }
    node.parentElement.replaceChild(frag,node);
    return p;
}

/***/

Docuplextron.prototype.activate = function(el,multi){
    
    /* Set our activeItem and do any required representational modifications on it.*/

    if(el.classList.contains("nonContextual")) {
	if(!multi){
	    el.toTop();
	}
	return;
    }
    
    if(!multi){
	for(var ix in this.activeItems){
	    if(this.activeItems[ix] != el){
		this.deactivate(this.activeItems[ix]);
	    }
	}
	this.activeItems = [el];
	el.classList.add("alphActiveItem");
	if(el != this.mainDiv){
	    el.tab.classList.add("alphActiveItem");
	    el.toTop();
	}
    } else{
	var itemIx = this.activeItems.indexOf(el);
	if(itemIx >= 0){
	    // Floater is already in the array. Remove it.
	    this.deactivate(el);
	    this.activeItems.splice(itemIx,1);
	} else {
	    el.classList.add("alphActiveItem");
	    el.tab.classList.add("alphActiveItem");
	    this.activeItems.push(el);
	}
    }
    //console.log(this.activeItems);
}

/***/

Docuplextron.prototype.deactivate = function(floater){
    
    /* Do things to the no-longer active item... */
    
    floater.classList.remove("alphActiveItem");
    if(floater != this.mainDiv){
	floater.tab.classList.remove("alphActiveItem");
    }
}

/***/

Docuplextron.prototype.gradientMorpher = function(tron){
    
    /* Totally unnecessary, but a pleasant effect. Mutates the
       background gradient of the workspace. */

    if(document.getElementById("dptronGradMorphToggle").checked){
    
	if(tron.mainDiv.style.background.includes("linear-gradient")){
	    g = tron.mainDiv.style.background;
	} else if(window.getComputedStyle(tron.mainDiv).backgroundImage.includes("linear-grad")){
	    g = window.getComputedStyle(tron.mainDiv).backgroundImage;
	} else return;
	var deg = g.split("deg")[0];
	deg = deg.split("(");
	deg = parseInt(deg[deg.length-1]);
	g = g.split("rgb(");
	var gb = g[2].trim('"').split(')')[0].split(',');
	g = g[1].trim('"').split(")")[0].split(',');
	
	var g_ix = parseInt(Math.random()*4);
	var gb_ix = parseInt(Math.random()*4);
	var r1 = 1 - parseInt(Math.random()*3);
	var r2 = 1 - parseInt(Math.random()*3);
	var r3 = 1 - parseInt(Math.random()*3);
	
	g[g_ix] = Math.max(Math.min(parseInt(g[g_ix]) + r1, 255),0);
	gb[gb_ix] = Math.max(Math.min(parseInt(gb[gb_ix]) + r2, 256),0);
	deg = (parseInt(deg) + r3) % 360;
    
	tron.mainDiv.style.background = "linear-gradient(" + deg + "deg, rgb(" + g[0] + ", " + g[1] + ", " + g[2] + "), rgb(" + gb[0] + ", " + gb[1] + ", " + gb[2] + "))";
    }
    tron.morpherTimeout = setTimeout(tron.gradientMorpher,300,tron);
}

Docuplextron.prototype.sessionSave = function(dryrun, withNoodles){
    
    /* Save sessions to local storage. 

       Returns a non-stringified session object. 

       When called with dryrun == true, the session is not actually saved.

       When called with withNoodles == true, any noodles open in the 
       workspace will be included in the session object. This is for 
       exporting workspaces. */
    
    var restore = [];
    var floaters = this.floaterManager.getMembers();
    for (var ix in floaters){
	var floater = floaters[ix];
	var fSave = {};
	if(floater.classList.contains("noodleBox")){
	    fSave.type = "noodle";
	    var noodleID = floater.querySelector("x-text").getAttribute("src");
	    fSave.source = noodleID;
	    if(withNoodles){
		fSave.content = this.noodleManager.get(noodleID);
	    }
	} else if(floater.style.display != "none"){
	    fSave.type = "composition";
	    if(floater.id.startsWith("http:") ||
	       floater.id.startsWith("https:") ||
	       floater.id.startsWith("tag:") ||
	       floater.id.startsWith("_:EDL") ||
	       floater.id.startsWith("ld+") ){
		/* These are all resources that loadExternal() can take
		   care of. Don't bother saving the HTML contents of the 
		   floater. */
		fSave.source = floater.id;
	    } else {
		var floaterClone = floater.cloneNode(true);
		floaterClone.removeChild(floaterClone.querySelector("svg.alph-floater-overlay"));
		/* We need to empty-out the X-TEXT elements in these
		   things! I just realized that I've got all of Edgar
		   Rice Burroughs' A Princess of Mars as well as
		   Bacon's Novum Organum sitting in my workspace
		   storage. Yikes! Empty 'em out and just fetch-and-fill on 
		   workspace restore, right? */
		var xtxts = Array.from(floaterClone.querySelectorAll("x-text"));
		for (var ix in xtxts){
		    xtxts[ix].textContent="…";
		}   
		fSave.source = encodeURIComponent(floaterClone.outerHTML);
	    }
	} else {
	    continue;
	}
	
	/* There is almost certainly a better way to do this. Save all attributes to an 
	   attributes property or something. */
	
	fSave.z = parseInt(floater.getAttribute("data-z")) || parseInt(floater.style.zIndex) || 100;
	fSave.scale = floater.getAttribute("data-scale") || parseFloat(floater.style.transform.match(/scale\((.*)\)/)[1]);
	fSave.sticky = floater.sticky;
	fSave.style = floater.style.cssText;

	restore.push(fSave);
    }

    // Save links...
    var graphKeys = Object.keys(this.linkStore.graphs);
    
    for (var graphIx in graphKeys){

	var graphDoc = this.linkStore.graphs[graphKeys[graphIx]];
	
	/* Let's do this like we do floaters: if it's an external resource with an
	   http(s) URL, just save the URL and have the Docuplextron try to reload
	   it when the session/workspace is called-up. */
	
	if(graphDoc["@id"].startsWith("http")){
	    
	    restore.push({"type" : "link",
			  "link" : graphDoc["@id"] });
	    
	} else {
	    
	    /* In the LinkStore, the graph's "graph" property is a DOM
	       element and it won't stringify, so we need to make a
	       copy of the graph and replace its "graph" with a
	       localStorage-safe representation. */

	    /* 2024-06-16 - for now, we'll stop saving graphs in here since
	       LinkStore.js has been changed to save all graphs to localStorage.
	       We'll continue loading graphs back-in via sessionRestore() though.

	    var graphDocCopy = JSON.parse(JSON.stringify(graphDoc));
	    var safeGraph = [];
	    var linkElements = Array.from(graphDoc.graph.children);
	    for(var lix in linkElements){
		safeGraph.push(
		    {
			"anode" : linkElements[lix].getAttribute("anode"),
			"bnode" : linkElements[lix].getAttribute("bnode"),
			"rel" : linkElements[lix].getAttribute("rel")
		    }
		);
	    }

	    graphDocCopy.graph = safeGraph;
	    
	    restore.push({"type" : "link",
			  "link" : graphDocCopy });
*/
	}
    }

    /* Store the active graphs list ... */
    
    restore.push({"type":"activeGraphs",
		  "list":this.activeGraphs});
    
    /* Save some other settings ... */

    var dptronState = {
	    "type":"dptronState",
	    "mainDivStyle":this.mainDiv.style.cssText
    };
    if(document.getElementById("dptronUserStyle")){
	dptronState.userStyle = document.getElementById("dptronUserStyle").textContent;
    }
    
    restore.push(dptronState);
    
    if(!dryrun){
	localStorage.setItem("dptronSession-"+window.location.toString(),
			     JSON.stringify(restore));
    }
    //console.log(restore);
    return restore;
}

Docuplextron.prototype.autoRestore = function(){
    if(localStorage["dptronSession-"+window.location.toString()]){
	this.sessionRestore("dptronSession-"+window.location.toString());
    }
}
	
Docuplextron.prototype.sessionRestore = function(session,merge){   
    var restore;

    /* Turn OFF session auto-save. In the event that this restore fucks-up,
       we don't want to automatically over-write the old session, do we? */

    dptron.autoSaveSafe = false;

    if(!merge){
	this.floaterManager.destroyAll();
	
	/* We dont' want to nuke the linkStore anymore, just the list of 
	   active graphs... */
	// this.linkStore.graphs = {};
    }
    
    if(session){
	try{
	    restore = JSON.parse(localStorage.getItem(session));
	} catch(e){
	    console.log("DPTRON SESSION RESTORE FAILED: ",e);
	    return false;
	}
    } else {
	restore = {};
	delete this.mainDiv.backgroundColor;
	delete this.mainDiv.backgroundImage;
    }

    /* Initialize the active graphs list. */
    this.activeGraphs = [];
    
    console.log("Trying to restore session from: ",restore);
    
    for(var ix in restore){
	var floater = restore[ix];
	if(floater.type == "dptronState"){
	    if(floater.mainDivStyle){
		this.mainDiv.style.cssText = floater.mainDivStyle;
	    }
	    if(floater.userStyle){
		document.getElementById("dptronUserCSS").value = floater.userStyle;
		var uSheet = document.getElementById("dptronUserStyle") ||
		    document.getElementById(document.body.appendChild(document.createElement("style")).id = "dptronUserStyle");
		uSheet.textContent = floater.userStyle;
	    }
	} else if(floater.type == "link"){
	    
	    /* We need this to not crap its pants when loading old-style links. */
	    
	    try{
		if ( typeof(floater.link) == "string" ){
		    
		    /* This is just a URL of a graph document -- in theory -- so
		       just throw it to loadExternal() */

		    this.loadExternal(floater.link, "AlphLink");
		    
		} else {

		    /* A quick check to see if this is one of our new-style 
		       graph documents. If it's not, don't load it; into
		       the eternal bit-bucket it goes. */

		    if ( floater.link.graph && floater.link["@id"]){
			var graphDoc = floater.link;

			/* Turn the JSON-safe graph list back into a DOM */
			
			var graphElement = document.createElement("Graph");
			for(var lix in graphDoc.graph){
			    var linkElement = document.createElement("Link");
			    linkElement.setAttribute("anode", graphDoc.graph[lix].anode);
			    linkElement.setAttribute("bnode", graphDoc.graph[lix].bnode);
			    linkElement.setAttribute("rel", graphDoc.graph[lix].rel);
			    graphElement.appendChild(linkElement);
			}
			graphDoc.graph = graphElement;

			this.linkStore.graphs[graphDoc["@id"]] = graphDoc;
		    }
		}

	    } catch(e){
		    
		console.log("Problem loading link during session restore: ", floater, e);

	    }
	} else if(floater.type == "activeGraphs"){
	    /* SHOULD be as easy as this... */
	    this.activeGraphs = floater.list;
	} else {
	    if(floater.type == "composition"){
		if(floater.source.startsWith("http:") ||
		   floater.source.startsWith("https:") ||
		   floater.source.startsWith("tag:") ||
		   floater.source.startsWith("_:") ||
		   floater.source.startsWith("ld+") ){
		    var f = this.loadExternal(floater.source);
		    if(!f) continue;
		    //console.log("Got ", f, " back from loadExternal()");
		} else {
		    
		    /* Make a DOM from the stored HTML fragment */
		    
		    var parser = new DOMParser();
		    var t = parser.parseFromString(decodeURIComponent(floater.source),"text/html").querySelector("x-floater");

		    /* Because t is an <x-floater>, but NOT a Floater
		       object, we have to copy over its children, and
		       reassign its attributes to a newly-created
		       floater to make it work properly. */
		    
		    var f = this.floaterManager.create(null,t.id);
		    for (var iy in t.children){
			try{
			    f.appendChild(t.children[iy]);
			} catch(e){
			    //console.log(t.children,iy);
			}
		    }
		    
		    for (var iy in Object.keys(t.attributes)){
			f.setAttribute(
			    t.attributes[iy].name,
			    t.attributes[iy].value);
		    }
		}
	    } else if(floater.type == "noodle"){
		if(floater.content){
		    /* The noodle has been bundled with the workspace. What we
		       SHOULD be doing here is checking to see if a noodle with a
		       matching ID is already in the noodleStore, and if there is, 
		       we should show both versions to the user and let them choose 
		       whether to overwrite the existing noodle with the import or
		       create a new copy, yadda, yadda... 

		       ...but! That's what we SHOULD be doing. Just to get this
		       going in a hurry, we're gonna clobber whatever might already
		       be in the noodleStore */
		    
		    this.noodleManager.noodles[floater.content.key] = floater.content;
		}         
		var f = this.newdleBox(floater.source,null,true);
	    }

	    /* Put this whole mess in a try{} block... ugh. */

	    try{
		f.style.cssText = floater.style;

		if(f.style.transform){
		    f.setAttribute("data-scale", parseFloat(f.style.transform.match(/scale\((.*)\)/)[1]));
		} else {
		    f.setAttribute("data-scale", floater.scale);
		    f.style.transform = "scale(" + floater.scale + ");";
		}

		if(f.style.zIndex){
		    f.setAttribute("data-z",f.style.zIndex);
		} else {
		    f.setAttribute("data-z", floater.z);
		    f.style.zIndex = floater.z;
		}

		if(floater.sticky){
		    f.sticky = floater.sticky;
		    f.setAttribute("data-sticky",f.sticky);
		}
	    } catch(e){
		console.log("Floater styling error: ", e);
	    }
	}
    }
    alph.getXSources();
    alph.fetchAndFill();
    dptron.autoSaveSafe = true;
    this.initBarGDS();    
    return true;
}

Docuplextron.prototype.makeSessionExport = function(sessionID, withNoodles){
    
    /* Creates a JSON(-LD?) document containing the workspace  */
    
    var withNoodles = (typeof withNoodles == 'undefined') ? true : withNoodles; 
    var session = {"@context":"http://alph.io/terms.jsonld",
		   "@type":"Docuplex"};
    if(!sessionID){
	// Create a new session object from the current session
	session.items = this.sessionSave(true, withNoodles); 
    } else {
	try{
	    session.items = JSON.parse(localStorage.getItem(sessionID));
	} catch(e){
	    this.pukeText("Could not prepare session for export.", e);
	    return;
	}
    }
    return JSON.stringify(session, null, 5);
}

Docuplextron.prototype.exportSession = function(sessionID, withNoodles){

    /* Used to just be this:

       window.open("data:application/json;encoding=utf-8," +
            encodeURIComponent(JSON.stringify(this.sessionSave(true)))
	    ); 

       There really should be a stage in the process BEFORE this. There should
       be a modal that: 

       - lets the user choose the name of the exported 'plex as
       well as its target server/directory; 
       - has the option to append a timestamp string to the filename;
       - has a textarea in which to preview the JSON; 
       - ...other things...
 
    */
    
    this.postMedia("PUT",
		   this.makeSessionExport(sessionID, withNoodles),
		   true,
		   null,
		   null,
		   function(xhr,url,method,data,bin){
		       /* Do something when the upload succeeds... */
		       var d = Docuplextron.newDialog(
			   null,
			   "Uploaded to " + url + " successfully. Open it in a new tab?"
		       );
		       d.okbutton.onclick = function(url,floater){
			   window.open(url,"foo");
			   floater.destroy();
		       }.bind(this,url,d.floater);
		   }.bind(this)
		  );
}

Docuplextron.highlightMatching = function(text,matchtext,container){
    var matchIx = 0;
    while(matchIx >= 0){
	var matchIx = text.toLowerCase().indexOf(matchtext.toLowerCase());
	if(!matchIx || matchIx < 0) break;
	
	var matchHead = text.substring(0,matchIx);
	var matchBody = document.createElement("mark");
	matchBody.textContent = text.substr(matchIx,matchtext.length);
	container.appendChild(document.createTextNode(matchHead));
	container.appendChild(matchBody);
	text = text.substring(matchIx + matchtext.length);
    }
    container.appendChild(document.createTextNode(text));
}

Docuplextron.makePaletteItems = function(items,container){
    for(var ix in items){
	var item = items[ix].split("|");
	var span = document.createElement("span");
	span.className = "dptronPaletteItem";
	var marked = document.createElement("mark");
	marked.textContent = item[1];
	span.appendChild(document.createTextNode(item[0]));
	span.appendChild(marked);
	span.appendChild(document.createTextNode(item[2]));
	container.appendChild(span);
    }
}

Docuplextron.colorIxFromStyle = function(color){
    if(!color || color == "transparent") return 0;
    color = color.split("(")[1];
    color = color.split(")")[0];
    color = color.split(",");
    for(var ix in color){
	color[ix] = parseInt(color[ix]);
    }
    
    return (color[0] * 65536) + (color[1] * 256) + color[2];
}

Docuplextron.hexColorFromIx = function(index){
    var r = parseInt(index / 64);
    var g = parseInt((index - (r * 64)) / 8);
    var b = index % 8;
    console.log(r,g,b);
    return "#" + parseInt(2 * r,16) + parseInt(2 * g,16) + parseInt(2 * b,16);
}

Docuplextron.pad = function(str,len){
    if(str.length >= len) return str;
    var padString = "";
    for(var ix = str.length; ix < len; ix++){
	padString += "0";
    }
    return "" + padString.toString() + str;
}

Docuplextron.colorSpin = function(color,dir){
    //var colorIX = ((dir * (524538 + 2048 + 8)) +
    //var colorIX = ((dir * (459002 + 1792 + 7)) +
    var colorIX = ((dir * (1016293 + 3968 + 15)) +
		   Docuplextron.colorIxFromStyle(color)) % 16777216;
    if (colorIX < 0){
	colorIX = 16777216 + colorIX;
    }
    return Docuplextron.pad(colorIX.toString(16),6);
}
/* 




   ARRANGEMENT




*/

Docuplextron.prototype.showActiveElement = function(){
    
    /*  If the activeElement is not inside of the visible workspace,
	move its container into view. */
    
    if(this.activeElement.nodeType != Node.ELEMENT_NODE){
	var r = this.activeElement.parentElement.getBoundingClientRect();
    } else {
	var r = this.activeElement.getBoundingClientRect();
    }
    
    if(r.top > window.innerHeight){
	var pe = Docuplextron.getParentItem(this.activeElement);
	if(pe){
	    var oldTop = parseInt(pe.style.top);
	    pe.style.top = oldTop - (r.height * 2) + "px";
	}
    } else if(r.top < 0){
	var pe = Docuplextron.getParentItem(this.activeElement);
	if(pe){
	    pe.style.top = "0px";
	}
    }
    this.DOMCursor.select(this.activeElement);
}

Docuplextron.prototype.cursorOnscreen = function(top,bottom,left,right,frame){

    /* Pans the workspace so that the caret remains visible. Handy
       when editing text in a floater and the next line goes off the
       end of the screen. Kind-of a nuisance sometimes though -- like
       when you're not editing text and any keypress causes the
       workspace to pan to wherever you last left the caret. */
        
    top = parseFloat(top);
    bottom = parseFloat(bottom);
    left = parseFloat(left);
    right = parseFloat(right);

    /* This IFRAME bit can ~PROBABLY~ be removed. */
    
    if (frame.tagName == "IFRAME"){
	var frameBox = frame.getBoundingClientRect();
	var frameScale = parseFloat(Docuplextron.getParentItem(frame).getAttribute("data-scale"));
	top = (top * frameScale) + frameBox.top;
	bottom = (bottom * frameScale) + frameBox.top;
	left = (left * frameScale) + frameBox.left;
	right = (right * frameScale) + frameBox.left;
    }
    
    var deltaX = 0;
    var deltaY = 0;
    var upperBound = (window.innerHeight - this.floaterManager.topBoundary) * 0.05;
    var lowerBound = window.innerHeight * 0.95;
    var innerBound = window.innerWidth * 0.05;
    var outerBound = window.innerWidth * 0.95;
    
    if(bottom > lowerBound){
	deltaY = lowerBound - bottom;
    } else if(top < upperBound){
	deltaY = upperBound - top;
    }

    if(right > outerBound){
	deltaX = outerBound - right;
    } else if(left < innerBound){
	deltaX = innerBound - left;
    }

    if(deltaX != 0 || deltaY != 0){
	this.floaterManager.smoothPanWorkspace(deltaX*1.05,deltaY*1.05);
    }
}

/***/

Docuplextron.prototype.lineEmUp = function(){
    
    /* Find all the floaters and put 'em in a row. Panic move. */
    
    var floaters = Array.from(document.querySelectorAll(".alph-floater"));
    for(var ix in floaters){
	var f = floaters[ix];
	f.setAttribute("data-z",50);
	f.setAttribute("data-scale",0.5);
	f.style.transform = "scale(0.5)";
	f.style.zIndex = "50";
	f.style.top = this.bar.getBoundingClientRect().bottom + "px";
	if(ix > 0){
	    f.style.left = (parseInt(floaters[ix-1].style.left) + floaters[ix-1].getBoundingClientRect().width) + "px";
	} else {
	    f.style.left = "0px";
	}
    }
}

/*





  LINKS 
  (a hot mess)





*/

Docuplextron.prototype.pushAllLinks = function(){

    /* Find all of the <link> and <a> links in the document
       (workspace?), convert them to an Alph representation, then post
       them out to the parent context.*/

    /* This is essentially deprecated. What we need is something that
       gets fired-off when loadExternal() brings in a document. It needs
       to grab the HTTP headers from the request and store any links from 
       there, then grab any <link> elements in an HTML document's <head> and
       store links from there. <a>nchors... I don't know if I still want to do that.

       Leaving this in for the time being, because I may want to look over
       it when implementing the newer method. */

    var headlinks = Array.from(document.head.querySelectorAll('link'));
    
    if(this.dptronMode){	
	var anchors = Array.from(document.getElementById("alph-floaters").querySelectorAll('a'));
	console.log("found anchors: ",anchors);
    } else {
	var anchors = Array.from(document.body.querySelectorAll('a'));

	/* If we're not in Docuplextron mode, we still don't want to get any
	   anchors that are part of the Alph crap that's been loaded into the page,
	   so...
	
	   Ugh. This is so ugly. */
	
	var nonalph = [];
	for(var ix in anchors){
	    if (!Docuplextron.getParentById(anchors[ix],"alph-main") &&
		!Docuplextron.getParentById(anchors[ix],"dptron-flap") &&
		!Docuplextron.getParentById(anchors[ix],"alphOverlay") &&
		!Docuplextron.getParentById(anchors[ix],"alph-footer") &&
		!Docuplextron.getParentById(anchors[ix],"alph-DOMCursor") &&
		!Docuplextron.getParentById(anchors[ix],"alph-DOMCursorInfoText")
	       ){
		nonalph.push(anchors[ix]);	
	    }
	}
	anchors = nonalph;
    }
    
    for(var ix in headlinks){
	
	/* Convert <LINK> elements in the <HEAD> into our internal link
	   representation. */
	
	var hlink = headlinks[ix];
	var alink = Alink.fromLink(hlink);
	this.linkStore[alink.id] = alink;
    }
    
    for(var iy in anchors){
	var anchor = anchors[iy];
	if(!anchor.href) continue;

	if (this.dptronMode){
	    var anchorFloater = Docuplextron.getParentItem(anchor);
	    var link = new Alink(
		anchorFloater.id + (
		    (anchor.id != "") ?
			("#" + anchor.id) :
			("#" + Alphjs.getElementPointer(anchor,anchorFloater))
			//("#xpath:" + Alphjs.getXPath(anchor,anchorFloater))
		),
		"Jumplink",
		Alinks.bodyFromAnchor(anchor),
		Alinks.targetFromAnchor(anchor));
	} else {
	    var link = new Alink(
		document.location.toString() + (
		    (anchor.id != "") ?
			("#" + anchor.id) :
			("#" +  Alphjs.getElementPointer(anchor))
			//("#xpath:" + Alphjs.getXPath(anchor))
		),
		"Jumplink",
		Alinks.bodyFromAnchor(anchor),
		Alinks.targetFromAnchor(anchor));

	}
	
	if(anchor.rel != "") link.rel = anchor.rel;
	if(anchor.title != "") link.title = anchor.title;
	
	this.linkStore[link.id] = link;
    }
    //this.buildLinkpointerSpans();
}

/***/

Docuplextron.prototype.getNodeRects = function(node){
    /* 
       Return DOMRects highlighting the media in the span.

       node should be an AlphSpan. 

       Because we may be returning multiple DOMRect arrays, we return an
       array of objects with a reference to the containing floater, an 
       array of DOMRects, and - because we may be returning rects for
       partial matches, a flag indicating the completeness of match:

           { 
	     floater: reference_to_containing_floater,
             rects: [DOMRects...],
	     clip: "none" || "origin" || "extent" || "both"
	   } 

       -----

       Right now this only works for media in the docuplextron,
       not for <IFRAME> contents.

       This function is becoming humongous. Needs to be modularized. A lot.

    */
    
    var groupArrays = [];

    if(!node.origin && node != 0){

	/* No origin/extent means the target is a document or
	   DOM element. That's easy! */

	try{
	    var hashIx = node.src.indexOf("#");
	} catch(e){
	    //console.log("Trouble in getNodeRects() with this node: ", node);
	    return groupArrays;
	}
	
	if(node.src.includes("#")) {
	    
	    /* Oh, did I say easy! Well, first...

	       This could be a URL with a barename pointer, like:
	         - http://foo.bar/file#element

	       This could be a URL with an XPointer, like:
	         - http://foo.bar/file#element(/1/4/5)
		 - http://foo.bar/file#range(point(/13/5.41),point(/2/4.56))


	       XPointer ranges are handled below, because they SHOULD be
	       hitting this function with their point() selectors set as
	       the origin/offset.

	       Barename and element() pointers SHOULD be implemented, but
	       I'm debating whether everything after the fragment selector 
	       token should be converted to origin/offset, including
	       barenemes and element pointers. */
	    var contentSource = node.src.split("#")[0];
	    var fragSelector = node.src.split("#")[1];

	    if(fragSelector.startsWith("element(")){

		/* We can actually use Alphjs.resolvePoint() for this, because a
		   point selector is the same as an element selector without
		   an offset. */
		
	    }
	    
	} else {
	    
	    /* Whole document */

	    if(node.src.startsWith("~")){
		/* does src points to a noodle? Find all floaters
		   containing an instance of that noodle. */
		var floaters = Array.from(document.body.querySelectorAll(
		    "x-text[src='" + node.src + "']"));
	    } else {
		/* does src point to a document which we may have
		   loaded into the workspace in a composition
		   floater/shadow DOM (loadExternal() marks these with
		   a content-source attribute)? */
		var floaters = Array.from(document.body.querySelectorAll(
		    "[content-source='" + node.src + "']"));
	    }
	    for(var iy in floaters){
		groupArrays.push(
		    {
			"floater" : Docuplextron.getParentItem(floaters[iy]),
			"rects" : Array.from(floaters[iy].getClientRects())
		    }
		);
	    }
	    
	    // Do we have a floater with an id that matches src? 
	    var f = document.getElementById(node.src);
	    if(f){
		groupArrays.push(
		    {
			"floater" : f,
			"rects" : Array.from(f.getClientRects()),
			"clip" : "none"
		    }
		);
	    }
	    
	    /* Finally, do we have a fragment of the document in the workspace
	       somewhere? If so, return rects of the fragment and mark its clip.
	       For the time being, I'm marking all such fragments as "both". */
	    var looseFrags = Array.from(
		document.body.querySelectorAll("[src='" + node.src + "']"));
	    for (var iy in looseFrags){
		var ffloater = Docuplextron.getParentItem(looseFrags[iy]);
		if (ffloater){
		    /* If that floater's id matches the resource's src, then 
		       we already got it up above.*/
		    if (ffloater.id == node.src) continue;
		    groupArrays.push(
			{
			    "floater": ffloater,
			    "rects" : Array.from(looseFrags[iy].getClientRects()),
			    "clip" : "both"
			}
		    );
		}
	    }
	}
    } else {
	/* So, the span has an origin/offset pair. That means this is
	   a range of some kind. We already have a function that does
	   this for text (getMatchingContentRects()), but I'm going to
	   re-write/-factor that here, in hopes of making a better
	   version. This also -- perhaps stupidly -- does some of the
	   work of the findMatching...() functions. */

	if(node.origin.toString().startsWith("point(")){
	    
	    /* This is a point() XPointer (without an offset, it's the
	       same as an element() XPointer). Two of these give us an
	       easy-peasy DOM Range. */
	    
	    var floaters = Array.from(document.body.querySelectorAll(
		"[content-source='" + node.src + "'],[content-source^='" + node.src + "#']"));

	    for(var ix in floaters){
		var clip;
		/* This WAS just:
		   var rootNode = floaters[ix];
		   
		   However, to set the rootNode for the resolvePoint()
		   queries, we need to look at the floater to see if
		   it has an X-DOM, and then set the root node to that
		   X-DOM's shadowRoot, or simply the floater itself if
		   no shadowDOM is present. I think this will work. */
		var rootNode = floaters[ix].querySelector('x-dom') ?
		    floaters[ix].querySelector('x-dom').shadowRoot : floaters[ix];
		var anchor = Alphjs.resolvePoint(node.origin,rootNode);
		var focus = Alphjs.resolvePoint(node.extent,rootNode);

		/* If we have neither anchor nor focus nodes, we're 
		   fucked, so we quit. However, if we have one or the 
		   other, this is at least a PARTIAL match. So, we return
		   a range that goes either to the start or end of the 
		   rootNode. */
		
		if(!anchor && !focus) continue;
		var rng = new Range();

		if(!anchor){
		    rng.setStartBefore(floaters[ix]);
		    rng.setEnd(focus[0],focus[1]);
		    clip = "origin";
		} else if(!focus){
		    rng.setStart(anchor[0],anchor[1]);
		    rng.setEndAfter(floaters[ix]);
		    clip = "extent";
		} else {
		    rng.setStart(anchor[0],anchor[1]);
		    rng.setEnd(focus[0],focus[1]);
		    clip = "none";
		}

		groupArrays.push(
		    { "floater" : Docuplextron.getParentItem(floaters[ix]),
		      "rects" : Array.from(rng.getClientRects()),
		      "clip" : clip
		    }
		);		
	    }
	} else {
	    /* Are there any elements in the workspace that have this
	       source as an src value?  We look for either the source
	       exactly, or a source with a fragment, which we
	       initially ignore. This is because audiovisual media
	       carry their fragment specifier in the 'src' attribute,
	       while <X-TEXT> has origin/extent attrs. */

	    var sources = Array.from(document.body.querySelectorAll(
		"[src='" + node.src + "'],[src^='" + node.src + "#']"));

	    /* Include matching sources from shadowDOMs ... */

	    var sdoms = Array.from(document.getElementsByTagName('X-DOM'));
	    for(var iy in sdoms) {
		var ssources = Array.from(
		    sdoms[iy].shadowRoot.querySelectorAll(
			"[src='" + node.src + "'],[src^='" + node.src + "#']")
		);
		if (ssources.length > 0){
		    sources = sources.concat(ssources);
		}
	    }
	    
	    for(var iy in sources){
		switch(sources[iy].tagName.toLowerCase()){
		case "x-text":
		    var xel = AlphSpan.fromXtext(sources[iy]);
		    
		    // Test for a content intersection

		    var xsect = node.intersection(xel);
		    if(!xsect) continue;

		    // If we have an intersection, use a Range object
		    // to get client rects for the relevant portion of
		    // the Text node

		    var clip;
		    if(xsect.origin == node.origin){
			clip = (xsect.extent == node.extent) ?
			    "none" : "extent";
		    }else{
			clip = (xsect.extent == xsect.extent) ?
			    "origin" : "both";
		    }
		
		    var rng = document.createRange();
		    var rngStart = xsect.origin - xel.origin;
		    var rngEnd = (xsect.extent - xsect.origin) + rngStart;

		    rng.setStart(sources[iy].childNodes[0],rngStart);
		    
		    /* If rngEnd points at an index beyond the end of the
		       text node, then there's a mismatch in the text inside
		       of the x-text and what *should* be in there. Set
		       the end of the range to the end of the text node so
		       that we can get some rects and move on, but also
		       call alph.fulfilSource() to refresh the x-text's 
		       content from its source and get the right codepoints
		       in there. */

		    if(rngEnd > sources[iy].childNodes[0].length ){

			rng.setEnd(sources[iy].childNodes[0], sources[iy].childNodes[0].length -1);

			alph.fulfilSource( node.src , sources[iy].parentElement);

		    } else {
			rng.setEnd(sources[iy].childNodes[0], rngEnd);
		    }
		    
		    groupArrays.push(
			{
			    "floater": Docuplextron.getParentItem(sources[iy]),
			    "rects" : Array.from(rng.getClientRects()),
			    "clip" : clip
			}
		    );
		    break;
		case "img":
		    if (sources[iy].hasAttribute("origin") &&
			sources[iy].hasAttribute("extent")){
			/* An x-img. This might contain the fragment in the
			   span and it might not. Easiest case is if it's 
			   a perfect match... */
			var sOrigin = sources[iy].getAttribute("origin");
			var sExtent = sources[iy].getAttribute("extent");
			if(node.origin == sOrigin && node.extent == sExtent){
			    // EASY
			    groupArrays.push(
				{
				    "floater": Docuplextron.getParentItem(sources[iy]),
				    "rects" : Array.from(sources[iy].getClientRects()),
				    "clip" : "none"
				}
			    );
			} else {
			    /* Okay, so we've ~potentially~ got a partial match, here.
			       I should probably extend Alphjs.intersection() to help
			       with this, but for the moment I'll just do it inline 
			       here....*/

			    // Get the corners of the two rectangles
			    var nOPoint = Alphjs.pointFromSpec(node.origin);
			    var nEPoint = Alphjs.pointFromSpec(node.extent);
			    var fOPoint = Alphjs.pointFromSpec(sources[iy].getAttribute("origin"));
			    var fEPoint = Alphjs.pointFromSpec(sources[iy].getAttribute("extent"));
			    
    
			    /* Now, get the overlap area*/
			    
			    var overlapX = Math.max(0, Math.min(nEPoint.x, fEPoint.x) -
						    Math.max(nOPoint.x, fOPoint.x));
			    var overlapY = Math.max(0, Math.min(nEPoint.y, fEPoint.y) -
						    Math.max(nOPoint.y, fOPoint.y));
			    if(overlapX * overlapY){

				/* Woohoo! Now just build a DOMRect that describes 
				   the overlap...*/
				
				var fullRect = Array.from(sources[iy].getClientRects())[0];
				var scaleFactor = fullRect.width / (fEPoint.x - fOPoint.x);
				/* Now... where's our origin point? */
				
				var newRect = new DOMRect(fullRect.x + ((Math.max(nOPoint.x, fOPoint.x) - fOPoint.x) * scaleFactor),
							  fullRect.y + ((Math.max(nOPoint.y, fOPoint.y)- fOPoint.y) * scaleFactor),
							  overlapX * scaleFactor,
							  overlapY * scaleFactor);
				console.log("DOMRect of image: ", fullRect, "and its naturalWidth: ", sources[iy].naturalWidth);
				groupArrays.push(
				    {
					"floater": Docuplextron.getParentItem(sources[iy]),
					"rects" : [newRect],
					"clip" : ((overlapX == (nEPoint.x - nOPoint.x)) &&
						  (overlapY == (nEPoint.y - nOPoint.y))) ? "none" : "both"
				    }
				);
			    }
			}
			
		    } else {
			/* This ~should be~ the whole image, which means that
			   we just need to get a rect representing the portion of
			   the image specified in the span. */

			/* getClientRects() will give us a DOMRectsList with one 
			   DOMRect inside of it that contains the whole image. 
			   We just need to copy that and modify its dimensions to match
			   the ... */

			var fullRect = Array.from(sources[iy].getClientRects())[0];
			var newRect = new DOMRect();
			var originPoint = Alphjs.pointFromSpec(node.origin);
			var extentPoint = Alphjs.pointFromSpec(node.extent);
			var scaleFactor = fullRect.width / sources[iy].naturalWidth;

			newRect.x = fullRect.x + (originPoint.x * scaleFactor);
			newRect.y = fullRect.y + (originPoint.y * scaleFactor);
			newRect.width = (extentPoint.x - originPoint.x) * scaleFactor;
			newRect.height = (extentPoint.y - originPoint.y) * scaleFactor;
			groupArrays.push(
			    {
				"floater": Docuplextron.getParentItem(sources[iy]),
				"rects" : [newRect],
				"clip" : "none"
			    }
			);
		    }
		    break;
		case "video":
		    break;
		case "audio":
		    break;
		case "source":
		    break;
		}
	    }

	}
    }
    return groupArrays;
    
}

/***/

Docuplextron.prototype.paintLinkTerminals = function(){

    /* This draws ALL of the link terminals and nexûs for the active
       graphs and sets-up SimplePointers between them. 

       There needs to be a more targeted version of this so that we
       don't wipe-out everything and start over every time the
       workspace changes.

       This is being re-written, but I'm leaving the old code commented below,
       for my own reference. 

    */    
    
    // Remove old terminals

    var oldTerms = Array.from(document.body.querySelectorAll(".linkTerminalGroup"));
    for(var ix in oldTerms){
	if (oldTerms[ix].classList.contains("dptron-linkerTerminalB") ||
	    oldTerms[ix].classList.contains("dptron-linkerTerminalA")){
	    /* Don't kill the linker terminals! */
	} else {
	    oldTerms[ix].remove();
	}
    }

    /* Remove old nexûs

      this.simpleNexi is an object where each nexus corresponds to a
      node in the linkStore, and is keyed by the node's @id.

      Here's how we SHOULD remove the nexus dots from the DOM and rebuild
      the simpleNexi object. */
    
    for (var nexKey in this.simpleNexi){
	    this.simpleNexi[nexKey].dot.remove();
    }
    this.simpleNexi = {};

    /* Okay, so! Run through all the nodes in all the graphs in the
       LinkStore and try to create SimpleNexus objects for any of them
       that are present in the workspace.  */
    
    for(var graphID in this.linkStore.graphs){

	/* Only do this for graphs that are currently active (they'll
	   have their box checked in the LINKS flap). */
	if (this.activeGraphs.indexOf(graphID) < 0) continue;
	
	var graph = this.linkStore.graphs[graphID];

	for(var nodeID in graph.nodes){
	    
	    if (this.simpleNexi[nodeID]) continue; // Skip if it's already in there
	    
	    var node = graph.nodes[nodeID];

	    /* 'terminals' are rectangles that enclose the media that
	       the node refers to. */
	    
	    var terminals = []; 
	    
	    /* Use getNodeRects() to get DOMRects for the media in the
	       workspace. Because it may be present in more than one place,
	       getNodeRects() returns an array of matches. */

	    var rects;
	    
	    if (node["@type"] == "EDL"){

		/* EDLs have multiple media spans to send to getNodeRects(),
		   so we have to concat() the results... */
		
		rects = [ ] ;

		for (var itemIx in node["@list"]){
		    var edlSpan = node["@list"][itemIx];
		    rects = rects.concat(
			this.getNodeRects(new AlphSpan(edlSpan["src"],
						       edlSpan["origin"],
						       edlSpan["extent"])));
		}
		
	    } else {
		
		if(typeof(node) == "string"){
		    
		    rects = this.getNodeRects(new AlphSpan(node));

		} else if(node["@type"] == "AlphSpan"){

		    rects = this.getNodeRects(new AlphSpan(node["src"],
							   node["origin"],
							   node["extent"]));
		} else {
		    try {
			rects = this.getNodeRects(new AlphSpan(node["@id"]));
		    } catch(e) {
			console.log("Couldn't get nodeRects for ", node);
			continue;
		    }
		}
	    }

	    if(rects.length == 0) continue;

	    /* So 'rects' now contains an array of objects like this:

	       { rects : the DOMRects that enclose the media in a specific context
	         floater : a reference to the containing floater for this match
	         clip : a flag indicating if this is a partial match }

	       Go through these objects and use linkTerminal() to
	       convert the DOMRects into SVG Rects and enclose them in
	       an SVG Group, then attach those to the floater's SVG
	       layer */

	    /* What we should probably do here is look for
	       complete matches first and prioritize those. If we have a
	       complete match, draw a solid .dptron-linkBridge styled line
	       to it, and just ignore all the partial matches. If we don't
	       have one of those matches, then maybe we can draw a 
	       .dptron-partialSP styled line to the partials...? */
	    
	    var wholeMatch = false;
	    for(var iz in rects){
		if(rects[iz].clip == "none") wholeMatch = true;
		var term = linkTerminal(
		    rects[iz].rects,
		    rects[iz].floater,
		    rects[iz].clip
		);
		terminals.push(term);
		rects[iz].floater.querySelector("svg").appendChild(term);
	    }

	    /* And, at last, create the SimpleNexus for this node, 
	       and SimplePointers fanning-out from it to all instances 
	       of its content in the workspace. */
	    
	    var nexus = new SimpleNexus(
		terminals,
		this.overlay,
		this,
		node,
		true,
		(wholeMatch) ? "whole" : "partial" );
	    
	    for(var iy in terminals){
		if(terminals[iy].clip == "none"){
		    this.simplePointers.push(
			new SimplePointer(
			    terminals[iy],
			    nexus.dot,
			    this.overlay)
		    );
		} else {
		    this.simplePointers.push(
			new SimplePointer(terminals[iy], nexus.dot, this.overlay, "dptron-partialSP")
		    );
		}
	    }
	    this.simpleNexi[nodeID] = nexus;
	}
    }

    /* Now that we've got nexûs for all the nodes, draw SimplePointers between all
       connected nodes. */

    this.connectLinkTerminals();
	    
}
/***/

Docuplextron.prototype.connectLinkTerminals = function(){

    /* Run through all the links in the LinkStore and connect the
       nodes that are present in this.simpleNexi.  */

    /* Let's clean-up the existing link lines first, though*/

//    for(var ix in this.simplePointers){
//	if(this.simplePointers[ix].line.classList.includes("dptron-linkBridge")
    
    for (var graphID in this.linkStore.graphs){
	var links = Array.from(this.linkStore.graphs[graphID].graph.children);
	for(var ix in links){
	    
	    var anodeID = links[ix].getAttribute("anode");
	    var bnodeID = links[ix].getAttribute("bnode");
	    var rel = links[ix].getAttribute("rel");
	    
	    /* If both nodes are in simpleNexi ... */
	    
	    if(this.simpleNexi[anodeID] && this.simpleNexi[bnodeID] ){
		var style = "dptron-partialSP";
		if( (this.simpleNexi[anodeID].type == "whole") && 
		    (this.simpleNexi[bnodeID].type == "whole") ) {
		    style = "dptron-linkBridge";
		}
		var p = new SimplePointer(
		    this.simpleNexi[anodeID].dot,
		    this.simpleNexi[bnodeID].dot,
		    this.overlay,
		    style,
		    {"marker-end":"url('#dptron-linkPointer')",
		     "title": rel});
		p.line.setAttribute("title", links[ix].getAttribute("rel"));
		p.line.setAttribute("rel", links[ix].getAttribute("rel"));
		p.line.setAttribute("anode", anodeID);
		p.line.setAttribute("bnode", bnodeID);
		p.line.setAttribute("graph", graphID);
		p.line.onclick = function(line){
		    this.linkMunglor(line.getAttribute("anode"),
				     line.getAttribute("bnode"),
				     line.getAttribute("rel"),
				     line.getAttribute("graph"));
		}.bind(this, p.line);
		p.line.oncontextmenu = function(line,evt){
		    evt.preventDefault();
		    this.activeMenu = this.linkLineContextMenu(line);
		}.bind(this, p.line);
		this.simplePointers.push(p);
	    }
	}
    }

}

/***/
function linkTerminal(rects,offset,clip){
    
    /* Takes an array of DOMRects and a DOM element (an x-floater
       usually) to use as an offset container. Converts the DOMRects
       into an SVG Polygon and puts it in an SVG Group for scaling/offsetting. 
       Returns the Group.  */
    
    var g = svgGroup();

    g.clip = clip || "none";

    if(offset){
	var fRect = offset.getBoundingClientRect();
	var fScale = parseFloat(offset.getAttribute("data-scale"));
	var tScale = 1 / fScale;
	/* We also need to compensate for any border that the floater might have...*/
	var borderOffset = {"x": fScale * parseFloat(getComputedStyle(offset)["border-left-width"]),
			    "y": fScale * parseFloat(getComputedStyle(offset)["border-top-width"]) };
	g.setAttribute("transform",
		       "translate(" + (tScale * (0 - (fRect.left + borderOffset.x))) + "," +
		       (tScale * (0 - (fRect.top + borderOffset.y))) + ")" +
		       " scale(" + tScale + ")"
		      );
    }
    g.setAttribute("class","linkTerminalGroup");

    /* These handlers don't actually do anything right now, because
       link terminals are styled to ignore pointer events.  */

    g.onmouseover = function(evt){
	this.style.fill = "rgba(192,192,192,0.66)";
	console.log(this);
    }.bind(g);
    g.onmouseout = function(evt){
	this.style.fill = "";
    }.bind(g);

    /* Convert the passed rects into an enclosed polygon, like we 
       do in getMatchingContentRects(). */

    var pointStringL = "";
    var pointStringR = "";
    for (var ix in rects){
	var l = rects[ix];
	var r = rects[(rects.length - 1) - ix];
	pointStringL += l.left + "," + l.top + " " +
	    l.left + "," + l.bottom + " ";
	pointStringR += r.right + "," + r.bottom + " " +
	    r.right + "," + r.top + " ";
    }
    g.appendChild(svgPolygon(pointStringL + pointStringR, ""));
    
    /* We used to just convert the DOMRects to SVGRects, like so...

    for (var ix in rects){
	var domRect = rects[ix];
	var r = svgRect(domRect.left,
			domRect.top,
			domRect.width,
			domRect.height,
			"linkTerminal"
		       );
	g.appendChild(r);
    } */

    return g;
}

function flashRects(layer,rects){
    /* Testing function. 

       rects is an Array of DOMRect objects.
       layer is an <SVG> element
       
       Draw some SVG rectangles onto the passed layer and have 
       those rectangles remove themselves after a moment. 
    */
    
    for (var iz in rects){
	var dr = rects[iz];
	var r = svgRect(dr.left,
			dr.top,
			dr.width,
			dr.height,
			"linkTerminal"
		       );
	layer.appendChild(r);
	window.setTimeout(function(){
	    this.remove();
	}.bind(r), 2000);
    }
}

/***/


/*





  OVERLAY LOOP 





 */

Docuplextron.prototype.redraw = function(){

    /* Lots of little update/redraw functions to call, here... */
    
    this.killSvgEphemerons();
    this.buildBridges();
    this.DOMCursor.updatePosition();
    this.updateNexusBiases();
    this.updateNexus();
    this.updateSimplePointers();
    this.alphCaret.update();
    if(this.linker) this.linker.update();
    
    /* Fix the size of the overlay layer if the window has changed dimensions. */
    
    if (this.overlay.getAttribute("width") != window.innerWidth){
	this.overlay.setAttribute("width", window.innerWidth);
    }
    
    if (this.overlay.getAttribute("height") != window.innerHeight){
	this.overlay.setAttribute("height", window.innerHeight);
	this.floaterManager.bottomBoundary = window.innerHeight;
    }

}

Docuplextron.prototype.updateSimplePointers = function(){

        for(var ix in this.simplePointers){
	if (!this.simplePointers[ix].dead){
	    this.simplePointers[ix].update();
	}
    }

}

Docuplextron.prototype.updateNexusBiases = function(){

    /* The idea here is, find all pairs of connected SimpleNexus
       objects, have them report to each other which direction they're
       oriented-in, so that when they run their update() functions
       they can determine which side of their link terminals to appear
       on. */

    for (var id in this.simpleNexi){
	
	let nexus = this.simpleNexi[id];

	/* Reset the bias */

	nexus.xBias = 0;
	nexus.yBias = 0;
	
	/* Get a list of Link objects from the LinkStore that
	   reference this node ... */
	
	var matches = this.linkStore.getLinksWithNode(id);

	for (var mix in matches){

	    /* Find out what the other node is in the link ... */
	    
	    let othernode = ( matches[mix].link.getAttribute("anode") == id ) ?
		matches[mix].link.getAttribute("bnode") :
		matches[mix].link.getAttribute("anode");

	    /* Look for the other node in simpleNexi. If it's not in there,
	       then the resource/fragment is not in the workspace. */
	    
	    let conexus = this.simpleNexi[othernode];

	    if(!conexus) continue;

	    /* Okay, now that we have the nexus of another node, we need
	       to determine its orientation relative to the node that 'id'
	       refers to. */

	    nexus.adjustBias(
		( (nexus.mean.right - ((nexus.mean.right - nexus.mean.left) / 2)) <
		  (conexus.mean.right - ((conexus.mean.right - conexus.mean.left) / 2)) )  ? 1 : -1 , 0 );
	}
    }
}

Docuplextron.prototype.updateNexus = function(){

    for(var nexus in this.simpleNexi){
	try{
	    if(!this.simpleNexi[nexus].dead){
		this.simpleNexi[nexus].update();
	    }
	} catch (e){
	    
	}
    }

}
/* 





   TRANSPOINTERS





*/

Docuplextron.prototype.updateContexts = function(){
    
    /* Get an array of all the contexts/documents in the workspace.

       These contexts will be used as the identifying elements in the
       dptron.edlStore arrays. This gives us a list of contexts to
       interrogate about their EDLs.

       NOT BEING USED, H27.1158
    */
    
    if(this.dptronMode){
	this.contexts = this.floaterManager.getMembers();
    } else {
	this.contexts = [ window ];
    }

    // ***KILLMELATER
    var iframes = Array.from(document.getElementsByTagName("IFRAME"));
    for(var ix in iframes){
	try{
	    this.contexts.push(iframes[ix].contentWindow);
	} catch(e) {
	    //console.log(e);
	}
    }
}

/***/

Docuplextron.prototype.findMatchingContent = function(){
    
    // Dispatcher for the various find*Matching*() functions.
    
    if(this.transpointerMode == 0){
	this.findAllMatchingContent();
    } else if(this.transpointerMode == 1){
	this.findSomeMatchingContent(this.currentContext);
    } else if(this.transpointerMode == 2){
	this.findMatchingContentFor(this.currentSpan);
    } else {
	// Do nothing if transpointerMode is 3
    }
}



Docuplextron.prototype.findMatchingContentFor = function(span){

    /* Compare the passed content span against the EDLs of all open
       documents, and store matches to dptron.transpointerSpans. */

    if (!span) return this.updateAllRects();
    if (!this.currentContext) return this.updateAllRects();
        
    // Iterate over the EDLs of all other documents
    
    for(var targetEdlIndex in this.edlStore) {
   
	var targetEdl = this.edlStore[targetEdlIndex];
	    
	/* Slightly different than the other findMatching...()
	   functions: no spanOffset */
	
	for(var targetIx = 1; targetIx < targetEdl.length; targetIx++){

	    /* HOWEVER, there is a problem, here. We can't have a span
	    matching against itself, otherwise we get screwed-up
	    transpointers, but how do we know which precise span we're
	    comparing against? We don't.

	    Here's a dumb fix, but it's dumb. The side-effect is that
	    separate instances of the same span in the parent context
	    workspace will not be transpointed.

	    One better fix will be to support contexts in edlStore and
	    dptron.currentContext and elsewhere as floater DIV
	    references in addition to Window/IFRAME objects (which has
	    always been part of the plan, but is not yet implemented).

	    Best approach is to pass a live X-TEXT to this function
	    instead of an AlphSpan. */

	    var targetSpan = targetEdl[targetIx];
	    
	    if(span.src == targetSpan[0] &&
	       span.origin == targetSpan[1] &&
	       span.extent == targetSpan[2]){
	       continue;
	    }
	    
	    var spanMatch = Alphjs.compareSpans(
		[span.src, span.origin, span.extent],
		targetSpan );

	    
	    if(spanMatch){
		this.addTranspointerSpan(
		    spanMatch.join("~"),
		    this.currentContext,
		    targetEdl[0]);
	    }
	}
    }
    this.updateAllRects();
}

/***/

Docuplextron.prototype.findSomeMatchingContent = function(context,edlIndex){
    if(!edlIndex){
	var sourceEdl = this.edlStore.find(function(c){
	    return c[0] == this;
	},context);
    } else {
	var sourceEdl = this.edlStore[edlIndex];
    }
    
    if(!sourceEdl){
	console.log("No edl for ", context);
	//debugger;
	return;
    }

    // Iterate over each media span (notice our index starts a 1, not 0)
    for(var spanIx = 1; spanIx < sourceEdl.length; spanIx++){
	// Then iterate over the EDLs of all other documents
	for(var targetEdlIndex = 0;
	    targetEdlIndex < this.edlStore.length;
	    targetEdlIndex++)
	{
	    var targetEdl = this.edlStore[targetEdlIndex];
	    
	    // If we're searching for matching spans within the same
	    // document, we start this loop with an offset
	    var spanOffset = (targetEdl == sourceEdl) ? spanIx : 0;
		
	    for(var targetIx = 1 + spanOffset;
		targetIx < targetEdl.length;
		targetIx++){

		spanMatch = Alphjs.compareSpans(sourceEdl[spanIx],
					 targetEdl[targetIx]);
		   
		if(spanMatch){
		    this.addTranspointerSpan(
			spanMatch.join("~"),
			targetEdl[0],
			sourceEdl[0]);
		}
	    }
	}
    }
    //this.rebuildTranspointers();
    //this.updateRects(context);
    this.updateAllRects();
}

/***/

Docuplextron.prototype.findAllMatchingContent = function(){
    
    /* Compare ALL CONTENT SPANS in the EDLs of all open documents and
       look for matching content. Store matches to dptron.transpointerSpans. */
    
    // *** This nests too deeply and should be broken into a few functions
        
    // Iterate over each EDL in the edlStore
    for(var sourceEdlIndex in this.edlStore){
	var sourceEdl = this.edlStore[sourceEdlIndex];
	/*
	  Entries in the EDL store look like this:

	  [ (context),
	    ["http://server/path/resource","origin","offset"],
	    ["http://server/path/resource","origin","offset"],
	    ...
	  ]

	  So there's a reference to the window/iframe/div which contains
	  the document at the top, then a series of content spans in the
	  form of 3-element arrays.
	*/

	// Iterate over each media span (notice our index starts a 1, not 0)
	for(var spanIx = 1; spanIx < sourceEdl.length; spanIx++){

	    // We want to compare this media span with all of the media spans
	    // in this and all remaining documents, so ...
	    for(var targetEdlIndex = sourceEdlIndex;
		targetEdlIndex < this.edlStore.length;
		targetEdlIndex++)
	    {
		var targetEdl = this.edlStore[targetEdlIndex];

	  	// If we're searching for matching spans within the same
		// document...
		var spanOffset = (targetEdlIndex == sourceEdlIndex) ? spanIx : 0;
		
		for(var targetIx = 1 + spanOffset;
		    targetIx < targetEdl.length;
		    targetIx++){

		    var spanMatch = Alphjs.compareSpans(sourceEdl[spanIx],
					     targetEdl[targetIx]);
		    
		    // compareSpans returns a simple src,origin,extent array
		    
		    // dptron.transpointerSpans is an object which uses
		    // content span strings as property keys, and the
		    // value for each property is an array of arrays,
		    // the first item in each being a reference to the
		    // window which contains the matching content, the
		    // subsequent items being SVGPolygon elements. The
		    // "points" attribute of the polygon will contain
		    // an outline of the matching content in the
		    // window.
		    
		    if(spanMatch){
			this.addTranspointerSpan(
			    spanMatch.join("~"),
			    targetEdl[0],
			    sourceEdl[0]);
			
		    }
		}
	    }
	}
    }

    // When we're all done, get SVG polygons for all the matching
    // content.
    this.updateAllRects();
}

/***/

Docuplextron.prototype.addTranspointerSpan = function(index, target, source){

    // Create the transpointerSpans entry for this span if we need to.
    if(!this.transpointerSpans[index]){
	this.transpointerSpans[index] = [
	    [target], // [ target, SVGRect, SVGRect, ...]
	    [source]
	];
    } else {
	
	// If we have an entry for this span, but NOT for one of the
	// contexts in this match, push one on.
	
	if(!this.transpointerSpans[index].find(function(s){
	    return s[0] == this;
	},target)){
	    this.transpointerSpans[index].push(
		[target]
	    );
	}
	if(!this.transpointerSpans[index].find(function(s){
	    return s[0] == this;
	},source)){
	    this.transpointerSpans[index].push(
		[source]
	    );
	}
    }
}

/***/

Docuplextron.prototype.updateAllRects = function(){
    
    /* Populate dptron.transpointerSpans by sending "requestRects"
       messages to IFRAMEs and by calling getMatchingContentRects() on
       top-level contexts. */

    /* [... Why does this function duplicte everything from
       updateRects()?  Shouldn't it just be a loop that calls
       updateRects() on all the floaters in the workspace? ] */
    
    var spanKeys = Object.keys(this.transpointerSpans);
    for(var ix in spanKeys){
	var spanObj = spanKeys[ix].split("~");
	var rectSet = this.transpointerSpans[spanKeys[ix]];
	for(var iy in rectSet){
	    var msgTarget = rectSet[iy][0];
	    var id = false;
	    if(typeof(msgTarget) == "string"){
		id = msgTarget;
		msgTarget = window;
	    } else if (msgTarget != window){
		if(msgTarget.contentWindow){
		    msgTarget = msgTarget.contentWindow;
		} else {
		    //console.log("updateAllRects(): Bad entry " + iy + " in transpointerSpans?",rectSet[iy]);
		    rectSet.splice(iy,1);
		    iy--;
		    continue;
		}
	    }
	    if(!spanObj[1]) continue;
	    var matchingType = (Alphjs.COORD_2D.test(spanObj[1])) ? "IMG" : "X-TEXT";
	    this.receiveRects({
		"id": id,
		"span": spanObj[0] + "~" + spanObj[1] + "~" + spanObj[2],
		"matches": this.getMatchingContentRects(matchingType,
							spanObj[0],
							spanObj[1],
							spanObj[2],
							id)
	    }, window);	    
	}		
    }
}

/***/

Docuplextron.prototype.updateRects = function(target){
    
    /* Request span highlights for all media spans in a particular context.
       'target' will either be a Window reference, or an id string */

    var spanKeys = Object.keys(this.transpointerSpans);
    for(var ix in spanKeys){
	var spanObj = spanKeys[ix].split("~");
	var rectSet = this.transpointerSpans[spanKeys[ix]];
	for(var iy in rectSet){
	    var msgTarget = rectSet[iy][0];
	    var id = false;
	    if (typeof(msgTarget) == "string"){
		if(msgTarget != target){
		    continue;
		}
		id = msgTarget;
		msgTarget = window;
	    } else if (msgTarget != window){
		if(msgTarget.contentWindow){
		    msgTarget = msgTarget.contentWindow;
		} else {
		    //console.log("updateRects(): Bad entry " + iy + " in transpointerSpans?",rectSet[iy]);
		    rectSet.splice(iy,1);
		    iy--;
		    continue;
		}
		if(msgTarget != target){
		    continue;
		}
	    }
	    var matchingType = (Alphjs.COORD_2D.test(spanObj[1])) ? "IMG" : "X-TEXT";
	    this.receiveRects({
		"id": id,
		"span": spanObj[0] + "~" + spanObj[1] + "~" + spanObj[2],
		"matches": this.getMatchingContentRects(matchingType,
							spanObj[0],
							spanObj[1],
							spanObj[2],
							id)
	    }, window);
	}
    }		
}

/***/

Docuplextron.prototype.getMatchingContentRects = function(type,src,origin,extent,offsetSrc){

    /* We're passed a content span (src/origin/extent), and we search for 
       matching content in our document/fragment.

       We return a point string for an SVGPolygon that encloses the matched
       content area. Works only for text and images right now. 

       offsetSrc should be the id of an x-floater. 

       updateRects() or updateAllRects() call us to feed polygon point
       strings into receiveRects() which actually makes the SVG elements
       and attaches them to their respective floaters.

       This is a monster! At the very least, the different media types
       should be broken-out into their own functions, getMatchingTextRects()
       and getMatchingImgRects() ...
       */

    var xoff = 0;
    var yoff = 0;
    var contextRect;
    var context = document;
    
    if(offsetSrc){
	try{
	    context = document.getElementById(offsetSrc);
	    var contextRect = context.getBoundingClientRect();
	} catch(e){
	    // id passed as offsetSrc refers to a dead element. 
	    this.rebuildEdlStore();
	    return [];
	}
	xoff = contextRect.left + parseFloat(getComputedStyle(context)["border-left-width"]);
	yoff = contextRect.top + parseFloat(getComputedStyle(context)["border-top-width"]);	
    }
    
    var sdom = context.querySelector("x-dom");

    switch(type){
    case "X-TEXT":
	var spans = (sdom) ?
	    Array.from(sdom.shadowRoot.querySelectorAll("x-text[src='"+src+"']")) :
	    Array.from(context.querySelectorAll("x-text[src='"+src+"']"));
	if(!spans) return;
	var targetRange = document.createRange();
	var matches = [];
	for (var ix in spans){
	    // Sort 'em
	    if (parseInt(spans[ix].getAttribute("origin")) < origin ){
		if(parseInt(spans[ix].getAttribute("extent")) < origin ||
		   parseInt(spans[ix].getAttribute("origin")) > extent ) continue;
		var leftOrigin = parseInt(spans[ix].getAttribute("origin"));
		var leftExtent = parseInt(spans[ix].getAttribute("extent"));
		var rightOrigin = origin;
		var rightExtent = extent;
		var targetSide = "l";
	    } else {
		var targetSide = "r";
		var rightOrigin = parseInt(spans[ix].getAttribute("origin"));
		var rightExtent = parseInt(spans[ix].getAttribute("extent"));
		var leftOrigin = origin;
		var leftExtent = extent;
	    }

	    // Look for matches
	    if(leftExtent <= rightOrigin) {
		// Left span lies ouside of right span
		continue;
	    } else if (leftExtent < rightExtent){
		// Left span partially overlaps right span
		var match = true;		
		var matchLength = leftExtent - rightOrigin;
		var spanRangeIndex = rightOrigin - leftOrigin;
		var matchOrigin = origin;
		var textNode = spans[ix].childNodes[0];
		if(targetSide == "l"){
		    targetRange.setStart(textNode, spanRangeIndex);
		    targetRange.setEnd(textNode, spanRangeIndex + matchLength);
		    matchOrigin += spanRangeIndex;
		} else {
		    targetRange.setStart(textNode, 0);
		    try{
			targetRange.setEnd(textNode, matchLength);
		    } catch(e) {
			console.log("bad index: ", matchLength);
		    }
		}
		var matchExtent = matchOrigin + matchLength;
	    } else if(leftExtent >= rightExtent){
		// Left span encloses right span
		var match = true;
		var matchLength = rightExtent - rightOrigin;
		var spanRangeIndex = rightOrigin - leftOrigin;
		var matchOrigin = origin;
		var textNode = spans[ix].childNodes[0];
		if(targetSide == "l"){
		    try{
			targetRange.setStart(textNode, spanRangeIndex);
			targetRange.setEnd(textNode,spanRangeIndex + matchLength);
			matchOrigin += spanRangeIndex;
		    } catch(e){
			console.log(e, textNode, spanRangeIndex, matchLength);
		    }
		} else {
		    try{
			targetRange.setStart(textNode, 0);
			targetRange.setEnd(textNode, matchLength);
		    } catch(e) {
			/* Usually, if there's a mismatch in here,
			   it's because whitespace has been altered by
			   the browser. */

			//console.log("¡DING!: ", spans[ix]);

			/* Just set the target range to what we have right
			   now so we can get some rects... */
			
			targetRange.setStart(textNode, 0);
			targetRange.setEnd(textNode, textNode.length -1);

			/* Then call alph.fillSpan() to make sure
			   that text span contents are true-to-source. It's
			   an async request, so it won't hang-up this
			   function. */
			
			alph.fillSpan(spans[ix]);
			//continue;
		    }
		}
		var matchExtent = matchOrigin + matchLength;
	    } else {
		// This shouldn't happen.
		console.log("¡DONG!: ", spans[ix]);
	    }

	    if(matchOrigin){
		// Make a polygon point string and push it onto matches.
		var divisor = (context.nodeType == Node.ELEMENT_NODE) ?
		    parseFloat(context.getAttribute("data-scale")) : 1;
		var rects = Array.from(targetRange.getClientRects());
		var pointStringL = "";
		var pointStringR = "";
		for(var rectIx = 0; rectIx < rects.length; rectIx++){
		    var l = rects[rectIx]; // Down the left side...
		    var r = rects[(rects.length - 1) - rectIx]; // ...and up the right.
		    pointStringL += (l.left - xoff)/divisor + "," + (l.top - yoff)/divisor + " " +
			(l.left - xoff)/divisor + "," + (l.bottom - yoff)/divisor + " ";
		    pointStringR += (r.right - xoff)/divisor + "," + (r.bottom - yoff)/divisor + " " +
			(r.right - xoff)/divisor + "," + (r.top - yoff)/divisor + " ";
		}
		matches.push(pointStringL + pointStringR);
	    }
	}
	return matches;
	break;
    case "IMG":
	var spans = (sdom) ?
	    Array.from(sdom.shadowRoot.querySelectorAll("img[src='"+src+"']")) :
	    Array.from(context.querySelectorAll("img[src='"+src+"']"));
	if(!spans) return;
	var matches = [];
	for (var ix in spans){

	    /* Confirm whether or not this fragment matches our spec or not */
	    var targetOrigin = spans[ix].getAttribute("origin") || "0,0";
	    var targetExtent = spans[ix].getAttribute("extent") ||
		(spans[ix].naturalWidth + "," + spans[ix].naturalHeight);

	    var matchedArea = Alphjs.compareSpans(
		[src, origin, extent], [src, targetOrigin, targetExtent]);
	    if (matchedArea[1] == origin && matchedArea[2] == extent){

		/* Woohoo! It matches! Now, I have to be completely honest and
		   admit that I'm not sure why this section works. We use the 
		   pixel dimensions from the fragment specs to figure out the
		   relative size and position of the rectangle, but ...we're
		   mapping that onto */
		
		/* Convert our origin/extent specs to point objects.*/
		var sOPoint = Alphjs.pointFromSpec(origin);
		var sEPoint = Alphjs.pointFromSpec(extent);
		var tOPoint = Alphjs.pointFromSpec(targetOrigin);
		var tEPoint = Alphjs.pointFromSpec(targetExtent);

		/* The pixel width of the span and target, and their relative proportion... */
		var spanWidth = sEPoint.x - sOPoint.x;
		var targetWidth = tEPoint.x - tOPoint.x;
		var proportionX = spanWidth / targetWidth;

		/* Same thing, but for heights... */
		var spanHeight = sEPoint.y - sOPoint.y;
		var targetHeight = tEPoint.y - tOPoint.y;
		var proportionY = spanHeight / targetHeight;

		/* Get the DOMRect of the target image, then create a new DOMRect for
		   the matching span. */

		var domRect = spans[ix].getClientRects()[0];
		if (domRect === undefined){
		    console.log("Couldn't get DOMRect for ", spans[ix]);
		    continue;
		}
		var imgScaleX = parseFloat(getComputedStyle(spans[ix]).width) / targetWidth;
		var imgScaleY = parseFloat(getComputedStyle(spans[ix]).height) / targetHeight;
		var contextScale = (context.nodeType == Node.ELEMENT_NODE) ?
		    parseFloat(context.getAttribute("data-scale")) : 1;

		/* Okay, let's break this down:
		   (domRect.x - xoff) gives us the distance between the left edge of the image 
		   and the left edge of the floater, in screen pixels. 

		   We divide that by contextScale to undo the scaling applied by the workspace.

		   (sOPoint.x - tOPoint.x) then gives us the distance between the left edge of 
		   the matching rectangle and the left edge of the image element. 

		   We then have to multiply that distance by the amount that the image is scaled 
		   *in its layout*.  And because the image may be scaled to a non-square pixel
		   aspect ratio, we have to get the X and Y scaling factors separately. */
		
		var newRect = new DOMRect(((domRect.x - xoff)/contextScale) + ((sOPoint.x - tOPoint.x) * imgScaleX),
					  ((domRect.y - yoff)/contextScale) + ((sOPoint.y - tOPoint.y) * imgScaleY),
					  spanWidth * imgScaleX,
					  spanHeight * imgScaleY);

		/* And constructing the SVG points string is really easy since it's just
		   a rectangle... */
		var pointString = newRect.left + "," + newRect.top + " " +
		    newRect.left + "," + newRect.bottom + " " + 
		    newRect.right + "," + newRect.bottom + " " +
		    newRect.right + "," + newRect.top;

		matches.push(pointString);
		
	    }
	}
	return matches;
	break;
    case "AUDIO":
	break;
    case "VIDEO":
	break;
    }
}

/* * */

Docuplextron.prototype.receiveRects = function(msg,source){
    
    /* Handle some rects that we requested.

       [G9A.1009 - Not actually rects, but SVG polygon point strings.]
       msg["matches"] will contain the point strings for each matching
       content area. */
    
    var issuingFrame = (msg["id"]) ? msg["id"] : Docuplextron.frameOf(source);

    
    if(msg["matches"].length > 0){
	var matches = msg["matches"];
	var matchingSpanArray = this.transpointerSpans[msg["span"]];

	// Find the array containing a reference to the window that sent this
	// message in the dptron.transpointerSpans object.
	var targetArray;
	for(var ix in matchingSpanArray){
	    if(matchingSpanArray[ix][0] == issuingFrame){
		targetArray = matchingSpanArray[ix];
	    }
	}
	    
	if(!targetArray){
	    // dptron.currentContext or dptron.currentSpan has changed since the
	    // message was sent. We don't need these rects anymore.
	    return;
	}
	    
	var lastPolyIx = 0;
	for(var ix = 1; ix <= matches.length; ix++){
	    // Set the "points" attributes of the SVGPolyons in the array to the
	    // strings in matches, or create the SVGPolygon if it doesn't exist.
	    if(targetArray[ix]){
		targetArray[ix].setAttribute("points", matches[ix-1]);
	    } else {
		targetArray[ix] = svgPolygon(matches[ix-1],"coincidence");
		if(issuingFrame == window){
		    // Append SVG to global overlay
		    this.overlay.appendChild(targetArray[ix]);
		} else if( typeof(issuingFrame) == "string"){
		    var floater = document.getElementById(issuingFrame);
		    if(floater){
			floater.overlay.appendChild(targetArray[ix]);
		    }
		} else {
		    // Append SVG to floater's local overlay
		    Docuplextron.getParentItem(issuingFrame).overlay.appendChild(targetArray[ix]);		
		}
	    }
	    lastPolyIx = ix;
	}
	// In case the number of matches has been reduced...
	if(targetArray.length > (1+lastPolyIx)){
	    targetArray = targetArray.slice(0,lastPolyIx);
	}
    }
}

/***/

Docuplextron.prototype.killSvgEphemerons = function(){
    var deletableSVG = Array.from(this.overlay.querySelectorAll(".killme"));    
    for(var ix in deletableSVG){
	deletableSVG[ix].remove();
    }
}

/***/


Docuplextron.prototype.buildBridges = function(){
    
    /* Draw all of the SVG rectangles linking the segments in transpointerSpans */

    /* TODO
       Let's make this more efficient, so that it re-uses existing SVG polys
       in the way that DOMCursor.updatePosition() re-uses divs.
    */
    
    //var dsvgix = 0;
    var keys = Object.keys(this.transpointerSpans);
    for(var ix in keys){
	var contentSpan = this.transpointerSpans[keys[ix]];
	
	/* contentSpan refers to an array of arrays containing SVG
	   polygons that enclose matching content spans in different
	   contexts (windows/documents).  The sub-arrays look like
	   this: [ window_ref, SVGPolygon, SVGpolygon... ] */

	var polygons = [];
	for(var contextIx = 0; contextIx < contentSpan.length; contextIx++){
	    var context = contentSpan[contextIx];
	    for(var polyIx = 1; polyIx < context.length; polyIx++){
		polygons.push(context[polyIx]);
	    }
	}
	// Now we have all of the SVG Polygons of this content portion in an array.
	// Let's draw a line between them.
	for(var polyIx = 1; polyIx < polygons.length; polyIx++){
	    if(polygons[polyIx-1].getBoundingClientRect().left <
	       polygons[polyIx].getBoundingClientRect().left){
		var lBox = polygons[polyIx-1].getBoundingClientRect();
		var rBox = polygons[polyIx].getBoundingClientRect();
	    } else {
		var rBox = polygons[polyIx-1].getBoundingClientRect();
		var lBox = polygons[polyIx].getBoundingClientRect();
	    }
	    var lPoly = svgPolygon("" +
				   lBox.right + "," + lBox.top + " " +
				   rBox.left  + "," + rBox.top  + " " +
				   rBox.left  + "," + rBox.bottom  + " " +
				   lBox.right  + "," + lBox.bottom, 'coincidence');
	    lPoly.classList.add("killme");
	    this.overlay.appendChild(lPoly);
	}
    }
}

Docuplextron.prototype.rebuildTranspointers = function(){
    
    /* This is a brute-force approach, and needs to be improved. I
       really need a scheme for maintenance of the transpointerSpans
       object; findMatchingContent() updates and adds spans, but
       there's nothing going in and removing invalid ones. */
    
    this.transpointerSpans = {};
    
    //var svgElements = Array.from(document.getElementsByTagName("polygon"));
    var svgElements = Array.from(document.querySelectorAll("polygon.coincidence"));
    
    for(var ix in svgElements){

	svgElements[ix].remove();
    }

    if(document.getElementById("vlinksEnable").checked){
	//this.buildLinkpointerSpans();
    }
    
    this.findMatchingContent();
}

/*





   EDL MANAGEMENT __________________________________________________





*/

Docuplextron.prototype.postEDL = function(context){
    
    /* Generate an EDL from all of the content spans in the passed
       context, and post it up to the parent context. A "context" here
       may be any DOM node; this will usually be either the entire document, 
       or a div.alph-floater/div.alph-slider

       Any function that manupulates the document should call
       postEDL() once the damage is done. postEDL() sends the
       "storeEDL" message, which triggers the storeEDL() function in
       the parent context, which calls findMatchingContent() which
       maintains dptron.transpointerSpans.*/
    
    var context = context || document;
    
    if(context == document && this.dptronMode){
	// Don't post the full-document EDL in this case; instead,
	// post EDLs for each floating context
	var floaterContexts = this.floaterManager.getMembers();
	for (var ix in floaterContexts){
	    var narrowContext = floaterContexts[ix];
	    if(!narrowContext.querySelector("iframe")){
		this.postEDL(narrowContext);		
	    }
	}
    } else {

	var edl = [];

	// Does this floater contain a Shadow DOM?
	let sdom = context.querySelector("x-dom");
	let base = (sdom) ?
	    sdom.shadowRoot :
	    context;

	var spans = Array.from(base.querySelectorAll("[src]"));

	for (var ix in spans){
	    var span = spans[ix];
	    // Just doing text right now. Images and A/V later.
	    if(span.tagName == "X-TEXT"){
		edl.push([span.getAttribute("src"),
			  span.getAttribute("origin"),
			  span.getAttribute("extent")]);
	    } else if(span.tagName == "IMG"){
		var spanArray = [span.getAttribute("src"),"",""];
		if(span.hasAttribute("origin") && span.hasAttribute("extent")){
		    spanArray[1] = span.getAttribute("origin");
		    spanArray[2] = span.getAttribute("extent");
		    edl.push(spanArray);
		} else {
		    spanArray[1] = "0,0";
		    spanArray[2] = span.naturalWidth + "," + span.naturalHeight;
		    edl.push(spanArray);
		}
	    }
	}
	if(context.id == ""){
	    
	    /* The element needs an 'id' attribute so that we can
	       reference it in our edlStore. This will be ignored if
	       we're posting up from an IFRAME, but if this is for a
	       floater in the parent context, it's essential. */
	    
	    context.id = "context_" + Date.now();
	}

	/* What to do differently here? ... Send the shadowRoot for window? Store
	   actual object (floater, etc) references instead of ids? ... */

	this.storeEDL(window,edl,context.id);
    }
}

/***/

Docuplextron.prototype.storeEDL = function(w,edl,id){

    /* 'w' is a the source window of a "storeEDL" message. It might be
       the contentWindow of an iframe. If it is, get a handle on that
       iframe instead. */
    
    //console.log("Storing EDL for ", id);
    
    var contextTag;
    
    if(w != window){
	try{
	    var iframe = Docuplextron.frameOf(w);
	} catch(e){
	    this.findMatchingContent();
	    return;
	}
	contextTag = iframe;
    } else {
	// If this *is* coming from the top-level window, we want to
	// use the passed 'id' as the first edl element if one was passed.
	//
	if(id){
	    contextTag = id;
	} else {
	    contextTag = w;
	}
    }
    // Then put the window/iframe at the front of the array.
    edl.unshift(contextTag);

    // Store it to dptron.edlStore. If one is there already, replace it.
    var stored = false;
    for(var ix in this.edlStore){
	if(this.edlStore[ix][0] == contextTag){
	    this.edlStore[ix] = edl;
	    stored = true;
	}
    }
    if(!stored){
	this.edlStore.push(edl);
    }
    
    this.rebuildTranspointers();
    this.paintLinkTerminals();
}

/***/

Docuplextron.prototype.removeEDL = function(target){
    // 'target' is an iframe or an id string
    this.edlStore = this.edlStore.filter( function (el){
	return (el[0] != this);
    },target);
    
    this.rebuildTranspointers();
    this.paintLinkTerminals();
}

/***/

Docuplextron.prototype.rebuildEdlStore = function(){

    this.edlStore = [];
    this.postEDL();
   
}

Docuplextron.colorspaceCanvas = function(){
    var canvas = document.createElement("canvas");
    canvas.width = 256;
    canvas.height = 256;
    var context = canvas.getContext("2d");
    var grad1 = context.createLinearGradient(0,0,0,256);
    var grad2 = context.createLinearGradient(0,0,256,0);
    grad1.addColorStop(0,"rgb(0,0,0)");
    grad1.addColorStop(1,"rgb(255,255,255)");
    
    grad2.addColorStop(0,"rgb(255,0,0)");
    grad2.addColorStop(0.333,"rgb(0,255,0)");
    grad2.addColorStop(0.666,"rgb(0,0,255)");
    grad2.addColorStop(1,"rgb(255,0,0)");
    context.fillStyle = grad1;
    context.fillRect(0,0,256,256);
    context.globalCompositeOperation = "multiply";
    context.fillStyle = grad2;
    context.fillRect(0,0,256,256);
    return canvas;
}

Docuplextron.colorSwatches = function(backgrounds,foregrounds,caption){

    /* I return a table of color swatches, each of which call my
       action() method on mouseup, passing the background/foreground
       color in my swatch. To use me, call with arrays of background
       and foreground colors and a caption, then define my action()
       method to do whatever you want. */
    
    var tab = document.createElement("table");
    tab.className = "dptronColorPicker";

    tab.cap = document.createElement("caption");
    tab.appendChild(tab.cap);
    tab.cap.textContent = caption || "";
    
    var row = document.createElement("tr");
    tab.appendChild(row);

    /* Default to 16 pastel shades if no colors are passed. */
    var swatchBGs = backgrounds || ["var(--dptron-BG-00)","var(--dptron-BG-01)",
				    "var(--dptron-BG-02)","var(--dptron-BG-03)",
				    "var(--dptron-BG-04)","var(--dptron-BG-05)",
				    "var(--dptron-BG-06)","var(--dptron-BG-07)",
				    "var(--dptron-BG-08)","var(--dptron-BG-09)",
				    "var(--dptron-BG-10)","var(--dptron-BG-11)",
				    "var(--dptron-BG-12)","var(--dptron-BG-13)",
				    "var(--dptron-BG-14)","var(--dptron-BG-15)"];
    
    var swatchFGs = foregrounds || ["var(--dptron-FG-00)","var(--dptron-FG-01)",
				    "var(--dptron-FG-02)","var(--dptron-FG-03)",
				    "var(--dptron-FG-04)","var(--dptron-FG-05)",
				    "var(--dptron-FG-06)","var(--dptron-FG-07)",
				    "var(--dptron-FG-08)","var(--dptron-FG-09)",
				    "var(--dptron-FG-10)","var(--dptron-FG-11)",
				    "var(--dptron-FG-12)","var(--dptron-FG-13)",    
				    "var(--dptron-FG-14)","var(--dptron-FG-15)"];
    
    tab.action = function(color1,color2){
	/* Define this method when you use me! */
    };
    
    for(var ix in swatchBGs){
	var swatch = document.createElement("td");
	swatch.style.backgroundColor = swatchBGs[ix];
	swatch.style.color = swatchFGs[ix];
	swatch.color1 = swatchBGs[ix];
	swatch.color2 = swatchFGs[ix];
	swatch.textContent = ix;
	row.appendChild(swatch);
	swatch.onmouseup = function(swatches){
	    swatches.action(this.color1,this.color2);
	}.bind(swatch,tab);
    }
    return tab;
}

Docuplextron.prototype.selectionRectOn = function(target, atX, atY){

    /* target should be an IMG element. 
       atX and atY will be the offsetX and offsetY properties of the
       MouseEvent that's triggering this new selectionRect. */

    if(this.selectionRect) this.selectionRect.remove();

    /* Get the target's floater */
    
    var floater = Docuplextron.getParentItem(target);
    if (floater) {
	
	/* Figure out the offset between the floater's top-left and
	   the target's top-left */
	
	var fRect = floater.getBoundingClientRect();
	var tRect = target.getBoundingClientRect();

	/* How much is the floater scaled right now? */
	
	var fScale = getComputedStyle(floater).transform;

	/* That returns something like "matrix(0.5646, 0, 0, 0.5646, 0, 0)"
	   And we only need this bit ----------\____/  */
	
	fScale = fScale.split("(")[1];
	fScale = parseFloat(fScale.split(",")[0]);

	/* We also need to modify the offset by whatever border
	   the floater's got set right now */
	var borders = {"top" : parseFloat(getComputedStyle(floater).borderTopWidth),
		       "left" : parseFloat(getComputedStyle(floater).borderLeftWidth) };

	var offsets = [ ((tRect.left - fRect.left) * (1/fScale)) - borders.left,
			    ((tRect.top - fRect.top) * (1/fScale)) - borders.top];

	if (target.getRootNode() instanceof ShadowRoot){
	    
	    /* Placing the SVG selection rectangle is pretty straightforward
	       when the event is coming from a shadowDOM because atX and atY 
	       are already set to the offset from the document's top-left, 
	       rather than the image's. */

	    /* HOWEVER -- there's a very weird thing with X-DOM elements that
	       I can't figure out: their origin is 16px higher than the origin 
	       of the floater that they're contained in. So we have to factor
	       that in here. */
	    
	    this.selectionRect = svgRect(atX,
					 atY - 16,
					 1, 1, "selectionRect");
	    
	    this.selectionRect.anchorX = atX;
	    this.selectionRect.anchorY = atY;

	    /* Pass along the offset values for when selectionRectToAlphSpan()
	       is called... */
	    
	    this.selectionRect.offsets = offsets;
	} else {
	    
	    /* 3. Create the SVG rectangle and attach it to the floater's SVG overlay */

	    this.selectionRect = svgRect(offsets[0] + atX,
					 offsets[1] + atY,
					 1,
					 1,
					 "selectionRect");

	    this.selectionRect.anchorX = atX;
	    this.selectionRect.anchorY = atY;
	}
	
	this.selectionRect.target = target;
	this.selectionRect.active = true;
	floater.overlay.appendChild(this.selectionRect);
    }
}

Docuplextron.selectionRectToAlphSpan = function(sRect){

    /* The selectionRect is an SVG rectangle drawn directly
       over the image we're making a selection from.

       Its anchorX and anchorY properties were set by 
       selectionRectOn() to be either:
       - the X and Y inset from the element's top-left, OR
       - the X and Y inset from the containing shadowDOM's 
         top-left

       If the latter, we should also have some offset
       values that need to be subtracted from anchorX and
       anchorY ... */
    
    if(sRect.target.getRootNode() instanceof ShadowRoot){
	
	/* The '16' is to compensate for X-DOM elements having
	   a vertical offset that I can't figure out. */
	
	var selOrigin = [sRect.anchorX - sRect.offsets[0],
			 sRect.anchorY - sRect.offsets[1] - 16 ];
    } else {
	var selOrigin = [sRect.anchorX, sRect.anchorY];
    }

    /* The image element may be scaled in its layout, so 
       we need that number... */
    
    var tgtScale = 1 / Docuplextron.getTotalScale(sRect.target);

    /* Our selOrigin coordinates are the offset, in pixels,
       of the selection rectangle's origin from the image
       element's origin -- but the image itself may be
       a crop of a larger source image.  */
    
    if(sRect.target.hasAttribute("origin")){
	var origin = sRect.target.getAttribute("origin").split(",");
	origin[0] = parseFloat(origin[0]);
	origin[1] = parseFloat(origin[1]);
	selOrigin[0] = origin[0] + (tgtScale * selOrigin[0]);
	selOrigin[1] = origin[1] + (tgtScale * selOrigin[1]);
    }
    
    var span = new AlphSpan(
	sRect.target.getAttribute("src"),
	parseInt(selOrigin[0]) + "," + parseInt(selOrigin[1]),
	parseInt(selOrigin[0] + (tgtScale * parseFloat(sRect.getAttribute("width")))) + "," + parseInt(selOrigin[1] + (tgtScale * parseFloat(sRect.getAttribute("height"))))
    );

    return span;
}

/*





   SVG _____________________________________________________





*/

var svgns = "http://www.w3.org/2000/svg";

function svgGroup(){
    var g = document.createElementNS(svgns,"g");
    return g;
}

function svgLine(x1,y1,x2,y2,c){
    var l = document.createElementNS(svgns,"line");
    l.setAttribute("x1", x1);
    l.setAttribute("y1", y1);
    l.setAttribute("x2", x2);
    l.setAttribute("y2", y2);
    l.setAttribute("class", c || "");
    return l;
}
function svgRect(x,y,w,h,c){
    var r = document.createElementNS(svgns,"rect");
    r.setAttribute("x", x);
    r.setAttribute("y", y);
    r.setAttribute("width", w);
    r.setAttribute("height", h);
    r.setAttribute("class", c || "");
    return r;
}
function svgPolyline(p,c){
    var r = document.createElementNS(svgns,"polyline");
    //r.setAttribute("stroke","black");
    //r.setAttribute("stroke-width","6");
    r.setAttribute("points", p);
    r.setAttribute("class", c || "");
    return r;
}
function svgPolygon(p,c){
    var r = document.createElementNS(svgns,"polygon");
    r.setAttribute("points", p);
    r.setAttribute("class", c || "");
    return r;
}
function svgText(string,x,y){
    var t = document.createElementNS(svgns,"text");
    t.textContent = string;
    t.setAttribute("x",x);
    t.setAttribute("y",y);
    return t;
}

function svgLinkerNexus(aorb){
    //var n = document.createElementNS(svgns,"circle");
    var n = document.createElementNS(svgns,"use");
    n.setAttribute("id", (aorb == "a") ? "dptron-linkerNodeA" : "dptron-linkerNodeB");
    n.setAttribute("href", (aorb == "a") ? "#linkerANode" : "#linkerBNode");
    n.setAttribute("width","12");
    n.setAttribute("height","34");
    //n.setAttribute("r", 6);
    n.held = false;
    n.span = new AlphSpan();
    n.context = null;
    n.terminals = [];
    n.pickUp = function(){
	this.held = true;
	this.style.pointerEvents = "none";
    }
    n.putDown = function(){
	this.held = false;
	this.style.pointerEvents = "all";
    }
    n.onmousedown = function(){
	this.pickUp();
//	this.held = true;
//	this.style.pointerEvents = "none";
    }
    return n;
}

function selectOption(val,txt){
    var opt = document.createElement("option");
    opt.setAttribute("value",val || "");
    opt.textContent = txt || "";
    return opt;
}


/* 
   RGB <-> HSV converters, from:
   https://stackoverflow.com/questions/17242144/javascript-convert-hsb-hsv-color-to-rgb-accurately

*/

/* accepts parameters
 * h  Object = {h:x, s:y, v:z}
 * OR 
 * h, s, v
 */

function HSVtoRGB(h, s, v) {
    var r, g, b, i, f, p, q, t;
    if (arguments.length === 1) {
        s = h.s, v = h.v, h = h.h;
    }
    i = Math.floor(h * 6);
    f = h * 6 - i;
    p = v * (1 - s);
    q = v * (1 - f * s);
    t = v * (1 - (1 - f) * s);
    switch (i % 6) {
        case 0: r = v, g = t, b = p; break;
        case 1: r = q, g = v, b = p; break;
        case 2: r = p, g = v, b = t; break;
        case 3: r = p, g = q, b = v; break;
        case 4: r = t, g = p, b = v; break;
        case 5: r = v, g = p, b = q; break;
    }
    return {
        r: Math.round(r * 255),
        g: Math.round(g * 255),
        b: Math.round(b * 255)
    };
}

function RGBtoHSV(r, g, b) {
    if (arguments.length === 1) {
        g = r.g, b = r.b, r = r.r;
    }
    var max = Math.max(r, g, b), min = Math.min(r, g, b),
        d = max - min,
        h,
        s = (max === 0 ? 0 : d / max),
        v = max / 255;

    switch (max) {
        case min: h = 0; break;
        case r: h = (g - b) + d * (g < b ? 6: 0); h /= 6 * d; break;
        case g: h = (b - r) + d * 2; h /= 6 * d; break;
        case b: h = (r - g) + d * 4; h /= 6 * d; break;
    }

    return {
        h: h,
        s: s,
        v: v
    };
}

function YMDMins(){
    var d = new Date();
    var y = (d.getFullYear() - 2000).toString(36);
    var m = d.getMonth().toString(36);
    var day = d.getDate().toString(36);
    var mins = (d.getHours() * 60) + d.getMinutes();
    return y + m + day + "." + mins.toString();
}

function rgbToHex(color){
    rgb = color.replace( /[rgba()]/g ,"").split(",");
    return "#" + parseInt(rgb[0]).toString(16).padStart(2,"0") +
	parseInt(rgb[2]).toString(16).padStart(2,"0") +
	parseInt(rgb[2]).toString(16).padStart(2,"0");
}
